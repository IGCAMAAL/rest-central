package com.javiermoreno.springboot.rest;



import com.centraldecompras.acceso.UserCentral;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @PersistenceContext
    private EntityManager em;

    @Override
    //@Cacheable
    public UserCentral loadUserByUsername(String username) throws UsernameNotFoundException {
        UserCentral userDetails = null;
        try {
            userDetails =  (UserCentral) em
                    .createNamedQuery("UserCentral.findByUsername")
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (userDetails == null) {
            throw new UsernameNotFoundException(
                    String.format("{0} username was not found.", username));
        }

        return userDetails;
    }

}
