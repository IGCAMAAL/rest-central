package com.javiermoreno.springboot.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ExceptionReportDTO generalException(Exception exc) {
        ExceptionReportDTO report = new ExceptionReportDTO();
        report.originalErrorMessage = exc.getMessage();
        return report;
    }

    private class ExceptionReportDTO {

        public String originalErrorMessage;
    }
}
