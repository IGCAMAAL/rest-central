/*
 * Copyright 2014, Javier Moreno.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 * And have fun ;-)
 */
package com.javiermoreno.springboot.rest;

import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
/*
 @Configuration
 @EnableWebSecurity 
 @EnableGlobalMethodSecurity(prePostEnabled = true) // Admite seguridad a nivel de métodos
 @Order(1)
*/
public class UserSecurityAdapter extends WebSecurityConfigurerAdapter {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UserSecurityAdapter.class.getName());
    
    @Autowired
    private UserCentralService userCentralService; 

    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        log.info("................................................................................CONTROL");

        auth.inMemoryAuthentication()
                .withUser("admin").password("adminadmin").roles("ADMIN", "USER")
                .and()
                .withUser("user").password("useruser").roles("USER");

        //auth.userDetailsService(UserCentralService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info("................................................................................CONTROL");
        
        http.authorizeRequests()
                .antMatchers("/public/**", "/views/**", "/errores/**")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/credentials/**")
                .hasRole("USER")
                .antMatchers(HttpMethod.GET, "/private/**")
                .hasRole("ADMIN")
                //.access("hasRole('ROLE_ADMIN')")
                .anyRequest()
                .authenticated()
                .and()
                .formLogin().loginPage("/login").failureUrl("/login?error")
                .usernameParameter("username").passwordParameter("password")
                .and()
                .httpBasic();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsServiceBean() throws Exception {
        log.info("................................................................................CONTROL");
        
        return super.userDetailsServiceBean();
    }


}
