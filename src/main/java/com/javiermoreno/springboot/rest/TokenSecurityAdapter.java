package com.javiermoreno.springboot.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
 
public class TokenSecurityAdapter extends WebSecurityConfigurerAdapter {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TokenSecurityAdapter.class.getName());

    private static final String TOKEN_NAME = "X-Token";

    @Autowired
    private UserDetailsServiceImpl customUserDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        log.info(".........................................................................configure(AuthenticationManagerBuilder.......CONTROL");

        auth.inMemoryAuthentication()
                //    .withUser("admin").password("adminadmin").roles("ADMIN", "USER")
                //  .and()
                .withUser("user").password("useruser").roles("USER");

        auth.userDetailsService(customUserDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info(".......................................................................configure(HttpSecurity.........CONTROL");

        http.csrf()
                .disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(new Http403ForbiddenEntryPoint())
                .and()
                // Filter order: http://docs.spring.io/spring-security/site/docs/3.2.0.RELEASE/apidocs/org/springframework/security/config/annotation/web/HttpSecurityBuilder.html#addFilter%28javax.servlet.Filter%29
                .addFilterAfter(authenticationTokenProcessingFilterBean(), BasicAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/api-docs/**", "/public/**", "/views/**", "/errores/**",
                        "/health/**", "/metrics/**", "/configprops/**")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/private/**")
                .hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/shutdown/**")
                .hasRole("ADMIN")
                //.access("hasRole('ROLE_ADMIN')")
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }

    @Bean
    public AuthenticationTokenProcessingFilter authenticationTokenProcessingFilterBean() throws Exception {
        log.info(".......................................................................authenticationTokenProcessingFilterBean.........CONTROL");
        return new AuthenticationTokenProcessingFilter(
                tokenCryptographyServiceBean(), userDetailsServiceBean(), authenticationManagerBean());
    }

    @Bean
    @Override
    public UserDetailsService userDetailsServiceBean() throws Exception {
        log.info(".......................................................................userDetailsServiceBean.........CONTROL");
        return super.userDetailsServiceBean();
    }

    @Bean
    public CryptographyService tokenCryptographyServiceBean() throws Exception {
        log.info(".......................................................................tokenCryptographyServiceBean.........CONTROL");
        return new CryptographyServiceImplBlowfish();
    }

}
