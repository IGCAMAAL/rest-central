/* Copyright 2014, Javier Moreno.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 * And have fun ;-)
 */
package com.centraldecompras.zglobal;

/**
 *
 * @author Miguel
 */
public class DataBaseGeneralParam {

    String driverClassName;
    String urlDB;
    String username;
    String password;
    String hostDestino;
    int puertoDestino;
    int puertoDestinoSSL;
    int sizeAplicationImage;
    int sizeAplicationCatalog;
    int ultimoNivel;
    
    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrlDB() {
        return urlDB;
    }

    public void setUrlDB(String urlDB) {
        this.urlDB = urlDB;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getHostDestino() {
        return hostDestino;
    }

    public void setHostDestino(String hostDestino) {
        this.hostDestino = hostDestino;
    }

    public int getPuertoDestino() {
        return puertoDestino;
    }

    public void setPuertoDestino(int puertoDestino) {
        this.puertoDestino = puertoDestino;
    }

    public int getPuertoDestinoSSL() {
        return puertoDestinoSSL;
    }

    public void setPuertoDestinoSSL(int puertoDestinoSSL) {
        this.puertoDestinoSSL = puertoDestinoSSL;
    }

    public int getSizeAplicationImage() {
        return sizeAplicationImage;
    }

    public void setSizeAplicationImage(int sizeAplicationImage) {
        this.sizeAplicationImage = sizeAplicationImage;
    }

    public int getUltimoNivel() {
        return ultimoNivel;
    }

    public void setUltimoNivel(int ultimoNivel) {
        this.ultimoNivel = ultimoNivel;
    }

    public int getSizeAplicationCatalog() {
        return sizeAplicationCatalog;
    }

    public void setSizeAplicationCatalog(int sizeAplicationCatalog) {
        this.sizeAplicationCatalog = sizeAplicationCatalog;
    }

}
