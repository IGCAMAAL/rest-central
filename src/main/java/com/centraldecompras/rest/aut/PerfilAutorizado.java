/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.aut;

import java.util.ArrayList;

/**
 *
 * @author Miguel
 */
public class PerfilAutorizado {

    private String perfilId;
    private String rolPerfil;
    private String perfilNom;
    private String estado;
    private ArrayList<SectoresRol> sectoresAutorizados;

    public PerfilAutorizado() {
    }

    public PerfilAutorizado(String perfilId, String rolPerfil, String perfilNom, String estado, ArrayList<SectoresRol> sectoresAutorizados) {
        this.perfilId = perfilId;
        this.rolPerfil = rolPerfil;
        this.perfilNom = perfilNom;
        this.estado = estado;
        this.sectoresAutorizados = sectoresAutorizados;
    }


    public String getPerfilId() {
        return perfilId;
    }

    public void setPerfilId(String perfilId) {
        this.perfilId = perfilId;
    }

    public String getRolPerfil() {
        return rolPerfil;
    }

    public void setRolPerfil(String rolPerfil) {
        this.rolPerfil = rolPerfil;
    }


    
    public String getPerfilNom() {
        return perfilNom;
    }

    public void setPerfilNom(String perfilNom) {
        this.perfilNom = perfilNom;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<SectoresRol> getSectoresAutorizados() {
        return sectoresAutorizados;
    }

    public void setSectoresAutorizados(ArrayList<SectoresRol> sectoresAutorizados) {
        this.sectoresAutorizados = sectoresAutorizados;
    }

}
