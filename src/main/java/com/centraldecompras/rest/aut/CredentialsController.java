package com.centraldecompras.rest.aut;


import com.centraldecompras.acceso.UserCentral;
import com.javiermoreno.springboot.rest.CryptographyService;
import com.javiermoreno.springboot.rest.TokenIdUser;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/credentials")
@Api(value = "Autentificación por token", description = "Permite a un usuario autentificado obtener un token de identificación.")
public class CredentialsController {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CredentialsController.class.getName());
    
    @Value("${application.tokenTTL}")
    private int TOKEN_TTL;
    
    @Autowired
    private CryptographyService cryptoService;
    
    @RequestMapping(value="/token", method = RequestMethod.GET)
    @ApiOperation(value = "Generación token", notes = "Retorna un token en Base64 con una duración marcada por TOKEN_TTL.")
    public TokenIdUser createNewAuthToken(HttpServletRequest request) {
        log.info("................................................................................CONTROL");        
        
        long ttl = System.currentTimeMillis() + 1000 * TOKEN_TTL;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        String username = principal instanceof UserCentral ? ((UserCentral) principal).getUsername() : principal.toString();
        String idUser = ((UserCentral) principal).getId() ;
        
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null)  {
            ip = request.getRemoteAddr();
        }

        TokenIdUser token = new TokenIdUser(cryptoService, username, ip, ttl, idUser);
 
        return token;
    }

}
