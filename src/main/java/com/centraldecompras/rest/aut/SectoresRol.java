/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.aut;

/**
 *
 * @author Miguel
 */
public class SectoresRol {

    private String sectorId;
    private String sectorNom;
    private String estado;

    public SectoresRol() {
    }

    public SectoresRol(String sectorId, String sectorNom, String estado) {
        this.sectorId = sectorId;
        this.sectorNom = sectorNom;
        this.estado = estado;
    }

    public String getSectorId() {
        return sectorId;
    }

    public void setSectorId(String sectorId) {
        this.sectorId = sectorId;
    }

    public String getSectorNom() {
        return sectorNom;
    }

    public void setSectorNom(String sectorNom) {
        this.sectorNom = sectorNom;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
