/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.aut;

import java.util.ArrayList;

/**
 *
 * @author Miguel
 */
public class UserData {

    private String idUser;
    private String Nombre;
    private byte[] fotografiaInfoUsuario;
    private String estado;
    private ArrayList<Roles> roles;

    public UserData() {
    }

    public UserData(String idUser, String Nombre, byte[] fotografiaInfoUsuario, String estado, ArrayList<Roles> roles) {
        this.idUser = idUser;
        this.Nombre = Nombre;
        this.fotografiaInfoUsuario = fotografiaInfoUsuario;
        this.estado = estado;
        this.roles = roles;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public byte[] getFotografiaInfoUsuario() {
        return fotografiaInfoUsuario;
    }

    public void setFotografiaInfoUsuario(byte[] fotografiaInfoUsuario) {
        this.fotografiaInfoUsuario = fotografiaInfoUsuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<Roles> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<Roles> roles) {
        this.roles = roles;
    }

}
