/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.aut;

/**
 *
 * @author ciberado
 */
public class PersonaCredenciales {

    private Boolean isAuthorized;
    private String idWeb;
    private UserData userData;

    public PersonaCredenciales() {
    }

    public PersonaCredenciales(Boolean isAuthorized, String idWeb,  UserData userData) {
        this.isAuthorized = isAuthorized;
        this.idWeb = idWeb;
        this.userData = userData;
    }

    public Boolean isIsAuthorized() {
        return isAuthorized;
    }

    public void setIsAuthorized(Boolean isAuthorized) {
        this.isAuthorized = isAuthorized;
    }

    public String getIdWeb() {
        return idWeb;
    }

    public void setIdWeb(String idWeb) {
        this.idWeb = idWeb;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

}
