/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.aut;

import java.util.ArrayList;

/**
 *
 * @author Miguel
 */
public class Roles {

    private String empresaId;
    private String empresaNom;
    private String estado;
    private ArrayList<PerfilAutorizado> perfiles;

    public Roles() {
    }

    public Roles(String empresaId, String empresaNom, String estado, ArrayList<PerfilAutorizado> perfiles) {
        this.empresaId = empresaId;
        this.empresaNom = empresaNom;
        this.estado = estado;
        this.perfiles = perfiles;
    }

    public String getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(String empresaId) {
        this.empresaId = empresaId;
    }

    public String getEmpresaNom() {
        return empresaNom;
    }

    public void setEmpresaNom(String empresaNom) {
        this.empresaNom = empresaNom;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<PerfilAutorizado> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(ArrayList<PerfilAutorizado> perfiles) {
        this.perfiles = perfiles;
    }

}
