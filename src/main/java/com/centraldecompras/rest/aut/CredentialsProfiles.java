/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.aut;

import com.centraldecompras.acceso.Sector;
import com.centraldecompras.zglobal.DataBaseGeneralParam;
import com.centraldecompras.acceso.SectoresAutorizados;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.acceso.service.interfaces.SectorService;
import com.centraldecompras.acceso.service.interfaces.SectoresAutorizadosService;
import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import com.centraldecompras.acceso.service.interfaces.UsrSocPrfService;
import com.centraldecompras.acceso.service.interfaces.UsrSocService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ciberado
 */
@RestController 
@RequestMapping("/credenciales")
public class CredentialsProfiles {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CredentialsProfiles.class.getName());

    @Autowired
    private CounterService counterService;

    @Autowired
    //private PersonaService personaService;
    private UserCentralService userCentralService;

    @Autowired
    private UsrSocService prsSocService;

    @Autowired
    private UsrSocPrfService prsSocPrfService;
    
    @Autowired
    private   SectorService sectorService;
    @Autowired
    
    private SectoresAutorizadosService sectoresAutorizadosfService;

    @Value("${application.sizeAplicationImage}")
    private int sizeAplicationImage;
    
    @Value("${application.sizeAplicationCatalog}")
    private int sizeAplicationCatalog;
    /*
    @Value("${application.hostDestino}")
    private String applicationHostDestino;

    @Value("${application.puertoDestino}")
    private int applicationPuertoDestino;

    @Value("${application.puertoDestinoSSL}")
    private int applicationPuertoDestinoSSL;
*/
    @RequestMapping("/perfiles/{id}")
    @ResponseStatus(HttpStatus.OK)
    PersonaCredenciales getPerfilesUsuario(
            @PathVariable String id, 
            @RequestHeader("X-Auth-Token") String token
    ) {
        log.info("................................................................................CONTROL");

        PersonaCredenciales reply = new PersonaCredenciales();

        // 1º Recuperar OBJETO_PERSONA con identificador Web del usuario (No es el Id)
        UserCentral userCentral = userCentralService.findUserCentral(id); 

        reply.setIsAuthorized(true);
        reply.setIdWeb(userCentral.getUsername());

        UserData userData = new UserData();
        userData.setIdUser(userCentral.getId());
        userData.setNombre(userCentral.getApellido1() + " " + userCentral.getApellido2() + ", " + userCentral.getNombre());
        userData.setEstado(userCentral.getAtributosAuditoria().getEstadoRegistro());

        // 2º Recuperar las sociedades que gestiona un usuario PRSSOC
        ArrayList<Roles> roles = new ArrayList();
        List<UsrSoc> prsSocs = prsSocService.findUsrSocByUserCentral(userCentral);
        for (UsrSoc prsSoc : prsSocs) {
            Roles rolAutorizado = new Roles();
            //String ff = prsSoc.getSociedad().getIdSociedad();
            rolAutorizado.setEmpresaId(prsSoc.getSociedad().getIdSociedad());
            //rolAutorizado.setEmpresaNom(prsSoc.getSociedad().getNifnrf() + "-" + prsSoc.getSociedad().getNombre());
            rolAutorizado.setEmpresaNom(prsSoc.getSociedad().getNombre());
            rolAutorizado.setEstado(prsSoc.getAtributosAuditoria().getEstadoRegistro());

            // 3º Por cada persona/sociedad, recuperar los perfiles asignados 
            ArrayList<PerfilAutorizado> perfilesAutorizados = new ArrayList();
            List<UsrSocPrf> prsSocsPrfs = prsSocPrfService.findByUsrSoc(prsSoc);
            for (UsrSocPrf prsSocPrf : prsSocsPrfs) {
                PerfilAutorizado perfilAutorizado = new PerfilAutorizado();
                perfilAutorizado.setPerfilId(prsSocPrf.getPerfil().getIdPerfil());
                perfilAutorizado.setPerfilNom(prsSocPrf.getPerfil().getCodPerfil() + "- " + prsSocPrf.getPerfil().getDescPerfil());
                perfilAutorizado.setEstado(prsSocPrf.getAtributosAuditoria().getEstadoRegistro());
                perfilAutorizado.setRolPerfil(prsSocPrf.getPerfil().getCodPerfil()); 
                
                // 4º Por cada persona/empresa/perfil recupera los sectores autorizados
                ArrayList<SectoresRol> sectoresRol = new ArrayList();
                List<SectoresAutorizados> sectoresAutorizados = sectoresAutorizadosfService.findSectorByPrsSocPrf(prsSocPrf);
                for (SectoresAutorizados sectorAutorizado : sectoresAutorizados) {
                    SectoresRol sectorRol = new SectoresRol();
                    sectorRol.setSectorId(sectorAutorizado.getSector().getIdSector());
                    sectorRol.setSectorNom(sectorAutorizado.getSector().getDescripcionSector());
                    sectorRol.setEstado(sectorAutorizado.getAtributosAuditoria().getEstadoRegistro());

                    sectoresRol.add(sectorRol);
                }

                perfilAutorizado.setSectoresAutorizados(sectoresRol);
                perfilesAutorizados.add(perfilAutorizado);
            }

            rolAutorizado.setPerfiles(perfilesAutorizados);
            roles.add(rolAutorizado);
        }

        userData.setRoles(roles);
        reply.setUserData(userData);

        return reply;
    }

    @RequestMapping("/hostpuerto/{id}")
    @ResponseStatus(HttpStatus.OK)
    DataBaseGeneralParam getHostPuerto(
            @PathVariable String id, 
            @RequestHeader("X-Auth-Token") String token,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException {


        log.info("=====================getHostPuerto");

        log.info("token-----" + token);
        log.info("idUser-----" + idUser);
        log.info("idSoc-----" + idSoc);
        log.info("idPrf-----" + idPrf);
        log.info("idSec-----" + idSec);

        Sector sector = sectorService.findSector(idSec);
        if(sector==null){
                    Object[] valores = {sector};
                    String mensaje = MessageFormat.format("ERROR: CredentialsProfiles. Sector id  no existe. {0}", valores);
                    throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }
        
        DataBaseGeneralParam dataBaseGeneralParam = new DataBaseGeneralParam();
        dataBaseGeneralParam.setSizeAplicationImage(sizeAplicationImage);
        dataBaseGeneralParam.setSizeAplicationCatalog(sizeAplicationCatalog);
        dataBaseGeneralParam.setUltimoNivel(sector.getUltimoNivel());
        dataBaseGeneralParam.setHostDestino(sector.getHostDestino()); 
        dataBaseGeneralParam.setPuertoDestino(sector.getPuertoDestino());
        dataBaseGeneralParam.setPuertoDestinoSSL(sector.getPuertoDestinoSSL());

        return dataBaseGeneralParam;
    }

    @RequestMapping("/test")
    @ResponseStatus(HttpStatus.OK)
    String[] home() {
        log.info("................................................................................CONTROL");

        log.info(".............................................CredencialesController.home.........................");
        counterService.increment("ctrl.private.invoked");
        return new String[]{"Ok, puedes acceder a la parte privada."};
    }

}
