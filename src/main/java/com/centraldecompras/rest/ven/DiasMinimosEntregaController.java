package com.centraldecompras.rest.ven;

import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.DiasEntrega;
import com.centraldecompras.modelo.SocZonGeoExcl;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoExcludesService;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonaGeograficaService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.modelo.ven.service.interfaces.DiasEntregaService;
import com.centraldecompras.rest.ven.ser.interfaces.DiasMinimosEntregaRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ven")
@Api(value = "Recurso privado", description = "Gestion de almacenes")
public class DiasMinimosEntregaController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DiasMinimosEntregaController.class.getName());

    @Autowired
    private DiasEntregaService diasEntregaService;

    @Autowired
    private DiasMinimosEntregaRestService diasEntregaRestService;

    @Autowired
    private SociedadRplService sociedadRplService;

    @Autowired
    private ZonaGeograficaService zonaGeograficaService;

    @Autowired
    private SocZonGeoExcludesService socZonGeoExcludesService;

    @RequestMapping(value = "/diasMinimosEntrega", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Condiciones entrega", notes = "ComentarioJson, formato de comunicacion con cliente")
    List<Map<String, Object>> postCondicionesEntrega(
            @RequestBody List<Map<String, Object>> diasMinimosEntregaMaps,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");
        List<Map<String, Object>> reply = new ArrayList();

        List<Object[]> listaObjetos = new ArrayList();

        for (Iterator<Map<String, Object>> it = diasMinimosEntregaMaps.iterator(); it.hasNext();) {
            Map<String, Object> diasMinimosEntregaMap = it.next();

            String idZonaGeografica = (String) diasMinimosEntregaMap.get("idZonaGeografica");
            Boolean lunes = (Boolean) diasMinimosEntregaMap.get("lunes");
            Boolean martes = (Boolean) diasMinimosEntregaMap.get("martes");
            Boolean miercoles = (Boolean) diasMinimosEntregaMap.get("miercoles");
            Boolean jueves = (Boolean) diasMinimosEntregaMap.get("jueves");
            Boolean viernes = (Boolean) diasMinimosEntregaMap.get("viernes");
            Boolean sabado = (Boolean) diasMinimosEntregaMap.get("sabado");
            Boolean domingo = (Boolean) diasMinimosEntregaMap.get("domingo");

            Double pedidoMinimo = 0.0;
            Object pedidoMinimoObject = diasMinimosEntregaMap.get("pedidoMinimo");
            if (pedidoMinimoObject instanceof Integer) {
                pedidoMinimo = Double.parseDouble(String.valueOf(pedidoMinimoObject));
            } else {
                pedidoMinimo = (Double) pedidoMinimoObject;
            }

            Double pedidoMinimoLibreTrans = 0.0;
            Object pedidoMinimoLibreTransObject = diasMinimosEntregaMap.get("pedidoMinimoLibreTrans");
            if (pedidoMinimoLibreTransObject instanceof Integer) {
                pedidoMinimoLibreTrans = Double.parseDouble(String.valueOf(pedidoMinimoLibreTransObject));
            } else {
                pedidoMinimoLibreTrans = (Double) pedidoMinimoLibreTransObject;
            }

            Double precioTransporte = 0.0;
            Object precioTransporteObject = diasMinimosEntregaMap.get("precioTransporte");
            if (precioTransporteObject instanceof Integer) {
                precioTransporte = Double.parseDouble(String.valueOf(precioTransporteObject));
            } else {
                precioTransporte = (Double) precioTransporteObject;
            }

            Integer diasDemora = (Integer) diasMinimosEntregaMap.get("diasDemora");

            Integer fechaPrevistaActivacion = (Integer) diasMinimosEntregaMap.get("fechaPrevistaActivacion");
            Integer fechaPrevistaDesactivacion = (Integer) diasMinimosEntregaMap.get("fechaPrevistaDesactivacion");

            String estado = (String) diasMinimosEntregaMap.get("estado");
            String metodo = (String) diasMinimosEntregaMap.get("method");

            SociedadRpl sociedadRpl = null;
            sociedadRpl = sociedadRplService.findSociedadRpl(idSoc);
            if (sociedadRpl == null) {
                Object[] valores = {idSoc};
                String mensaje = MessageFormat.format("ERROR: Asignacion dias entrega. Sociedad {0}, no encontrada. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }
            ZonaGeografica zonaGeografica = zonaGeograficaService.findZonaGeografica(idZonaGeografica);
            if (zonaGeografica == null) {
                Object[] valores = {idZonaGeografica};
                String mensaje = MessageFormat.format("ERROR: Asignacion dias entrega. zonaGeografica {0}, no encontrada. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            DiasEntrega diasEntrega = null;
            if (KApp.POST.getKApp().equals(metodo)) {
                diasEntrega = new DiasEntrega();
                diasEntrega.setSociedad(sociedadRpl);
                diasEntrega.setZonaGeografica(zonaGeografica);
                diasEntrega.setAtributosAuditoria(new DatosAuditoria());
                diasEntrega.getAtributosAuditoria().setCreated(new Date());
                diasEntrega.getAtributosAuditoria().setDeleted(null);

            } else {
                diasEntrega = diasEntregaService.findDiasEntregaBy_S_Z(sociedadRpl, zonaGeografica);
                diasEntrega.getAtributosAuditoria().setUpdated(new Date());
            }

            if (diasEntrega != null) {
                diasEntrega.setLunes(lunes);
                diasEntrega.setMartes(martes);
                diasEntrega.setMiercoles(miercoles);
                diasEntrega.setJueves(jueves);
                diasEntrega.setViernes(viernes);
                diasEntrega.setSabado(sabado);
                diasEntrega.setDomingo(domingo);
                diasEntrega.setPedidoMinimo(new BigDecimal(pedidoMinimo).setScale(2, RoundingMode.HALF_EVEN));
                diasEntrega.setPedidoMinimoLibreTrans(new BigDecimal(pedidoMinimoLibreTrans).setScale(2, RoundingMode.HALF_EVEN));
                diasEntrega.setPrecioTransporte(new BigDecimal(precioTransporte).setScale(2, RoundingMode.HALF_EVEN));
                diasEntrega.setDiasDemora(diasDemora);

                diasEntrega.getAtributosAuditoria().setEstadoRegistro(estado);
                diasEntrega.getAtributosAuditoria().setFechaPrevistaActivacion(fechaPrevistaActivacion);
                diasEntrega.getAtributosAuditoria().setFechaPrevistaDesactivacion(fechaPrevistaDesactivacion);
                diasEntrega.getAtributosAuditoria().setUltimaAccion(metodo);
                diasEntrega.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            }

            Object[] aaa = new Object[5];
            aaa[0] = metodo;
            aaa[1] = "diasEntrega";
            aaa[2] = diasEntrega;
            aaa[3] = sociedadRpl;
            aaa[4] = zonaGeografica;

            listaObjetos.add(aaa);
        }

        diasEntregaRestService.persistencia(listaObjetos);

        reply = montaJson(idSoc);

        return reply;

    }

    @RequestMapping(value = "/diasMinimosEntregaSoc", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera condiciones entrega de una sociedad", notes = "List<ComentarioJson>, formato de comunicacion con cliente")
    List<Map<String, Object>> getCondicionesEntrega(
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList();

        if (idSoc != null) {
            reply = montaJson(idSoc);
        }
        return reply;
    }

    @RequestMapping(value = "/diasMinimosEntregaSocZon", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera condiciones entrega de una sociedad", notes = "List<ComentarioJson>, formato de comunicacion con cliente")
    List<Map<String, Object>> diasMinimosEntregaSocZon(
            @RequestBody Map<String, Object> inputMap,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList();
        if (!inputMap.containsKey("idSocProveedor") || !inputMap.containsKey("idZonaGeografica")) {
            throw new Exception("idSocProveedor o idZonaGeografica no son parametros");
        }

        String idSocProveedor = (String) inputMap.get("idSocProveedor");
        String idZonaGeografica = (String) inputMap.get("idZonaGeografica");

        if (idSocProveedor != null && idZonaGeografica != null) {
            reply = montaJson(idSocProveedor, idZonaGeografica);
        } else {
            throw new Exception("idSocProveedor o idZonaGeografica no son validos");
        }
        return reply;
    }

    private List<Map<String, Object>> montaJson(String idSoc) throws JsonProcessingException, Exception {
        return montaJson(idSoc, null);
    }

    private List<Map<String, Object>> montaJson(String idSoc, String idZonaGeografica) throws JsonProcessingException, Exception {
        List<Map<String, Object>> reply = new ArrayList();
        List<DiasEntrega> diasEntregas = new ArrayList();

        if (idZonaGeografica == null) {
            diasEntregas = (List<DiasEntrega>) diasEntregaService.findDiasEntregaBy_idS(idSoc);
        } else {
            SociedadRpl sociedad = sociedadRplService.findSociedadRpl(idSoc);
            ZonaGeografica zona = zonaGeograficaService.findZonaGeografica(idZonaGeografica);

            if (sociedad != null && zona != null) {
                String zonasGeograficasId = zona.getRoute();

                String sReversed = "";
                StringTokenizer tokens = new StringTokenizer(zonasGeograficasId, ".");
                while (tokens.hasMoreTokens()) {
                    sReversed = tokens.nextToken() + "." + sReversed;
                }
                StringTokenizer tokensReversed = new StringTokenizer(sReversed, ".");

                Boolean informado = false;
                while (tokensReversed.hasMoreTokens()) {
                    String str = tokensReversed.nextToken();
                    
                    ZonaGeografica zonaGeografica = zonaGeograficaService.findZonaGeografica(str);
                    if (zonaGeografica != null) {
                        SocZonGeoExcl socZonGeoExcl = socZonGeoExcludesService.findSocZonGeoExcludesBy_idZ_r(sociedad.getIdSociedad(), zonaGeografica.getRoute());
                        if (socZonGeoExcl != null) {
                            diasEntregas.clear();
                            break;
                        }
                        DiasEntrega diaEntregas = (DiasEntrega) diasEntregaService.findDiasEntregaBy_S_Z(sociedad, zonaGeografica);
                        if (diaEntregas != null) {
                            if (!informado) {
                                informado = true;
                                diasEntregas = new ArrayList();
                                diasEntregas.add(diaEntregas);
                            }
                        }
                    }
                }

            } else {
                throw new Exception("Socieda o ZonaGeografica no encontradas");
            }
        }

        if (diasEntregas.size() > 0) {
            for (DiasEntrega diasEntrega : diasEntregas) {
                Map<String, Object> diasMinimosEntregaMap = new HashMap<>();
                diasMinimosEntregaMap.put("idsociedad", diasEntrega.getSociedad().getIdSociedad());
                diasMinimosEntregaMap.put("idZonaGeografica", diasEntrega.getZonaGeografica().getIdZonaGeografica());
                diasMinimosEntregaMap.put("lunes", diasEntrega.getLunes());
                diasMinimosEntregaMap.put("martes", diasEntrega.getMartes());
                diasMinimosEntregaMap.put("miercoles", diasEntrega.getMiercoles());
                diasMinimosEntregaMap.put("jueves", diasEntrega.getJueves());
                diasMinimosEntregaMap.put("viernes", diasEntrega.getViernes());
                diasMinimosEntregaMap.put("sabado", diasEntrega.getSabado());
                diasMinimosEntregaMap.put("domingo", diasEntrega.getDomingo());
                diasMinimosEntregaMap.put("pedidoMinimo", diasEntrega.getPedidoMinimo());
                diasMinimosEntregaMap.put("pedidoMinimoLibreTrans", diasEntrega.getPedidoMinimoLibreTrans());
                diasMinimosEntregaMap.put("precioTransporte", diasEntrega.getPrecioTransporte());
                diasMinimosEntregaMap.put("diasDemora", diasEntrega.getDiasDemora());
                diasMinimosEntregaMap.put("fechaPrevistaActivacion", diasEntrega.getAtributosAuditoria().getFechaPrevistaActivacion());
                diasMinimosEntregaMap.put("fechaPrevistaDesactivacion", diasEntrega.getAtributosAuditoria().getFechaPrevistaDesactivacion());
                diasMinimosEntregaMap.put("estado", diasEntrega.getAtributosAuditoria().getEstadoRegistro());
                diasMinimosEntregaMap.put("method", diasEntrega.getAtributosAuditoria().getUltimaAccion());

                reply.add(diasMinimosEntregaMap);
            }
        } else {
            Map<String, Object> diasMinimosEntregaMap = new HashMap<String, Object>();
            diasMinimosEntregaMap.put("idsociedad", idSoc);
            diasMinimosEntregaMap.put("idZonaGeografica", idZonaGeografica);
            diasMinimosEntregaMap.put("lunes", Boolean.FALSE);
            diasMinimosEntregaMap.put("martes", Boolean.FALSE);
            diasMinimosEntregaMap.put("miercoles", Boolean.FALSE);
            diasMinimosEntregaMap.put("jueves", Boolean.FALSE);
            diasMinimosEntregaMap.put("viernes", Boolean.FALSE);
            diasMinimosEntregaMap.put("sabado", Boolean.FALSE);
            diasMinimosEntregaMap.put("domingo", Boolean.FALSE);
            diasMinimosEntregaMap.put("pedidoMinimo", 0);
            diasMinimosEntregaMap.put("pedidoMinimoLibreTrans", 0);
            diasMinimosEntregaMap.put("precioTransporte", 0);
            diasMinimosEntregaMap.put("diasDemora", 0);
            diasMinimosEntregaMap.put("fechaPrevistaActivacion", 0);
            diasMinimosEntregaMap.put("fechaPrevistaDesactivacion", 0);
            diasMinimosEntregaMap.put("estado", KApp.ACTIVO.getKApp());
            diasMinimosEntregaMap.put("method", KApp.DUMMY_METHOD.getKApp());
    }

        return reply;
    }
}
