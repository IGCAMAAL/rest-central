package com.centraldecompras.rest.ven.ser.interfaces;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.Oferta;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.Map;

public interface OfertaRestService {

    void persistencia(List<Object[]> listaObjetos) throws Exception;

    Oferta montaOferta(Map<String, Object> ofertaJsonIn, Articulo artculo, String idUser) throws NonexistentEntityException, Exception;
    /*
     Object[] montaOfertaLangDesc(Articulo articulo, TraduccionDescripcion traduccionDescripcion, String idUser, String metodo);
     */

    public Object[] montaOfertaAprobacion(Articulo articulo, String estadoAprobacion, String idUser, String method) throws NonexistentEntityException, Exception;

    Map<String, Object> toJson(Oferta oferta);
}
