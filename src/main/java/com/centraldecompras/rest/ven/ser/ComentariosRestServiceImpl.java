package com.centraldecompras.rest.ven.ser;

import com.centraldecompras.rest.ven.ser.interfaces.ComentariosRestService;
import com.centraldecompras.modelo.Comentarios;
import com.centraldecompras.modelo.ven.service.interfaces.ComentariosService;
import com.centraldecompras.rest.ven.ComentariosController;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ComentariosRestServiceImpl implements ComentariosRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ComentariosRestServiceImpl.class.getName());


    @Autowired
    private ComentariosService comentariosService;
 
    @Transactional
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");
        
        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("comentarios").equals((String) bbb[1])) {
                    comentariosService.create((Comentarios) bbb[2]);
                }
            }

            if (("PUT").equals((String) bbb[0])) {
                if (("comentarios").equals((String) bbb[1])) {
                    comentariosService.edit((Comentarios) bbb[2]);
                }
            }
        }
        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("comentarios").equals((String) bbb[1])) {
                    comentariosService.destroy((String) bbb[3]);
                }
            }
        }
    }

}
