package com.centraldecompras.rest.ven.ser;


import com.centraldecompras.rest.ven.ser.interfaces.DiasMinimosEntregaRestService;
import com.centraldecompras.modelo.ConDatFra;
import com.centraldecompras.modelo.DiasEntrega;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.ven.service.interfaces.ConDatFraService;
import com.centraldecompras.modelo.ven.service.interfaces.DiasEntregaService;
import com.centraldecompras.rest.ven.ComentariosController;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DiasMinimosEntregaRestServiceImpl implements DiasMinimosEntregaRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DiasMinimosEntregaRestServiceImpl.class.getName());


    @Autowired
    private DiasEntregaService diasEntregaService;
    
    @Autowired
    ConDatFraService conDatFraService;
             
    @Transactional
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");
        
        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("diasEntrega").equals((String) bbb[1])) {
                    diasEntregaService.create((DiasEntrega) bbb[2]);
                }
                if (("conDatFra").equals((String) bbb[1])) {
                    conDatFraService.create((ConDatFra) bbb[2]);
                }
            }
            if (("PUT").equals((String) bbb[0])) {
                if (("diasEntrega").equals((String) bbb[1])) {
                    diasEntregaService.edit((DiasEntrega) bbb[2]);
                }
                if (("conDatFra").equals((String) bbb[1])) {
                    conDatFraService.edit((ConDatFra) bbb[2]);
                }
            }
        }
        
        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("diasEntrega").equals((String) bbb[1])) {
                    diasEntregaService.destroyBy_S_Z((SociedadRpl) bbb[3],(ZonaGeografica) bbb[4]);
                }
                if (("conDatFra").equals((String) bbb[1])) {
                    conDatFraService.destroy((String) bbb[3]);
                }
            }
        }
        
    }
}
