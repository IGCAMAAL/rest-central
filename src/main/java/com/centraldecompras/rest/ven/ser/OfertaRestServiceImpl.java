package com.centraldecompras.rest.ven.ser;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Oferta;
import com.centraldecompras.modelo.ven.service.interfaces.OfertaService;
import com.centraldecompras.rest.ven.ser.interfaces.OfertaRestService;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.rest.ven.ComentariosController;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OfertaRestServiceImpl implements OfertaRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(OfertaRestServiceImpl.class.getName());


    @Autowired
    private OfertaService ofertaService;

    @Transactional
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");
        
        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("oferta").equals((String) bbb[1])) {
                    ofertaService.create((Oferta) bbb[2]);
                }
            }

            if (("PUT").equals((String) bbb[0])) {
                if (("oferta").equals((String) bbb[1])) {
                    ofertaService.edit((Oferta) bbb[2]);
                }
            }
        }

        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("oferta").equals((String) bbb[1])) {
                    ofertaService.destroy((String) bbb[3]);
                }

            }
        }

    }

    public Oferta montaOferta(Map<String, Object> ofertaJsonIn, Articulo artculo, String idUser) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");
                
        Oferta reply = null;

        List<TraduccionDescripcion> descripcionOferta = (List<TraduccionDescripcion>) ofertaJsonIn.get("descripcionOferta");

        if (("POST").equals((String) ofertaJsonIn.get("method"))) {
            reply = new Oferta();
            reply.setIdOferta(UUID.randomUUID().toString().replaceAll("-", ""));
            reply.setAtributosAuditoria(new DatosAuditoria());
            reply.getAtributosAuditoria().setCreated(new Date());

        } else {
            reply = ofertaService.findOferta((String) ofertaJsonIn.get("id"));
            reply.getAtributosAuditoria().setUpdated(new Date());
        }

        if (reply != null) {
            reply.setArticulo(artculo);
            reply.setEstadoAprobacion(KApp.PENDIENTE.getKApp()); 
            
            
            Object uniMinVtaOfertaO = (Object) ofertaJsonIn.get("uniMinVtaOferta");
            Double uniMinVtaOfertaD=0.0;
            if(uniMinVtaOfertaO instanceof Integer){
                uniMinVtaOfertaD = (Double)((Integer)uniMinVtaOfertaO).doubleValue();
            } else {
                uniMinVtaOfertaD = (Double) ofertaJsonIn.get("uniMinVtaOferta");
            }
            reply.setUniMinVtaOferta((new BigDecimal(uniMinVtaOfertaD)).setScale(2,RoundingMode.HALF_EVEN));
            
            
            Object precioOfertaUniMedO = (Object) ofertaJsonIn.get("precioOfertaUniMed");
            Double precioOfertaUniMedD=0.0;
            if(precioOfertaUniMedO instanceof Integer){
                precioOfertaUniMedD = (Double)((Integer)precioOfertaUniMedO).doubleValue();
            } else {
                precioOfertaUniMedD = (Double) ofertaJsonIn.get("precioOfertaUniMed");
            }
            reply.setPrecioOfertaUniMed((new BigDecimal(precioOfertaUniMedD)).setScale(2,RoundingMode.HALF_EVEN));
            
            
            Object costeDiarioO = (Object) ofertaJsonIn.get("costeDiario");
            Double costeDiarioD=0.0;
            if(costeDiarioO instanceof Integer){
                costeDiarioD = (Double)((Integer)costeDiarioO).doubleValue();
            } else {
                costeDiarioD = (Double) ofertaJsonIn.get("costeDiario");
            }
            reply.setCosteDiario((new BigDecimal(costeDiarioD)).setScale(4,RoundingMode.HALF_EVEN));
            
            
            reply.setDescripcionOferta(" ");
            reply.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            reply.getAtributosAuditoria().setFechaPrevistaActivacion((int) ofertaJsonIn.get("fechaPrevistaActivacion"));
            reply.getAtributosAuditoria().setFechaPrevistaDesactivacion((int) ofertaJsonIn.get("fechaPrevistaDesactivacion"));
            reply.getAtributosAuditoria().setEstadoRegistro((String) ofertaJsonIn.get("estado"));
            reply.getAtributosAuditoria().setUltimaAccion((String) ofertaJsonIn.get("method"));

        }

        return reply;
    }
/*
    public Object[] montaOfertaLangDesc(Articulo articulo, TraduccionDescripcion traduccionDescripcion, String idUser, String metodo) {

        ArticuloLangDesc articuloLang = null;
        articuloLang = articuloLangDescService.findArticuloLangDescBy_IdA_Lang(articulo.getIdArticulo(), traduccionDescripcion.getIdioma());

        if (articuloLang == null) {
            articuloLang = new ArticuloLangDesc();
            articuloLang.setArticuloId(articulo.getIdArticulo());
            articuloLang.setArticuloEntity(articulo);
            articuloLang.setIdioma(traduccionDescripcion.getIdioma());
            articuloLang.setAtributosAuditoria(new DatosAuditoria());
            articuloLang.getAtributosAuditoria().setCreated(new Date());
            metodo = KApp.POST.getKApp();
        } else {
            articuloLang.getAtributosAuditoria().setUpdated(new Date());
        }

        articuloLang.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
        articuloLang.getAtributosAuditoria().setFechaPrevistaActivacion(0);
        articuloLang.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
        articuloLang.getAtributosAuditoria().setEstadoRegistro(articulo.getAtributosAuditoria().getEstadoRegistro());
        articuloLang.getAtributosAuditoria().setUltimaAccion(metodo);
        articuloLang.setTraduccionDesc(traduccionDescripcion.getTraduccion());

        Object[] aaa = new Object[5];
        aaa[0] = metodo;
        aaa[1] = "articuloLangDesc";
        aaa[2] = articuloLang;
        aaa[3] = articulo.getIdArticulo();
        aaa[4] = articuloLang.getIdioma();

        traduccionDescripcion.setIdioma(articuloLang.getIdioma());  // Para busqueda al final del método y presentación de la persistencia

        return aaa;
    }
*/
    public Object[] montaOfertaAprobacion(Articulo articulo, String estadoAprobacion, String idUser, String method) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");
        
        Object[] reply = new Object[4];

        Boolean actualizar = false;
        if (articulo.getAtributosAuditoria().getEstadoRegistro().equals(KApp.ACTIVO.getKApp())) {
            if (articulo.getEstadoAprobacion().equals(KApp.PEND_ALTA_ART.getKApp())) {
                articulo.setEstadoAprobacion(estadoAprobacion);
                actualizar = true;
            } else if (articulo.getEstadoAprobacion().equals(KApp.PEND_MOD_PRECIO.getKApp())) {
                articulo.setEstadoAprobacion(estadoAprobacion);
                articulo.setPrecioUniMed(articulo.getPrecioUniMedNuevo());
                articulo.setPrecioUniMedNuevo(new BigDecimal(0));
                actualizar = true;
            }

            if (actualizar) {
                articulo.getAtributosAuditoria().setUltimaAccion(method);
                articulo.getAtributosAuditoria().setUpdated(new Date());
                articulo.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

                reply[0] = method;
                reply[1] = "articulo";
                reply[2] = articulo;
                reply[3] = articulo.getIdArticulo();
            }
        }

        return reply;
    }

    public Map<String, Object> toJson(Oferta oferta) {
        log.info("....................................................................mbm............CONTROL");
                
        Map<String, Object> reply = new HashMap();

        reply.put("id", oferta.getIdOferta());
        reply.put("idArticulo", oferta.getArticulo().getIdArticulo());
        reply.put("uniMinVtaOferta", oferta.getUniMinVtaOferta());
        reply.put("precioOfertaUniMed", oferta.getPrecioOfertaUniMed());
        reply.put("costeDiario", oferta.getCosteDiario());

        reply.put("estadoAprobacion", oferta.getEstadoAprobacion());
        reply.put("estado", oferta.getAtributosAuditoria().getEstadoRegistro());
        reply.put("method", oferta.getAtributosAuditoria().getUltimaAccion());
        reply.put("fechaPrevistaActivacion", oferta.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.put("fechaPrevistaDesactivacion", oferta.getAtributosAuditoria().getFechaPrevistaDesactivacion());
/*
        List<OfertaLangDesc> ofertasLangDesc = ofertaLangDescService.findOfertaLangDescBy_O(oferta);
        List<TraduccionDescripcion> traduccionesDesc = new ArrayList<TraduccionDescripcion>();
        for (ArticuloLangDesc ofertaLangDesc : ofertasLangDesc) {
            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(ofertaLangDesc.getIdioma());
            traduccionDescripcion.setTraduccion(ofertaLangDesc.getTraduccionDesc());

            traduccionesDesc.add(traduccionDescripcion);
        }
        reply.put("descripcionOferta", traduccionesDesc);
*/
        reply.put("descripcionOferta", " ");
        return reply;
    }

}
