package com.centraldecompras.rest.ven.ser;

import com.centraldecompras.rest.ven.ser.interfaces.ValoracionesRestService;
import com.centraldecompras.modelo.Valoraciones;
import com.centraldecompras.modelo.ven.service.interfaces.ValoracionesService;
import com.centraldecompras.rest.ven.ComentariosController;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ValoracionesRestServiceImpl implements ValoracionesRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ValoracionesRestServiceImpl.class.getName());


    @Autowired
    private ValoracionesService valoracionesService;
 
    @Transactional
    @Override
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");
        
        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("valoraciones").equals((String) bbb[1])) {
                    valoracionesService.create((Valoraciones) bbb[2]);
                }
            }

            if (("PUT").equals((String) bbb[0])) {
                if (("valoraciones").equals((String) bbb[1])) {
                    valoracionesService.edit((Valoraciones) bbb[2]);
                }
            }
        }
        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("valoraciones").equals((String) bbb[1])) {
                    valoracionesService.destroy((String) bbb[3]);
                }
            }
        }
    }

}
