package com.centraldecompras.rest.ven;

import com.centraldecompras.rest.ven.json.ComentarioJson;
import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.Comentarios;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.modelo.ven.service.interfaces.ComentariosService;
import com.centraldecompras.rest.ven.ser.interfaces.ComentariosRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vencom")
@Api(value = "Recurso privado", description = "Gestion de almacenes")
public class ComentariosController { 

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ComentariosController.class.getName());

    @Autowired
    private ComentariosService comentariosService;

    @Autowired
    private ComentariosRestService comentariosRestService;

    @Autowired
    SociedadRplService sociedadRplService;

    @Autowired
    ArticuloService articuloService;

    @RequestMapping(value = "/comentario", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Comentarios", notes = "ComentarioJson, formato de comunicacion con cliente")
    List<ComentarioJson> postComentario(
            @RequestBody List<ComentarioJson> comentariosJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ComentarioJson> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();

        for (ComentarioJson comentarioJson : comentariosJson) {

            String id = comentarioJson.getId();
            String metodo = comentarioJson.getMethod();

            HashMap<String, Object> parYaud = new HashMap<String, Object>();
            parYaud.put("idUser", idUser);
            parYaud.put("idSoc", idSoc);
            parYaud.put("idPrf", idPrf);
            parYaud.put("idSec", idSec);

            parYaud.put("id", comentarioJson.getId());
            parYaud.put("estado", comentarioJson.getEstado());
            parYaud.put("method", comentarioJson.getMethod());
            parYaud.put("fpAct", comentarioJson.getFechaPrevistaActivacion());
            parYaud.put("fpDes", comentarioJson.getFechaPrevistaDesactivacion());

            SociedadRpl sociedadOpinanteRpl = null;
            SociedadRpl sociedadOpinadaRpl = null;
            Articulo articuloOpinado = null;
            Comentarios comentarios = null;

            if (("0").equals(id) && ("POST").equals(metodo) || !("0").equals(id) && !("POST").equals(metodo)) {

                Object[] aaa = new Object[4];
                if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {

                    sociedadOpinanteRpl = sociedadRplService.findSociedadRpl(idSoc);
                    if (sociedadOpinanteRpl == null) {
                        Object[] valores = {idSoc};
                        String mensaje = MessageFormat.format("ERROR: Sociedad Opinante {0}, no encontrada. ", valores);
                        throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                    }

                    sociedadOpinadaRpl = sociedadRplService.findSociedadRpl(comentarioJson.getSociedadOpinadaId());
                    if (sociedadOpinadaRpl == null) {
                        Object[] valores = {idSoc};
                        String mensaje = MessageFormat.format("ERROR: Sociedad Opinada {0}, no encontrada. ", valores);
                        throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                    }

                    if (!"".equals(comentarioJson.getArticuloOpinadoId())) {
                        articuloOpinado = articuloService.findArticulo(comentarioJson.getArticuloOpinadoId());
                        if (articuloOpinado == null) {
                            Object[] valores = {comentarioJson.getArticuloOpinadoId()};
                            String mensaje = MessageFormat.format("ERROR: Articulo Opinado {0}, no encontrado. ", valores);
                            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                        } else {
                            if (!articuloOpinado.getSociedad().getIdSociedad().equals(sociedadOpinadaRpl.getIdSociedad())) {
                                Object[] valores = {sociedadOpinadaRpl.getIdSociedad(), comentarioJson.getArticuloOpinadoId()};
                                String mensaje = MessageFormat.format("ERROR: El articulo Opinado {1} no pertenece a la Sociedad {0} indicada  .", valores);
                                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                            }
                        }
                    }

                    comentarios = montaComentarios(comentarioJson, idUser, sociedadOpinanteRpl, sociedadOpinadaRpl, articuloOpinado);
                    comentarioJson.setId(comentarios.getIdComentario());  // Para busqueda al final del método y presentación de la persistencia
                    aaa[2] = comentarios;
                    aaa[3] = null;
                } else if (KApp.DELETE.getKApp().equals(metodo)) {
                    aaa[2] = null;
                    aaa[3] = comentarioJson.getId();
                    parYaud.put("method", KApp.DELETE.getKApp());
                }
                aaa[0] = metodo;
                aaa[1] = "comentarios";
                listaObjetos.add(aaa);

            } else {
                Object[] valores = {""};
                String mensaje = MessageFormat.format("ERROR: CRUD Comentarios. Id y métodos no están sincronizaods {0}", valores);
                throw new Exception(MessageFormat.format(mensaje, new Throwable()));

            }
        }

        comentariosRestService.persistencia(listaObjetos);

        for (ComentarioJson comentarioJson : comentariosJson) {
            Comentarios comentario = comentariosService.findComentarios(comentarioJson.getId());
            if (comentario != null) {
                reply.add(toJson(comentario));
            }
        }

        return reply;
    }

    @RequestMapping(value = "/comentariosoc/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera los Comentarios de una sociedad", notes = "ComentarioJson, formato de comunicacion con cliente")
    List<ComentarioJson> getComentario(
            @PathVariable String id,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<ComentarioJson> reply = new ArrayList();

        List<Comentarios> comentarios = comentariosService.findComentariosBy_idS(id);

        for (Comentarios comentario : comentarios) {
            reply.add(toJson(comentario));
        }

        return reply;
    }

    private Comentarios montaComentarios(ComentarioJson comentarioJson, String idUser, SociedadRpl sociedadOpinanteRpl, SociedadRpl sociedadOpinadaRpl, Articulo articuloOpinado) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");
                
        Comentarios comentarios = null;

        if (("POST").equals(comentarioJson.getMethod())) {
            comentarios = new Comentarios();
            comentarios.setIdComentario(UUID.randomUUID().toString().replaceAll("-", ""));
            comentarios.setAtributosAuditoria(new DatosAuditoria());
            comentarios.getAtributosAuditoria().setCreated(new Date());
        } else {  // Modificacion o Delete
            comentarios = comentariosService.findComentarios(comentarioJson.getId());
            comentarios.getAtributosAuditoria().setUpdated(new Date());
        }

        if (comentarios != null) {
            comentarios.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            comentarios.getAtributosAuditoria().setFechaPrevistaActivacion(comentarioJson.getFechaPrevistaActivacion());
            comentarios.getAtributosAuditoria().setFechaPrevistaDesactivacion(comentarioJson.getFechaPrevistaDesactivacion());
            comentarios.getAtributosAuditoria().setEstadoRegistro(comentarioJson.getEstado());
            comentarios.getAtributosAuditoria().setUltimaAccion(comentarioJson.getMethod());

            comentarios.setAprobacion(comentarioJson.getAprobacion());
            comentarios.setOpinion(comentarioJson.getComentario());
            comentarios.setRelCalidadPrecio(comentarioJson.getRelCalidadPrecio());
            comentarios.setValoracion(comentarioJson.getValoracion());

            comentarios.setSociedadOpinante(sociedadOpinanteRpl);
            comentarios.setSociedadOpinada(sociedadOpinadaRpl);
            comentarios.setArticuloOpinado(articuloOpinado);
            //   comentarios.setSociedadOpinanteIdk(sociedadOpinanteRpl.getIdSociedad());
            //   comentarios.setSociedadOpinadaIdk(sociedadOpinadaRpl != null ? sociedadOpinadaRpl.getIdSociedad() : "");
            //   comentarios.setArticuloOpinadoIdk((articuloOpinado != null ? articuloOpinado.getIdArticiulo() : ""));
        }

        return comentarios;
    }

    private ComentarioJson toJson(Comentarios comentarios) {
        log.info("....................................................................mbm............CONTROL");
        
        ComentarioJson reply = new ComentarioJson();

        reply.setId(comentarios.getIdComentario());
        reply.setAprobacion(comentarios.getAprobacion());
        reply.setArticuloOpinadoId(comentarios.getArticuloOpinado().getIdArticulo());
        reply.setComentario(comentarios.getOpinion());
        reply.setRelCalidadPrecio(comentarios.getRelCalidadPrecio());
        reply.setSociedadOpinadaId(comentarios.getSociedadOpinada().getIdSociedad());
        reply.setSociedadOpinanteId(comentarios.getSociedadOpinante().getIdSociedad());
        reply.setValoracion(comentarios.getValoracion());

        reply.setEstado(comentarios.getAtributosAuditoria().getEstadoRegistro());
        reply.setMethod(comentarios.getAtributosAuditoria().getUltimaAccion());
        reply.setFechaPrevistaActivacion(comentarios.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(comentarios.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setUsuarioOpinanteId(comentarios.getAtributosAuditoria().getUsuarioUltimaAccion());
        return reply;
    }

}
