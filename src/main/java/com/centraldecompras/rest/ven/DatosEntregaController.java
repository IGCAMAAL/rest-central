package com.centraldecompras.rest.ven;

import com.centraldecompras.rest.BORRAR.DiasEntregaJson;
import com.centraldecompras.modelo.ConDatFra;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.modelo.ven.service.interfaces.ConDatFraService;
import com.centraldecompras.rest.ven.json.auxi.ConDatFraJson;
import com.centraldecompras.rest.ven.ser.interfaces.DiasMinimosEntregaRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vencon")
@Api(value = "Recurso privado", description = "Gestion de almacenes")
public class DatosEntregaController { 

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DatosEntregaController.class.getName());

    @Autowired
    ConDatFraService conDatFraService;

    @Autowired
    private DiasMinimosEntregaRestService diasEntregaRestService;

    @Autowired
    private SociedadRplService sociedadRplService;

    @Autowired
    AlmacenService almacenService;

    @RequestMapping(value = "/condicionesentrega", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Condiciones entrega", notes = "ComentarioJson, formato de comunicacion con cliente")
    DiasEntregaJson postCondicionesEntrega(
            @RequestBody DiasEntregaJson diasEntregaJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        DiasEntregaJson reply = new DiasEntregaJson();
        List<Object[]> listaObjetos = new ArrayList();


        HashMap<String, Object> parYaud = new HashMap<String, Object>();
        parYaud.put("idUser", idUser);
        parYaud.put("idSoc", idSoc);
        parYaud.put("idPrf", idPrf);
        parYaud.put("idSec", idSec);

        SociedadRpl sociedadRpl = null;
        sociedadRpl = sociedadRplService.findSociedadRpl(idSoc);
        if (sociedadRpl == null) {
            Object[] valores = {idSoc};
            String mensaje = MessageFormat.format("ERROR: Sociedad {0}, no encontrada. ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }
        parYaud.put("sociedadRpl", sociedadRpl);

        for (ConDatFraJson condicionJson : diasEntregaJson.getCondiciones()) {

            String id = condicionJson.getId();
            String metodo = condicionJson.getMethod();
            parYaud.put("tipReg", KApp.CONDICIONES.getKApp());
            if (("0").equals(id) && KApp.POST.getKApp().equals(metodo) || !("0").equals(id) && !KApp.POST.getKApp().equals(metodo)) {

                Object[] aaa = new Object[4];
                if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
                    ConDatFra conDatFra = null;
                    conDatFra = montaConDatFra(condicionJson, parYaud);
                    condicionJson.setId(conDatFra.getIdConDatFra());  // Para busqueda al final del método y presentación de la persistencia
                    aaa[2] = conDatFra;
                    aaa[3] = null;
                } else if (KApp.DELETE.getKApp().equals(metodo)) {
                    aaa[2] = null;
                    aaa[3] = condicionJson.getId();
                    parYaud.put("method", KApp.DELETE.getKApp());
                }
                aaa[0] = metodo;
                aaa[1] = "conDatFra";
                listaObjetos.add(aaa);

            } else {
                Object[] valores = {""};
                String mensaje = MessageFormat.format("ERROR: CRUD Dias entrega. Id y métodos no están sincronizaods {0}", valores);
                throw new Exception(MessageFormat.format(mensaje, new Throwable()));
            }
        }

        for (ConDatFraJson datoFacturacionJson : diasEntregaJson.getDatosFacturacion()) {

            String id = datoFacturacionJson.getId();
            String metodo = datoFacturacionJson.getMethod();
            parYaud.put("tipReg", KApp.DATOS_FACTURA.getKApp());
            if (("0").equals(id) && KApp.POST.getKApp().equals(metodo) || !("0").equals(id) && !KApp.POST.getKApp().equals(metodo)) {

                Object[] aaa = new Object[4];
                if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
                    ConDatFra conDatFra = null;
                    conDatFra = montaConDatFra(datoFacturacionJson, parYaud);
                    datoFacturacionJson.setId(conDatFra.getIdConDatFra());  // Para busqueda al final del método y presentación de la persistencia
                    aaa[2] = conDatFra;
                    aaa[3] = null;
                } else if (KApp.DELETE.getKApp().equals(metodo)) {
                    aaa[2] = null;
                    aaa[3] = datoFacturacionJson.getId();
                    parYaud.put("method", KApp.DELETE.getKApp());
                }
                aaa[0] = metodo;
                aaa[1] = "conDatFra";
                listaObjetos.add(aaa);

            } else {
                Object[] valores = {""};
                String mensaje = MessageFormat.format("ERROR: CRUD Dias entrega. Id y métodos no están sincronizaods {0}", valores);
                throw new Exception(MessageFormat.format(mensaje, new Throwable()));
            }
        }
 
        diasEntregaRestService.persistencia(listaObjetos);

        reply = montaJson(idSoc);

        return reply;
    }

    @RequestMapping(value = "/condicionesentregasoc/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera condiciones entrega de una sociedad", notes = "List<ComentarioJson>, formato de comunicacion con cliente")
    DiasEntregaJson getCondicionesEntrega(
            @PathVariable String id, // Id de sociedad
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        DiasEntregaJson reply = new DiasEntregaJson();

        if (id != null) {
            reply = montaJson(id);
        }
        return reply;
    }

    private ConDatFra montaConDatFra(ConDatFraJson condicionJson, HashMap<String, Object> parYaud) throws NonexistentEntityException, Exception {
        ConDatFra conDatFra = null;

        if (KApp.POST.getKApp().equals(condicionJson.getMethod())) {
            conDatFra = new ConDatFra();
            conDatFra.setIdConDatFra(UUID.randomUUID().toString().replaceAll("-", ""));
            conDatFra.setAtributosAuditoria(new DatosAuditoria());
            conDatFra.getAtributosAuditoria().setCreated(new Date());
        } else {
            conDatFra = conDatFraService.findConDatFra(condicionJson.getId());
            conDatFra.getAtributosAuditoria().setUpdated(new Date());
        }

        if (conDatFra != null) {
            conDatFra.getAtributosAuditoria().setUsuarioUltimaAccion((String) parYaud.get("idUser"));
            conDatFra.getAtributosAuditoria().setFechaPrevistaActivacion(condicionJson.getFechaPrevistaActivacion());
            conDatFra.getAtributosAuditoria().setFechaPrevistaDesactivacion(condicionJson.getFechaPrevistaDesactivacion());
            conDatFra.getAtributosAuditoria().setEstadoRegistro(condicionJson.getEstado());
            conDatFra.getAtributosAuditoria().setUltimaAccion(condicionJson.getMethod());

            conDatFra.setSociedad((SociedadRpl) parYaud.get("sociedadRpl"));
            conDatFra.setRegType((String) parYaud.get("tipReg"));
            conDatFra.setCondDatfra(condicionJson.getDescripcion());

        }

        return conDatFra;
    }

    private DiasEntregaJson montaJson(String idSoc) {
        log.info("....................................................................mbm............CONTROL");
        
        DiasEntregaJson reply = new DiasEntregaJson();
        reply.setSociedadId(idSoc);

        List<ConDatFra> conDatFra = (List<ConDatFra>) conDatFraService.findConDatFraBy_idS(idSoc);
        List<ConDatFraJson> condicionesJson = new ArrayList();
        List<ConDatFraJson> datosFacturaJson = new ArrayList();
        if (conDatFra != null) {
            for (ConDatFra conDatFraB : conDatFra) {
                ConDatFraJson conDatFraJson = new ConDatFraJson();
                conDatFraJson.setId(conDatFraB.getIdConDatFra());
                conDatFraJson.setDescripcion(conDatFraB.getCondDatfra());
                conDatFraJson.setEstado(conDatFraB.getAtributosAuditoria().getEstadoRegistro());
                conDatFraJson.setMethod(conDatFraB.getAtributosAuditoria().getUltimaAccion());
                conDatFraJson.setFechaPrevistaActivacion(conDatFraB.getAtributosAuditoria().getFechaPrevistaActivacion());
                conDatFraJson.setFechaPrevistaDesactivacion(conDatFraB.getAtributosAuditoria().getFechaPrevistaDesactivacion());

                if (KApp.CONDICIONES.getKApp().equals(conDatFraB.getRegType())) {
                    condicionesJson.add(conDatFraJson);
                } else if (KApp.DATOS_FACTURA.getKApp().equals(conDatFraB.getRegType())) {
                    datosFacturaJson.add(conDatFraJson);
                }
            }
        }
        reply.setCondiciones(condicionesJson);

        reply.setDatosFacturacion(datosFacturaJson);

        return reply;
    }

}
