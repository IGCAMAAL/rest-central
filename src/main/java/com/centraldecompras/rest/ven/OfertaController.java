package com.centraldecompras.rest.ven;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.Oferta;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloService;
import com.centraldecompras.modelo.ven.service.interfaces.OfertaService;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.rest.ven.ser.interfaces.OfertaRestService;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mat")
@Api(value = "Recurso privado", description = "Gestion de almacenes")
public class OfertaController {
 
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(OfertaController.class.getName());

    @Autowired
    private ArticuloService articuloService;

    @Autowired
    private OfertaService ofertaService;

    @Autowired
    private OfertaRestService ofertaRestService;

    @Autowired
    private SociedadRplService sociedadRplService;

    @RequestMapping(value = "/articulooferta", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Oferta", notes = "Formato Map, formato de comunicacion con cliente")
    List<Map<String, Object>> postArticulo(
            @RequestBody List<Map<String, Object>> ofertasJsonIn,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();

        for (Map<String, Object> ofertaJsonIn : ofertasJsonIn) {
            String id = (String) ofertaJsonIn.get("id");
            String idArticulo = (String) ofertaJsonIn.get("idArticulo");
            String metodo = (String) ofertaJsonIn.get("method");
            List<TraduccionDescripcion> descripcionOferta = (List<TraduccionDescripcion>) ofertaJsonIn.get("descripcionOferta");

            Articulo articulo = null;
            if (("0").equals(id) && !("POST").equals(metodo) || !("0").equals(id) && ("POST").equals(metodo)) {
                Object[] valores = {id, metodo};
                String mensaje = MessageFormat.format("Error: CentralCompras. CRUD Ofertas. id Oferta  {0}  y Metodo  {1} ne estan sincronizados ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            articulo = articuloService.findArticulo(idArticulo);
            if (articulo == null) {
                Object[] valores = {idArticulo};
                String mensaje = MessageFormat.format("ERROR: Articulo con id  {0}, no encontrado. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            Oferta oferta = null;
            Object[] aaa = new Object[4];

            oferta = ofertaRestService.montaOferta(ofertaJsonIn, articulo, idUser);
            ofertaJsonIn.replace("id", oferta.getIdOferta());   // Para busqueda al final del método y presentación de la persistencia

            aaa[0] = metodo;
            aaa[1] = "oferta";
            aaa[2] = oferta;
            aaa[3] = oferta.getIdOferta();

            listaObjetos.add(aaa);

            /*
             for (TraduccionDescripcion traduccionDescripcion : descripcionOferta) {
             Object[] bbb = new Object[5];

             bbb = ofertaRestService.montaOfertaLangDesc(articulo, traduccionDescripcion, idUser, metodo);
             ArticuloLangDesc articuloLangDesc = (ArticuloLangDesc) bbb[2];
             traduccionDescripcion.setIdioma(articuloLangDesc.getIdioma());  // Para busqueda al final del método y presentación de la persistencia
             listaObjetos.add(bbb);
             }
             */
        }

        try {
            ofertaRestService.persistencia(listaObjetos);
        } catch (Exception e) {
            e.printStackTrace();
            Object[] valores = {e.getMessage()};
            String mensaje = MessageFormat.format("ERROR: CRUD Oferta. {0} ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        for (Map<String, Object> ofertaJsonIn : ofertasJsonIn) {
            Oferta oferta = new Oferta();
            oferta = ofertaService.findOferta((String) ofertaJsonIn.get("id"));
            if (oferta != null) {
                Map<String, Object> ofertaJsonOut = new HashMap();
                ofertaJsonOut = ofertaRestService.toJson(oferta);
                reply.add(ofertaJsonOut);
            }
        }

        return reply;
    }

    @RequestMapping(value = "/articuloofertaids", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera los datos de un articulo", notes = "Formato Map, formato de comunicacion con cliente")
    List<Map<String, Object>> getOferta(
            @RequestBody List<Map<String, Object>> ofertaIds,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList();
        Map<String, Object> ofertaJson = null;

        for (Map<String, Object> ofertaIdMap : ofertaIds) {
            String ofertaId = (String) ofertaIdMap.get("id");
            Oferta oferta = ofertaService.findOferta(ofertaId);

            if (oferta != null) {
                ofertaJson = ofertaRestService.toJson(oferta);
                reply.add(ofertaJson);
            }
        }

        return reply;
    }

    @RequestMapping(value = "/articuloofertasporsociedadlist", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera las ofertas de un proveedor", notes = "Formato Map, formato de comunicacion con cliente")
    List<Map<String, Object>> getOfertasPorSoc(
            @RequestBody List<Map<String, Object>> sociedadIds,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList<>();

        for (Map<String, Object> sociedadIdMap : sociedadIds) {
            String socId = (String) sociedadIdMap.get("idSoc");
            SociedadRpl sociedadRpl = sociedadRplService.findSociedadRpl(socId);

            if (sociedadRpl == null) {
                Object[] valores = {idSoc};
                String mensaje = MessageFormat.format("ERROR: Lista Articuloas . Sociedad no existe {0}", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            SimpleDateFormat fechaActualFormat = new SimpleDateFormat("yyyyMMdd");
            Calendar fechaActualCalendar = new GregorianCalendar();
            int fechaActualInt = Integer.parseInt(fechaActualFormat.format(fechaActualCalendar.getTime()));
            log.info("fechaActualInt..................................................................." + fechaActualInt);

            List<String> estadosAprobacion = new ArrayList();
            estadosAprobacion.add(KApp.APROBADO.getKApp());
            estadosAprobacion.add(KApp.PENDIENTE.getKApp());
            List<Oferta> ofertas = ofertaService.findOfertasBy_idS_Activas(sociedadRpl.getIdSociedad(), estadosAprobacion, fechaActualInt);
            for (Oferta oferta : ofertas) {
                Map<String, Object> ofertaJsonOut = new HashMap();
                ofertaJsonOut = ofertaRestService.toJson(oferta);
                reply.add(ofertaJsonOut);
            }
        }

        return reply;
    }

}
