package com.centraldecompras.rest.ven;

import com.centraldecompras.rest.ven.json.ValoracionJson;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.Valoraciones;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.modelo.ven.service.interfaces.ValoracionesService;
import com.centraldecompras.rest.ven.ser.interfaces.ValoracionesRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/venval")
@Api(value = "Recurso privado", description = "Gestion de almacenes")
public class ValoracionesController { 

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ValoracionesController.class.getName());

    @Autowired
    private ValoracionesService valoracionService;

    @Autowired
    private ValoracionesRestService valoracionRestService;

    @Autowired
    SociedadRplService sociedadRplService;

    @Autowired
    ArticuloService articuloService;

    @RequestMapping(value = "/valoracion", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Comentarios", notes = "ComentarioJson, formato de comunicacion con cliente")
    List<ValoracionJson> postValoracion(
            @RequestBody List<ValoracionJson> valoracionesJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ValoracionJson> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();

        for (ValoracionJson valoracionJson : valoracionesJson) {

            String id = valoracionJson.getId();
            String metodo = valoracionJson.getMethod();

            HashMap<String, Object> parYaud = new HashMap<String, Object>();
            parYaud.put("idUser", idUser);
            parYaud.put("idSoc", idSoc);
            parYaud.put("idPrf", idPrf);
            parYaud.put("idSec", idSec);

            parYaud.put("id", valoracionJson.getId());
            parYaud.put("estado", valoracionJson.getEstado());
            parYaud.put("method", valoracionJson.getMethod());
            parYaud.put("fpAct", valoracionJson.getFechaPrevistaActivacion());
            parYaud.put("fpDes", valoracionJson.getFechaPrevistaDesactivacion());

            SociedadRpl sociedadOpinanteRpl = null;
            SociedadRpl sociedadOpinadaRpl = null;
            Valoraciones valoraciones = null;

            if (("0").equals(id) && ("POST").equals(metodo) || !("0").equals(id) && !("POST").equals(metodo)) {

                Object[] aaa = new Object[4];
                if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {

                    sociedadOpinanteRpl = sociedadRplService.findSociedadRpl(idSoc);
                    if (sociedadOpinanteRpl == null) {
                        Object[] valores = {idSoc};
                        String mensaje = MessageFormat.format("ERROR: Sociedad Opinante {0}, no encontrada. ", valores);
                        throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                    }

                    sociedadOpinadaRpl = sociedadRplService.findSociedadRpl(valoracionJson.getSociedadOpinadaId());
                    if (sociedadOpinadaRpl == null) {
                        Object[] valores = {idSoc};
                        String mensaje = MessageFormat.format("ERROR: Sociedad Opinada {0}, no encontrada. ", valores);
                        throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                    }

                    valoraciones = montaValoraciones(valoracionJson, idUser, sociedadOpinanteRpl, sociedadOpinadaRpl);
                    valoracionJson.setId(valoraciones.getIdValoracion());  // Para busqueda al final del método y presentación de la persistencia
                    aaa[2] = valoraciones;
                    aaa[3] = null;
                } else if (KApp.DELETE.getKApp().equals(metodo)) {
                    aaa[2] = null;
                    aaa[3] = valoracionJson.getId();
                    parYaud.put("method", KApp.DELETE.getKApp());
                }
                aaa[0] = metodo;
                aaa[1] = "valoraciones";
                listaObjetos.add(aaa);

            } else {
                Object[] valores = {""};
                String mensaje = MessageFormat.format("ERROR: CRUD valoraciones. Id y métodos no están sincronizaods {0}", valores);
                throw new Exception(MessageFormat.format(mensaje, new Throwable()));

            }
        }

        valoracionRestService.persistencia(listaObjetos);

        for (ValoracionJson valoracionJson : valoracionesJson) {
            Valoraciones valoracion = valoracionService.findValoraciones(valoracionJson.getId());
            if(valoracion!=null){
                reply.add(toJson(valoracion));
            }
        }

        return reply;
    }

    @RequestMapping(value = "/valoracion/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera un Comentarios", notes = "ComentarioJson, formato de comunicacion con cliente")
    List<ValoracionJson> getValoracion(
            @PathVariable String id,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        
        List<ValoracionJson> reply = new ArrayList();

        List<Valoraciones> valoraciones = valoracionService.findValoracionesBy_idS(id);

        for (Valoraciones valoracion : valoraciones) {
            reply.add(toJson(valoracion));
        }

        return reply;
    }

    private Valoraciones montaValoraciones(ValoracionJson valoracionJson, String idUser, SociedadRpl sociedadOpinanteRpl, SociedadRpl sociedadOpinadaRpl) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");
                
        Valoraciones valoracion = null;

        if (("POST").equals(valoracionJson.getMethod())) {
            valoracion = new Valoraciones();
            valoracion.setIdValoracion(UUID.randomUUID().toString().replaceAll("-", ""));
            valoracion.setAtributosAuditoria(new DatosAuditoria());
            valoracion.getAtributosAuditoria().setCreated(new Date());
        } else {  // Modificacion o Delete
            valoracion = valoracionService.findValoraciones(valoracionJson.getId());
            valoracion.getAtributosAuditoria().setUpdated(new Date());
        }

        if (valoracion != null) {
            valoracion.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            valoracion.getAtributosAuditoria().setFechaPrevistaActivacion(valoracionJson.getFechaPrevistaActivacion());
            valoracion.getAtributosAuditoria().setFechaPrevistaDesactivacion(valoracionJson.getFechaPrevistaDesactivacion());
            valoracion.getAtributosAuditoria().setEstadoRegistro(valoracionJson.getEstado());
            valoracion.getAtributosAuditoria().setUltimaAccion(valoracionJson.getMethod());

            valoracion.setAprobacion(valoracionJson.getAprobacion());
            valoracion.setValAtencion(valoracionJson.getValAtencion());
            valoracion.setValDisponibilidad(valoracionJson.getValDisponibilidad());
            valoracion.setValEntrega(valoracionJson.getValEntrega());
            valoracion.setValRelCalidadPrecio(valoracionJson.getValRelCalidadPrecio());
            valoracion.setValServicio(valoracionJson.getValServicio());

            valoracion.setSociedadOpinante(sociedadOpinanteRpl);
            valoracion.setSociedadOpinada(sociedadOpinadaRpl);

        }

        return valoracion;
    }

    private ValoracionJson toJson(Valoraciones valoracion) {
        log.info("....................................................................mbm............CONTROL");
                

        ValoracionJson reply = new ValoracionJson();

        reply.setId(valoracion.getIdValoracion());
        reply.setAprobacion(valoracion.getAprobacion());
        reply.setSociedadOpinadaId(valoracion.getSociedadOpinada().getIdSociedad());
        reply.setSociedadOpinanteId(valoracion.getSociedadOpinante().getIdSociedad());

        reply.setUsuarioOpinanteId(valoracion.getAtributosAuditoria().getUsuarioUltimaAccion());
        reply.setValAtencion(valoracion.getValAtencion());
        reply.setValDisponibilidad(valoracion.getValDisponibilidad());
        reply.setValEntrega(valoracion.getValEntrega());
        reply.setValRelCalidadPrecio(valoracion.getValRelCalidadPrecio());
        reply.setValServicio(valoracion.getValServicio());

        reply.setEstado(valoracion.getAtributosAuditoria().getEstadoRegistro());
        reply.setMethod(valoracion.getAtributosAuditoria().getUltimaAccion());
        reply.setFechaPrevistaActivacion(valoracion.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(valoracion.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setUsuarioOpinanteId(valoracion.getAtributosAuditoria().getUsuarioUltimaAccion());
        return reply;
    }

}
