package com.centraldecompras.rest.ven.json;

import java.math.BigDecimal;

public class ComentarioJson {
    
        private String id;
        private String sociedadOpinanteId;      // SociedadId que emite la comentario. No necesario por recoger del header
        private String usuarioOpinanteId;       // UsuarioId que emite la comentario.No necesario por recoger del haeder
        private String articuloOpinadoId; 
        private String sociedadOpinadaId; 
        private String comentario; 
        private String aprobacion;              // PT=Pendiente, AP=Aprobado, NA=No aprobado
        private BigDecimal relCalidadPrecio;    // Input es Entero entre 1 y 10, Para articulo
        private BigDecimal valoracion;          // Input es  Entero entre 1 y 10, Para articulo
        
        private String estado; 
        private String method; 
        private int fechaPrevistaActivacion; 
        private int fechaPrevistaDesactivacion; 

    public ComentarioJson() {
    }

    public ComentarioJson(String id, String sociedadOpinanteId, String usuarioOpinanteId, String articuloOpinadoId, String sociedadOpinadaId, String comentario, String aprobacion, BigDecimal relCalidadPrecio, BigDecimal valoracion, String estado, String method, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion) {
        this.id = id;
        this.sociedadOpinanteId = sociedadOpinanteId;
        this.usuarioOpinanteId = usuarioOpinanteId;
        this.articuloOpinadoId = articuloOpinadoId;
        this.sociedadOpinadaId = sociedadOpinadaId;
        this.comentario = comentario;
        this.aprobacion = aprobacion;
        this.relCalidadPrecio = relCalidadPrecio;
        this.valoracion = valoracion;
        this.estado = estado;
        this.method = method;
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSociedadOpinanteId() {
        return sociedadOpinanteId;
    }

    public void setSociedadOpinanteId(String sociedadOpinanteId) {
        this.sociedadOpinanteId = sociedadOpinanteId;
    }

    public String getUsuarioOpinanteId() {
        return usuarioOpinanteId;
    }

    public void setUsuarioOpinanteId(String usuarioOpinanteId) {
        this.usuarioOpinanteId = usuarioOpinanteId;
    }

    public String getArticuloOpinadoId() {
        return articuloOpinadoId;
    }

    public void setArticuloOpinadoId(String articuloOpinadoId) {
        this.articuloOpinadoId = articuloOpinadoId;
    }

    public String getSociedadOpinadaId() {
        return sociedadOpinadaId;
    }

    public void setSociedadOpinadaId(String sociedadOpinadaId) {
        this.sociedadOpinadaId = sociedadOpinadaId;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(String aprobacion) {
        this.aprobacion = aprobacion;
    }

    public BigDecimal getRelCalidadPrecio() {
        return relCalidadPrecio;
    }

    public void setRelCalidadPrecio(BigDecimal relCalidadPrecio) {
        this.relCalidadPrecio = relCalidadPrecio;
    }

    public BigDecimal getValoracion() {
        return valoracion;
    }

    public void setValoracion(BigDecimal valoracion) {
        this.valoracion = valoracion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

}
