/* Copyright 2014, Javier Moreno.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 * And have fun ;-)
 */
package com.centraldecompras.rest.ven.json.auxi;

public class ConDatFraJson {

    private String id;
    private String descripcion;
    private String estado;
    private String method;
    private int fechaPrevistaActivacion;
    private int fechaPrevistaDesactivacion;

    public ConDatFraJson() {
    }

    public ConDatFraJson(String id, String descripcion, String estado, String method, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion) {
        this.id = id;
        this.descripcion = descripcion;
        this.estado = estado;
        this.method = method;
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

}
