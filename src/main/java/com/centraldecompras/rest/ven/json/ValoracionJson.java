package com.centraldecompras.rest.ven.json;

import java.math.BigDecimal;

public class ValoracionJson {

    private String id;
    private String sociedadOpinanteId;      // SociedadId que emite la opinion. No necesario por recoger del header
    private String usuarioOpinanteId;       // UsuarioId que emite la opinion.No necesario por recoger del haeder
    private String sociedadOpinadaId;

    private BigDecimal valServicio;
    private BigDecimal valEntrega;
    private BigDecimal valDisponibilidad;
    private BigDecimal valRelCalidadPrecio;
    private BigDecimal valAtencion;
    private String aprobacion;

    private String estado;
    private String method;
    private int fechaPrevistaActivacion;
    private int fechaPrevistaDesactivacion;

    public ValoracionJson() {
    }

    public ValoracionJson(String id, String sociedadOpinanteId, String usuarioOpinanteId, String sociedadOpinadaId, BigDecimal valServicio, BigDecimal valEntrega, BigDecimal valDisponibilidad, BigDecimal valRelCalidadPrecio, BigDecimal valAtencion, String aprobacion, String estado, String method, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion) {
        this.id = id;
        this.sociedadOpinanteId = sociedadOpinanteId;
        this.usuarioOpinanteId = usuarioOpinanteId;
        this.sociedadOpinadaId = sociedadOpinadaId;
        this.valServicio = valServicio;
        this.valEntrega = valEntrega;
        this.valDisponibilidad = valDisponibilidad;
        this.valRelCalidadPrecio = valRelCalidadPrecio;
        this.valAtencion = valAtencion;
        this.aprobacion = aprobacion;
        this.estado = estado;
        this.method = method;
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSociedadOpinanteId() {
        return sociedadOpinanteId;
    }

    public void setSociedadOpinanteId(String sociedadOpinanteId) {
        this.sociedadOpinanteId = sociedadOpinanteId;
    }

    public String getUsuarioOpinanteId() {
        return usuarioOpinanteId;
    }

    public void setUsuarioOpinanteId(String usuarioOpinanteId) {
        this.usuarioOpinanteId = usuarioOpinanteId;
    }

    public String getSociedadOpinadaId() {
        return sociedadOpinadaId;
    }

    public void setSociedadOpinadaId(String sociedadOpinadaId) {
        this.sociedadOpinadaId = sociedadOpinadaId;
    }

    public BigDecimal getValServicio() {
        return valServicio;
    }

    public void setValServicio(BigDecimal valServicio) {
        this.valServicio = valServicio;
    }

    public BigDecimal getValEntrega() {
        return valEntrega;
    }

    public void setValEntrega(BigDecimal valEntrega) {
        this.valEntrega = valEntrega;
    }

    public BigDecimal getValDisponibilidad() {
        return valDisponibilidad;
    }

    public void setValDisponibilidad(BigDecimal valDisponibilidad) {
        this.valDisponibilidad = valDisponibilidad;
    }

    public BigDecimal getValRelCalidadPrecio() {
        return valRelCalidadPrecio;
    }

    public void setValRelCalidadPrecio(BigDecimal valRelCalidadPrecio) {
        this.valRelCalidadPrecio = valRelCalidadPrecio;
    }

    public BigDecimal getValAtencion() {
        return valAtencion;
    }

    public void setValAtencion(BigDecimal valAtencion) {
        this.valAtencion = valAtencion;
    }

    public String getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(String aprobacion) {
        this.aprobacion = aprobacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    
}
