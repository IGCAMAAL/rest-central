package com.centraldecompras.rest.lgt.ser;

import com.centraldecompras.rest.lgt.ser.interfaces.DatosAuditoriaRestService;
import com.centraldecompras.rest.lgt.ser.interfaces.ContactoRestService;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.cmo.service.interfaces.ContactoService;
import com.centraldecompras.rest.fin.json.ContactoJson;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContactoRestServiceImpl implements ContactoRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ContactoRestServiceImpl.class.getName());  

    @Autowired
    private ContactoService contactoService;

    @Autowired
    private DatosAuditoriaRestService datosAuditoriaRestService;

    @Transactional
    @Override
    public List<Object[]> montaContactos(SociedadRpl sociedad, Persona persona, Almacen almacen, List<ContactoJson> contactosJson, HashMap<String, Object> parYaud) {
        log.info("....................................................................mbm............CONTROL");
                
        List<Object[]> reply = new ArrayList<>();

        Contacto contacto = null;
        for (ContactoJson contactoJson : contactosJson) {
            String metodo = null;
            Object[] contacto_Array = new Object[4]; 

            if (KApp.DELETE.getKApp().equals(parYaud.get("method"))) {
                metodo = KApp.DELETE.getKApp();
            } else {
                metodo = contactoJson.getMethod();
            }

            if (KApp.POST.getKApp().equals(metodo)) {
                contacto = new Contacto();
                contacto.setIdContacto(UUID.randomUUID().toString().replaceAll("-", ""));
            } else if (KApp.PUT.getKApp().equals(metodo)) {
                contacto = contactoService.findContacto(contactoJson.getId());
            }

            if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
                contacto.setSociedad(sociedad);
                contacto.setPersona(persona);
                contacto.setAlmacen(almacen);
                contacto.setTipo(contactoJson.getTipo());
                contacto.setValor(contactoJson.getValor());
                contacto.setDescripcion(contactoJson.getDescripcion());
                contacto.setNombre(contactoJson.getNombre());
                contacto.setVisibleProve(contactoJson.getVisibleProve());
                contacto.setVisibleAsoc(contactoJson.getVisibleAsoc());
                contacto.setCargo(contactoJson.getCargo());

                parYaud.put("method", metodo);
                contacto.setAtributosAuditoria(datosAuditoriaRestService.cargaAtributosAuditoria(parYaud));
                contacto_Array[2] = contacto;
                contacto_Array[3] = null;
            } else if (KApp.DELETE.getKApp().equals(metodo)) {
                contacto_Array[2] = null;
                contacto_Array[3] = contactoJson.getId();
            }

            if (!KApp.DUMMY_METHOD.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
                contacto_Array[0] = metodo;
                contacto_Array[1] = "contacto";
                reply.add(contacto_Array);
            }
        }

        return reply;
    }

    public List<ContactoJson> recuperaContactos(SociedadRpl sociedad, Persona persona, Almacen almacen) {
        log.info("....................................................................mbm............CONTROL");
                
        List<Contacto> contactos = null;
        
        if (sociedad != null) {
            contactos = contactoService.findContactosBy_S(sociedad);
        } else if (persona != null) {
            contactos = contactoService.findContactosBy_P(persona);
        } else if (almacen != null) {
            contactos = contactoService.findContactosBy_A(almacen);
        }
        
        if(contactos==null){
            contactos = new ArrayList<>();
        }
        
        List<ContactoJson> reply = new ArrayList();
        for (Contacto contacto : contactos) {
            ContactoJson contactoJson = new ContactoJson();
            contactoJson.setId(contacto.getIdContacto());
            contactoJson.setEstado(contacto.getAtributosAuditoria().getEstadoRegistro());
            contactoJson.setMethod(contacto.getAtributosAuditoria().getUltimaAccion());
            contactoJson.setDescripcion(contacto.getDescripcion());
            contactoJson.setTipo(contacto.getTipo());
            contactoJson.setValor(contacto.getValor());
            contactoJson.setNombre(contacto.getNombre());
            contactoJson.setCargo(contacto.getCargo());
            contactoJson.setVisibleProve(contacto.getVisibleProve());
            contactoJson.setVisibleAsoc(contacto.getVisibleAsoc());

            reply.add(contactoJson);
        }

        return reply;
    }

}
