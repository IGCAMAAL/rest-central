package com.centraldecompras.rest.lgt.ser;

import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SocProductoAvatar;
import com.centraldecompras.modelo.SocProductoExcl;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.SocZonGeoExcl;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoExcludesService;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoService;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonaGeograficaService;
import com.centraldecompras.rest.lgt.json.ZonaProductoAsigJson;
import com.centraldecompras.rest.lgt.ser.interfaces.SocZonGeoRestService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SocZonGeoRestServiceImpl implements SocZonGeoRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocZonGeoRestServiceImpl.class.getName());

    @Autowired
    private SocZonGeoService socZonGeoService;

    @Autowired    
    private ZonaGeograficaService zonaGeograficaService;

    @Autowired        
    private SocZonGeoExcludesService socZonGeoExcludesService;
    
    public List<Object[]> montaZonasGeoAsigadasASociedad(List<String> socDateIn, SociedadRpl sociedadRpl, String idUser, String idSoc, String idPrf, String idSec) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Object[]> reply = new ArrayList();
        String metodo = null;

        List<SocZonGeo> socZonGeos = new ArrayList<>();

        if (socDateIn.get(0) != null && !"".equals(socDateIn.get(0))) {
            int fechaActivacion = 0;
            int fechaDesactivacion = 0;

            try {
                fechaActivacion = Integer.parseInt(socDateIn.get(1));
                fechaDesactivacion = Integer.parseInt(socDateIn.get(2));
            } catch (Exception e) {
                throw new Exception("Error en las fechas de activacion y desactivacion");
            }

            try {
                socZonGeos = socZonGeoService.findSocZonGeoBy_idS(socDateIn.get(0));
            } catch (Exception e) {
                throw new Exception("Error al leer la sociedad destino " + socDateIn.get(0));
            }

            SocZonGeo socZonGeo = null;

            for (SocZonGeo socZonGeoOrigen : socZonGeos) {
                Object[] socZonGeo_Array = new Object[4]; 

                socZonGeo = socZonGeoService.findSocZonGeoBy_idZidS(socZonGeoOrigen.getZonaGeograficaid(), sociedadRpl.getIdSociedad());
                if (socZonGeo == null) {
                    socZonGeo = new SocZonGeo();
                    socZonGeo.setIdSocZonGeo(UUID.randomUUID().toString().replaceAll("-", ""));
                    socZonGeo.setSociedad(sociedadRpl);
                    socZonGeo.setSociedadid(sociedadRpl.getIdSociedad());
                    socZonGeo.setZonaGeografica(socZonGeoOrigen.getZonaGeografica());
                    socZonGeo.setZonaGeograficaid(socZonGeoOrigen.getZonaGeograficaid());
                    socZonGeo.setAtributosAuditoria(new DatosAuditoria());
                    metodo = KApp.POST.getKApp();
                } else {
                    metodo = KApp.PUT.getKApp();
                }

                socZonGeo.setAsignada(socZonGeoOrigen.getAsignada());
                socZonGeo.getAtributosAuditoria().setCreated(new Date());
                socZonGeo.getAtributosAuditoria().setUpdated(null);
                socZonGeo.getAtributosAuditoria().setDeleted(null);
                socZonGeo.getAtributosAuditoria().setEstadoRegistro("AC");
                socZonGeo.getAtributosAuditoria().setFechaPrevistaActivacion(fechaActivacion);
                socZonGeo.getAtributosAuditoria().setFechaPrevistaDesactivacion(fechaDesactivacion);
                socZonGeo.getAtributosAuditoria().setUltimaAccion(ElementEnum.KApp.POST.getKApp());
                socZonGeo.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

                socZonGeo_Array[0] = metodo;
                socZonGeo_Array[1] = "socZonGeo";
                socZonGeo_Array[2] = socZonGeo;
                socZonGeo_Array[3] = null;

                reply.add(socZonGeo_Array);

            }

        }
        return reply;
    }

    public List<ZonaProductoAsigJson> recuperaZonasGeoAsignadasASociedad(String sociedadId) {
        log.info("....................................................................mbm............CONTROL");

        List<ZonaProductoAsigJson> reply = new ArrayList();
        List<SocZonGeo> socZonGeos = socZonGeoService.findSocZonGeoBy_idS(sociedadId);
        for (SocZonGeo socZonGeo : socZonGeos) {
            reply.add(datosToJson(socZonGeo));
        }
        return reply;
    }

    private ZonaProductoAsigJson datosToJson(SocZonGeo socZonGeo) {
        log.info("....................................................................mbm............CONTROL");

        ZonaProductoAsigJson reply = new ZonaProductoAsigJson();
        reply = new ZonaProductoAsigJson();
        reply.setId(socZonGeo.getZonaGeograficaid());
        reply.setSociedadId(socZonGeo.getSociedadid());
        reply.setAsignada(socZonGeo.getAsignada());
        reply.setFechaPrevistaActivacion(socZonGeo.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(socZonGeo.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setMethod(socZonGeo.getAtributosAuditoria().getUltimaAccion());
        return reply;
    }

    
    @Transactional
    public void persistencia(List<Object> createdObjets, List<Object> updatedObjets, List<Object> deletdObjets) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Object createdObjet : createdObjets) {
            if (createdObjet instanceof SocZonGeo) {
                socZonGeoService.create((SocZonGeo) createdObjet);
            }
            if (createdObjet instanceof SocZonGeoExcl) {
                socZonGeoExcludesService.create((SocZonGeoExcl) createdObjet);
            }
        }

        for (Object updatedObjet : updatedObjets) {
            if (updatedObjet instanceof SocZonGeo) {
                socZonGeoService.edit((SocZonGeo) updatedObjet);
            }
            if (updatedObjet instanceof SocZonGeoExcl) {
                socZonGeoExcludesService.edit((SocZonGeoExcl) updatedObjet);
            }
        }

        Collections.reverse(deletdObjets);
        for (Object deletdObjet : deletdObjets) {
            if (deletdObjet instanceof SocZonGeo) {
                socZonGeoService.destroy(((SocZonGeo) deletdObjet).getIdSocZonGeo());
            }
            if (deletdObjet instanceof SocZonGeoExcl) {
                socZonGeoExcludesService.destroySocZonGeoExcludesBy_idZ_r(((SocZonGeoExcl) deletdObjet).getSociedadId(), ((SocZonGeoExcl) deletdObjet).getRoute());
            }
        }

    }


        
    public Map<String, Object> crdExcludes(SocZonGeo socZonGeo, String accion) throws Exception { 

        Map<String, Object> reply = new HashMap<>();

        List<SocZonGeoExcl> createExcludes = new ArrayList<>();
        List<SocZonGeoExcl> updateExcludes = new ArrayList<>();
        List<SocZonGeoExcl> deleteExcludes = new ArrayList<>();

        // La tabla SocProductoExcl contendrÃ¡ productos excluidos para una sociedad con la path completa hasta el Ãºltimo nivel.
        // PK para verificar existencia: sociedadId, y route: 
        //    CASOS:
        //    A) EXCLUSION: S-P. ExtracciÃ³n de todos los productos de Ãºltimo nivel que sean hijos de S-P con el atributo route (r) informado.
        //       1.- Hacer lo siguiente por cada hijo de Ãºlitmo nivel encontrado (p1, p2, p... pn):
        //          1a.- Verificar que la PK, (S-r) ya existe en la tabla.
        //          1b.- Cuando la PK NO existe, INSERTAR, e informar ademas productoId, nivInt del producto excluido
        //          1c.- Cuando la PK SI existe, informar en BBDD el productoId y nivInt del hijo (UPDATE cuando sea necesario)
        // 
        //    B) ASIGNACION: S-P. ExtracciÃ³n de todos los productos de Ãºltimo nivel que sean hijos de S-P con el atributo route (r) informado.
        //       1.- Hacer lo siguiente por cada hijo de Ãºlitmo nivel encontrado (p1, p2, p... pn):
        //          1a.- Verificar que la PK, (A-r) ya existe en la tabla.
        //          1b.- Cuando la PK NO existe, la asignaciÃ³n no tiene efectos sobre la tabla
        //          1c.- Cuando la PK SI existe, puede ocuarrir: 
        //               * Que productoId e intNiv asignado sea PADRE del Pk de la tabla --> No se toca porque estÃ¡ excluido con un hijo del asignado. 
        //               * Que productoId e intNiv asignado sea HIJO  del Pk de la tabla --> No se toca porque estÃ¡ excluido por estructura de arbol         
        //               * Que productoId e intNiv de tabla SI sea igual que productoId e intNiv asignado. Explorar arbol de padres.
        //                 - ExploraciÃ³n del arbol de padres. Cuando SI encuentra el primer padre excluido, UPDATE, informar en productoId e nivInt los valores del padre excluido
        //                 -                                   Cuando NO encuentra padre excluido, DELETE del registro.        
        // OK
        //    B) HERENCIA: S-P. Puedes aplicar la herencia a una EXCLUSION o a una ASIGNACION
        //       1.- Aplicar la herencia a una exclusion
        //          1a. Buscar exclusiones en el arbol de padres        
        //              **- Cuando SI encuentra exclusiÃ³n en el arbol de padres, actuar como una exclusion.
        //              **- Cuando NO encuentra exclusiÃ³n en el arbol de padres, revisa los hijos
        //                  
        //       2.- Aplicar la herencia a una asignaciÃ³n        
        //          1a. Buscar exclusiones en el arbol de padres        
        //              **- Cuando SI encuentra exclusiÃ³n en el arbol de padres, actuar como una asignaciÃ³n a productoId que tiene un padre con exclusiÃ³n.
        //              **- Cuando NO encuentra exclusiÃ³n en el arbol de padres, revis los hijoa
        //
        //
        // 
        String sociedadId = socZonGeo.getSociedad().getIdSociedad();
        List<ZonaGeografica> zonasGeograficas = zonaGeograficaService.findZonasGeograficasBy_idZ(socZonGeo.getZonaGeografica().getIdZonaGeografica());
        for (ZonaGeografica zonaGeografica : zonasGeograficas) {
            SocZonGeoExcl socZonGeoExcl = socZonGeoExcludesService.findSocZonGeoExcludesBy_idZ_r(sociedadId, zonaGeografica.getRoute());  
            Map<String, Object> aupeUD = new HashMap();

            if ("E".equals(accion)) {   // Exclusion
                if (socZonGeoExcl != null) {
                    // Inoformar productoId y nivInt del hijo en el registro: UPDATE del regsitro si es necesario
                    if (socZonGeo.getZonaGeografica().getNivelInt() > socZonGeoExcl.getNivelInt()) {
                        socZonGeoExcl.setNivelInt(socZonGeo.getZonaGeografica().getNivelInt());
                        socZonGeoExcl.setZonaGeograficaId(socZonGeo.getZonaGeografica().getIdZonaGeografica());
                        updateExcludes.add(socZonGeoExcl);
                    }
                } else {
                    // INSERT Registro de AlmacenUsuProdExcl
                    socZonGeoExcl = new SocZonGeoExcl();

                    socZonGeoExcl.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                    socZonGeoExcl.setSociedadId(socZonGeo.getSociedad().getIdSociedad());
                    socZonGeoExcl.setRoute(zonaGeografica.getRoute());
                    socZonGeoExcl.setZonaGeograficaId(zonaGeografica.getIdZonaGeografica());
                    socZonGeoExcl.setNivelInt(socZonGeo.getZonaGeografica().getNivelInt());

                    createExcludes.add(socZonGeoExcl);
                }

            } else if ("A".equals(accion)) {    // Asignacion
                if (socZonGeoExcl != null) {
                    if (socZonGeo.getZonaGeografica().getNivelInt() > socZonGeoExcl.getNivelInt()) {
                        buscaPadreExcluido(socZonGeo, socZonGeoExcl, aupeUD);

                        if (aupeUD.containsKey("aupeU")) {
                            updateExcludes.add((SocZonGeoExcl) aupeUD.get("aupeU"));
                        }
                        if (aupeUD.containsKey("aupeD")) {
                            deleteExcludes.add((SocZonGeoExcl) aupeUD.get("aupeD"));
                        }
                    }
                }

            } else if ("H".equals(accion)) {                // Herencia
                if (socZonGeoExcl != null) {
                    buscaPadreExcluido(socZonGeo, socZonGeoExcl, aupeUD);

                    if (aupeUD.containsKey("aupeU")) {
                        updateExcludes.add((SocZonGeoExcl) aupeUD.get("aupeU"));
                    }
                    if (aupeUD.containsKey("aupeD")) {
                        deleteExcludes.add((SocZonGeoExcl) aupeUD.get("aupeD"));
                    }
                }
            };
        }
        reply.put("createExcludes", createExcludes);
        reply.put("updateExcludes", updateExcludes);
        reply.put("deleteExcludes", deleteExcludes);
        return reply;
    }
 
    private void buscaPadreExcluido(SocZonGeo _socZonGeo, SocZonGeoExcl socZonGeoExcludes, Map<String, Object> aupeUD) throws Exception {

        List<Boolean> padreExcluidoBoolean = new ArrayList(); //true-- Tiene padre excluido, false==No tiene padre excluido 
        List<ZonaGeografica> padreExcluidoObject = new ArrayList();

        // ExploraciÃ³n de los padres
        // Si encuentra padre excluido --> informa almacenUsuarioProductoExcludes y reply = true
        // Si no encuentra padre excluido --> No informa almacenUsuarioProductoExcludes y reply = false 
        if (_socZonGeo.getZonaGeografica().getNivelInt() > 1) {
            recurrenteProductosPadre_Excluido(_socZonGeo, _socZonGeo.getZonaGeografica(), padreExcluidoBoolean, padreExcluidoObject);
        }
        if (padreExcluidoBoolean.size() > 0) {
            // Inoformar productoId y nivInt del padte en el registro: UPDATE del regsitro si es necesario
            if (padreExcluidoObject.size() > 0) {
                socZonGeoExcludes.setNivelInt((padreExcluidoObject.get(0)).getNivelInt());
                socZonGeoExcludes.setZonaGeograficaId((padreExcluidoObject.get(0)).getIdZonaGeografica());
                aupeUD.put("aupeU", socZonGeoExcludes);

            } else {
                throw new Exception("Hay padre excluido pero no se envÃ­a objeto");
            }

        } else {
            // DELETE registro de AlmacenUsuProdExcl
            aupeUD.put("aupeD", socZonGeoExcludes);
        }
    } 

    private void recurrenteProductosPadre_Excluido(SocZonGeo _socZonGeo, ZonaGeografica _zonaGeograficaBuscado, List<Boolean> _padreExcluido, List<ZonaGeografica> _padreExcluidoObject) { 
        log.info("Buscamos.." + _zonaGeograficaBuscado.getIdPadreJerarquia());

        ZonaGeografica zonaGeograficaPadreBuscado = zonaGeograficaService.findZonaGeografica(_zonaGeograficaBuscado.getIdPadreJerarquia()); 

        if (zonaGeograficaPadreBuscado != null && zonaGeograficaPadreBuscado.getNivelInt() > 0) {
            SocZonGeo socZonGeo = socZonGeoService.findSocZonGeoBy_idS_idZ_Estado(
                    _socZonGeo.getSociedad().getIdSociedad(), 
                    zonaGeograficaPadreBuscado.getIdZonaGeografica(), 
                    ElementEnum.KApp.ACTIVO.getKApp());
            if (socZonGeo != null && false == socZonGeo.getAsignada()) {
                _padreExcluido.add(Boolean.TRUE);       /* ESTO ES LO QUE BUSCO Y ENCUENTRO: asignacion para almacen/ usuario / producto, que esta excluida (AC no asignado (false)) */
                _padreExcluidoObject.add(zonaGeograficaPadreBuscado);
            } else {
                recurrenteProductosPadre_Excluido(_socZonGeo, zonaGeograficaPadreBuscado, _padreExcluido, _padreExcluidoObject);
            }
        }
    }

}
