package com.centraldecompras.rest.lgt.ser;

import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.rest.lgt.ser.interfaces.AlmacenRestService;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.modelo.AlmacenUsuProdExcl;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.DireccionGoogleComponente;
import com.centraldecompras.modelo.DireccionGoogleType;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.ProductoLang;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.cmo.service.interfaces.ContactoService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleComponenteService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleTypeService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionService;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenService;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenUsuProdExcludesService;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenUsuarioProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.rest.lgt.json.AlmacenJson;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AlmacenRestServiceImpl implements AlmacenRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AlmacenRestServiceImpl.class.getName());

    @Autowired
    private AlmacenService almacenService;

    @Autowired
    private ContactoService contactoService;

    @Autowired
    private DireccionService direccionService;

    @Autowired
    private DireccionGoogleComponenteService direccionGoogleComponenteService;

    @Autowired
    private DireccionGoogleTypeService direccionGoogleTypeService;

    @Autowired
    private AlmacenUsuarioProductoService almacenUsuarioProductoService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private ProductoService productoService;

    @Autowired
    private AlmacenUsuProdExcludesService almacenUsuarioProductoExcludesService;

    @Transactional
    public void persistenciaAlmacen(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("almacen").equals((String) bbb[1])) {
                    almacenService.create((Almacen) bbb[2]);
                }

                if (("direccion").equals((String) bbb[1])) {
                    direccionService.create((Direccion) bbb[2]);
                }
                if (("direccionGoogleComponente").equals((String) bbb[1])) {
                    direccionGoogleComponenteService.create((DireccionGoogleComponente) bbb[2]);
                }

                if (("contacto").equals((String) bbb[1])) {
                    contactoService.create((Contacto) bbb[2]);
                }

            }

            if (("PUT").equals((String) bbb[0])) {
                if (("almacen").equals((String) bbb[1])) {
                    almacenService.edit((Almacen) bbb[2]);
                }

                if (("direccion").equals((String) bbb[1])) {
                    Direccion dir = (Direccion) bbb[2];
                    direccionGoogleTypeService.destroyDireccionesGoogleTypeBy_DDC_B(dir.getIdDireccion(), null);
                    direccionService.edit((Direccion) bbb[2]);
                }
                if (("direccionGoogleComponente").equals((String) bbb[1])) {
                    DireccionGoogleComponente comp = (DireccionGoogleComponente) bbb[2];
                    direccionGoogleTypeService.destroyDireccionesGoogleTypeBy_DDC_B(null, comp.getIdDireccionGoogleComponente());
                    direccionGoogleComponenteService.edit((DireccionGoogleComponente) bbb[2]);
                }

                if (("contacto").equals((String) bbb[1])) {
                    contactoService.edit((Contacto) bbb[2]);
                }

            }

            if (("direccionGoogleType").equals((String) bbb[1])) {
                direccionGoogleTypeService.create((DireccionGoogleType) bbb[2]);
            }
        }

        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("almacen").equals((String) bbb[1])) {
                    almacenService.destroy((String) bbb[3]);
                }

                if (("direccion").equals((String) bbb[1])) {
                    direccionGoogleTypeService.destroyDireccionesGoogleTypeBy_DDC_B((String) bbb[3], null);
                    direccionService.destroy((String) bbb[3]);
                }
                if (("direccionGoogleComponente").equals((String) bbb[1])) {
                    direccionGoogleTypeService.destroyDireccionesGoogleTypeBy_DDC_B(null, (String) bbb[3]);
                    direccionGoogleComponenteService.destroy((String) bbb[3]);
                }

                if (("contacto").equals((String) bbb[1])) {
                    contactoService.destroy((String) bbb[3]);
                }

            }
        }
    }

    @Transactional
    public void persistencia(List<Object> createdObjects, List<Object> updatedObjects, List<Object> deletdObjets) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Object object : createdObjects) {
            if (object instanceof AlmacenUsuarioProducto) {
                almacenUsuarioProductoService.create((AlmacenUsuarioProducto) object);
            }
            if (object instanceof AlmacenUsuProdExcl) {
                almacenUsuarioProductoExcludesService.create((AlmacenUsuProdExcl) object);
            }
        }

        for (Object object : updatedObjects) {
            if (object instanceof AlmacenUsuarioProducto) {
                almacenUsuarioProductoService.edit((AlmacenUsuarioProducto) object);
            }
            if (object instanceof AlmacenUsuProdExcl) {
                almacenUsuarioProductoExcludesService.edit((AlmacenUsuProdExcl) object);
            }
        }

        for (Object object : deletdObjets) {
            if (object instanceof AlmacenUsuProdExcl) {
                String almacenId = ((AlmacenUsuProdExcl) object).getAlmacenId();
                String usuarioId = ((AlmacenUsuProdExcl) object).getUsuarioId();
                String route = ((AlmacenUsuProdExcl) object).getRoute();

                almacenUsuarioProductoExcludesService.destroyAlmacenUsuarioProductoExcludesBy_idA_idU_r(almacenId, usuarioId, route);
            }
        }

    }

    public Almacen montaAlmacen(AlmacenJson almacenJson, String idUser, ZonaGeografica zonaGeografica, SociedadRpl sociedadRpl) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");

        Almacen almacen = new Almacen();

        if (("POST").equals(almacenJson.getMethod())) {
            almacen = new Almacen();
            almacen.setIdAlmacen(UUID.randomUUID().toString().replaceAll("-", ""));
            almacen.setAtributosAuditoria(new DatosAuditoria());
            almacen.getAtributosAuditoria().setCreated(new Date());
        } else {  // Modificacion o Delete
            almacen = almacenService.findAlmacen(almacenJson.getId());
            almacen.getAtributosAuditoria().setUpdated(new Date());
        }

        if (almacen != null) {
            almacen.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            almacen.getAtributosAuditoria().setFechaPrevistaActivacion(almacenJson.getFechaPrevistaActivacion());
            almacen.getAtributosAuditoria().setFechaPrevistaDesactivacion(almacenJson.getFechaPrevistaDesactivacion());
            almacen.getAtributosAuditoria().setEstadoRegistro(almacenJson.getEstado());
            almacen.getAtributosAuditoria().setUltimaAccion(almacenJson.getMethod());

            almacen.setNombreAlmacen(almacenJson.getNombreAlmacen());
            almacen.setTipoAlmacen(almacenJson.getTipoAlmacen());
            almacen.setZonaGeografica(zonaGeografica);
            almacen.setDescripcionAlmacen(almacenJson.getDescripcion());
            almacen.setSociedad(sociedadRpl);

        }

        return almacen;
    }

    public AlmacenUsuarioProducto montaAlmacenUsuarioProducto(Map<String, Object> almacenUsuarioProductoJsonEle) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        AlmacenUsuarioProducto almacenUsuarioProducto = null;

        String tipoReg = null;

        Almacen almacen = null;
        Persona usuario = null;
        Producto producto = null;
        String method = null;
        Boolean asignada = null;
        int fechaPrevistaActivacion = 0;
        int fechaPrevistaDesactivacion = 0;
        Map<String, Object> configuracion = null;
        Boolean verPrecio = null;
        String modoCompra = null;

        tipoReg = (String) almacenUsuarioProductoJsonEle.get("tipoReg");

        almacen = almacenService.findAlmacen((String) almacenUsuarioProductoJsonEle.get("almacenId"));
        if (almacen == null) {
            throw new Exception("Almacen no encontado con id: " + (String) almacenUsuarioProductoJsonEle.get("almacenId"));
        }

        if (tipoReg.equals("AU") || tipoReg.equals("AUP")) {
            usuario = personaService.findPersona((String) almacenUsuarioProductoJsonEle.get("usuarioId"));
        } else {
            usuario = personaService.findPersona(KApp.DUMMY.getKApp());
        }
        if (usuario == null) {
            throw new Exception("Usuario no encontado con id: " + (String) almacenUsuarioProductoJsonEle.get("usuarioId"));
        }

        if (tipoReg.equals("AP") || tipoReg.equals("AUP")) {
            producto = productoService.findProducto((String) almacenUsuarioProductoJsonEle.get("productoId"));
        } else {
            producto = productoService.findProducto(KApp.DUMMY.getKApp());
        }
        if (producto == null) {
            throw new Exception("Producto no encontado con id: " + (String) almacenUsuarioProductoJsonEle.get("producto"));
        }

        if (almacenUsuarioProductoJsonEle.containsKey("method")) {
            method = (String) almacenUsuarioProductoJsonEle.get("method");
        }

        if (almacenUsuarioProductoJsonEle.containsKey("asignada")) {
            asignada = (Boolean) almacenUsuarioProductoJsonEle.get("asignada");
        }

        if (almacenUsuarioProductoJsonEle.containsKey("fechaPrevistaActivacion")) {
            fechaPrevistaActivacion = (int) almacenUsuarioProductoJsonEle.get("fechaPrevistaActivacion");
        }

        if (almacenUsuarioProductoJsonEle.containsKey("fechaPrevistaDesactivacion")) {
            fechaPrevistaDesactivacion = (int) almacenUsuarioProductoJsonEle.get("fechaPrevistaDesactivacion");
        }

        if (almacenUsuarioProductoJsonEle.containsKey("configuracion")) {
            configuracion = (Map<String, Object>) almacenUsuarioProductoJsonEle.get("configuracion");
            if (configuracion.containsKey("verPrecio")) {
                verPrecio = (Boolean) configuracion.get("verPrecio");
            }
            if (configuracion.containsKey("verPrecio")) {
                modoCompra = (String) configuracion.get("modoCompra");
            }
        }

        try {
            almacenUsuarioProducto = almacenUsuarioProductoService.findAlmacenUsuarioProductoBy_AUP(almacen, usuario, producto);
        } catch (Exception ex) {
            log.info(ex.getMessage() + ".............No se ha encontrado la combinaciÃ³n Almacen / Persona / Producto  dada:  " + almacen.getNombreAlmacen() + " / " + usuario.getNombre() + " / " + producto.getDescripcion());
        }

        if (almacenUsuarioProducto == null) {
            almacenUsuarioProducto = new AlmacenUsuarioProducto();

            almacenUsuarioProducto.setAlmacen(almacen);
            almacenUsuarioProducto.setUsuario(usuario);
            almacenUsuarioProducto.setProducto(producto);
            almacenUsuarioProducto.setTipoReg(tipoReg);

            almacenUsuarioProducto.setAtributosAuditoria(new DatosAuditoria());
            almacenUsuarioProducto.getAtributosAuditoria().setCreated(new Date());
            almacenUsuarioProducto.getAtributosAuditoria().setFechaPrevistaActivacion(fechaPrevistaActivacion);
            almacenUsuarioProducto.getAtributosAuditoria().setFechaPrevistaDesactivacion(fechaPrevistaDesactivacion);
            almacenUsuarioProducto.getAtributosAuditoria().setDeleted(null);
            almacenUsuarioProducto.getAtributosAuditoria().setUpdated(null);

        } else {
            if ("POST".equals(method)) {
                almacenUsuarioProductoJsonEle.put("method", KApp.PUT.getKApp());
            }
        }

        if (!"DELETE".equals(method)) {
            // Asigna o Excluye
            almacenUsuarioProducto.getAtributosAuditoria().setEstadoRegistro(KApp.ACTIVO.getKApp());
            almacenUsuarioProducto.getAtributosAuditoria().setUpdated(new Date());
        } else {
            // Herencia
            almacenUsuarioProducto.getAtributosAuditoria().setEstadoRegistro(KApp.DESACTIVADO.getKApp());
            almacenUsuarioProducto.getAtributosAuditoria().setDeleted(new Date());
        }

        almacenUsuarioProducto.setModoCompra(modoCompra);
        almacenUsuarioProducto.setVerPrecio(verPrecio);
        almacenUsuarioProducto.setAsignada(asignada);
        almacenUsuarioProducto.getAtributosAuditoria().setUltimaAccion(method);
        almacenUsuarioProducto.getAtributosAuditoria().setUsuarioUltimaAccion((String) almacenUsuarioProductoJsonEle.get("idUser"));

        return almacenUsuarioProducto;
    }

    @Override
    public Map<String, Object> crdExcludes(AlmacenUsuarioProducto almacenUsuarioProducto, String accion) throws Exception {

        Map<String, Object> reply = new HashMap<>();

        List<AlmacenUsuProdExcl> createExcludes = new ArrayList<>();
        List<AlmacenUsuProdExcl> updateExcludes = new ArrayList<>();
        List<AlmacenUsuProdExcl> deleteExcludes = new ArrayList<>();

        // La tabla AlmacenUsuProdExcl contendrÃ¡ productos de excluidos para un almacen / usuario con la path completa hasta el Ãºltimo nivel.
        // PK para verificar existencia: almacenId, usuarioId y route: 
        //    CASOS:
        //    A) EXCLUSION: A-U-P. ExtracciÃ³n de todos los productos de Ãºltimo nivel que sean hijos de A-U-P con el atributo route (r) informado.
        //       1.- Hacer lo siguiente por cada hijo de Ãºlitmo nivel encontrado (p1, p2, p... pn):
        //          1a.- Verificar que la PK, (A-U-r) ya existe en la tabla.
        //          1b.- Cuando la PK NO existe, INSERTAR, e informar ademas productoId, nivInt del producto excluido
        //          1c.- Cuando la PK SI existe, informar en BBDD el productoId y nivInt del hijo (UPDATE cuando sea necesario)
        // OK
        //    B) ASIGNACION: A-U-P. ExtracciÃ³n de todos los productos de Ãºltimo nivel que sean hijos de A-U-P con el atributo route (r) informado.
        //       1.- Hacer lo siguiente por cada hijo de Ãºlitmo nivel encontrado (p1, p2, p... pn):
        //          1a.- Verificar que la PK, (A-U-r) ya existe en la tabla.
        //          1b.- Cuando la PK NO existe, la asignaciÃ³n no tiene efectos sobre la tabla
        //          1c.- Cuando la PK SI existe, puede ocuarrir: 
        //               * Que productoId e intNiv asignado sea PADRE del Pk de la tabla --> No se toca porque estÃ¡ excluido con un hijo del asignado. 
        //               * Que productoId e intNiv asignado sea HIJO  del Pk de la tabla --> No se toca porque estÃ¡ excluido por estructura de arbol         
        //               * Que productoId e intNiv de tabla SI sea igual que productoId e intNiv asignado. Explorar arbol de padres.
        //                 - ExploraciÃ³n del arbol de padres. Cuando SI encuentra el primer padre excluido, UPDATE informar en productoId e nivInt los valores del padre excluido
        //                 -                                  Cuando NO encuentra padre excluido, DELETE del registro.        
        // OK
        //    B) HERENCIA: A-U-P. Puedes aplicar la herencia a una EXCLUSION o a una ASIGNACION
        //       1.- Aplicar la herencia a una exclusion
        //          1a. Buscar exclusiones en el arbol de padres        
        //              **- Cuando SI encuentra exclusiÃ³n en el arbol de padres, actuar como una exclusion.
        //              **- Cuando NO encuentra exclusiÃ³n en el arbol de padres, revisa los hijos
        //                  
        //       2.- Aplicar la herencia a una asignaciÃ³n        
        //          1a. Buscar exclusiones en el arbol de padres        
        //              **- Cuando SI encuentra exclusiÃ³n en el arbol de padres, actuar como una asignaciÃ³n a productoId que tiene un padre con exclusiÃ³n.
        //              **- Cuando NO encuentra exclusiÃ³n en el arbol de padres, revis los hijoa
        String almacenId = almacenUsuarioProducto.getAlmacen().getIdAlmacen();
        String usuarioId = almacenUsuarioProducto.getUsuario().getIdPersona();
        //SelecciÃ³n de routes que continen AlmacenUsuarioProducto que tienen el productoId en el root. Join entre Productos y AlmacenUsuarioProducto

        List<Producto> productos = productoService.findProductosBy_idP(almacenUsuarioProducto.getProducto().getIdProducto());

        for (Producto producto : productos) {
            AlmacenUsuProdExcl almacenUsuarioProductoExcludes = almacenUsuarioProductoExcludesService.findAlmacenUsuarioProductoExcludesBy_idA_idU_r(almacenId, usuarioId, producto.getRoute());
            Map<String, Object> aupeUD = new HashMap();

            if ("E".equals(accion)) {   // Exclusion
                if (almacenUsuarioProductoExcludes != null) {
                    // Inoformar productoId y nivInt del hijo en el registro: UPDATE del regsitro si es necesario
                    if (almacenUsuarioProducto.getProducto().getNivelInt() > almacenUsuarioProductoExcludes.getNivelInt()) {
                        almacenUsuarioProductoExcludes.setNivelInt(almacenUsuarioProducto.getProducto().getNivelInt());
                        almacenUsuarioProductoExcludes.setProductoId(almacenUsuarioProducto.getProducto().getIdProducto());
                        updateExcludes.add(almacenUsuarioProductoExcludes);
                    }
                } else {
                    // INSERT Registro de AlmacenUsuProdExcl
                    almacenUsuarioProductoExcludes = new AlmacenUsuProdExcl();

                    almacenUsuarioProductoExcludes.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                    almacenUsuarioProductoExcludes.setAlmacenId(almacenUsuarioProducto.getAlmacen().getIdAlmacen());
                    almacenUsuarioProductoExcludes.setUsuarioId(almacenUsuarioProducto.getUsuario().getIdPersona());
                    almacenUsuarioProductoExcludes.setRoute(producto.getRoute());
                    almacenUsuarioProductoExcludes.setProductoId(producto.getIdProducto());
                    almacenUsuarioProductoExcludes.setNivelInt(almacenUsuarioProducto.getProducto().getNivelInt());

                    createExcludes.add(almacenUsuarioProductoExcludes);
                }

            } else if ("A".equals(accion)) {    // Asignacion
                if (almacenUsuarioProductoExcludes != null) {
                    if (almacenUsuarioProducto.getProducto().getNivelInt() > almacenUsuarioProductoExcludes.getNivelInt()) {
                        buscaPadreExcluido(almacenUsuarioProducto, almacenUsuarioProductoExcludes, aupeUD);

                        if (aupeUD.containsKey("aupeU")) {
                            updateExcludes.add((AlmacenUsuProdExcl) aupeUD.get("aupeU"));
                        }
                        if (aupeUD.containsKey("aupeD")) {
                            deleteExcludes.add((AlmacenUsuProdExcl) aupeUD.get("aupeD"));
                        }
                    }
                }

            } else if ("H".equals(accion)) {                // Herencia
                if (almacenUsuarioProductoExcludes != null) {
                    buscaPadreExcluido(almacenUsuarioProducto, almacenUsuarioProductoExcludes, aupeUD);

                    if (aupeUD.containsKey("aupeU")) {
                        updateExcludes.add((AlmacenUsuProdExcl) aupeUD.get("aupeU"));
                    }
                    if (aupeUD.containsKey("aupeD")) {
                        deleteExcludes.add((AlmacenUsuProdExcl) aupeUD.get("aupeD"));
                    }
                }
            };
        }
        reply.put("createExcludes", createExcludes);
        reply.put("updateExcludes", updateExcludes);
        reply.put("deleteExcludes", deleteExcludes);
        return reply;
    }

    private void buscaPadreExcluido(
            AlmacenUsuarioProducto almacenUsuarioProducto,
            AlmacenUsuProdExcl almacenUsuarioProductoExcludes,
            Map<String, Object> aupeUD) throws Exception {

        List<Boolean> padreExcluidoBoolean = new ArrayList(); //true-- Tiene padre excluido, false==No tiene padre excluido 
        List<Producto> padreExcluidoObject = new ArrayList();

        // ExploraciÃ³n de los padres
        // Si encuentra padre excluido --> informa almacenUsuarioProductoExcludes y reply = true
        // Si no encuentra padre excluido --> No informa almacenUsuarioProductoExcludes y reply = false 
        if (almacenUsuarioProducto.getProducto().getNivelInt() > 1) {
            recurrenteProductosPadre_Excluido(almacenUsuarioProducto, almacenUsuarioProducto.getProducto(), padreExcluidoBoolean, padreExcluidoObject);
        }
        if (padreExcluidoBoolean.size() > 0) {
            // Inoformar productoId y nivInt del padte en el registro: UPDATE del regsitro si es necesario
            if (padreExcluidoObject.size() > 0) {
                almacenUsuarioProductoExcludes.setNivelInt((padreExcluidoObject.get(0)).getNivelInt());
                almacenUsuarioProductoExcludes.setProductoId((padreExcluidoObject.get(0)).getIdProducto());
                aupeUD.put("aupeU", almacenUsuarioProductoExcludes);

            } else {
                throw new Exception("Hay padre excluido pero no se envÃ­a objeto");
            }

        } else {
            // DELETE registro de AlmacenUsuProdExcl
            if (almacenUsuarioProducto.getProducto().getIdNivelProducto() >= almacenUsuarioProductoExcludes.getNivelInt()) {
                aupeUD.put("aupeD", almacenUsuarioProductoExcludes);
            }
        }
    }

    private void recurrenteProductosPadre_Excluido(AlmacenUsuarioProducto _almacenUsuarioProducto, Producto _productoBuscado, List<Boolean> _padreExcluido, List<Producto> _padreExcluidoObject) {
        log.info("Buscamos.." + _productoBuscado.getIdPadreJerarquia());

        Producto productoPadreBuscado = productoService.findProducto(_productoBuscado.getIdPadreJerarquia());

        if (productoPadreBuscado != null && productoPadreBuscado.getNivelInt() > 0) {
            AlmacenUsuarioProducto almacenUsuarioProducto = almacenUsuarioProductoService.findAlmacenUsuarioProductoBy_idA_idU_idP_Estado(_almacenUsuarioProducto.getAlmacen().getIdAlmacen(), _almacenUsuarioProducto.getUsuario().getIdPersona(), productoPadreBuscado.getIdProducto(), KApp.ACTIVO.getKApp());
            if (almacenUsuarioProducto != null && false == almacenUsuarioProducto.isAsignada()) {
                _padreExcluido.add(Boolean.TRUE);       /* ESTO ES LO QUE BUSCO Y ENCUENTRO: asignacion para almacen/ usuario / producto, que esta excluida (AC no asignado (false)) */

                _padreExcluidoObject.add(productoPadreBuscado);
            } else {
                recurrenteProductosPadre_Excluido(_almacenUsuarioProducto, productoPadreBuscado, _padreExcluido, _padreExcluidoObject);
            }
        }
    }

    public void recurrenteProductosPadre_Asignado(Almacen _almacen, UserCentral _usuario, Producto _productoBuscado, List<Boolean> _padreAsignado, List<Producto> _padreAsignadoObject) {
        log.info("Buscamos.." + _productoBuscado.getIdPadreJerarquia());

        Producto productoPadreBuscado = productoService.findProducto(_productoBuscado.getIdPadreJerarquia());

        if (productoPadreBuscado != null && productoPadreBuscado.getNivelInt() > 0) {
            AlmacenUsuarioProducto almacenUsuarioProducto = almacenUsuarioProductoService.findAlmacenUsuarioProductoBy_idA_idU_idP_Estado(_almacen.getIdAlmacen(), _usuario.getId(), productoPadreBuscado.getIdProducto(), KApp.ACTIVO.getKApp());
            if (almacenUsuarioProducto != null && true == almacenUsuarioProducto.isAsignada()) {
                _padreAsignado.add(Boolean.TRUE);                       /* ESTO ES LO QUE BUSCO Y ENCUENTRO: asignacion para almacen/ usuario / producto, que NO esta excluida  */

                _padreAsignadoObject.add(productoPadreBuscado);
            } else {
                recurrenteProductosPadre_Asignado(_almacen, _usuario, productoPadreBuscado, _padreAsignado, _padreAsignadoObject);
            }
        }
    }
}
