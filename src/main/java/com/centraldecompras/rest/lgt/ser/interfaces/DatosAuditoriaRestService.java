package com.centraldecompras.rest.lgt.ser.interfaces;

import com.centraldecompras.acceso.DatosAuditoriaAcceso;
import com.centraldecompras.modelo.DatosAuditoria;
import java.util.HashMap;

public interface DatosAuditoriaRestService{

    DatosAuditoria cargaAtributosAuditoria(HashMap parYaud);
    
    DatosAuditoriaAcceso cargaAtributosAuditoriaAcceso(HashMap parYaud);
}
