/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.lgt.ser.interfaces;

import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.rest.lgt.json.AlmacenJson;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.Map;

public interface AlmacenRestService {

    void persistenciaAlmacen(List<Object[]> listaObjetos) throws Exception;
    
    void persistencia(List<Object> createdObjects, List<Object> updatedObjects, List<Object> deletdObjets) throws Exception;

    Almacen montaAlmacen(AlmacenJson almacenJson, String idUser, ZonaGeografica zonaGeografica, SociedadRpl sociedadRpl) throws NonexistentEntityException, Exception;
    
    AlmacenUsuarioProducto montaAlmacenUsuarioProducto(Map<String, Object> almacenUsuarioProductoJsonEle) throws Exception;
            
    Map<String, Object> crdExcludes(AlmacenUsuarioProducto almacenUsuarioProducto, String accion)  throws Exception ;
 
    void recurrenteProductosPadre_Asignado(Almacen _almacen, UserCentral _usuario, Producto _productoBuscado, List<Boolean> _padreAsignado, List<Producto> _padreAsignadoObject);
}


