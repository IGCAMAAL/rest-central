package com.centraldecompras.rest.lgt.ser.interfaces;

import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.rest.fin.json.DireccionJson;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.HashMap;
import java.util.List;

public interface DireccionRestService {

    List<DireccionJson> recuperaDirecciones(SociedadRpl sociedadRpl, Persona persona, Almacen almacen);

    List<Object[]> montaDirecciones(SociedadRpl sociedadRpl, Persona persona, Almacen almacen, List<DireccionJson> direccionesJson, HashMap<String, Object> parYaud) throws NonexistentEntityException;
 
}
