package com.centraldecompras.rest.lgt.ser;

import com.centraldecompras.rest.lgt.ser.interfaces.DatosAuditoriaRestService;
import com.centraldecompras.acceso.DatosAuditoriaAcceso;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.zglobal.enums.ElementEnum;
import java.util.Date;
import java.util.HashMap;
import org.springframework.stereotype.Service;

@Service
public class DatosAuditoriaRestServiceImpl implements DatosAuditoriaRestService{

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DatosAuditoriaRestServiceImpl.class.getName()); 

    public DatosAuditoria cargaAtributosAuditoria(HashMap parYaud) {
        log.info("....................................................................mbm............CONTROL");
                
        DatosAuditoria reply = new DatosAuditoria();

        reply.setUsuarioUltimaAccion((String) parYaud.get("idUser"));
        reply.setEstadoRegistro((String) parYaud.get("estado"));
        reply.setFechaPrevistaActivacion((Integer) parYaud.get("fpAct"));
        reply.setFechaPrevistaDesactivacion((Integer) parYaud.get("fpDes"));
        reply.setUltimaAccion((String) parYaud.get("method"));
        if ((reply.getUltimaAccion().equals(ElementEnum.KApp.POST.getKApp()))) {
            reply.setCreated(new Date());
        } else {
            reply.setUpdated(new Date());
        }

        return reply;
    }

    public DatosAuditoriaAcceso cargaAtributosAuditoriaAcceso(HashMap parYaud) {
        DatosAuditoriaAcceso reply = new DatosAuditoriaAcceso();

        reply.setUsuarioUltimaAccion((String) parYaud.get("idUser"));
        reply.setEstadoRegistro((String) parYaud.get("estado"));
        reply.setFechaPrevistaActivacion((Integer) parYaud.get("fpAct"));
        reply.setFechaPrevistaDesactivacion((Integer) parYaud.get("fpDes"));
        reply.setUltimaAccion((String) parYaud.get("method"));
        if ((reply.getUltimaAccion().equals(ElementEnum.KApp.POST.getKApp()))) {
            reply.setCreated(new Date());
        } else {
            reply.setUpdated(new Date());
        }

        return reply;
    }


}
