package com.centraldecompras.rest.lgt.ser;

import com.centraldecompras.rest.lgt.ser.interfaces.DatosAuditoriaRestService;
import com.centraldecompras.rest.lgt.ser.interfaces.DireccionRestService;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.DireccionGoogleComponente;
import com.centraldecompras.modelo.DireccionGoogleType;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleComponenteService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleTypeService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionService;
import com.centraldecompras.rest.fin.json.ComponenteJson;
import com.centraldecompras.rest.fin.json.DireccionJson;
import com.centraldecompras.rest.fin.json.auxi.DireccionJsonOut;
import com.centraldecompras.rest.fin.json.LocationJson;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DireccionRestServiceImpl implements DireccionRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DireccionRestServiceImpl.class.getName()); 

    @Autowired
    private DireccionService direccionService;

    @Autowired
    private DireccionGoogleTypeService direccionGoogleTypeService;

    @Autowired
    private DireccionGoogleComponenteService direccionGoogleComponenteService;

    @Autowired
    private DatosAuditoriaRestService datosAuditoriaRestService;

    public List<Object[]> montaDirecciones(SociedadRpl sociedadRpl, Persona persona, Almacen almacen, List<DireccionJson> direccionesJson, HashMap<String, Object> parYaud) throws NonexistentEntityException {
        log.info("....................................................................mbm............CONTROL");
        
        List<Object[]> reply = new ArrayList();
        HashMap<String, Object> parYaudLocal = null;
        Direccion direccion = null;
        DireccionJsonOut direccionJsonOut = null;
        
        for (DireccionJson direccionJson : direccionesJson) {
            parYaudLocal = (HashMap<String, Object>) parYaud.clone();
            String metodo=null;
            
            if(KApp.DELETE.getKApp().equals(parYaudLocal.get("method"))){
                metodo = KApp.DELETE.getKApp();
            } else {
                metodo = direccionJson.getMethod();
            }

            Object[] direccion_Array = new Object[4]; 
            
            if (KApp.POST.getKApp().equals(metodo)) {
                direccion = new Direccion();
                direccion.setIdDireccion(UUID.randomUUID().toString().replaceAll("-", ""));
            } else if (KApp.PUT.getKApp().equals(metodo)) {
                direccion = direccionService.findDireccion(direccionJson.getId());
            }

            if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
                direccion.setSociedad(sociedadRpl);
                direccion.setPersona(persona);
                direccion.setAlmacen(almacen);
                direccion.setTipoDireccion(" ");
                direccion.setDireccionGoogleFormateada(direccionJson.getFormateada());
                direccion.setLatitud(direccionJson.getLocation().getLat());
                direccion.setLongitud(direccionJson.getLocation().getLng());
                direccion.setVisibleProve(direccionJson.getVisibleProve());
                direccion.setVisibleAsoc(direccionJson.getVisibleAsoc());
                direccion.setDescripcion(direccionJson.getDescripcion());
                parYaudLocal.put("method", metodo);
                direccion.setAtributosAuditoria(datosAuditoriaRestService.cargaAtributosAuditoria(parYaudLocal));
                direccion_Array[2] = direccion;
                direccion_Array[3] = null;
            } else if (KApp.DELETE.getKApp().equals(metodo)) {
                parYaudLocal.put("method", metodo);                
                direccion_Array[2] = null;
                direccion_Array[3] = direccionJson.getId();
            }
            if (!KApp.DUMMY_METHOD.getKApp().equals(metodo)) {
                direccion_Array[0] = metodo;
                direccion_Array[1] = "direccion";
                reply.add(direccion_Array);
            }

            direccionJsonOut = new DireccionJsonOut();
            direccionJsonOut.setDireccion(direccion);
            if (!direccionJson.getMethod().equals(ElementEnum.KApp.DUMMY_METHOD.getKApp())) {
                List<Object[]> componentes = montaDireccionGoogleComponete(direccionJson.getComponentes(), direccion, parYaudLocal);
                reply.addAll(componentes);

                if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
                    parYaudLocal.put("method", KApp.POST.getKApp());
                    List<Object[]> tiposDireccion = montaDireccionType(direccionJson.getTypes(), direccion, null, parYaudLocal);
                    reply.addAll(tiposDireccion);
                }
            }
        }

        return reply;
    }

    public List<Object[]> montaDireccionGoogleComponete(List<ComponenteJson> componentesJson, Direccion direccion, HashMap<String, Object> parYaud) throws NonexistentEntityException {
        log.info("....................................................................mbm............CONTROL");
                
        List<Object[]> reply = new ArrayList();
        HashMap<String, Object> parYaudLocal = null;
        
        DireccionGoogleComponente direccionGoogleComponente = null;

        for (ComponenteJson componenteJson : componentesJson) {
            parYaudLocal = (HashMap<String, Object>) parYaud.clone();
            String metodo=null;
            
            if(KApp.DELETE.getKApp().equals(parYaudLocal.get("method"))){
                metodo = KApp.DELETE.getKApp();
            } else {
                metodo = componenteJson.getMethod();
            }

            Object[] direccionGoogleComponente_Array = new Object[4]; 

            if (KApp.POST.getKApp().equals(metodo)) {
                direccionGoogleComponente = new DireccionGoogleComponente();
                direccionGoogleComponente.setIdDireccionGoogleComponente(UUID.randomUUID().toString().replaceAll("-", ""));
            } else if (KApp.PUT.getKApp().equals(metodo)) {
                direccionGoogleComponente = direccionGoogleComponenteService.findDireccionGoogleComponente(componenteJson.getId());
            }

            if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
                direccionGoogleComponente.setLongName(componenteJson.getLong_name());
                direccionGoogleComponente.setShortName(componenteJson.getShort_name());
                direccionGoogleComponente.setDireccion(direccion);
                parYaudLocal.put("method", metodo);
                direccionGoogleComponente.setAtributosAuditoria(datosAuditoriaRestService.cargaAtributosAuditoria(parYaudLocal));
                direccionGoogleComponente_Array[2] = direccionGoogleComponente;
                direccionGoogleComponente_Array[3] = null;
            } else if (KApp.DELETE.getKApp().equals(metodo)) {
                parYaudLocal.put("method", metodo);
                direccionGoogleComponente_Array[2] = null;
                direccionGoogleComponente_Array[3] = componenteJson.getId();
            }

            if (!KApp.DUMMY_METHOD.getKApp().equals(metodo)) {
                direccionGoogleComponente_Array[0] = metodo;
                direccionGoogleComponente_Array[1] = "direccionGoogleComponente";
                reply.add(direccionGoogleComponente_Array);
            }

            if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
                parYaudLocal.put("method", KApp.POST.getKApp());
                List<Object[]> tiposComponente = montaDireccionType(componenteJson.getTypes(), null, direccionGoogleComponente, parYaudLocal);
                reply.addAll(tiposComponente);
            }
        }

        return reply;
    }

    public List<Object[]> montaDireccionType(List<String> tiposDireccion, Direccion direccion, DireccionGoogleComponente direccionGoogleComponente, HashMap<String, Object> parYaud) throws NonexistentEntityException {
        log.info("....................................................................mbm............CONTROL");
                

        List<Object[]> reply = new ArrayList();
        String metodo = (String) parYaud.get("method");

        for (String tipoDireccion : tiposDireccion) {
            Object[] direccionGoogleType_Array = new Object[4]; 
            DireccionGoogleType direccionGoogleType = new DireccionGoogleType();

            direccionGoogleType.setIdDireccionGoogleType(UUID.randomUUID().toString().replaceAll("-", ""));
            direccionGoogleType.setDireccionidk((direccion == null) ? null : direccion.getIdDireccion());
            direccionGoogleType.setDirecciongooglegomponenteid((direccionGoogleComponente == null) ? null : direccionGoogleComponente.getIdDireccionGoogleComponente());
            direccionGoogleType.setDireccion(direccion);
            direccionGoogleType.setDireccionGoogleComponente(direccionGoogleComponente);
            direccionGoogleType.setType(tipoDireccion);
            direccionGoogleType.setAtributosAuditoria(datosAuditoriaRestService.cargaAtributosAuditoria(parYaud));

            direccionGoogleType_Array[0] = metodo;
            direccionGoogleType_Array[1] = "direccionGoogleType";
            direccionGoogleType_Array[2] = direccionGoogleType;
            direccionGoogleType_Array[3] = null;

            reply.add(direccionGoogleType_Array);
        }

        return reply;
    }

    public List<DireccionJson> recuperaDirecciones(SociedadRpl sociedad, Persona persona, Almacen almacen) {
        log.info("....................................................................mbm............CONTROL");
                
        List<Direccion> direcciones = null;
        
        if (sociedad != null) {
            direcciones = direccionService.findDireccionesBy_S(sociedad);;
        } else if (persona != null) {
            direcciones = direccionService.findDireccionesBy_P(persona);
        } else if (almacen != null) {
            direcciones = direccionService.findDireccionesBy_A(almacen);;
        }
        
        if(direcciones==null){
            direcciones = new ArrayList<>();
        }

        List<DireccionJson> reply = new ArrayList();
        for (Direccion direccion : direcciones) {
            DireccionJson direccionJson = new DireccionJson();
            direccionJson.setId(direccion.getIdDireccion());
            direccionJson.setEstado(direccion.getAtributosAuditoria().getEstadoRegistro());
            direccionJson.setFormateada(direccion.getDireccionGoogleFormateada());
            direccionJson.setMethod(direccion.getAtributosAuditoria().getUltimaAccion());
            direccionJson.setVisibleProve(direccion.getVisibleProve());
            direccionJson.setVisibleAsoc(direccion.getVisibleAsoc());
            direccionJson.setDescripcion(direccion.getDescripcion());
            LocationJson locationJson = new LocationJson();
            locationJson.setLat(direccion.getLatitud());
            locationJson.setLng(direccion.getLongitud());
            direccionJson.setLocation(locationJson);

            direccionJson.setTypes(typesToJson(direccion.getIdDireccion(), null));

            List<DireccionGoogleComponente> componentes = direccionGoogleComponenteService.findDireccionGoogleComponenteBy_idD(direccion);
            List<ComponenteJson> componentesJson = new ArrayList();
            for (DireccionGoogleComponente componente : componentes) {
                ComponenteJson componenteJson = new ComponenteJson();
                componenteJson.setId(componente.getIdDireccionGoogleComponente());
                componenteJson.setEstado(componente.getAtributosAuditoria().getEstadoRegistro());
                componenteJson.setMethod(componente.getAtributosAuditoria().getUltimaAccion());
                componenteJson.setShort_name(componente.getShortName());
                componenteJson.setLong_name(componente.getLongName());
                componenteJson.setTypes(typesToJson(null, componente.getIdDireccionGoogleComponente()));
                componentesJson.add(componenteJson);
            }
            direccionJson.setComponentes(componentesJson);

            reply.add(direccionJson);
        }

        return reply;
    }

    public List<String> typesToJson(String direccionId, String componenteId) {
        log.info("....................................................................mbm............CONTROL");
                
        List<String> reply = new ArrayList();

        List<DireccionGoogleType> direccionesGoogleType = direccionGoogleTypeService.findDireccionesGoogleTypeBy_DDC(direccionId, componenteId);
        for (DireccionGoogleType direccionGoogleType : direccionesGoogleType) {
            reply.add(direccionGoogleType.getType());
        }
        return reply;
    }

}
