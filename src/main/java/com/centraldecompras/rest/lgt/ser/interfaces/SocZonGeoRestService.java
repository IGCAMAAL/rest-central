package com.centraldecompras.rest.lgt.ser.interfaces;

import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SocProductoExcl;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.rest.lgt.json.ZonaProductoAsigJson;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public interface SocZonGeoRestService {

    List<Object[]> montaZonasGeoAsigadasASociedad(List<String> socDateIn, SociedadRpl sociedadRpl, String idUser, String idSoc, String idPrf, String idSec) throws NonexistentEntityException, Exception;

    List<ZonaProductoAsigJson> recuperaZonasGeoAsignadasASociedad(String sociedadId);

    public void persistencia(List<Object> createdObjets, List<Object> updatedObjets, List<Object> deletdObjets) throws Exception;

    //void persistencia(List<Object[]> listaObjetos) throws Exception;
    //
    
    Map<String, Object> crdExcludes(SocZonGeo socZonGeo, String accion) throws Exception ;
}
