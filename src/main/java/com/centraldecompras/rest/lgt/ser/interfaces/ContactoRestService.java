package com.centraldecompras.rest.lgt.ser.interfaces;

import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.rest.fin.json.ContactoJson;
import java.util.HashMap;
import java.util.List;

public interface ContactoRestService {



    List<Object[]> montaContactos(SociedadRpl sociedad, Persona persona, Almacen almacen, List<ContactoJson> contactosJson, HashMap<String, Object> parYaud);
    
    List<ContactoJson> recuperaContactos(SociedadRpl sociedad, Persona persona, Almacen almacen);
}
