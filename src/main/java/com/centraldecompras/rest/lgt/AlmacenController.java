package com.centraldecompras.rest.lgt;

import com.centraldecompras.rest.lgt.json.AlmacenJson;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.modelo.AlmacenUsuProdExcl;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenService;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenUsuarioProductoService;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoService;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonaGeograficaService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.rest.lgt.ser.interfaces.AlmacenRestService;
import com.centraldecompras.rest.lgt.ser.interfaces.ContactoRestService;
import com.centraldecompras.rest.lgt.ser.interfaces.DireccionRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/lgt")
@Api(value = "Recurso privado", description = "Gestion de almacenes")
public class AlmacenController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AlmacenController.class.getName());

    @Autowired
    private AlmacenService almacenService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private ProductoService productoService;

    @Autowired
    SociedadRplService sociedadRplService;

    @Autowired
    private ZonaGeograficaService zonaGeograficaService;
    @Autowired
    SocZonGeoService socZonGeoService;

    @Autowired
    private DireccionRestService direccionRestService;
    @Autowired
    private ContactoRestService contactoRestService;

    @Autowired
    private AlmacenRestService almacenRestService;

    @Autowired
    private AlmacenUsuarioProductoService almacenUsuarioProductoService;

    public enum tipoRegEnum {

        AU, AP, AUP
    };

    public enum methodEnum {

        POST, PUT, GET, DELETE
    };

    @RequestMapping(value = "/almacen", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Almacen", notes = "AlmacenJson, formato de comunicacion con cliente")
    List<AlmacenJson> postAlmace(
            @RequestBody List<AlmacenJson> almacenesJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<AlmacenJson> reply = new ArrayList<>();

        for (AlmacenJson almacenJson : almacenesJson) {

            String id = almacenJson.getId();
            String metodo = almacenJson.getMethod();

            HashMap<String, Object> parYaud = new HashMap<String, Object>();
            parYaud.put("idUser", idUser);
            parYaud.put("idSoc", idSoc);
            parYaud.put("idPrf", idPrf);
            parYaud.put("idSec", idSec);

            parYaud.put("id", almacenJson.getId());
            parYaud.put("estado", almacenJson.getEstado());
            parYaud.put("method", almacenJson.getMethod());
            parYaud.put("fpAct", almacenJson.getFechaPrevistaActivacion());
            parYaud.put("fpDes", almacenJson.getFechaPrevistaDesactivacion());

            SociedadRpl sociedadRpl = null;
            ZonaGeografica zonaGeografica = null;

            List<Object[]> listaObjetos = new ArrayList();
            if (("0").equals(id) && ("POST").equals(metodo) || !("0").equals(id) && !("POST").equals(metodo)) {

                Almacen almacen = null;
                Object[] aaa = new Object[4];

                sociedadRpl = sociedadRplService.findSociedadRpl(almacenJson.getSociedadId());
                if (sociedadRpl == null) {
                    Object[] valores = {almacenJson.getSociedadId()};
                    String mensaje = MessageFormat.format("ERROR: Sociedad {0}, no encontrada. ", valores);
                    throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                }

                zonaGeografica = zonaGeograficaService.findZonaGeografica(almacenJson.getZonaGeograficaId());
                if (zonaGeografica == null) {
                    Object[] valores = {almacenJson.getZonaGeograficaId()};
                    String mensaje = MessageFormat.format("ZonaGeografica: {0}, no encontrada", valores);
                    throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                }

                almacen = almacenRestService.montaAlmacen(almacenJson, idUser, zonaGeografica, sociedadRpl);
                almacenJson.setId(almacen.getIdAlmacen());  // Para busqueda al final del método y presentación de la persistencia

                aaa[0] = metodo;
                aaa[1] = "almacen";
                aaa[2] = almacen;
                aaa[3] = almacenJson.getId();

                if (KApp.DELETE.getKApp().equals(metodo)) {
                    parYaud.put("method", KApp.DELETE.getKApp());
                }

                listaObjetos.add(aaa);

                if (almacenJson.getDirecciones() != null) {
                    List<Object[]> direcciones = direccionRestService.montaDirecciones(null, null, almacen, almacenJson.getDirecciones(), parYaud); // Sociedad, Persona, Almacen
                    listaObjetos.addAll(direcciones);
                }
                if (almacenJson.getContactos() != null) {
                    List<Object[]> contactos = contactoRestService.montaContactos(null, null, almacen, almacenJson.getContactos(), parYaud);    // Sociedad, Persona, Almacen
                    listaObjetos.addAll(contactos);
                }

                try {
                    almacenRestService.persistenciaAlmacen(listaObjetos);
                } catch (Exception ex) {
                    log.info("CentralCompras. CRUD Almacenes. id zonaGeograficaJson: Error en persistencia de datos");
                    ex.printStackTrace();
                }

            } else {
                throw new Exception("CentralCompras. CRUD Almacenes. id zonaGeograficaJson  y Metodo ne estÃ¡n sincronizados");
            }
        }

        for (AlmacenJson almacenJson : almacenesJson) {
            reply.add(almacenToJson(almacenService.findAlmacen(almacenJson.getId())));
        }

        return reply;
    }

    @RequestMapping(value = "/almacenuno/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "REcupera los datos de un almacen", notes = "AlmacenJson, formato de comunicacion con cliete")
    AlmacenJson getAlmacen(
            @PathVariable String id,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        AlmacenJson reply = new AlmacenJson();

        Almacen almacen = almacenService.findAlmacen(id);

        if (almacen != null) {
            reply = almacenToJson(almacen);
        }
        return reply;
    }

    @RequestMapping(value = "/almaceneslist/{idSociedad}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera los almacenes de una sociedad", notes = "")
    List<Map<String, Object>> getAlmacenes(
            @PathVariable String idSociedad,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList();
        List<Almacen> almacenes = almacenService.findAlmacenesBy_idS(idSociedad);

        for (Almacen almacen : almacenes) {
            Map<String, Object> almacenesOut = new HashMap();
            AlmacenJson almacenJson = almacenToJson(almacen);
            almacenesOut.put("almacen", almacenJson);
            reply.add(almacenesOut);
        }

        return reply;
    }

    @RequestMapping(value = "/crudalmacenusuarioproducto", method = RequestMethod.POST)
    @ApiOperation(value = "Registra Almacenes y/o Usuarios y/o Productos ", notes = "almacenUsuarioProductoJson, formato de comunicacion con cliente")
    List<Map<String, Object>> crudAlmacenUsuarioProducto(
            @RequestBody List<Map<String, Object>> almacenUsuarioProductoJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList<>();
        List<Object> createdObjets = new ArrayList();
        List<Object> updatedObjets = new ArrayList();
        List<Object> deletdObjets = new ArrayList();

        try {
            verificaParam_crudAlmUsuPrd(almacenUsuarioProductoJson);

            for (Map<String, Object> almacenUsuarioProductoJsonEle : almacenUsuarioProductoJson) {
                String tipoReg = (String) almacenUsuarioProductoJsonEle.get("tipoReg");
                String method = (String) almacenUsuarioProductoJsonEle.get("method");

                almacenUsuarioProductoJsonEle.put("idUser", idUser);
                AlmacenUsuarioProducto almacenUsuarioProducto = almacenRestService.montaAlmacenUsuarioProducto(almacenUsuarioProductoJsonEle);

                method = (String) almacenUsuarioProductoJsonEle.get("method");

                if ("POST".equals(method)) {
                    createdObjets.add(almacenUsuarioProducto);
                }
                if ("PUT".equals(method) || "DELETE".equals(method)) {
                    updatedObjets.add(almacenUsuarioProducto);
                }

                String accion = "H"; // Herencia
                if(!"DELETE".equals(method)){
                    if(almacenUsuarioProducto.isAsignada()){
                       accion = "A";  // Asigna
                    } else {
                       accion = "E"; // Excluye
                    }
                    
                }
                Map<String, Object> crdExcludesMap = almacenRestService.crdExcludes(almacenUsuarioProducto, accion);

                List<AlmacenUsuProdExcl> createExcludes = (List<AlmacenUsuProdExcl>) crdExcludesMap.get("createExcludes");
                List<AlmacenUsuProdExcl> updateExcludes = (List<AlmacenUsuProdExcl>) crdExcludesMap.get("updateExcludes");
                List<AlmacenUsuProdExcl> deleteExcludes = (List<AlmacenUsuProdExcl>) crdExcludesMap.get("deleteExcludes");

                createdObjets.addAll(createExcludes);
                updatedObjets.addAll(updateExcludes);
                deletdObjets.addAll(deleteExcludes);

            }

            almacenRestService.persistencia(createdObjets, updatedObjets, deletdObjets);

        } catch (Exception ex) {
            log.warn(ex);
            ex.printStackTrace();
            throw ex;
        }

        for (Map<String, Object> almacenUsuarioProductoJsonEle : almacenUsuarioProductoJson) {
            String tipoReg = (String) almacenUsuarioProductoJsonEle.get("tipoReg");
            Persona usuario = null;
            Producto producto = null;

            Almacen almacen = almacenService.findAlmacen((String) almacenUsuarioProductoJsonEle.get("almacenId"));

            if (tipoReg.equals("AU") || tipoReg.equals("AUP")) {
                usuario = personaService.findPersona((String) almacenUsuarioProductoJsonEle.get("usuarioId"));
            } else {
                usuario = personaService.findPersona(KApp.DUMMY.getKApp());
            }

            if (tipoReg.equals("AP") || tipoReg.equals("AUP")) {
                producto = productoService.findProducto((String) almacenUsuarioProductoJsonEle.get("productoId"));
            } else {
                producto = productoService.findProducto(KApp.DUMMY.getKApp());
            }

            AlmacenUsuarioProducto almacenUsuarioProducto = almacenUsuarioProductoService.findAlmacenUsuarioProductoBy_AUP(almacen, usuario, producto);

            if (almacenUsuarioProducto != null) {
                reply.add(almacenUsuarioProductoToJson(almacenUsuarioProducto));
            }
        }

        return reply;
    }

    @RequestMapping(value = "/listalmacenusuarioproducto", method = RequestMethod.POST)
    @ApiOperation(value = "Lista de productos asignados a Almacen o almacen/usuario o Lista de usuarios asignados a almacen", notes = "almacenUsuarioProductoJson, formato de comunicacion con cliente")
    List<Map<String, Object>> listAlmacenUsuarioProducto(
            @RequestBody List<Map<String, Object>> almacenUsuarioProductoJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList<>();
        List<Object> createdObjetos = new ArrayList();
        List<Object> updatedObjetos = new ArrayList();

        try {
            verificaParam_ListAlmUsuPrd(almacenUsuarioProductoJson);

        } catch (Exception ex) {
            throw ex;
        }

        for (Map<String, Object> almacenUsuarioProductoJsonEle : almacenUsuarioProductoJson) {
            String tipoReg = (String) almacenUsuarioProductoJsonEle.get("tipoReg");
            Almacen almacen = null;
            Persona usuario = null;

            almacen = almacenService.findAlmacen((String) almacenUsuarioProductoJsonEle.get("almacenId"));
            if (almacen == null) {
                throw new Exception("Almacen no encontado con id: " + (String) almacenUsuarioProductoJsonEle.get("almacenId"));
            }
            if (tipoReg.equals("AUP")) {
                usuario = personaService.findPersona((String) almacenUsuarioProductoJsonEle.get("usuarioId"));
                if (usuario == null) {
                    throw new Exception("Usuario no encontado con id: " + (String) almacenUsuarioProductoJsonEle.get("usuarioId"));
                }
            }

            List<String> estados = new ArrayList();
            estados.add(KApp.ACTIVO.getKApp());

            List<AlmacenUsuarioProducto> almacenUsuarioProductoList = null;
            if (tipoReg.equals("AU") || tipoReg.equals("AP")) {
                almacenUsuarioProductoList = almacenUsuarioProductoService.findAlmacenUsuarioProductoBy_A(tipoReg, almacen, estados);
            }
            if (tipoReg.equals("AUP")) {
                almacenUsuarioProductoList = almacenUsuarioProductoService.findAlmacenUsuarioProductoBy_AU(tipoReg, almacen, usuario, estados);
            }

            for (AlmacenUsuarioProducto almacenUsuarioProducto : almacenUsuarioProductoList) {
                reply.add(almacenUsuarioProductoToJson(almacenUsuarioProducto));
            }
        }

        return reply;
    }

    private AlmacenJson almacenToJson(Almacen almacen) {
        log.info("....................................................................mbm............CONTROL");

        AlmacenJson reply = new AlmacenJson();
        reply.setDirecciones(new ArrayList<>());
        reply.setContactos(new ArrayList<>());

        reply.setId(almacen.getIdAlmacen());

        reply.setNombreAlmacen(almacen.getNombreAlmacen());
        reply.setTipoAlmacen(almacen.getTipoAlmacen());
        reply.setZonaGeograficaId(almacen.getZonaGeografica().getIdZonaGeografica());
        reply.setMethod(almacen.getAtributosAuditoria().getUltimaAccion());
        reply.setEstado(almacen.getAtributosAuditoria().getEstadoRegistro());
        reply.setFechaPrevistaActivacion(almacen.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(almacen.getAtributosAuditoria().getFechaPrevistaDesactivacion());

        reply.setDirecciones(direccionRestService.recuperaDirecciones(null, null, almacen));
        reply.setContactos(contactoRestService.recuperaContactos(null, null, almacen));

        reply.setSociedadId(almacen.getSociedad().getIdSociedad());
        reply.setDescripcion(almacen.getDescripcionAlmacen());
        return reply;
    }

    private Map<String, Object> almacenUsuarioProductoToJson(AlmacenUsuarioProducto almacenUsuarioProducto) {
        log.info("....................................................................mbm............CONTROL");

        Map<String, Object> reply = new HashMap();

        reply.put("tipoReg", almacenUsuarioProducto.getTipoReg());

        reply.put("almacenId", almacenUsuarioProducto.getAlmacen().getIdAlmacen());
        reply.put("usuarioId", almacenUsuarioProducto.getUsuario().getIdPersona());
        reply.put("productoId", almacenUsuarioProducto.getProducto().getIdProducto());
        reply.put("asignada", almacenUsuarioProducto.isAsignada());

        reply.put("method", almacenUsuarioProducto.getAtributosAuditoria().getUltimaAccion());

        reply.put("fechaPrevistaActivacion", almacenUsuarioProducto.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.put("fechaPrevistaDesactivacion", almacenUsuarioProducto.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.put("estado", almacenUsuarioProducto.getAtributosAuditoria().getEstadoRegistro());

        Map<String, Object> configuracion = new HashMap();
        configuracion.put("verPrecio", almacenUsuarioProducto.isVerPrecio());
        configuracion.put("modoCompra", almacenUsuarioProducto.getModoCompra());
        reply.put("configuracion", configuracion);

        return reply;
    }

    private void verificaParam_crudAlmUsuPrd(List<Map<String, Object>> almacenUsuarioProductoJson) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Map<String, Object> almacenUsuarioProductoJsonEle : almacenUsuarioProductoJson) {

            String tipoReg = null;
            String method = null;

            try {

                if (!almacenUsuarioProductoJsonEle.containsKey("tipoReg")) {
                    throw new Exception("Parametro: tipeReg no informado");
                } else {
                    tipoReg = (String) almacenUsuarioProductoJsonEle.get("tipoReg");
                    tipoRegEnum.valueOf(tipoReg);
                }

                if (!almacenUsuarioProductoJsonEle.containsKey("method")) {
                    throw new Exception("Parametro: method no informado");
                } else {
                    method = (String) almacenUsuarioProductoJsonEle.get("method");
                    methodEnum.valueOf(method);
                }

                if (!almacenUsuarioProductoJsonEle.containsKey("almacenId")) {
                    throw new Exception("Parametro: almacenId no informado");
                }

                if (tipoRegEnum.valueOf(tipoReg).equals("AU") || tipoRegEnum.valueOf(tipoReg).equals("AUP")) {
                    if (!almacenUsuarioProductoJsonEle.containsKey("usuarioId")) {
                        throw new Exception("Parametro: usuarioId no informado");
                    }
                    if (!almacenUsuarioProductoJsonEle.containsKey("configuracion") && (methodEnum.valueOf(method).equals("POST") || methodEnum.valueOf(method).equals("PUT"))) {
                        throw new Exception("Parametro: configuracion no informado");
                    } else {
                        Map<String, Object> configuracion = (Map<String, Object>) almacenUsuarioProductoJsonEle.get("configuracion");
                        if (!almacenUsuarioProductoJsonEle.containsKey("verPrecio")) {
                            throw new Exception("Parametro: verPrecio no informado");
                        }
                        if (!almacenUsuarioProductoJsonEle.containsKey("modoCompra")) {
                            throw new Exception("Parametro: modoCompra no informado");
                        }
                    }
                }

                if (tipoRegEnum.valueOf(tipoReg).equals("AP") || tipoRegEnum.valueOf(tipoReg).equals("AUP")) {
                    if (!almacenUsuarioProductoJsonEle.containsKey("productoId")) {
                        throw new Exception("Parametro: productoId no informado");
                    }
                }

                if ((methodEnum.valueOf(method).equals("POST") || methodEnum.valueOf(method).equals("PUT"))) {
                    if (!almacenUsuarioProductoJsonEle.containsKey("asignada")) {
                        throw new Exception("Parametro: asignada no informado");
                    }
                    if (!almacenUsuarioProductoJsonEle.containsKey("fechaPrevistaActivacion")) {
                        throw new Exception("Parametro: fechaPrevistaActivacion no informado");
                    }
                    if (!almacenUsuarioProductoJsonEle.containsKey("fechaPrevistaDesactivacion")) {
                        throw new Exception("Parametro: fechaPrevistaDesactivacion no informado");
                    }
                }

            } catch (Exception ex) {
                throw ex;
            }
        }
    }

    private void verificaParam_ListAlmUsuPrd(List<Map<String, Object>> almacenUsuarioProductoJson) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Map<String, Object> almacenUsuarioProductoJsonEle : almacenUsuarioProductoJson) {

            String tipoReg = null;

            try {

                if (!almacenUsuarioProductoJsonEle.containsKey("tipoReg")) {
                    throw new Exception("Parametro: tipeReg no informado");
                } else {
                    tipoReg = (String) almacenUsuarioProductoJsonEle.get("tipoReg");
                    tipoRegEnum.valueOf(tipoReg);
                }

                if (!almacenUsuarioProductoJsonEle.containsKey("almacenId")) {
                    throw new Exception("Parametro: almacenId no informado");
                }

                if (tipoRegEnum.valueOf(tipoReg).equals("AU") || tipoRegEnum.valueOf(tipoReg).equals("AUP")) {
                    if (!almacenUsuarioProductoJsonEle.containsKey("usuarioId")) {
                        throw new Exception("Parametro: usuarioId no informado");
                    }
                }

            } catch (Exception ex) {
                throw ex;
            }
        }
    }

}
