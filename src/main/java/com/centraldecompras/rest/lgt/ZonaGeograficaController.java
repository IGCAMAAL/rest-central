package com.centraldecompras.rest.lgt;

import com.centraldecompras.rest.lgt.json.ZonaGeograficaJson;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.ZonasGeograficasAutorizadas;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoService;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonaGeograficaService;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonasGeograficasAutorizadasService;
import com.centraldecompras.rest.excel.interfaces.ZonGeoExcelService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ciberado
 */
@RestController
@RequestMapping("/lgt")
@Api(value = "Recursos pÃºblicos", description = "Demo de recursos accesibles pÃºblicamente sin autentificaciÃ³n.")
public class ZonaGeograficaController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ZonaGeograficaController.class.getName());

    @Autowired
    private ZonaGeograficaService zonaGeograficaService;

    @Autowired
    private SocZonGeoService socZonGeoService;

    @Autowired
    private ZonasGeograficasAutorizadasService zonasGeograficasAutorizadasService;

    @Autowired
    private ZonGeoExcelService zonGeoExcelService;

    @RequestMapping(value = "/zonageografica", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Zonas Geograficas", notes = "ZonaGeograficaJson, formato de comunicaciÃ³n con cliete")
    List<ZonaGeograficaJson> postZonaGeografica(
            @RequestBody List<ZonaGeograficaJson> zonasGeograficasJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ZonaGeograficaJson> retval = new ArrayList<>();

        for (ZonaGeograficaJson zonaGeograficaJson : zonasGeograficasJson) {
            String id = zonaGeograficaJson.getId();
            String metodo = zonaGeograficaJson.getMethod();
            String estado = zonaGeograficaJson.getEstado();

            if (id.equals("0") && metodo.equals("POST") || !id.equals("0") && metodo.equals("PUT")) {

                ZonaGeografica zonaGeografica = null;

                String idZona;

                if (id.equals("0")) {  // Alta
                    zonaGeografica = new ZonaGeografica();
                    idZona = UUID.randomUUID().toString().replaceAll("-", "");
                    zonaGeografica.setIdZonaGeografica(idZona);
                    zonaGeografica.setAtributosAuditoria(new DatosAuditoria());
                    zonaGeografica.getAtributosAuditoria().setCreated(new Date());

                } else {  // Modificacion
                    zonaGeografica = zonaGeograficaService.findZonaGeografica(id);
                    zonaGeografica.getAtributosAuditoria().setUpdated(new Date());
                    idZona = zonaGeografica.getIdZonaGeografica();
                }

                if (zonaGeografica != null) {
                    zonaGeografica.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
                    zonaGeografica.getAtributosAuditoria().setFechaPrevistaActivacion(zonaGeograficaJson.getFechaPrevistaActivacion());
                    zonaGeografica.getAtributosAuditoria().setFechaPrevistaDesactivacion(zonaGeograficaJson.getFechaPrevistaDesactivacion());
                    zonaGeografica.getAtributosAuditoria().setEstadoRegistro(estado);
                    zonaGeografica.getAtributosAuditoria().setUltimaAccion(metodo);

                    zonaGeografica.setEtiqueta("");
                    zonaGeografica.setDescripcion(zonaGeograficaJson.getNombre());
                    zonaGeografica.setPadreJerarquia(zonaGeograficaService.findZonaGeografica(zonaGeograficaJson.getParent()));
                    zonaGeografica.setNivelInt(zonaGeograficaJson.getNivel());
                    zonaGeografica.setIdPadreJerarquia(zonaGeograficaJson.getParent());

                    informaRoute(zonaGeografica);

                    if (id.equals("0")) { // Alta
                        zonaGeograficaService.create(zonaGeografica);
                    } else {  // Modificacion
                        zonaGeograficaService.edit(zonaGeografica);
                    }

                    ZonaGeograficaJson zonaGeograficaRetorno = zonaGeograficaToJson(zonaGeograficaService.findZonaGeografica(idZona));
                    retval.add(zonaGeograficaRetorno);
                }
            } else {
                throw new Exception("CentralCompras. CRUD zonaGeograficaJson. id zonaGeograficaJson  y Metodo ne estÃ¡n sincronizados");
            }
        }
        return retval;
    }

    @RequestMapping(value = "/zonageografica/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Registrar Zonas Geograficas", notes = "ZonaGeograficaJson, formato de comunicaciÃ³n con cliete")
    ZonaGeograficaJson getzonasGeograficas(
            @PathVariable String id,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        ZonaGeograficaJson reply = new ZonaGeograficaJson();
        reply.setSubzonas(new ArrayList<ZonaGeograficaJson>());

        if (id.equals("0")) {
            id = KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp();
        }
        ZonaGeografica zonaGeografica = zonaGeograficaService.findZonaGeografica(id);

        if (zonaGeografica != null) {
            reply = zonaGeograficaToJson(zonaGeografica);
        }
        return reply;
    }

    @RequestMapping(value = "/zonageograficahijos", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera Zonas Geograficas hijos", notes = " ")
    List<Map<String, Object>> getzonasGeograficasHijos(
            @RequestBody List<String> zonasGeograficasIdJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList<>();

        for (String zonaGeograficaIdJson : zonasGeograficasIdJson) {

            if (zonaGeograficaIdJson.equals("0")) {
                zonaGeograficaIdJson = KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp();
            }

            ZonaGeografica zonaGeografica = zonaGeograficaService.findZonaGeografica(zonaGeograficaIdJson);
            if (zonaGeografica != null) {
                List<ZonaGeografica> subZonasGeograficas = zonaGeograficaService.findZonasGeograficasBy_ParentId(zonaGeografica);

                if (subZonasGeograficas == null) {
                    subZonasGeograficas = new ArrayList<ZonaGeografica>();
                }
                Map<String, Object> replyEle = new HashMap<>();
                replyEle.put("id", zonaGeograficaIdJson);
                replyEle.put("hijos", (subZonasGeograficas.size() > 0) ? true : false);
                reply.add(replyEle);
            }
        }
        return reply;
    }

    @RequestMapping(value = "/zonageografica/seleccion/{cadena}/{estado}/{cantidad}", method = RequestMethod.GET)
    List<ZonaGeograficaJson> seleccionZonas(
            @PathVariable String cadena,
            @PathVariable String estado,
            @PathVariable String cantidad,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        log.info("cadena: " + cadena);
        log.info("estado: " + estado);
        log.info("cantidad: " + cantidad);

        List<ZonaGeograficaJson> reply = new ArrayList<ZonaGeograficaJson>();
        List<ZonaGeografica> zonasGeograficas = new ArrayList<ZonaGeografica>();

        int cantidadInt = 0;
        try {
            cantidadInt = Integer.parseInt(cantidad);
        } catch (NumberFormatException ex) {
            throw new Exception("CentralCompras. Combo zonaGeograficaJson. Parametro 'cantidad' de resultados no es valido");
        }

        if (cadena == null || cadena.equals("")) {
            throw new Exception("CentralCompras. Combo zonaGeograficaJson. Parametro 'cadena' no es valido");
        }

        /*Simulacion******************
         Aqui se ha de buscar las 'cantidad' zonas cuyo nombre contiene 'cadena' y cuyo estado es 'estado'
         Estados posibles: 'AC', 'DA', 'TODOS'
        
         Yo haria lo siguiente:
         Primero: Buscar las zonas que EMPIEZANn con 'cadena'
         Segundo: Si con la busqueda anterior no llego a lo 'cantidad' de resultados buscar los que CONTIENEN 'cadena' para 
         complementar hasta los 'cantidad'.
         */
        zonasGeograficas = zonaGeograficaService.findZonaGeograficaTypeAhead(1, cadena, estado.toUpperCase(), cantidadInt);
        reply = seleccionZonasCargaArray(reply, zonasGeograficas);

        if (reply.size() < cantidadInt) {
            zonasGeograficas = zonaGeograficaService.findZonaGeograficaTypeAhead(2, cadena, estado.toUpperCase(), cantidadInt - reply.size());
            reply = seleccionZonasCargaArray(reply, zonasGeograficas);
        }

        return reply;

    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/excel/{maxResults}/{firstResult}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera todas las Zonas geograficas y las  muestra en excel ", notes = "SuppListJson, formato de comunicacion con cliente")
    Map<String, String> getSuppExcel(
            @PathVariable int maxResults,
            @PathVariable int firstResult,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<ZonaGeografica> zonasGeografica = zonaGeograficaService.findZonasGeograficasOrderBy_N();
        List<SocZonGeo> socZonsGeo = socZonGeoService.findsocZonasGeograficasOrderBy_idS_N();
        List<ZonasGeograficasAutorizadas> zonasGeograficasAutorizadas = zonasGeograficasAutorizadasService.findZonasGeograficasAutorizadasEntities(true, maxResults, firstResult);

        XSSFWorkbook workbook = null;
        workbook = zonGeoExcelService.montaExcel(zonasGeografica, socZonsGeo, zonasGeograficasAutorizadas);
        String nameFile = KApp.ZONAS_GEOGRAFICAS.getKApp();
        String extensionFile = KApp.XLSX.getKApp();

        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        workbook.write(outByteStream);

        byte[] outArray = outByteStream.toByteArray();
        byte[] encoded = Base64.encodeBase64(outArray);
        String picture = new String(encoded);

        Map<String, String> reply = new HashMap<>();
        reply.put(KApp.NAME_FILE.getKApp(), nameFile);
        reply.put(KApp.EXTENSION_FILE.getKApp(), extensionFile);
        reply.put(KApp.PICTURE.getKApp(), picture);

        XSSFWorkbook workbooktest = null;
        workbooktest = zonGeoExcelService.montaExcel(zonasGeografica, socZonsGeo, zonasGeograficasAutorizadas);

        try { //Write the workbook in file system
            if (workbooktest != null) {
                FileOutputStream out = new FileOutputStream(new File(nameFile.concat(".").concat(extensionFile)));
                workbooktest.write(out);
                out.close();
                log.info(nameFile + "." + extensionFile + " written successfully on disk.");
            }
        } catch (Exception e) {
            Object[] valores = {"Generacion excel de sociedades"};
            String mensaje = MessageFormat.format("ERROR: SociedadesUsuariosPerfiles.xlsx no generado. {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        return reply;
    }

    private List<ZonaGeograficaJson> seleccionZonasCargaArray(List<ZonaGeograficaJson> reply, List<ZonaGeografica> zonasGeograficas) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (ZonaGeografica zonaGeografica : zonasGeograficas) {
            ZonaGeograficaJson zonaGeograficaJson = new ZonaGeograficaJson();
            zonaGeograficaJson.setId(zonaGeografica.getIdZonaGeografica());
            zonaGeograficaJson.setParent(zonaGeografica.getPadreJerarquia() == null ? KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp() : zonaGeografica.getPadreJerarquia().getIdZonaGeografica());
            zonaGeograficaJson.setNivel(zonaGeografica.getNivelInt());
            zonaGeograficaJson.setNombre(zonaGeografica.getDescripcion());
            zonaGeograficaJson.setEstado(zonaGeografica.getAtributosAuditoria().getEstadoRegistro());
            zonaGeograficaJson.setMethod(zonaGeografica.getAtributosAuditoria().getUltimaAccion());
            zonaGeograficaJson.setFechaPrevistaActivacion(zonaGeografica.getAtributosAuditoria().getFechaPrevistaActivacion());
            zonaGeograficaJson.setFechaPrevistaDesactivacion(zonaGeografica.getAtributosAuditoria().getFechaPrevistaDesactivacion());

            reply.add(zonaGeograficaJson);
        }
        return reply;
    }

    private ZonaGeograficaJson zonaGeograficaToJson(ZonaGeografica zonaGeografica) {
        log.info("....................................................................mbm............CONTROL");

        ZonaGeograficaJson reply = new ZonaGeograficaJson();
        reply.setSubzonas(new ArrayList<>());

        reply.setId(zonaGeografica.getIdZonaGeografica());
        if (zonaGeografica.getIdZonaGeografica().equals(KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp())) {
            reply.setParent(null);
        } else {
            reply.setParent(zonaGeografica.getPadreJerarquia().getIdZonaGeografica());
        }
        reply.setNombre(zonaGeografica.getDescripcion());
        reply.setEstado(zonaGeografica.getAtributosAuditoria().getEstadoRegistro());
        reply.setMethod(zonaGeografica.getAtributosAuditoria().getUltimaAccion());
        reply.setFechaPrevistaActivacion(zonaGeografica.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(zonaGeografica.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setNivel(zonaGeografica.getNivelInt());

        List<ZonaGeograficaJson> subZonasGeograficasJson = new ArrayList<ZonaGeograficaJson>();
        List<ZonaGeografica> subZonasGeograficas = zonaGeograficaService.findZonasGeograficasBy_ParentId(zonaGeografica);

        for (ZonaGeografica subZonaGeografica : subZonasGeograficas) {
            ZonaGeograficaJson subZonaGeograficaJson = new ZonaGeograficaJson();
            subZonaGeograficaJson.setId(subZonaGeografica.getIdZonaGeografica());
            subZonaGeograficaJson.setParent(subZonaGeografica.getPadreJerarquia().getIdZonaGeografica());
            subZonaGeograficaJson.setNivel(subZonaGeografica.getNivelInt());
            subZonaGeograficaJson.setNombre(subZonaGeografica.getDescripcion());
            subZonaGeograficaJson.setEstado(subZonaGeografica.getAtributosAuditoria().getEstadoRegistro());
            subZonaGeograficaJson.setMethod(subZonaGeografica.getAtributosAuditoria().getUltimaAccion());
            subZonaGeograficaJson.setFechaPrevistaActivacion(subZonaGeografica.getAtributosAuditoria().getFechaPrevistaActivacion());
            subZonaGeograficaJson.setFechaPrevistaDesactivacion(subZonaGeografica.getAtributosAuditoria().getFechaPrevistaDesactivacion());

            subZonasGeograficasJson.add(subZonaGeograficaJson);
        }

        reply.setSubzonas(subZonasGeograficasJson);

        return reply;
    }

    @PostConstruct
    public void init() throws Exception {
        try {
            List<Object> createdObjets = new ArrayList();
            List<Object> updatedObjets = new ArrayList();

            List<ZonaGeografica> zonasGeograficas = zonaGeograficaService.findZonaGeograficaEntities();
            for (ZonaGeografica zonaGeografica : zonasGeograficas) {
                createdObjets.clear();
                updatedObjets.clear();

                informaRoute(zonaGeografica);

                updatedObjets.add(zonaGeografica);
                zonaGeograficaService.persistencia(createdObjets, updatedObjets);

            }
        } catch (Exception ex) {
            log.warn(ex.getMessage());
            throw ex;
        }
    }

    public void informaRoute(ZonaGeografica zonaGeografica) throws Exception {
        StringBuilder route = new StringBuilder();
        
        List<String> routElementes = new ArrayList();
        routElementes.add(zonaGeografica.getIdZonaGeografica());
        routeInformaUno(zonaGeografica, routElementes);
        
        Collections.reverse(routElementes);
        route.append(".");

        for (String routElemente : routElementes) {
            route.append(routElemente);
            route.append(".");
        }
        zonaGeografica.setRoute(route.toString());
    }

    public void routeInformaUno(ZonaGeografica zonaGeografica, List<String> reply) {
        if (zonaGeografica.getNivelInt() > 0) {
            reply.add(zonaGeografica.getIdPadreJerarquia());
        }
        if (zonaGeografica.getNivelInt() > 1) {
            ZonaGeografica zonaGeograficaPadre = zonaGeograficaService.findZonaGeografica(zonaGeografica.getIdPadreJerarquia());
            routeInformaUno(zonaGeograficaPadre, reply);
        }
    }
}
