package com.centraldecompras.rest.lgt.json;

import com.centraldecompras.rest.fin.json.PermisoJson;

public class ZonaProductoAsigJson {
    
 	String id;                                          // : 'hvkdkjsahjfjbvhfe',
        String method;                                      //:'POST/PUT/DELETE',
        boolean asignada;                                   // 1=true, 0= false
 	int fechaPrevistaActivacion;                        //:20141201, 
 	int fechaPrevistaDesactivacion;                     //:0, 
        String sociedadId;
        PermisoJson permiso;
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public String getSociedadId() {
        return sociedadId;
    }

    public void setSociedadId(String sociedadId) {
        this.sociedadId = sociedadId;
    }

    public boolean isAsignada() {
        return asignada;
    }

    public void setAsignada(boolean activada) {
        this.asignada = activada;
    }

    public PermisoJson getPermiso() {
        return permiso;
    }

    public void setPermiso(PermisoJson permiso) {
        this.permiso = permiso;
    }

}
