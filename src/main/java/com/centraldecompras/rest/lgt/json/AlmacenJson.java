package com.centraldecompras.rest.lgt.json;

import com.centraldecompras.rest.fin.json.ContactoJson;
import com.centraldecompras.rest.fin.json.DireccionJson;
import com.centraldecompras.rest.fin.*;
import java.util.List;

public class AlmacenJson {

    String id;
    String nombreAlmacen;
    String descripcion;
    String tipoAlmacen;
    String zonaGeograficaId;
    String sociedadId;
   
    String estado;
    String method;
    int fechaPrevistaActivacion;
    int fechaPrevistaDesactivacion;   

    List<ContactoJson> contactos;
    List<DireccionJson> direcciones;

    
    public AlmacenJson() {
    }

    public AlmacenJson(String id, String nombreAlmacen, String descripcion, String tipoAlmacen, String zonaGeograficaId, String sociedadId, String estado, String method, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion, List<ContactoJson> contactos, List<DireccionJson> direcciones) {
        this.id = id;
        this.nombreAlmacen = nombreAlmacen;
        this.descripcion = descripcion;
        this.tipoAlmacen = tipoAlmacen;
        this.zonaGeograficaId = zonaGeograficaId;
        this.sociedadId = sociedadId;
        this.estado = estado;
        this.method = method;
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
        this.contactos = contactos;
        this.direcciones = direcciones;
    }

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public String getNombreAlmacen() {
        return nombreAlmacen;
    }

    public void setNombreAlmacen(String nombreAlmacen) {
        this.nombreAlmacen = nombreAlmacen;
    }

    public String getTipoAlmacen() {
        return tipoAlmacen;
    }

    public void setTipoAlmacen(String tipoAlmacen) {
        this.tipoAlmacen = tipoAlmacen;
    }

    public String getZonaGeograficaId() {
        return zonaGeograficaId;
    }

    public void setZonaGeograficaId(String zonaGeograficaId) {
        this.zonaGeograficaId = zonaGeograficaId;
    }

    public String getSociedadId() {
        return sociedadId;
    }

    public void setSociedadId(String sociedadId) { 
        this.sociedadId = sociedadId;
    }

    public List<ContactoJson> getContactos() {
        return contactos;
    }

    public void setContactos(List<ContactoJson> contactos) {
        this.contactos = contactos;
    }

    public List<DireccionJson> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(List<DireccionJson> direcciones) {
        this.direcciones = direcciones;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
