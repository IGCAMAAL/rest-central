/* Copyright 2014, Javier Moreno.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 * And have fun ;-)
 */
package com.centraldecompras.rest.lgt.json;

import java.util.List;

/**
 *
 * @author Miguel
 */

/*
INSERT INTO `centralcomprasdemo`.`cc_lgt_zona_geografica`
 (`id`, `created`, `estado_registro`, `fecha_prevista_activacion`, `fecha_prevista_desactivacion`, 
`ultima_accion`, `descripcion`, `etiqueta`, `version`, `usuario_ultima_accion`) 
VALUES ('ZONA_GEOGRAFICA_UNIVERSAL', CURRENT_TIMESTAMP(),  'AC', 20140101,  0,  
'POST', 'Global', 'Tierra', '0', '0');
*/
public class ZonaGeograficaJson {
    
 	String id;                                          // : 'hvkdkjsahjfjbvhfe',
        String parent;                                      // :'0'
        int nivel;
 	String nombre;                                      //: 'Europe',
 	String estado;                                      //:'AC/DA',
        String method;                                      //:'POST/PUT',
 	int fechaPrevistaActivacion;                        //:20141201, 
 	int fechaPrevistaDesactivacion;                     //:0, 
        List<ZonaGeograficaJson> subzonas;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public List<ZonaGeograficaJson> getSubzonas() {
        return subzonas;
    }

    public void setSubzonas(List<ZonaGeograficaJson> subzonas) {
        this.subzonas = subzonas;
    }

}
