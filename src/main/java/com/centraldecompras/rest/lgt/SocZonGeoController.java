package com.centraldecompras.rest.lgt;

import com.centraldecompras.modelo.AlmacenUsuProdExcl;
import com.centraldecompras.rest.lgt.json.ZonaProductoAsigJson;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SocProductoExcl;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoService;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonaGeograficaService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.rest.lgt.ser.interfaces.SocZonGeoRestService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/lgt")
@Api(value = "Recursos publicos", description = "Demo de recursos accesibles pÃºblicamente sin autentificaciÃ³n.")
public class SocZonGeoController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocZonGeoController.class.getName());
 
    @Autowired
    private SocZonGeoService socZonGeoService;

    @Autowired
    private ZonaGeograficaService zonaGeograficaService;

    @Autowired
    private SociedadRplService sociedadRplService;

    @Autowired        
    private SocZonGeoRestService socZonGeoRestService;

    
    @RequestMapping(value = "/zonageograficaasigcon/{idSociedad}", method = RequestMethod.GET)
    @ApiOperation(value = "Devuelve las Zonas Geograficas asingadas a la Sociedad", notes = "Reutiliza ZonaProductoAsigJson, formato de comunicacion con cliente")
    List<ZonaProductoAsigJson> zonaGeograficaAsigCon(
            @PathVariable String idSociedad,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");
        List<ZonaProductoAsigJson> reply = new ArrayList<>();
        reply = getZonasGeoAsignadasASociedad(idSociedad);
        return reply;
    }

    @RequestMapping(value = "/zonageograficaasig", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Asignacion Zonas Geograficas", notes = "Reutiliza ZonaProductoAsigJson, formato de comunicacion con cliete")
    List<ZonaProductoAsigJson> zonaGeograficaAsig(
            @RequestBody List<ZonaProductoAsigJson> zonasProductosAsigJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ZonaProductoAsigJson> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();
        String sociedadId = null;

        //---------------------------------------------------
        //                     SocProductoExclude
        List<Object> createdObjets = new ArrayList();
        List<Object> updatedObjets = new ArrayList();
        List<Object> deletdObjets = new ArrayList();
        //---------------------------------------------------
        //---------------------------------------------------
        
        for (ZonaProductoAsigJson zonaProductoAsigJson : zonasProductosAsigJson) {
            String id = zonaProductoAsigJson.getId();
            String metodo = zonaProductoAsigJson.getMethod();
            sociedadId = zonaProductoAsigJson.getSociedadId();
            boolean activada = zonaProductoAsigJson.isAsignada();
            int fechaActivacion = zonaProductoAsigJson.getFechaPrevistaActivacion();
            int fechaDesactivacion = zonaProductoAsigJson.getFechaPrevistaDesactivacion();

            if (sociedadId == null || id == null) {
                throw new Exception("CentralCompras. CRUD asignacion Zonas geogrficas. La zona geografica o la sociedad tienen valor no valido");
            }

            if (metodo.equals("POST") || (metodo.equals("PUT") || metodo.equals("DELETE"))) {

                SocZonGeo socZonGeo = null;
                ZonaGeografica zonaGeografica = null;
                SociedadRpl sociedadRpl = null;

                zonaGeografica = zonaGeograficaService.findZonaGeografica(id);
                sociedadRpl = sociedadRplService.findSociedadRpl(sociedadId);
                if (zonaGeografica == null || sociedadRpl == null) {
                    throw new Exception("CentralCompras. CRUD asignacion Zonas geogrficas. Entidad zona geografica o Sociedad no encontrados en BD");
                }

                // 1.- Buscar que existe (Siempre es lo primero que hace, tanto para POST, PUT y DELETE)
                socZonGeo = socZonGeoService.findSocZonGeoBy_idZidS(id, sociedadId);

                // Si no existe y es POST crea UUID asi como el registro.
                Boolean socZonGeoExiste = Boolean.TRUE;
                if (socZonGeo == null && metodo.equals("POST")) {
                    socZonGeo = new SocZonGeo();
                    socZonGeo.setIdSocZonGeo(UUID.randomUUID().toString().replaceAll("-", ""));
                    socZonGeo.setAtributosAuditoria(new DatosAuditoria());
                    socZonGeoExiste = Boolean.FALSE;
                }

                if (socZonGeo != null) {
                    // Tanto si es nuevo registro como recuperado, se actualiza como si fuera nuevo.
                    if (metodo.equals(KApp.POST.getKApp())) {
                        socZonGeo.setSociedadid(sociedadId);
                        socZonGeo.setSociedad(sociedadRpl);
                        socZonGeo.setZonaGeograficaid(id);
                        socZonGeo.setZonaGeografica(zonaGeografica);

                        socZonGeo.getAtributosAuditoria().setCreated(new Date());
                        socZonGeo.getAtributosAuditoria().setUpdated(null);
                        socZonGeo.getAtributosAuditoria().setDeleted(null);

                        socZonGeo.getAtributosAuditoria().setUpdated(new Date());
                    }

                    socZonGeo.getAtributosAuditoria().setEstadoRegistro(KApp.ACTIVO.getKApp());
                    socZonGeo.getAtributosAuditoria().setFechaPrevistaActivacion(fechaActivacion);
                    socZonGeo.getAtributosAuditoria().setFechaPrevistaDesactivacion(fechaDesactivacion);

                    if (metodo.equals(KApp.DELETE.getKApp())) {
                        socZonGeo.getAtributosAuditoria().setDeleted(new Date());
                        socZonGeo.getAtributosAuditoria().setEstadoRegistro(KApp.DESACTIVADO.getKApp());
                        socZonGeo.getAtributosAuditoria().setFechaPrevistaActivacion(0);
                        socZonGeo.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
                    }

                    socZonGeo.setAsignada(activada);
                    socZonGeo.getAtributosAuditoria().setUltimaAccion(metodo);
                    socZonGeo.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

                    if(KApp.DELETE.getKApp().equals(metodo)){
                        metodo = KApp.PUT.getKApp();
                    }
                    
                    
                    if(socZonGeoExiste==Boolean.FALSE){
                        createdObjets.add(socZonGeo);
                    } else {
                        updatedObjets.add(socZonGeo);
                    }
                    
                    
                    //----------------------------------------------------------------------------------------------------------
                    //                     SocZonGeoExclude
                    String accion = "H"; // Herencia
                    if (!"DELETE".equals(socZonGeo.getAtributosAuditoria().getUltimaAccion())) {
                        if (socZonGeo.getAsignada()) {
                            accion = "A";  // Asigna
                        } else {
                            accion = "E"; // Excluye
                        }
                    }
                                                        //socProductoRestService
                    Map<String, Object> crdExcludesMap = socZonGeoRestService.crdExcludes(socZonGeo, accion);                            //  almacenRestService.crdExcludes(almacenUsuarioProducto, accion);
                    List<Object> createExcludes = (List<Object>) crdExcludesMap.get("createExcludes");
                    List<Object> updateExcludes = (List<Object>) crdExcludesMap.get("updateExcludes");
                    List<Object> deleteExcludes = (List<Object>) crdExcludesMap.get("deleteExcludes");
                    createdObjets.addAll(createExcludes);
                    updatedObjets.addAll(updateExcludes);
                    deletdObjets.addAll(deleteExcludes);
                    //----------------------------------------------------------------------------------------------------------

                }
            } else {
                throw new Exception("CentralCompras. CRUD zonaGeograficaJson.  Metodo aplicado no está soportado");
            }
        }


        socZonGeoRestService.persistencia(createdObjets,updatedObjets,deletdObjets);
        
        reply = getZonasGeoAsignadasASociedad(sociedadId);
        return reply;
    }

    @RequestMapping(value = "/zonageograficaasiglist/{maxResults}/{firstResult}", method = RequestMethod.GET)
    @ApiOperation(value = "Devuelve todas las Zonas Geograficas por Sociedad", notes = "Reutiliza ZonaProductoAsigJson, formato de comunicacion con cliente")
    List<ZonaProductoAsigJson> zonaGeograficaAsiglist(
            @PathVariable int maxResults,
            @PathVariable int firstResult,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ZonaProductoAsigJson> reply = new ArrayList<>();
        List<SocZonGeo> socZonGeos = socZonGeoService.findSocZonGeoEntities(true, maxResults, firstResult);

        for (SocZonGeo socZonGeo : socZonGeos) {
            if (!socZonGeo.getAtributosAuditoria().getEstadoRegistro().equals(KApp.DESACTIVADO.getKApp())) {
                reply.add(datosToJson(socZonGeo));
            }
        }
        return reply;
    }

    public List<ZonaProductoAsigJson> getZonasGeoAsignadasASociedad(String sociedadId) {
        log.info("....................................................................mbm............CONTROL");
                
        List<ZonaProductoAsigJson> reply = new ArrayList();
        List<SocZonGeo> socZonGeos = socZonGeoService.findSocZonGeoBy_idS(sociedadId);
        for (SocZonGeo socZonGeo : socZonGeos) {
            if (!socZonGeo.getAtributosAuditoria().getEstadoRegistro().equals(ElementEnum.KApp.DESACTIVADO.getKApp())) {
                reply.add(datosToJson(socZonGeo));
            }
        }
        return reply;
    }

    public ZonaProductoAsigJson datosToJson(SocZonGeo socZonGeo) {
        log.info("....................................................................mbm............CONTROL");
                
        ZonaProductoAsigJson reply = new ZonaProductoAsigJson();
        reply = new ZonaProductoAsigJson();
        reply.setId(socZonGeo.getZonaGeograficaid());
        reply.setSociedadId(socZonGeo.getSociedadid());
        reply.setAsignada(socZonGeo.getAsignada());
        reply.setFechaPrevistaActivacion(socZonGeo.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(socZonGeo.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setMethod(socZonGeo.getAtributosAuditoria().getUltimaAccion());
        return reply;
    }

/*

    public Map<String, Object> crdExcludes(SocZonGeo socZonGeo, String accion) throws Exception { 

        Map<String, Object> reply = new HashMap<>();

        List<SocProductoExcl> createExcludes = new ArrayList<>();
        List<SocProductoExcl> updateExcludes = new ArrayList<>();
        List<SocProductoExcl> deleteExcludes = new ArrayList<>();

        // La tabla SocProductoExcl contendrÃ¡ productos excluidos para una sociedad con la path completa hasta el Ãºltimo nivel.
        // PK para verificar existencia: sociedadId, y route: 
        //    CASOS:
        //    A) EXCLUSION: S-P. ExtracciÃ³n de todos los productos de Ãºltimo nivel que sean hijos de S-P con el atributo route (r) informado.
        //       1.- Hacer lo siguiente por cada hijo de Ãºlitmo nivel encontrado (p1, p2, p... pn):
        //          1a.- Verificar que la PK, (S-r) ya existe en la tabla.
        //          1b.- Cuando la PK NO existe, INSERTAR, e informar ademas productoId, nivInt del producto excluido
        //          1c.- Cuando la PK SI existe, informar en BBDD el productoId y nivInt del hijo (UPDATE cuando sea necesario)
        // 
        //    B) ASIGNACION: S-P. ExtracciÃ³n de todos los productos de Ãºltimo nivel que sean hijos de S-P con el atributo route (r) informado.
        //       1.- Hacer lo siguiente por cada hijo de Ãºlitmo nivel encontrado (p1, p2, p... pn):
        //          1a.- Verificar que la PK, (A-r) ya existe en la tabla.
        //          1b.- Cuando la PK NO existe, la asignaciÃ³n no tiene efectos sobre la tabla
        //          1c.- Cuando la PK SI existe, puede ocuarrir: 
        //               * Que productoId e intNiv asignado sea PADRE del Pk de la tabla --> No se toca porque estÃ¡ excluido con un hijo del asignado. 
        //               * Que productoId e intNiv asignado sea HIJO  del Pk de la tabla --> No se toca porque estÃ¡ excluido por estructura de arbol         
        //               * Que productoId e intNiv de tabla SI sea igual que productoId e intNiv asignado. Explorar arbol de padres.
        //                 - ExploraciÃ³n del arbol de padres. Cuando SI encuentra el primer padre excluido, UPDATE, informar en productoId e nivInt los valores del padre excluido
        //                 -                                   Cuando NO encuentra padre excluido, DELETE del registro.        
        // OK
        //    B) HERENCIA: S-P. Puedes aplicar la herencia a una EXCLUSION o a una ASIGNACION
        //       1.- Aplicar la herencia a una exclusion
        //          1a. Buscar exclusiones en el arbol de padres        
        //              **- Cuando SI encuentra exclusiÃ³n en el arbol de padres, actuar como una exclusion.
        //              **- Cuando NO encuentra exclusiÃ³n en el arbol de padres, revisa los hijos
        //                  
        //       2.- Aplicar la herencia a una asignaciÃ³n        
        //          1a. Buscar exclusiones en el arbol de padres        
        //              **- Cuando SI encuentra exclusiÃ³n en el arbol de padres, actuar como una asignaciÃ³n a productoId que tiene un padre con exclusiÃ³n.
        //              **- Cuando NO encuentra exclusiÃ³n en el arbol de padres, revis los hijoa
        // 
        //
        //
        String sociedadId = socZonGeo.getSociedad().getIdSociedad();
        List<ZonaGeografica> zonasGeograficas = zonaGeograficaService.findZonasGeograficasBy_idZ(socZonGeo.getZonaGeografica().getIdZonaGeografica());
        for (ZonaGeografica zonaGeografica : zonasGeograficas) { 
            SocProductoExcl socProductoExcludes = socProductoExcludesService.findSocProductoExcludesBy_idS_r(sociedadId, zonaGeografica.getRoute());
            Map<String, Object> aupeUD = new HashMap();

            if ("E".equals(accion)) {   // Exclusion
                if (socProductoExcludes != null) {
                    // Inoformar productoId y nivInt del hijo en el registro: UPDATE del regsitro si es necesario
                    if (socZonGeo.getProducto().getNivelInt() > socProductoExcludes.getNivelInt()) {
                        socProductoExcludes.setNivelInt(socZonGeo.getProducto().getNivelInt());
                        socProductoExcludes.setProductoId(socZonGeo.getProducto().getIdProducto());
                        updateExcludes.add(socProductoExcludes);
                    }
                } else {
                    // INSERT Registro de AlmacenUsuProdExcl
                    socProductoExcludes = new SocProductoExcl();

                    socProductoExcludes.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                    socProductoExcludes.setSociedadId(socZonGeo.getSociedad().getIdSociedad());
                    socProductoExcludes.setRoute(zonaGeografica.getRoute());
                    socProductoExcludes.setProductoId(zonaGeografica.getIdProducto());
                    socProductoExcludes.setNivelInt(socZonGeo.getProducto().getNivelInt());

                    createExcludes.add(socProductoExcludes);
                }

            } else if ("A".equals(accion)) {    // Asignacion
                if (socProductoExcludes != null) {
                    if (socZonGeo.getProducto().getNivelInt() > socProductoExcludes.getNivelInt()) {
                        buscaPadreExcluido(socZonGeo, socProductoExcludes, aupeUD);

                        if (aupeUD.containsKey("aupeU")) {
                            updateExcludes.add((SocProductoExcl) aupeUD.get("aupeU"));
                        }
                        if (aupeUD.containsKey("aupeD")) {
                            deleteExcludes.add((SocProductoExcl) aupeUD.get("aupeD"));
                        }
                    }
                }

            } else if ("H".equals(accion)) {                // Herencia
                if (socProductoExcludes != null) {
                    buscaPadreExcluido(socZonGeo, socProductoExcludes, aupeUD);

                    if (aupeUD.containsKey("aupeU")) {
                        updateExcludes.add((SocProductoExcl) aupeUD.get("aupeU"));
                    }
                    if (aupeUD.containsKey("aupeD")) {
                        deleteExcludes.add((SocProductoExcl) aupeUD.get("aupeD"));
                    }
                }
            };
        }
        reply.put("createExcludes", createExcludes);
        reply.put("updateExcludes", updateExcludes);
        reply.put("deleteExcludes", deleteExcludes);
        return reply;
    }

    private void buscaPadreExcluido(SocProducto _socProducto, SocProductoExcl socProductoExcludes, Map<String, Object> aupeUD) throws Exception {

        List<Boolean> padreExcluidoBoolean = new ArrayList(); //true-- Tiene padre excluido, false==No tiene padre excluido 
        List<Producto> padreExcluidoObject = new ArrayList();

        // ExploraciÃ³n de los padres
        // Si encuentra padre excluido --> informa almacenUsuarioProductoExcludes y reply = true
        // Si no encuentra padre excluido --> No informa almacenUsuarioProductoExcludes y reply = false 
        if (_socProducto.getProducto().getNivelInt() > 1) {
            recurrenteProductosPadre_Excluido(_socProducto, _socProducto.getProducto(), padreExcluidoBoolean, padreExcluidoObject);
        }
        if (padreExcluidoBoolean.size() > 0) {
            // Inoformar productoId y nivInt del padte en el registro: UPDATE del regsitro si es necesario
            if (padreExcluidoObject.size() > 0) {
                socProductoExcludes.setNivelInt((padreExcluidoObject.get(0)).getNivelInt());
                socProductoExcludes.setProductoId((padreExcluidoObject.get(0)).getIdProducto());
                aupeUD.put("aupeU", socProductoExcludes);

            } else {
                throw new Exception("Hay padre excluido pero no se envÃ­a objeto");
            }

        } else {
            // DELETE registro de AlmacenUsuProdExcl
            aupeUD.put("aupeD", socProductoExcludes);
        }
    }

    private void recurrenteProductosPadre_Excluido(SocProducto _socProducto, Producto _productoBuscado, List<Boolean> _padreExcluido, List<Producto> _padreExcluidoObject) {
        log.info("Buscamos.." + _productoBuscado.getIdPadreJerarquia());

        Producto productoPadreBuscado = productoService.findProducto(_productoBuscado.getIdPadreJerarquia());

        if (productoPadreBuscado != null && productoPadreBuscado.getNivelInt() > 0) {
            SocProducto socProducto = socProductoService.findSocProductoBy_idS_idP_Estado(
                    _socProducto.getSociedad().getIdSociedad(), 
                    productoPadreBuscado.getIdProducto(), 
                    ElementEnum.KApp.ACTIVO.getKApp());
            if (socProducto != null && false == socProducto.getAsignada()) {
                _padreExcluido.add(Boolean.TRUE);       // ESTO ES LO QUE BUSCO Y ENCUENTRO: asignacion para almacen/ usuario / producto, que esta excluida (AC no asignado (false)) 

                _padreExcluidoObject.add(productoPadreBuscado);
            } else {
                recurrenteProductosPadre_Excluido(_socProducto, productoPadreBuscado, _padreExcluido, _padreExcluidoObject);
            }
        }
    }

*/

}
