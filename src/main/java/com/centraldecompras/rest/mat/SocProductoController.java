package com.centraldecompras.rest.mat;

import com.centraldecompras.modelo.AlmacenUsuProdExcl;
import com.centraldecompras.rest.prm.ser.interfaces.EtiquetaRestService;
import com.centraldecompras.rest.lgt.json.ZonaProductoAsigJson;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.EtiquetaLang;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.ProductoEtiqueta;
import com.centraldecompras.modelo.ProductoLang;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SocProductoAvatar;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoEtiquetaService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoLangService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoAvatarService;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoService;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaService;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.rest.fin.json.PermisoJson;
import com.centraldecompras.rest.mat.json.ProductoArbolJson;
import com.centraldecompras.rest.mat.ser.interfaces.ProductoRestService;
import com.centraldecompras.rest.mat.ser.interfaces.SocProductoRestService;
import com.centraldecompras.rest.prm.json.EtiquetaAsigJson;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mat")
@Api(value = "Recursos publicos", description = "Demo de recursos accesibles publicamente sin autentificacion.")
public class SocProductoController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocProductoController.class.getName());

    @Autowired
    private SocProductoService socProductoService;

    @Autowired
    private ProductoService productoService;

    @Autowired
    private ProductoRestService productoRestService;

    @Autowired
    private SociedadRplService sociedadRplService;

    @Autowired
    private EtiquetaService etiquetaService;

    @Autowired
    private ProductoEtiquetaService productoEtiquetaService;

    @Autowired
    private EtiquetaRestService productoEtiquetaRestService;

    @Autowired
    private EtiquetaLangService etiquetaLangService;

    @Autowired
    private SocProductoRestService socProductoRestService;

    @Autowired
    private SocProductoAvatarService socProductoAvatarService;

    @Autowired
    private ProductoLangService productoLangService;

    @RequestMapping(value = "/productoarbolasigcon/{idSociedad}", method = RequestMethod.GET)
    @ApiOperation(value = "Devuelve los Productos asig a la Sociedad", notes = "Reutiliza ZonaProductoAsigJson, formato de comunicacion con cliente")
    List<ZonaProductoAsigJson> getProductoAsigList(
            @PathVariable String idSociedad,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ZonaProductoAsigJson> reply = new ArrayList<>();
        reply = getProductosAsignadosASociedad(idSociedad);
        return reply;
    }

    @RequestMapping(value = "/productoarbolasig", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Asignacion del Arbol de productos", notes = "Reutiliza ZonaProductoAsigJson, formato de comunicacion con cliete")
    List<ZonaProductoAsigJson> postProductoAsig(
            @RequestBody List<ZonaProductoAsigJson> zonasProductosAsigJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ZonaProductoAsigJson> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();
        String sociedadId = null;
        try {
            
            //---------------------------------------------------
            //                     SocProductoExclude
            List<Object> createdObjets = new ArrayList();
            List<Object> updatedObjets = new ArrayList();
            List<Object> deletdObjets = new ArrayList();
            //---------------------------------------------------
            //---------------------------------------------------

            for (ZonaProductoAsigJson zonaProductoAsigJson : zonasProductosAsigJson) {
                String id = zonaProductoAsigJson.getId();
                sociedadId = zonaProductoAsigJson.getSociedadId();
                String metodo = zonaProductoAsigJson.getMethod();

                if (sociedadId == null || id == null) {
                    throw new Exception("CentralCompras. CRUD asignacion Productos. El producto o la sociedad tienen valor no valido");
                }

                if (metodo.equals(KApp.POST.getKApp()) || (metodo.equals(KApp.PUT.getKApp()) || metodo.equals(KApp.DELETE.getKApp()))) {

                    SociedadRpl sociedadRpl = null;
                    sociedadRpl = sociedadRplService.findSociedadRpl(sociedadId);
                    if (sociedadRpl == null) {
                        Object[] valores = {sociedadId};
                        String mensaje = MessageFormat.format("ERROR: CRUD asignacion Productos. Sociedad {0} no encontrada", valores);
                        throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                    }

                    Producto producto = null;
                    producto = productoService.findProducto(id);
                    if (producto == null) {
                        Object[] valores = {id};
                        String mensaje = MessageFormat.format("ERROR: CRUD asignacion Productos. Producot {0} no encontrada", valores);
                        throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                    }

                    // 1.- Buscar que existe (Siempre es lo primero que hace, tanto para POST, PUT y DELETE)
                    SocProducto socProducto = null;
                    socProducto = socProductoService.findSocProductoBy_idPidS(id, zonaProductoAsigJson.getSociedadId());

                    // Si no existe y es POST crea UUID asi como el registro.
                    Boolean socProductoExiste = Boolean.TRUE;
                    if (socProducto == null) {
                        if (zonaProductoAsigJson.getMethod().equals("POST")) {
                            socProducto = new SocProducto();
                            socProducto.setIdSocProducto(UUID.randomUUID().toString().replaceAll("-", ""));
                            socProducto.setAtributosAuditoria(new DatosAuditoria());
                            socProductoExiste = Boolean.FALSE;
                        } else {
                            Object[] valores = {zonaProductoAsigJson.getSociedadId(), id};
                            String mensaje = MessageFormat.format("ERROR: Asignacion sociedad {0}, producto {1} no encontrada. ", valores);
                            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                        }
                    }

                    if (socProducto != null) {

                        socProducto = creaSocProducto(socProducto, zonaProductoAsigJson, idUser);
                        socProducto.setProducto(producto);
                        socProducto.setSociedad(sociedadRpl);

                        if (KApp.DELETE.getKApp().equals(metodo)) {
                            metodo = KApp.PUT.getKApp();
                        }

                        if (socProductoExiste == Boolean.FALSE) {
                            createdObjets.add(socProducto);
                        } else {
                            updatedObjets.add(socProducto);
                        }

                    //----------------------------------------------------------------------------------------------------------
                        //                     SocProductoExclude
                        String accion = "H"; // Herencia
                        if (!"DELETE".equals(socProducto.getAtributosAuditoria().getUltimaAccion())) {
                            if (socProducto.getAsignada()) {
                                accion = "A";  // Asigna
                            } else {
                                accion = "E"; // Excluye
                            }
                        }

                        Map<String, Object> crdExcludesMap = socProductoRestService.crdExcludes(socProducto, accion);                            //  almacenRestService.crdExcludes(almacenUsuarioProducto, accion);
                        List<AlmacenUsuProdExcl> createExcludes = (List<AlmacenUsuProdExcl>) crdExcludesMap.get("createExcludes");
                        List<AlmacenUsuProdExcl> updateExcludes = (List<AlmacenUsuProdExcl>) crdExcludesMap.get("updateExcludes");
                        List<AlmacenUsuProdExcl> deleteExcludes = (List<AlmacenUsuProdExcl>) crdExcludesMap.get("deleteExcludes");
                        createdObjets.addAll(createExcludes);
                        updatedObjets.addAll(updateExcludes);
                        deletdObjets.addAll(deleteExcludes);
                        //----------------------------------------------------------------------------------------------------------

                        listaObjetos.addAll(avatarGestion(socProducto)); // TODO: ELIMINAR EN 2ºFASE, Y SUSTITUIR CON EXCLUDES Y STRING LINEAL DEL ARBOL DE PRODUCTOS

                    }
                } else {
                    throw new Exception("CentralCompras. CRUD zonaGeograficaJson.  Metodo aplicado no está soportado");
                }
            }

            socProductoRestService.persistencia(createdObjets, updatedObjets, deletdObjets, listaObjetos);

        } catch (Exception ex) {
            throw (ex);
        }
        reply = getProductosAsignadosASociedad(sociedadId);
        return reply;
    }

    @RequestMapping(value = "/productoarbolasiglist/{maxResults}/{firstResult}", method = RequestMethod.GET)
    @ApiOperation(value = "Devuelve todas los Productos por Sociedad", notes = "Reutiliza ZonaProductoAsigJson, formato de comunicacion con cliente")
    List<ZonaProductoAsigJson> getProductoAsigList(
            @PathVariable int maxResults,
            @PathVariable int firstResult,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");
        List<ZonaProductoAsigJson> reply = new ArrayList<>();
        List<SocProducto> socProductos = socProductoService.findSocProductoEntities(true, maxResults, firstResult);

        for (SocProducto socProducto : socProductos) {
            if (!socProducto.getAtributosAuditoria().getEstadoRegistro().equals(KApp.DESACTIVADO.getKApp())) {
                reply.add(datosToJson(socProducto));
            } else {
            }
        }
        return reply;
    }

    @RequestMapping(value = "/productoarbolasig/seleccion/{cadena}/{estado}/{cantidad}/{niveles}/{idioma}/{idSociedad}", method = RequestMethod.GET)
    List<ProductoArbolJson> seleccionProducto(
            @PathVariable String cadena,
            @PathVariable String estado,
            @PathVariable String cantidad,
            @PathVariable String niveles,
            @PathVariable String idioma,
            @PathVariable String idSociedad,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ProductoArbolJson> reply = new ArrayList<ProductoArbolJson>();
        List<String[]> prodS = new ArrayList<String[]>();

        SociedadRpl sociedadRpl = sociedadRplService.findSociedadRpl(idSociedad);
        if (sociedadRpl == null) {
            Object[] valores = {idSociedad};
            String mensaje = MessageFormat.format("ERROR: productoarbolasig: Sociedad {0}, no encontrada. ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        int cantidadInt = 0;
        try {
            cantidadInt = Integer.parseInt(cantidad);
        } catch (NumberFormatException ex) {
            throw new Exception("CentralCompras. Combo ProductoArbolAsig. Parametro 'cantidad' de resultados no es valido : " + ex.getMessage());
        }

        if (cadena == null || cadena.equals("")) {
            throw new Exception("CentralCompras. Combo ProductoArbolAsig. Parametro 'cadena' no es valido");
        }

        StringTokenizer st2 = new StringTokenizer(niveles, "-");
        List<Integer> nivelesList = new ArrayList<Integer>();

        try {
            while (st2.hasMoreElements()) {
                String niv = (String) st2.nextElement();
                nivelesList.add(Integer.valueOf(niv));
            }
        } catch (NumberFormatException ex) {
            throw new Exception("CentralCompras. Combo ProductoArbolAsig. Parametro 'niveles' contiene datos no validos");
        } catch (Exception ex) {
            throw new Exception("CentralCompras. Combo ProductoArbolAsig. Parametro 'niveles' contiene datos no validos");
        }

        //Simulacion****************** Igual que zonas geograficas 
        if (reply.size() < cantidadInt) {
            prodS = socProductoService.findSocProductoTypeAhead(1, cadena, estado.toUpperCase(), cantidadInt, nivelesList, idioma, idSociedad);
            productoRestService.seleccionProductosCargaArray(reply, prodS);
        }
        if (reply.size() < cantidadInt) {
            prodS = socProductoAvatarService.findSocProductoAvatarTypeAhead(1, cadena, estado.toUpperCase(), cantidadInt, nivelesList, idioma, idSociedad);
            productoRestService.seleccionProductosCargaArray(reply, prodS);
        }

        if (reply.size() < cantidadInt) {
            prodS = socProductoService.findSocProductoTypeAhead(2, cadena, estado.toUpperCase(), cantidadInt - reply.size(), nivelesList, idioma, idSociedad);
            productoRestService.seleccionProductosCargaArray(reply, prodS);
        }
        if (reply.size() < cantidadInt) {
            prodS = socProductoAvatarService.findSocProductoAvatarTypeAhead(2, cadena, estado.toUpperCase(), cantidadInt - reply.size(), nivelesList, idioma, idSociedad);
            productoRestService.seleccionProductosCargaArray(reply, prodS);
        }

        if (!reply.isEmpty()) {
            for (ProductoArbolJson reply1 : reply) {
                ProductoArbolJson productoArbolJson = (ProductoArbolJson) reply1;
                reply1.setTraducciones(new ArrayList<TraduccionDescripcion>());
                List<ProductoLang> productosLang = productoLangService.findProductoLangBy_IdP(productoArbolJson.getId());
                for (ProductoLang productoLang : productosLang) {
                    TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
                    traduccionDescripcion.setIdioma(productoLang.getIdioma());
                    traduccionDescripcion.setTraduccion(productoLang.getTraduccionDesc());
                    reply1.getTraducciones().add(traduccionDescripcion);
                }
            }
        }
        return reply;

    }

    @RequestMapping(value = "/productoetiquetaasig", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Asignacion de Etiquetas al Arbol de productos", notes = "Reutiliza ZonaProductoAsigJson, formato de comunicacion con cliete")
    List<EtiquetaAsigJson> postProductoEtiquetaAsig(
            @RequestBody List<EtiquetaAsigJson> etiquetasAsigJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");
        List<EtiquetaAsigJson> reply = new ArrayList();
        List<Object[]> datosMontadosProductoEtiqueta = new ArrayList();

        for (EtiquetaAsigJson etiquetaAsigJson : etiquetasAsigJson) {

            String metodo = etiquetaAsigJson.getMethod();
            Object[] aaa = new Object[5];

            Producto producto = null;
            Etiqueta etiqueta = null;

            producto = productoService.findProducto(etiquetaAsigJson.getId());
            etiqueta = etiquetaService.findEtiqueta(etiquetaAsigJson.getEtiquetaId());
            if (producto == null || etiqueta == null) {
                throw new Exception("CentralCompras. CRUD asignacion Etqueta a Productos. Entidad Producto o Etiquetga no encontrados en BD");
            }

            List<String> etiquetaObj = new ArrayList();
            etiquetaObj.add(etiquetaAsigJson.getMethod());
            etiquetaObj.add(etiquetaAsigJson.getEtiquetaId());
            aaa = productoRestService.montaProductoEtiqueta(producto, etiquetaObj, idUser, metodo);
            datosMontadosProductoEtiqueta.add(aaa);

        }

        productoEtiquetaRestService.persistencia(datosMontadosProductoEtiqueta);

        List<ProductoEtiqueta> productosEtiquetas = productoEtiquetaService.findProductoEtiquetaEntities();
        reply = productosEtiquetasToJson(productosEtiquetas);

        return reply;
    }

    @RequestMapping(value = "/productoetiquetaasig/{idProducto}", method = RequestMethod.GET)
    @ApiOperation(value = "Devuelve los ProductosEtiquetas asig a un producto", notes = "Reutiliza EtiquetaAsigJson, formato de comunicacion con cliente")
    List<EtiquetaAsigJson> getProductoEtiquetaAsigList(
            @PathVariable String idProducto,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<EtiquetaAsigJson> reply = new ArrayList();
        List<ProductoEtiqueta> productosEtiquetas = null;
        if (idProducto.equals("0")) {
            productosEtiquetas = productoEtiquetaService.findProductoEtiquetaEntities();
        } else {
            productosEtiquetas = productoEtiquetaService.findEtiquetasBy_idP(idProducto);
        }
        reply = productosEtiquetasToJson(productosEtiquetas);

        return reply;
    }

    private List<Object[]> avatarGestion(SocProducto socProducto) {
        List<Object[]> reply = new ArrayList<>();

        /*
         -- Sql de comprobación de la gestion de avatar
         SELECT 'ava' as ori, traduccion_desc, nivel_int, asignada, "" , producto_id as prod, id_padre_jerarquia 
         FROM centralcomprasdemo.cc_mat_soc_producto_avatar a
         join centralcomprasdemo.cc_mat_producto_lang b
         on a.producto_id = b.id

         union 

         SELECT 'asg' as ori, traduccion_desc, nivel_int, asignada, a.estado_registro, productoid as prod, padre_jerarquia	 
         FROM centralcomprasdemo.cc_mat_soc_producto a
         join centralcomprasdemo.cc_mat_producto_lang b
         join centralcomprasdemo.cc_mat_producto c
         on   a.productoid = b.id
         and  a.productoid = c.id

         where b.idioma = 'es' 
         order by  nivel_int, ori
         ;
        
         */
        if (socProducto.getAsignada()) {

            /* *****************************************************************
             Gestion de Avatar por ASIGNACION. 
             ***************************************************************** */
            /* -----------------------------------------------------------------
             Avatar Propio

             Eliminación del propio avatar, siempre que no tenga un avatar de exclusión por herencia.
             Delete de SocProductoAvatar del propio avatar con Soc, Prod. (Se elimina en todos los idiomas) */
            SocProductoAvatar socProdcutoPadreAvatar = socProductoAvatarService.findSocProductoAvatarBy_idS_idP(socProducto.getSociedadIdk(), socProducto.getProductoIdk());
            if (socProdcutoPadreAvatar != null && socProdcutoPadreAvatar.getAsignada()) {
                Object[] aaa = new Object[6];
                aaa[0] = KApp.DELETE.getKApp();
                aaa[1] = "socProductoAvatar";
                aaa[2] = null;
                aaa[3] = socProducto.getSociedadIdk();
                aaa[4] = socProducto.getProductoIdk();
                aaa[5] = null;
                reply.add(aaa);
            }

            /* -----------------------------------------------------------------
             Avatar de Padres

             Gestión de avatar de los padres. Si no existe un padre con exclusión, creo avatar de todos los padres.
             Método recursivo para buaqueda de padres. Identificación de padre con exclusion:
             Si excludido encontrado: no crea avatar
             Si excluido no encontrado: Crea avatar de todos los padres. */
            Boolean padreExcluido = avatarExcluyePadresRecurrente(socProducto.getProductoIdk(), socProducto.getSociedadIdk());
            List<Object[]> bbb = new ArrayList();
            if (!padreExcluido) {
                avatarCreaPadreRecurrente(socProducto.getProducto().getIdPadreJerarquia(), socProducto.getSociedadIdk(), true, bbb);
                reply.addAll(bbb);
            }

            /* -----------------------------------------------------------------
             Avatar de Hijos

             Gestión de avatar de los hijos. Crea los avatar de los hijos hasta que encuentre un hijo excluido. A partir de este excluido, ya no crea ningun avatar mas.
             Método recursivo para busqueda de hijos. 
             Si excludido encontrado: no crea avatar y parar.
             Si excluido no encontrado, crear avatar de asignacion y tratar siguiente hijo. En todos los idiomas. */
            List<Object[]> ccc = new ArrayList();
            avatarCreaHijoRecurrente(socProducto.getProducto(), socProducto.getSociedadIdk(), true, ccc);
            reply.addAll(ccc);

        } else {

            if (socProducto.getAtributosAuditoria().getEstadoRegistro().equals(KApp.ACTIVO.getKApp())) {
                /* *************************************************************
                 Gestion de Avatar por EXCLUSION. 
                 ************************************************************* */

                /* -------------------------------------------------------------
                 Avatar Propio

                 Eliminación del propio avatar. SIEMPRE
                 Delete de SocProductoAvatar del propio avatar con Soc, Prod. */
                Object[] aaa = new Object[6];
                aaa[0] = KApp.DELETE.getKApp();
                aaa[1] = "socProductoAvatar";
                aaa[2] = null;
                aaa[3] = socProducto.getSociedadIdk();
                aaa[4] = socProducto.getProductoIdk();
                aaa[5] = null;
                reply.add(aaa);

                /* -------------------------------------------------------------
                 Avatar de Padres                                             
                 No hay gestión.                                              */
                /* -------------------------------------------------------------
                 Avatar de Hijos

                 Gestión de avatar de los hijos.Crea avatar de exclusion de aquellos hijos que no tenga avatar de exclusion.
                 Método recursivo para busqueda de hijos. 
                 Si excludido encontrado: no crea avatar y parar.
                 Si excluido no encontrado, crear avatar de exclusion y tratar siguiente hijo. */
                List<Object[]> ddd = new ArrayList();
                avatarCreaHijoRecurrente(socProducto.getProducto(), socProducto.getSociedadIdk(), false, ddd);
                reply.addAll(ddd);

            } else {
                /* *************************************************************
                 Gestion de Avatar por HERENCIA. 
                 ************************************************************* */

                /* -------------------------------------------------------------
                 Avatar Propio                                                
                
                 Crear avatar propio, según el estado del padre (desde socProd o avatar)    */
                List<Object[]> eee = avatarCreaPropioHerencia(socProducto);
                SocProductoAvatar socProductoAvatarHerencia = (SocProductoAvatar) eee.get(0)[2];
                reply.addAll(eee);

                /* -------------------------------------------------------------
                 Avatar de Padres                                             
                 No hay gestión.                                              */
                /* -------------------------------------------------------------
                 Avatar de Hijos                                              
                
                 Gestión de avatar de los hijos. No hay gestión.
                 Método recursivo para busqueda de hijos. 
                 Si excludido encontrado: no crea avatar y parar.
                 Si excluido no encontrado, crear avatar de exclusion y tratar siguiente hijo. */
                if (eee.size() == 0) {
                    // Es el primer padre, por lo que hay que borrar todos los avatar hijo hasta encontrar un socHijo (asignado o excluido)
                    List<Object[]> fff = new ArrayList();
                    avatarEliminaHijosTodos(socProducto.getProducto(), socProducto.getSociedadIdk(), fff);
                    reply.addAll(fff);
                } else {
                    List<Object[]> ggg = new ArrayList();
                    avatarCreaHijoRecurrente(socProducto.getProducto(), socProducto.getSociedadIdk(), socProductoAvatarHerencia.getAsignada(), ggg);
                    reply.addAll(ggg);
                }
            }
        }

        return reply;

    }

    private void avatarEliminaHijosTodos(Producto producto, String sociedadId, List<Object[]> listAvatar) {
        log.info("....................................................................mbm............CONTROL");

        List<Producto> productos = productoService.findProductosBy_ParentId(producto);

        for (Producto productoEle : productos) {

            SocProductoAvatar socProductoHijoAvatar = socProductoAvatarService.findSocProductoAvatarBy_idS_idP(sociedadId, productoEle.getIdProducto());
            SocProducto socProductoHijo = socProductoService.findSocProductoBy_idPidS_Estado(productoEle.getIdProducto(), sociedadId, KApp.ACTIVO.getKApp());
            // MBM 1 Leidos solo los activos

            if (socProductoHijoAvatar != null) {
                Object[] aaa = new Object[6];
                aaa[0] = KApp.DELETE.getKApp();
                aaa[1] = "socProductoAvatar";
                aaa[2] = null;
                aaa[3] = socProductoHijoAvatar.getSociedadId();
                aaa[4] = socProductoHijoAvatar.getProductoId();
                aaa[5] = null;
                listAvatar.add(aaa);
            }

            if (socProductoHijo == null) {
                if (productoEle.getNivelInt() > 0) {
                    avatarEliminaHijosTodos(productoEle, sociedadId, listAvatar);
                }
            }

        }
    }

    private List<Object[]> avatarCreaPropioHerencia(SocProducto socProducto) {
        log.info("....................................................................mbm............CONTROL");

        List<Object[]> reply = new ArrayList<>();

        Boolean asignadaHerencia = null;
        Producto productoPadreHerencia = productoService.findProducto(socProducto.getProducto().getIdPadreJerarquia());

        if (productoPadreHerencia != null) {

            // MBM 2 Leidos solo los activos
            SocProducto socProductoPadreHerencia = socProductoService.findSocProductoBy_idPidS_Estado(productoPadreHerencia.getIdProducto(), socProducto.getSociedadIdk(), KApp.ACTIVO.getKApp());
            if (socProductoPadreHerencia != null) {
                asignadaHerencia = socProductoPadreHerencia.getAsignada();
            } else {
                SocProductoAvatar socProdcutoPadreAvatar = socProductoAvatarService.findSocProductoAvatarBy_idS_idP(socProducto.getSociedadIdk(), productoPadreHerencia.getIdProducto());
                asignadaHerencia = socProdcutoPadreAvatar.getAsignada();
            }

            String method = null;
            // Creacion o actualización del avatar
            SocProductoAvatar socProductoAvatar = socProductoAvatarService.findSocProductoAvatarBy_idS_idP(socProducto.getSociedadIdk(), socProducto.getProductoIdk());
            if (socProductoAvatar != null) {
                socProductoAvatar.setAsignada(asignadaHerencia);
                method = KApp.PUT.getKApp();
            } else {
                socProductoAvatar = new SocProductoAvatar();
                socProductoAvatar.setAsignada(asignadaHerencia);
                socProductoAvatar.setIdPadreJerarquia(socProducto.getProducto().getIdPadreJerarquia());
                socProductoAvatar.setNivelInt(socProducto.getProducto().getNivelInt());
                socProductoAvatar.setProductoId(socProducto.getProductoIdk());
                socProductoAvatar.setSociedadId(socProducto.getSociedadIdk());
                method = KApp.POST.getKApp();
            }

            if (socProductoAvatar != null) {
                Object[] aaa = new Object[6];
                aaa[0] = method;
                aaa[1] = "socProductoAvatar";
                aaa[2] = socProductoAvatar;
                aaa[3] = null;
                aaa[4] = null;
                aaa[5] = null;
                reply.add(aaa);
            }
        }

        return reply;
    }

    private Boolean avatarExcluyePadresRecurrente(String productoPadreId, String sociedadId) {
        log.info("....................................................................mbm............CONTROL");

        // Invwestiga si existe en la asignación a aociedad algún producto padre con exclusion. Devuelve true si lo encuentra.
        Boolean reply = false;

        // MBM 3 Leidos solo los activos
        SocProducto socProdcuto = socProductoService.findSocProductoBy_idPidS_Estado(productoPadreId, sociedadId, KApp.ACTIVO.getKApp());
        if (socProdcuto != null && !socProdcuto.getAsignada()) {
            reply = true;
        }

        Producto producto = productoService.findProducto(productoPadreId);
        if (producto.getNivelInt() > 0 && !reply) {
            avatarExcluyePadresRecurrente(producto.getIdPadreJerarquia(), sociedadId);
        }

        return reply;
    }

    private void avatarCreaPadreRecurrente(String productoPadreId, String sociedadId, Boolean asignado, List<Object[]> listAvatar) {
        log.info("....................................................................mbm............CONTROL");

        // Crea avatar de asignación de los padres.
        Producto producto = productoService.findProducto(productoPadreId);

        SocProductoAvatar socProdcutoPadreAvatar = socProductoAvatarService.findSocProductoAvatarBy_idS_idP(sociedadId, producto.getIdProducto());
        SocProducto socProductoPadre = socProductoService.findSocProductoBy_idPidS_Estado(producto.getIdProducto(), sociedadId, KApp.ACTIVO.getKApp());
        // MBM 4 Leidos solo los activos

        if (socProdcutoPadreAvatar == null && socProductoPadre == null) {
            SocProductoAvatar socProductoAvatar = new SocProductoAvatar();
            socProductoAvatar.setAsignada(asignado);
            socProductoAvatar.setIdPadreJerarquia(producto.getIdPadreJerarquia());
            socProductoAvatar.setNivelInt(producto.getNivelInt());
            socProductoAvatar.setProductoId(producto.getIdProducto());
            socProductoAvatar.setSociedadId(sociedadId);

            Object[] aaa = new Object[6];
            aaa[0] = KApp.POST.getKApp();
            aaa[1] = "socProductoAvatar";
            aaa[2] = socProductoAvatar;
            aaa[3] = null;
            aaa[4] = null;
            aaa[5] = null;
            listAvatar.add(aaa);
        }

        if (producto.getNivelInt() > 0) {
            avatarCreaPadreRecurrente(producto.getIdPadreJerarquia(), sociedadId, true, listAvatar);
        }

    }

    private void avatarCreaHijoRecurrente(Producto producto, String sociedadId, Boolean asignado, List<Object[]> listAvatar) {
        log.info("....................................................................mbm............CONTROL");

        // Crea avatar de asignación de los hijos.
        List<Producto> productos = productoService.findProductosBy_ParentId(producto);

        for (Producto productoEle : productos) {
            Boolean exclusionExiste = false;

            SocProductoAvatar socProductoHijoAvatar = socProductoAvatarService.findSocProductoAvatarBy_idS_idP(sociedadId, productoEle.getIdProducto());
            SocProducto socProductoHijo = socProductoService.findSocProductoBy_idPidS_Estado(productoEle.getIdProducto(), sociedadId, KApp.ACTIVO.getKApp());
            // MBM 5 Leidos solo los activos

            if (socProductoHijo == null && socProductoHijoAvatar == null) {
                SocProductoAvatar socProductoAvatar = new SocProductoAvatar();
                socProductoAvatar.setAsignada(asignado);
                socProductoAvatar.setIdPadreJerarquia(productoEle.getIdPadreJerarquia());
                socProductoAvatar.setNivelInt(productoEle.getNivelInt());
                socProductoAvatar.setProductoId(productoEle.getIdProducto());
                socProductoAvatar.setSociedadId(sociedadId);

                Object[] aaa = new Object[6];
                aaa[0] = KApp.POST.getKApp();
                aaa[1] = "socProductoAvatar";
                aaa[2] = socProductoAvatar;
                aaa[3] = null;
                aaa[4] = null;
                aaa[5] = null;
                listAvatar.add(aaa);
            }

            if (socProductoHijo == null && socProductoHijoAvatar != null) {
                if (!socProductoHijoAvatar.getAsignada().equals(asignado)) {

                    socProductoHijoAvatar.setAsignada(asignado);
                    Object[] aaa = new Object[6];

                    aaa[0] = KApp.PUT.getKApp();
                    aaa[1] = "socProductoAvatar";
                    aaa[2] = socProductoHijoAvatar;
                    aaa[3] = null;
                    aaa[4] = null;
                    aaa[5] = null;
                    listAvatar.add(aaa);
                }
            }

            // asignado = true, --> crea avatar de asignacion. 
            // asignado = false --> crea avatar de exclusion. 
            if (socProductoHijo != null && socProductoHijo.getAtributosAuditoria().getEstadoRegistro().equals(KApp.ACTIVO.getKApp()) && !socProductoHijo.getAsignada()) {
                if (asignado) {
                    exclusionExiste = true;
                }
            }

            if (productoEle.getNivelInt() > 0 && !exclusionExiste) {
                avatarCreaHijoRecurrente(productoEle, sociedadId, asignado, listAvatar);
            }
        }
    }

    private SocProducto creaSocProducto(SocProducto socProducto, ZonaProductoAsigJson zonaProductoAsigJson, String idUser) {
        log.info("....................................................................mbm............CONTROL");

        if (zonaProductoAsigJson.getMethod().equals(KApp.POST.getKApp())) {
            socProducto.setSociedadIdk(zonaProductoAsigJson.getSociedadId());
            socProducto.setSociedad(socProducto.getSociedad());
            socProducto.setProductoIdk(zonaProductoAsigJson.getId());
            socProducto.setProducto(socProducto.getProducto());

            socProducto.getAtributosAuditoria().setCreated(new Date());
            socProducto.getAtributosAuditoria().setUpdated(null);
            socProducto.getAtributosAuditoria().setDeleted(null);

            socProducto.getAtributosAuditoria().setUpdated(new Date());
        }

        socProducto.getAtributosAuditoria().setEstadoRegistro(KApp.ACTIVO.getKApp());
        socProducto.getAtributosAuditoria().setFechaPrevistaActivacion(zonaProductoAsigJson.getFechaPrevistaActivacion());
        socProducto.getAtributosAuditoria().setFechaPrevistaDesactivacion(zonaProductoAsigJson.getFechaPrevistaDesactivacion());

        if (zonaProductoAsigJson.getMethod().equals(KApp.DELETE.getKApp())) {
            socProducto.getAtributosAuditoria().setDeleted(new Date());
            socProducto.getAtributosAuditoria().setEstadoRegistro(KApp.DESACTIVADO.getKApp());
            socProducto.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            socProducto.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
        }

        socProducto.setAsignada(zonaProductoAsigJson.isAsignada());
        socProducto.getAtributosAuditoria().setUltimaAccion(zonaProductoAsigJson.getMethod());
        socProducto.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

        socProducto.setCargaDirectaArticulos(zonaProductoAsigJson.getPermiso().getCargaDirectaArticulos());
        socProducto.setModPreciosAlta(zonaProductoAsigJson.getPermiso().getModPreciosAlta());
        socProducto.setModPreciosBaja(zonaProductoAsigJson.getPermiso().getModPreciosBaja());

        socProducto.setPorcentajeMaxAlta((new BigDecimal(zonaProductoAsigJson.getPermiso().getPorcentajeMaxAlta())).setScale(2, RoundingMode.HALF_EVEN));
        socProducto.setPorcentajeMaxBaja((new BigDecimal(zonaProductoAsigJson.getPermiso().getPorcentajeMaxBaja())).setScale(2, RoundingMode.HALF_EVEN));
        socProducto.setRappel((new BigDecimal(zonaProductoAsigJson.getPermiso().getRappel())).setScale(2, RoundingMode.HALF_EVEN));

        return socProducto;
    }

    public List<ZonaProductoAsigJson> getProductosAsignadosASociedad(String sociedadId) {
        List<ZonaProductoAsigJson> reply = new ArrayList();
        List<SocProducto> socProductos = socProductoService.findSocProductoBy_idS(sociedadId);
        for (SocProducto socProducto : socProductos) {
            if (!socProducto.getAtributosAuditoria().getEstadoRegistro().equals(ElementEnum.KApp.DESACTIVADO.getKApp())) {
                reply.add(datosToJson(socProducto));
            }
        }
        return reply;
    }

    public ZonaProductoAsigJson datosToJson(SocProducto socProducto) {
        log.info("....................................................................mbm............CONTROL");

        ZonaProductoAsigJson reply = new ZonaProductoAsigJson();
        reply = new ZonaProductoAsigJson();
        reply.setId(socProducto.getProductoIdk());
        reply.setSociedadId(socProducto.getSociedadIdk());
        reply.setAsignada(socProducto.getAsignada());
        reply.setFechaPrevistaActivacion(socProducto.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(socProducto.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setMethod(socProducto.getAtributosAuditoria().getUltimaAccion());
        reply.setPermiso(new PermisoJson());
        reply.getPermiso().setId(socProducto.getIdSocProducto());
        reply.getPermiso().setCargaDirectaArticulos(socProducto.getCargaDirectaArticulos());
        reply.getPermiso().setModPreciosAlta(socProducto.getModPreciosAlta());
        reply.getPermiso().setModPreciosBaja(socProducto.getModPreciosBaja());
        reply.getPermiso().setPorcentajeMaxAlta(socProducto.getPorcentajeMaxAlta().doubleValue());
        reply.getPermiso().setPorcentajeMaxBaja(socProducto.getPorcentajeMaxBaja().doubleValue());
        reply.getPermiso().setRappel(socProducto.getRappel().doubleValue());
        return reply;
    }

    public List<EtiquetaAsigJson> productosEtiquetasToJson(List<ProductoEtiqueta> productosEtiquetas) {
        log.info("....................................................................mbm............CONTROL");

        List<EtiquetaAsigJson> reply = new ArrayList();
        for (ProductoEtiqueta productoEtiqueta : productosEtiquetas) {
            EtiquetaAsigJson etiquetaAsigJson = new EtiquetaAsigJson();
            etiquetaAsigJson.setId(productoEtiqueta.getProductoIdk());
            etiquetaAsigJson.setEtiquetaId(productoEtiqueta.getEtiquetaIdk());

            etiquetaAsigJson.setFechaPrevistaActivacion(productoEtiqueta.getAtributosAuditoria().getFechaPrevistaActivacion());
            etiquetaAsigJson.setFechaPrevistaDesactivacion(productoEtiqueta.getAtributosAuditoria().getFechaPrevistaDesactivacion());
            etiquetaAsigJson.setMethod(productoEtiqueta.getAtributosAuditoria().getUltimaAccion());

            List<EtiquetaLang> etiquetasLang = etiquetaLangService.findEtiquetaLangBy_E(productoEtiqueta.getEtiqueta());
            etiquetaAsigJson.setEtiquetaLang(etiquetasLang);

            reply.add(etiquetaAsigJson);
        }

        return reply;
    }

}
