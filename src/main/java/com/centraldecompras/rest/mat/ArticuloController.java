package com.centraldecompras.rest.mat;

import com.centraldecompras.rest.mat.json.ArticuloJson;
import com.centraldecompras.acceso.Sector;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.service.interfaces.SectorService;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.ArticuloDestacado;
import com.centraldecompras.modelo.ArticuloEtiqueta;
import com.centraldecompras.modelo.ArticuloFavorito;
import com.centraldecompras.modelo.ArticuloLangDesc;
import com.centraldecompras.modelo.ArticuloLangNomb;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.EtiquetaLang;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.ImgDoc;
import com.centraldecompras.modelo.ImgFoto;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgDocService;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgFotoService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloDestacadoService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloEtiquetaService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloFavoritoService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloLangDescService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloLangNombService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloProductoService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoService;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaService;
import com.centraldecompras.modelo.prm.service.interfaces.FormatoVtaService;
import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.rest.cmo.ser.interfaces.ImgDocRestService;
import com.centraldecompras.rest.excel.interfaces.ArticuloExcelService;
import com.centraldecompras.rest.fin.json.auxi.AuditImgJson;
import com.centraldecompras.rest.mat.ser.interfaces.ArticuloRestService;
import com.centraldecompras.rest.prm.ser.interfaces.EtiquetaRestService;
import com.centraldecompras.rest.prm.json.EtiquetaAsigJson;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mat")
@Api(value = "Recurso privado", description = "Gestion de almacenes")
public class ArticuloController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloController.class.getName()); 

    @Autowired
    private ArticuloService articuloService;

    @Autowired
    private ArticuloLangDescService articuloLangDescService;

    @Autowired
    private ArticuloLangNombService articuloLangNombService;

    @Autowired
    private ArticuloProductoService articuloProductoService;

    @Autowired
    private SectorService sectorService;

    @Autowired
    private SociedadRplService sociedadRplService;

    @Autowired
    private SociedadService sociedadService;

    @Autowired
    private SocProductoService socProductoService;

    @Autowired
    private ProductoService productoService;

    @Autowired
    private FormatoVtaService formatoVtaService;

    @Autowired
    private ArticuloRestService articuloRestService;

    @Autowired
    private ArticuloExcelService articuloExcelService;

    @Autowired
    private ArticuloEtiquetaService articuloEtiquetaService;

    @Autowired
    private EtiquetaRestService etiquetaRestService;

    @Autowired
    private EtiquetaService etiquetaService;

    @Autowired
    private EtiquetaLangService etiquetaLangService;

    @Autowired
    private ImgDocRestService imgDocRestService;

    @Autowired
    private ImgDocService imgDocService;

    @Autowired
    private ImgFotoService imgFotoService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private ArticuloDestacadoService articuloDestacadoService;

    @Autowired
    private ArticuloFavoritoService articuloFavoritoService;

    @RequestMapping(value = "/articulo", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Articulo", notes = "ArticuloJson, formato de comunicacion con cliente")
    List<ArticuloJson> postArticulo(
            @RequestBody List<ArticuloJson> articulosJsonIn,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ArticuloJson> reply = new ArrayList<>();
        List<String[]> imagenes = new ArrayList<>();
        Sector sector = null;
        Sociedad sociedad = null;
        SocProducto socProducto = null;

        List<Object[]> listaObjetos = new ArrayList();
        List<Object[]> datosMontadosImagen = new ArrayList();

        sector = sectorService.findSector(idSec);
        if (sector == null) {
            Object[] valores = {idSec};
            String mensaje = MessageFormat.format("ERROR: Sector {0}, no encontrado. ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        sociedad = sociedadService.findSociedad(idSoc);
        if (sociedad == null) {
            Object[] valores = {idSoc};
            String mensaje = MessageFormat.format("ERROR: Sociedad {0}, no encontrada. ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        for (ArticuloJson articuloJsonIn : articulosJsonIn) {

            String id = articuloJsonIn.getId();
            String metodo = articuloJsonIn.getMethod();

            FormatoVta formatoVta = null;
            Producto producto = null;
            String productoId = articuloJsonIn.getProductoId();

            if (("0").equals(id) && !("POST").equals(metodo) || !("0").equals(id) && ("POST").equals(metodo)) {
                Object[] valores = {articuloJsonIn.getId(), articuloJsonIn.getMethod()};
                String mensaje = MessageFormat.format("Error: CentralCompras. CRUD Articulos. id articulo  {0}  y Metodo  {1} ne estan sincronizados ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            formatoVta = formatoVtaService.findFormatoVta(articuloJsonIn.getFormatoVentaId());
            if (formatoVta == null) {
                Object[] valores = {articuloJsonIn.getFormatoVentaId()};
                String mensaje = MessageFormat.format("ERROR: Formato de venta  {0}, no encontrado. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            producto = productoService.findProducto(productoId);
            if (producto == null) {
                Object[] valores = {productoId};
                String mensaje = MessageFormat.format("ERROR: Producto  {0}, no encontrado. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }
            if (producto.getNivelInt() != sector.getUltimoNivel()) {
                Object[] valores = {productoId, producto.getNivelInt()};
                String mensaje = MessageFormat.format("ERROR: El articulo se está asignando a un nivel {0}de producto no permitido {1}. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            boolean running = true;
            Producto productoAsignadoExplicitamente = null;
            String productoAsignadoExplicitamenteId = productoId;
            while (running) {
                socProducto = socProductoService.findSocProductoBy_idPidS(productoAsignadoExplicitamenteId, idSoc);
                if(socProducto!=null && !KApp.ACTIVO.getKApp().equals(socProducto.getAtributosAuditoria().getEstadoRegistro())    ){
                    socProducto=null;
                }
                
                if (socProducto != null) {
                    running = false;
                } else {
                    productoAsignadoExplicitamente = productoService.findProducto(productoAsignadoExplicitamenteId);
                    if (productoAsignadoExplicitamente.getIdNivelProducto() < 1) {
                        running = false;
                    } else {
                        productoAsignadoExplicitamenteId = productoAsignadoExplicitamente.getIdPadreJerarquia();
                    }
                }
            }

            if (socProducto == null) {
                Object[] valores = {productoId, idSoc};
                String mensaje = MessageFormat.format("ERROR: ProductoId {0}/ Sociedad {1}, no encontrada. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            if (socProducto.getCargaDirectaArticulos() == 0) {
                log.info(".......La sociedad " + articuloJsonIn.getCodiParaProveedor() + " no puede cargar articulos");

            } else {
                Articulo articulo = null;
                Object[] aaa = new Object[4];

                articulo = articuloRestService.montaArticulo(articuloJsonIn, idUser, sociedad, formatoVta, socProducto);
                articuloJsonIn.setId(articulo.getIdArticulo());  // Para busqueda al final del método y presentación de la persistencia

                aaa[0] = metodo;
                aaa[1] = "articulo";
                aaa[2] = articulo;
                aaa[3] = articuloJsonIn.getId();

                listaObjetos.add(aaa);

                Object[] bbb = articuloRestService.montaArticuloProducto(articuloJsonIn, articulo, producto, idUser);
                listaObjetos.add(bbb);

                List<TraduccionDescripcion> traduccionesDescripcion = (List<TraduccionDescripcion>) articuloJsonIn.getTraduccionesDesc();
                for (TraduccionDescripcion traduccionDescripcion : traduccionesDescripcion) {
                    Object[] ccc = new Object[5];

                    ccc = articuloRestService.montaArticuloLangDesc(articulo, traduccionDescripcion, idUser, articuloJsonIn.getMethod());
                    ArticuloLangDesc articuloLangDesc = (ArticuloLangDesc) ccc[2];
                    traduccionDescripcion.setIdioma(articuloLangDesc.getIdioma());  // Para busqueda al final del método y presentación de la persistencia
                    listaObjetos.add(ccc);
                }

                List<TraduccionDescripcion> traduccionesNombre = (List<TraduccionDescripcion>) articuloJsonIn.getTraduccionesNombre();
                for (TraduccionDescripcion traduccionNombre : traduccionesNombre) {
                    Object[] ddd = new Object[5];

                    ddd = articuloRestService.montaArticuloLangNomb(articulo, traduccionNombre, idUser, articuloJsonIn.getMethod());
                    ArticuloLangNomb articuloLangNomb = (ArticuloLangNomb) ddd[2];
                    traduccionNombre.setIdioma(articuloLangNomb.getIdioma());  // Para busqueda al final del método y presentación de la persistencia
                    listaObjetos.add(ddd);
                }

                /*  FL=LOGO, CT=CATALOGO, CA=CATALOGO_ARTICULO, FU=FOTO_USUARIO, FP=FOTO_PRODUCTO,  FA=FOTO_ARTICULO */
                for (int ic0 = 0; ic0 < 3; ic0++) {
                    List<AuditImgJson> auditImgesJson = new ArrayList();

                    String type = null;
                    switch (ic0) {
                        case 0:
                            auditImgesJson = articuloJsonIn.getDataImgFoto();
                            type = KApp.FOTO_ARTICULO.getKApp();
                            break;
                        case 1:
                            auditImgesJson = articuloJsonIn.getDataImgCatalogo();
                            type = KApp.CATALOGO_ARTICULO.getKApp();
                            break;
                    }

                    if (auditImgesJson != null) {
                        for (AuditImgJson auditImgJson : auditImgesJson) {
                            boolean imagenPOST_OK = true;
                            if (KApp.POST.getKApp().equals(auditImgJson.getMethod()) && (auditImgJson.getB64Picture() == null || "".equals(auditImgJson.getB64Picture()))) {
                                imagenPOST_OK = false;      // Metodo POST sin imagen
                                imagenes.add(new String[]{auditImgJson.getMethod(), "KO Imagen no creada", " ", auditImgJson.getDescrpcion(), auditImgJson.getB64NombreOriginal(), ""});
                            }

                            if (imagenPOST_OK) {
                                datosMontadosImagen = imgDocRestService.crudDDBB(type, auditImgJson, articulo.getIdArticulo(), idUser, null);
                                if (datosMontadosImagen.size() > 0) {
                                    listaObjetos.addAll(datosMontadosImagen);
                                    String idImagen = ((ImgDoc) datosMontadosImagen.get(0)[2]).getIdImgDoc();
                                    auditImgJson.setId(idImagen);
                                }
                            }
                        }
                    } else {
                        auditImgesJson = new ArrayList();
                    }

                    switch (ic0) {
                        case 0:
                            articuloJsonIn.setDataImgFoto(auditImgesJson);
                            break;
                        case 1:
                            articuloJsonIn.setDataImgCatalogo(auditImgesJson);
                            break;
                    }
                }
            }
        }

        try {
            articuloRestService.persistencia(listaObjetos);
        } catch (Exception e) {
            e.printStackTrace();
            Object[] valores = {e.getMessage()};
            String mensaje = MessageFormat.format("ERROR: CRUD Articulo. {0} ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        List<String> entitiesId = new ArrayList();

        for (ArticuloJson articuloJson : articulosJsonIn) {
            Articulo articulo = articuloService.findArticulo(articuloJson.getId());
            if (articulo != null) {
                ArticuloJson articuloJsonOut = new ArticuloJson();

                articuloJsonOut = toJson(articulo);
                articuloJsonOut.setDataImgFoto(articuloJson.getDataImgFoto());
                articuloJsonOut.setDataImgCatalogo(articuloJson.getDataImgCatalogo());
                entitiesId.add(articulo.getIdArticulo());

                articuloJsonOut.setImagenes(imgDocService.findImgDocBy_SU(entitiesId));
                articuloJsonOut.getImagenes().addAll(imgFotoService.findImgFotoBy_SU(entitiesId));

                reply.add(articuloJsonOut);
            }
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/imagen/{idImagen}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera Imagen de Catalogo o foto de articulo", notes = "El parámetro {idImagen} es el id de la imagen")
    void getImagen(
            @PathVariable String idImagen,
            HttpServletResponse response,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        byte[] picture = null;

        ImgDoc imgDoc = null;
        ImgFoto imgFoto = null;

        String extension = null;
        String nombreOri = null;

        try {
            imgFoto = imgFotoService.findImgFoto(idImagen);

            if (imgFoto == null) {
                imgDoc = imgDocService.findImgDoc(idImagen);
                extension = imgDoc.getExtension();
                nombreOri = imgDoc.getNomOriginal();

                picture = imgDocService.findImgPic_idI(idImagen);

            } else {
                extension = imgFoto.getExtension();
                nombreOri = imgFoto.getNomOriginal();

                picture = imgFotoService.findImgPic_idI(idImagen);
            }

            response.setContentType("image/" + extension);
            response.setHeader("Content-disposition", "attachment; filename=" + nombreOri);

            response.getOutputStream().write(picture);
            response.flushBuffer();

        } catch (IOException ex) {
            log.info("Error writing file to output stream. Filename was " + idImagen + " - " + ex);
            throw new RuntimeException("IOError writing file to output stream");
        }
    }

    @RequestMapping(value = "/articulouno/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera los datos de un articulo", notes = "ArticuloJson, formato de comunicacion con cliente")
    ArticuloJson getArticulo(
            @PathVariable String id,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        ArticuloJson reply = new ArticuloJson();
        List<String> entitiesId = new ArrayList();
        Articulo articulo = articuloService.findArticulo(id);

        if (articulo != null) {
            reply = toJson(articulo);

            entitiesId.add(id);
            reply.setImagenes(imgDocService.findImgDocBy_SU(entitiesId));
            reply.getImagenes().addAll(imgFotoService.findImgFotoBy_SU(entitiesId));
        }

        return reply;
    }

    @RequestMapping(value = "/articulolist", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera los datos de una lista de articulos", notes = "ArticuloJson, formato de comunicacion con cliente")
    List<ArticuloJson> getArticulos(
            @RequestBody Map<String, Object> paramSeleccion,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<ArticuloJson> reply = new ArrayList<>();

        String sociedadRplId = (String) paramSeleccion.get("sociedadId");
        String productoId = (String) paramSeleccion.get("productoId");

        SociedadRpl sociedadRpl = sociedadRplService.findSociedadRpl(sociedadRplId);
        if (sociedadRpl == null) {
            Object[] valores = {idSoc};
            String mensaje = MessageFormat.format("ERROR: Lista Articuloas . Sociedad no existe {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        Producto producto = productoService.findProducto(productoId);
        if (producto == null) {
            Object[] valores = {idUser};
            String mensaje = MessageFormat.format("ERROR: Lista Articuloas  . Producto no existe {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        List<String> entitiesId = new ArrayList();
        List<Articulo> articulos = articuloService.findArticuloBy_idP_idS(productoId, sociedadRplId);

        for (Articulo articulo : articulos) {
            ArticuloJson articuloJson = toJson(articulo);
            entitiesId = new ArrayList();
            entitiesId.add(articulo.getIdArticulo());
            articuloJson.setImagenes(imgDocService.findImgDocBy_SU(entitiesId));
            articuloJson.getImagenes().addAll(imgFotoService.findImgFotoBy_SU(entitiesId));
            reply.add(articuloJson);
        }

        return reply;
    }

    @RequestMapping(value = "/articulo/seleccion/{cadena}/{estado}/{cantidad}/{idioma}/{idSociedad}", method = RequestMethod.GET)
    List<ArticuloJson> seleccionProducto(
            @PathVariable String cadena,
            @PathVariable String estado,
            @PathVariable String cantidad,
            @PathVariable String idioma,
            @PathVariable String idSociedad,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ArticuloJson> reply = new ArrayList<ArticuloJson>();
        List<String[]> prodS = new ArrayList<String[]>();

        SociedadRpl sociedadRpl = sociedadRplService.findSociedadRpl(idSociedad);
        if (sociedadRpl == null) {
            Object[] valores = {idSociedad};
            String mensaje = MessageFormat.format("ERROR: productoarbolasig: Sociedad {0}, no encontrada. ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        int cantidadInt = 0;
        try {
            cantidadInt = Integer.parseInt(cantidad);
        } catch (NumberFormatException ex) {
            throw new Exception("CentralCompras. Combo ProductoArbolAsig. Parametro 'cantidad' de resultados no es valido : " + ex.getMessage());
        }

        if (cadena == null || cadena.equals("")) {
            throw new Exception("CentralCompras. Combo ProductoArbolAsig. Parametro 'cadena' no es valido");
        }

        /*Simulacion****************** Igual que zonas geograficas */
        if (reply.size() < cantidadInt) {
            prodS = articuloProductoService.findArticuloProductoTypeAhead(1, cadena, estado.toUpperCase(), cantidadInt, idioma, idSociedad);
            articuloRestService.seleccionArticuloProductoCargaArray(reply, prodS);
        }
        if (reply.size() < cantidadInt) {
            prodS = articuloProductoService.findArticuloProductoAvatarTypeAhead(1, cadena, estado.toUpperCase(), cantidadInt, idioma, idSociedad);
            articuloRestService.seleccionArticuloProductoCargaArray(reply, prodS);
        }

        if (reply.size() < cantidadInt) {
            prodS = articuloProductoService.findArticuloProductoTypeAhead(2, cadena, estado.toUpperCase(), cantidadInt - reply.size(), idioma, idSociedad);
            articuloRestService.seleccionArticuloProductoCargaArray(reply, prodS);
        }
        if (reply.size() < cantidadInt) {
            prodS = articuloProductoService.findArticuloProductoAvatarTypeAhead(2, cadena, estado.toUpperCase(), cantidadInt - reply.size(), idioma, idSociedad);
            articuloRestService.seleccionArticuloProductoCargaArray(reply, prodS);
        }

        if (!reply.isEmpty()) {
            for (ArticuloJson reply1 : reply) {
                ArticuloJson articuloJson = (ArticuloJson) reply1;

                reply1.setTraduccionesNombre(new ArrayList<TraduccionDescripcion>());
                List<ArticuloLangNomb> articulosLangNomb = articuloLangNombService.findArticuloLangNombBy_IdA(articuloJson.getId());

                for (ArticuloLangNomb articuloLangNomb : articulosLangNomb) {
                    TraduccionDescripcion traduccionNomb = new TraduccionDescripcion();
                    traduccionNomb.setIdioma(articuloLangNomb.getIdioma());
                    traduccionNomb.setTraduccion(articuloLangNomb.getTraduccionNomb());
                    reply1.getTraduccionesNombre().add(traduccionNomb);
                }
            }
        }
        return reply;

    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/excelExportArticulo", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera todos los Articulos y los muestra en excel ", notes = "SuppListJson, formato de comunicacion con cliente")
    Map<String, String> getSuppExcel(
            @RequestBody Map<String, String> parametros,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        String idSociedad = parametros.get("idSociedad");
        String idioma = parametros.get("idioma");
        String estado = parametros.get("estado");

        Sector sector = sectorService.findSector(idSec);
        if (sector == null) {
            Object[] valores = {"Generacion excel de Articulos"};
            String mensaje = MessageFormat.format("ERROR: ArticulosProductos.xlsx no generado. Sector no existe {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        List<Object[]> articulosProductos = articuloLangDescService.findArticuloLangDescBy_idS_i(idSociedad, idioma);

        XSSFWorkbook workbook = null;

        workbook = articuloExcelService.montaExcel(articulosProductos, sector, idioma, estado);

        String nameFile = KApp.ARTICULOS_PRODUCTOS.getKApp();
        String extensionFile = KApp.XLSX.getKApp();

        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        workbook.write(outByteStream);

        byte[] outArray = outByteStream.toByteArray();
        byte[] encoded = Base64.encodeBase64(outArray);
        String picture = new String(encoded);

        Map<String, String> reply = new HashMap<>();
        reply.put(KApp.NAME_FILE.getKApp(), nameFile);
        reply.put(KApp.EXTENSION_FILE.getKApp(), extensionFile);
        reply.put(KApp.PICTURE.getKApp(), picture);

        return reply;

        /*
         workbook = articuloExcelService.montaExcel(articulosProductos, sector, idioma, estado);
         try { //Write the workbook in file system
         if (workbook != null) {
         FileOutputStream out = new FileOutputStream(new File(nameFile.concat(".").concat(extensionFile)));
         workbook.write(out);
         out.close();
         log.info(nameFile + "."+ extensionFile + " written successfully on disk.");
         }
         } catch (Exception e) {
         Object[] valores = {"Generacion excel de articulos"};
         String mensaje = MessageFormat.format("ERROR: ArticulosProductos.xlsx no generado. {0}", valores);
         throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
         }
         */
    }

    @RequestMapping(value = "/articuloetiquetaasig", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Asignacion de Etiquetas al Arbol de productos", notes = "Reutiliza ZonaProductoAsigJson, formato de comunicacion con cliete")
    List<EtiquetaAsigJson> postArticuloEtiquetaAsig(
            @RequestBody List<EtiquetaAsigJson> etiquetasAsigJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<Object[]> datosMontadosArticuloEtiqueta = new ArrayList();
        List<EtiquetaAsigJson> reply = new ArrayList();

        for (EtiquetaAsigJson etiquetaAsigJson : etiquetasAsigJson) {
            ArticuloEtiqueta articuloEtiqueta = null;

            String metodo = etiquetaAsigJson.getMethod();
            Object[] aaa = new Object[5];

            Articulo articulo = null;
            Etiqueta etiqueta = null;

            articulo = articuloService.findArticulo(etiquetaAsigJson.getId());
            etiqueta = etiquetaService.findEtiqueta(etiquetaAsigJson.getEtiquetaId());
            if (articulo == null || etiqueta == null) {
                throw new Exception("CentralCompras. CRUD asignacion Etqueta a Articulos. Entidad Aarticulo o Etiquetga no encontrados en BD");
            }

            int fechaActivacion = etiquetaAsigJson.getFechaPrevistaActivacion();
            int fechaDesactivacion = etiquetaAsigJson.getFechaPrevistaDesactivacion();

            if (KApp.POST.getKApp().equals(metodo)) {
                articuloEtiqueta = new ArticuloEtiqueta();
                articuloEtiqueta.setArticuloIdk(etiquetaAsigJson.getId());
                articuloEtiqueta.setEtiquetaIdk(etiquetaAsigJson.getEtiquetaId());
                articuloEtiqueta.setArticulo(articulo);
                articuloEtiqueta.setEtiqueta(etiqueta);
                articuloEtiqueta.setAtributosAuditoria(new DatosAuditoria());
                articuloEtiqueta.getAtributosAuditoria().setCreated(new Date());
                articuloEtiqueta.getAtributosAuditoria().setUpdated(null);
                articuloEtiqueta.getAtributosAuditoria().setDeleted(null);
            } else if (KApp.PUT.getKApp().equals(metodo)) {
                articuloEtiqueta = articuloEtiquetaService.findArticuloEtiquetaBy_idA_idE(etiquetaAsigJson.getId(), etiquetaAsigJson.getEtiquetaId());
                articuloEtiqueta.getAtributosAuditoria().setUpdated(new Date());
            }

            if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
                articuloEtiqueta.getAtributosAuditoria().setEstadoRegistro(KApp.ACTIVO.getKApp());
                articuloEtiqueta.getAtributosAuditoria().setFechaPrevistaActivacion(fechaActivacion);
                articuloEtiqueta.getAtributosAuditoria().setFechaPrevistaDesactivacion(fechaDesactivacion);
                articuloEtiqueta.getAtributosAuditoria().setUltimaAccion(metodo);
                articuloEtiqueta.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

                aaa[2] = articuloEtiqueta;
                aaa[3] = null;
            } else if (KApp.DELETE.getKApp().equals(metodo)) {
                aaa[2] = null;
                aaa[3] = etiquetaAsigJson.getId();
                aaa[4] = etiquetaAsigJson.getEtiquetaId();
            }

            if (!KApp.DUMMY_METHOD.getKApp().equals(metodo)) {
                aaa[0] = metodo;
                aaa[1] = "articuloEtiqueta";
                datosMontadosArticuloEtiqueta.add(aaa);
            }
        }

        etiquetaRestService.persistencia(datosMontadosArticuloEtiqueta);
        List<ArticuloEtiqueta> articulosEtiquetas = articuloEtiquetaService.findArticuloEtiquetaEntities();
        reply = articulosEtiquetasToJson(articulosEtiquetas);

        return reply;
    }

    @RequestMapping(value = "/articuloetiquetaasig/{idArticulo}", method = RequestMethod.GET)
    @ApiOperation(value = "Devuelve los ArtiuculosEtiquetas asig a un articulo", notes = "Reutiliza EtiquetaAsigJson, formato de comunicacion con cliente")
    List<EtiquetaAsigJson> getArticuloEtiquetaAsigList(
            @PathVariable String idArticulo,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<EtiquetaAsigJson> reply = new ArrayList();
        List<ArticuloEtiqueta> articulosEtiquetas = null;
        if (idArticulo.equals("0")) {
            articulosEtiquetas = articuloEtiquetaService.findArticuloEtiquetaEntities();
        } else {
            articulosEtiquetas = articuloEtiquetaService.findEtiquetasBy_idA(idArticulo);
        }
        reply = articulosEtiquetasToJson(articulosEtiquetas);

        return reply;
    }

    @RequestMapping(value = "/articulodestacadofavorito", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Articulos destacados y Favoritos", notes = "Reutiliza , formato de comunicacion con cliete")
    List<Map<String, Object>> postArticuloDestacadoFavorito(
            @RequestBody List<Map<String, Object>> destacadosFavoritos,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<Object[]> datosMontadosArticulosDestacadosFavoritos = new ArrayList();
        List<Map<String, Object>> reply = new ArrayList();

        List<String> destacadosTratados = new ArrayList<>();
        List<String> favoritosTratados = new ArrayList<>();
        List<String> destacadosFavoritosTratados = new ArrayList<>();

        SociedadRpl sociedadRpl = sociedadRplService.findSociedadRpl(idSoc);
        if (sociedadRpl == null) {
            Object[] valores = {idSoc};
            String mensaje = MessageFormat.format("ERROR: Articulos destacados/favoritos . Sociedad no existe {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        Persona persona = personaService.findPersona(idUser);
        if (persona == null) {
            Object[] valores = {idUser};
            String mensaje = MessageFormat.format("ERROR: Articulos destacados/favoritos . Persona no existe {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        for (Map<String, Object> destacadoFavorito : destacadosFavoritos) {
            boolean destacado = false;
            String estadoDest = null;
            String methodDest = null;
            int fechaPrevistaActivacionDest = 0;
            int fechaPrevistaDesactivacionDest = 0;

            boolean favorito = false;
            String estadoFavo = null;
            String methodFavo = null;
            int fechaPrevistaActivacionFavo = 0;
            int fechaPrevistaDesactivacionFavo = 0;

            String articuloId = (String) destacadoFavorito.get("articuloId");
            Map<String, Object> articuloDestacado = (Map<String, Object>) destacadoFavorito.get("articuloDestacado");
            Map<String, Object> articuloFavorito = (Map<String, Object>) destacadoFavorito.get("articuloFavorito");

            Articulo articulo = articuloService.findArticulo(articuloId);
            if (articulo == null) {
                Object[] valores = {articuloId};
                String mensaje = MessageFormat.format("ERROR: Articulos destacados/favoritos . Articulo no existe {0}", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            if (articuloDestacado == null && articuloFavorito == null) {
                Object[] valores = {articuloId};
                String mensaje = MessageFormat.format("ERROR: Articulos destacados/favoritos . No se informa como destacado ni como favorito {0}", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            if (articuloDestacado != null) {
                destacado = (boolean) articuloDestacado.get("destacado");
                estadoDest = (String) articuloDestacado.get("estado");
                methodDest = (String) articuloDestacado.get("method");
                fechaPrevistaActivacionDest = (int) articuloDestacado.get("fechaPrevistaActivacion");
                fechaPrevistaDesactivacionDest = (int) articuloDestacado.get("fechaPrevistaDesactivacion");

                Object[] aaa = new Object[6];
                aaa = articuloRestService.montaArticuloDestacado(articuloId, destacado, estadoDest, methodDest, fechaPrevistaActivacionDest, fechaPrevistaDesactivacionDest, idSoc, idUser);
                datosMontadosArticulosDestacadosFavoritos.add(aaa);
            }

            if (articuloFavorito != null) {
                favorito = (boolean) articuloFavorito.get("favorito");
                estadoFavo = (String) articuloFavorito.get("estado");
                methodFavo = (String) articuloFavorito.get("method");
                fechaPrevistaActivacionFavo = (int) articuloFavorito.get("fechaPrevistaActivacion");
                fechaPrevistaDesactivacionFavo = (int) articuloFavorito.get("fechaPrevistaDesactivacion");

                Object[] bbb = new Object[6];
                bbb = articuloRestService.montaArticuloFavorito(articuloId, favorito, estadoFavo, methodFavo, fechaPrevistaActivacionFavo, fechaPrevistaDesactivacionFavo, idSoc, idUser);
                datosMontadosArticulosDestacadosFavoritos.add(bbb);
            }

            if (articuloDestacado != null && articuloFavorito != null) {
                destacadosFavoritosTratados.add(articuloId);
            }
            if (articuloDestacado != null && articuloFavorito == null) {
                destacadosTratados.add(articuloId);
            }
            if (articuloDestacado == null && articuloFavorito != null) {
                favoritosTratados.add(articuloId);
            }
        }

        articuloRestService.persistencia(datosMontadosArticulosDestacadosFavoritos);

        Map<String, Object> articuloJson = null;
        for (String articuloId : destacadosFavoritosTratados) {
            ArticuloDestacado articuloDestacado = articuloDestacadoService.findArticuloDestacadoBy_idS_idA(idSoc, articuloId);
            ArticuloFavorito articuloFavorito = articuloFavoritoService.findArticuloFavoritoBy_idS_idA_idU(idSoc, articuloId, idUser);
            articuloJson = articuloDestacadoFavoritoToJson(articuloDestacado, articuloFavorito);
            reply.add(articuloJson);
        }

        for (String articuloId : destacadosTratados) {
            ArticuloDestacado articuloDestacado = articuloDestacadoService.findArticuloDestacadoBy_idS_idA(idSoc, articuloId);
            articuloJson = articuloDestacadoFavoritoToJson(articuloDestacado, null);
            reply.add(articuloJson);
        }

        for (String articuloId : favoritosTratados) {
            ArticuloFavorito articuloFavorito = articuloFavoritoService.findArticuloFavoritoBy_idS_idA_idU(idSoc, articuloId, idUser);
            articuloJson = articuloDestacadoFavoritoToJson(null, articuloFavorito);
            reply.add(articuloJson);
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/excelImportArticulos", method = RequestMethod.POST)
    @ApiOperation(value = "carga masiva de precios de articulos desde Excel ", notes = "Multipart, formato de comunicacion con cliente")
    @Consumes("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    String postExcelImportProductos(
            @RequestBody Map<String, String> excelMapIn,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        XSSFWorkbook workbook = null;
        List<Object[]> listaObjetos = new ArrayList();

        try {
            String nameFile = (String) excelMapIn.get("excelNomOriB64");
            String excelFileBase64 = (String) excelMapIn.get("excelFilesB64");

            byte[] excelFileByte64 = excelFileBase64.getBytes();
            byte[] excelFileBytes = Base64.decodeBase64(excelFileByte64);
            InputStream excelFileIs = (InputStream) new ByteArrayInputStream(excelFileBytes);

            workbook = (XSSFWorkbook) WorkbookFactory.create(excelFileIs);

            XSSFSheet sheet = workbook.getSheet("Articulos");
            Iterator<Row> rowIterator = sheet.iterator();

            Object[] aaa = null;

            boolean inicio = true;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (inicio == true) {
                    row = rowIterator.next();
                    inicio = false;
                }

                // La lines excel es correcta y la procesamos. -------------
                Cell cell_ProductoId = row.getCell(1);
                double precioUnidadMedida = (int) Math.round(row.getCell(5).getNumericCellValue() * 100) / (double) 100;
                double precioUnidadFormatoVta = (int) Math.round(row.getCell(6).getNumericCellValue() * 100) / (double) 100;
                Cell cell_CodigoParaProveedor = row.getCell(8);
                cell_CodigoParaProveedor.setCellType(Cell.CELL_TYPE_STRING);

                Articulo articulo = articuloService.findArticulo(cell_ProductoId.getStringCellValue());
                articulo.setPrecioUniMed(new BigDecimal(precioUnidadMedida).setScale(2, RoundingMode.HALF_EVEN));
                articulo.setUnidadFormatoVta(new BigDecimal(precioUnidadFormatoVta).setScale(2, RoundingMode.HALF_EVEN));
                articulo.setCodiParaProveedor(cell_CodigoParaProveedor.getStringCellValue());

                aaa = new Object[5];
                aaa[0] = KApp.PUT.getKApp();
                aaa[1] = "articulo";
                aaa[2] = articulo;
                aaa[3] = "";
                aaa[4] = "";
                listaObjetos.add(aaa);
            }
        } catch (IOException | InvalidFormatException e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

        articuloRestService.persistencia(listaObjetos);

        return "OK";
    }

    @RequestMapping(value = "/articulo/sociedadpendienteaprobacion/{estadosAprobacionParam}/{tipoResultado}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera lista de sociedades con articulos pendientes de aprobacion", notes = "ArticuloJson, formato de comunicacion con cliente")
    List<Object[]> getSocConArticulosPteAprob(
            @PathVariable String estadosAprobacionParam,    // PAA=PEND_ALTA_ART, PMP=PEND_MOD_PRECIO
            @PathVariable String tipoResultado,             // R=Resumen, T=Total
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Object[]> reply = new ArrayList<>();

        List<String> estadosAprobacion = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(estadosAprobacionParam, "|");
        while (st.hasMoreTokens()) {
            estadosAprobacion.add(st.nextToken());
        }

        List<String[]> sociedadesDetalle = new ArrayList<>();
        sociedadesDetalle = articuloService.findArticuloSel_PA_R(estadosAprobacion);

        String sociedadControl = "";
        Object[] sociedadResumen = null;
        long pendientesAprobacionTotal = 0;

        for (Object[] sociedadDetalle : sociedadesDetalle) {
            if ("R".equals(tipoResultado)) {
                if (!sociedadControl.equals((String) sociedadDetalle[0])) {
                    sociedadControl = (String) sociedadDetalle[0];
                    Sociedad sociedad = sociedadService.findSociedad(sociedadControl);
                    sociedadResumen = new Object[]{sociedad.getIdSociedad(), sociedad.getNombre(), 0L, 0L};
                    reply.add(sociedadResumen);
                }
                if (((String) sociedadDetalle[1]).equals(KApp.PEND_ALTA_ART.getKApp())) {
                    sociedadResumen[2] = (long) sociedadDetalle[2];
                }
                if (((String) sociedadDetalle[1]).equals(KApp.PEND_MOD_PRECIO.getKApp())) {
                    sociedadResumen[3] = (long) sociedadDetalle[2];
                }
            }
            pendientesAprobacionTotal += (long) sociedadDetalle[2];
        }

        if ("T".equals(tipoResultado)) {
            reply.add(new Object[]{KApp.TOTAL_PTE_APROBACION.getKApp(), pendientesAprobacionTotal, ""});
        }
        
        return reply;
    }

    @RequestMapping(value = "/articuloAprobacion", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Articulo", notes = "ArticuloJson, formato de comunicacion con cliente")
    List<ArticuloJson> postArticuloAprobacion(
            @RequestBody List<Map<String, String>> articulosAprobados,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ArticuloJson> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();
        List<String> articulosId = new ArrayList();

        for (Map<String, String> articuloAprobado : articulosAprobados) {

            String idArticulo = articuloAprobado.get("id");
            String estadoAprobacion = articuloAprobado.get("estadoAprobacion");
            String method = articuloAprobado.get("method");
            articulosId.add(idArticulo);

            Articulo articulo = articuloService.findArticulo(idArticulo);
            if (articulo == null) {
                Object[] valores = {idArticulo};
                String mensaje = MessageFormat.format("ERROR: Articulo {0}, no encontrado. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            Object[] aaa = articuloRestService.montaArticuloAprobacion(articulo, estadoAprobacion, idUser, method);
            listaObjetos.add(aaa);

        }

        articuloRestService.persistencia(listaObjetos);

        for (String articuloId : articulosId) {
            Articulo articulo = articuloService.findArticulo(articuloId);
            if (articulo != null) {
                ArticuloJson articuloJsonOut = new ArticuloJson();
                articuloJsonOut = toJson(articulo);
                reply.add(articuloJsonOut);
            }
        }

        return reply;
    }

    private ArticuloJson toJson(Articulo articulo) {
        log.info("....................................................................mbm............CONTROL");
        
        ArticuloJson reply = new ArticuloJson();
        Producto producto = articuloProductoService.findProductosBy_A(articulo.getIdArticulo());

        reply.setId(articulo.getIdArticulo());
        reply.setProductoId(producto.getIdProducto());
        reply.setUniMinVta(articulo.getUniMinVta().doubleValue());
        reply.setPrecioUniMed(articulo.getPrecioUniMed().doubleValue());
        reply.setSociedadId(articulo.getSociedad().getIdSociedad()); 
        reply.setSociedadNombre(articulo.getSociedad().getNombre());         
        reply.setUnidadFormatoVta(articulo.getUnidadFormatoVta().doubleValue());
        reply.setFormatoVentaId(articulo.getFormatoVta().getIdFormatoVta());
        reply.setMoneda(articulo.getMoneda());
        reply.setDescrArticulo(articulo.getDescrArticulo());
        reply.setNombreArticulo(articulo.getNombreArticulo());
        reply.setCodiParaProveedor(articulo.getCodiParaProveedor());
        reply.setCodigoEAN(articulo.getCodigoEAN());
        reply.setEstadoAprobacion(articulo.getEstadoAprobacion());
        reply.setPrecioUniMedNuevo(articulo.getPrecioUniMedNuevo().doubleValue());

        reply.setEstado(articulo.getAtributosAuditoria().getEstadoRegistro());
        reply.setMethod(articulo.getAtributosAuditoria().getUltimaAccion());
        reply.setFechaPrevistaActivacion(articulo.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(articulo.getAtributosAuditoria().getFechaPrevistaDesactivacion());

        List<ArticuloLangDesc> articulosLangDesc = articuloLangDescService.findArticuloLangDescBy_A(articulo);
        List<TraduccionDescripcion> traduccionesDesc = new ArrayList<TraduccionDescripcion>();
        for (ArticuloLangDesc articuloLangDesc : articulosLangDesc) {
            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(articuloLangDesc.getIdioma());
            traduccionDescripcion.setTraduccion(articuloLangDesc.getTraduccionDesc());

            traduccionesDesc.add(traduccionDescripcion);
        }
        reply.setTraduccionesDesc(traduccionesDesc);

        List<ArticuloLangNomb> articulosLangNomb = articuloLangNombService.findArticuloLangNombBy_A(articulo);
        List<TraduccionDescripcion> traduccionesNomb = new ArrayList<TraduccionDescripcion>();
        for (ArticuloLangNomb articuloLangNomb : articulosLangNomb) {
            TraduccionDescripcion traduccionNombre = new TraduccionDescripcion();
            traduccionNombre.setIdioma(articuloLangNomb.getIdioma());
            traduccionNombre.setTraduccion(articuloLangNomb.getTraduccionNomb());

            traduccionesNomb.add(traduccionNombre);
        }
        reply.setTraduccionesNombre(traduccionesNomb);

        return reply;
    }

    public List<EtiquetaAsigJson> articulosEtiquetasToJson(List<ArticuloEtiqueta> articulosEtiquetas) {
        log.info("....................................................................mbm............CONTROL");
                
        List<EtiquetaAsigJson> reply = new ArrayList();
        for (ArticuloEtiqueta articuloEtiqueta : articulosEtiquetas) {
            EtiquetaAsigJson etiquetaAsigJson = new EtiquetaAsigJson();
            etiquetaAsigJson.setId(articuloEtiqueta.getArticuloIdk());
            etiquetaAsigJson.setEtiquetaId(articuloEtiqueta.getEtiquetaIdk());

            etiquetaAsigJson.setFechaPrevistaActivacion(articuloEtiqueta.getAtributosAuditoria().getFechaPrevistaActivacion());
            etiquetaAsigJson.setFechaPrevistaDesactivacion(articuloEtiqueta.getAtributosAuditoria().getFechaPrevistaDesactivacion());
            etiquetaAsigJson.setMethod(articuloEtiqueta.getAtributosAuditoria().getUltimaAccion());

            List<EtiquetaLang> etiquetasLang = etiquetaLangService.findEtiquetaLangBy_E(articuloEtiqueta.getEtiqueta());
            etiquetaAsigJson.setEtiquetaLang(etiquetasLang);

            reply.add(etiquetaAsigJson);
        }

        return reply;
    }

    private Map<String, Object> articuloDestacadoFavoritoToJson(ArticuloDestacado articuloDestacado, ArticuloFavorito articuloFavorito) {
        log.info("....................................................................mbm............CONTROL");
                
        Map<String, Object> destacadoFavorito = new HashMap<>();
        Map<String, Object> destacado = new HashMap<>();
        Map<String, Object> favorito = new HashMap<>();

        String articuloIdk = null;

        if (articuloDestacado != null) {
            articuloIdk = articuloDestacado.getArticuloIdk();
            destacado.put("destacado", articuloDestacado.isDestacado());
            destacado.put("estado", articuloDestacado.getAtributosAuditoria().getEstadoRegistro());
            destacado.put("method", articuloDestacado.getAtributosAuditoria().getUltimaAccion());
            destacado.put("fechaPrevistaActivacion", articuloDestacado.getAtributosAuditoria().getFechaPrevistaActivacion());
            destacado.put("fechaPrevistaDesactivacion", articuloDestacado.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        }

        if (articuloFavorito != null) {
            articuloIdk = articuloFavorito.getArticuloIdk();
            favorito.put("favorito", articuloFavorito.isFavorito());
            favorito.put("estado", articuloFavorito.getAtributosAuditoria().getEstadoRegistro());
            favorito.put("method", articuloFavorito.getAtributosAuditoria().getUltimaAccion());
            favorito.put("fechaPrevistaActivacion", articuloFavorito.getAtributosAuditoria().getFechaPrevistaActivacion());
            favorito.put("fechaPrevistaDesactivacion", articuloFavorito.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        }

        destacadoFavorito.put("articuloId", articuloIdk);
        destacadoFavorito.put("articuloDestacado", destacado);
        destacadoFavorito.put("articuloFavorito", favorito);

        return destacadoFavorito;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/articulopag", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera una coleccion paginada de Articulos de una sociedad pendientes de Aprobar ", notes = "SuppListJson, formato de comunicacion con cliente")
    Map<String, Object> getSuppPag(
            @RequestBody Map<String, Object> filtro,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");
        Map<String, Object> reply = new HashMap<>();

        ArticuloJson articuloJson = new ArticuloJson();
        List<ArticuloJson> articulosJson = new ArrayList<ArticuloJson>();
        Map<String, Object> paginaArticulos = (Map<String, Object>) articuloService.findPagArticulosPASoc(filtro);

        int totalPaginas = (int) paginaArticulos.get("totalPaginas");
        int totalRegistros = (int) paginaArticulos.get("totalRegistros");
        int paginaActual = (int) paginaArticulos.get("paginaActual");
        List<Articulo> articulos = (List<Articulo>) paginaArticulos.get("articulos");

        //log.info("----<" + articulos);
        if (articulos == null) {
            log.info("----<Creamos uno vacio");
            articulos = new ArrayList<>();
        }

        for (Articulo articulo : articulos) {
            articuloJson = toJson(articulo);
            articulosJson.add(articuloJson);
        }

        reply.put("totalPaginas", totalPaginas);
        reply.put("totalRegistros", totalRegistros);
        reply.put("paginaActual", paginaActual);
        reply.put("articulosJson", articulosJson);
        return reply;
    }

}
