package com.centraldecompras.rest.mat.ser.interfaces;

import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SectorRpl;
import com.centraldecompras.rest.mat.json.ProductoArbolJson;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.HashMap;
import java.util.List;

public interface ProductoRestService {

    void persistencia(List<Object> createdObjets, List<Object> updatedObjets) throws Exception;

    Object[] montaProducto(ProductoArbolJson productoArbolJson, SectorRpl sector, String idUser, Producto parent) throws NonexistentEntityException, Exception;

    Object[] montaProductoLang(Producto producto, TraduccionDescripcion traduccionDescripcion, String idUser, String metodo);

    public Object[] montaProductoEtiqueta(Producto producto, List<String> etiquetaObj, String idUser, String metodo) throws NonexistentEntityException, Exception;

    int[] recurrenteCabecerasProducto(int nivel, String idioma, HashMap filtro, int ultimoNivelSector, int[] ramaCabec);

    List<String[]> recurrenteProductosHermanos(int nivel, String idioma, HashMap filtro, String padreJerarquia, int ultimoNivelSector, String[] ramaArbol, List<String[]> ramasArbol, String estado);

    String[] recurrenteProductosPadre(int nivel, String idioma, HashMap filtro, String padreJerarquia, int ultimoNivelSector, String[] ramaArbol, String estado);

    void persistencia(List<Object[]> listaObjetos) throws Exception;

    List<ProductoArbolJson> seleccionProductosCargaArray(List<ProductoArbolJson> reply, List<String[]> productos) throws Exception;

    void routeInformaInicio(Producto producto) throws Exception;
    
    public Producto informaRoute(Producto producto);

}
