package com.centraldecompras.rest.mat.ser;

import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.NivelProductoLang;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.ProductoEtiqueta;
import com.centraldecompras.modelo.ProductoLang;
import com.centraldecompras.modelo.SectorRpl;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenUsuarioProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoEtiquetaService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoLangService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoLangService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaService;
import com.centraldecompras.rest.mat.ArticuloController;
import com.centraldecompras.rest.mat.json.ProductoArbolJson;
import com.centraldecompras.rest.mat.ser.interfaces.ProductoRestService;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductoRestServiceImpl implements ProductoRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductoRestServiceImpl.class.getName());

    @Autowired
    private ProductoService productoService;

    @Autowired
    private ProductoLangService productoLangService;

    @Autowired
    private NivelProductoService nivelProductoService;

    @Autowired
    private UnidadMedidaService unidadMedidaService;

    @Autowired
    private EtiquetaService etiquetaService;

    @Autowired
    private ProductoEtiquetaService productoEtiquetaService;

    @Autowired
    private NivelProductoLangService nivelProductoLangService;

    @Autowired
    private AlmacenUsuarioProductoService almacenUsuarioProductoService;

    @Transactional
    public void persistencia(List<Object> createdObjets, List<Object> updatedObjets) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Object createdObjet : createdObjets) {
            if (createdObjet instanceof Producto) {
                productoService.create((Producto) createdObjet);
            }
            if (createdObjet instanceof AlmacenUsuarioProducto) {
                almacenUsuarioProductoService.create((AlmacenUsuarioProducto) createdObjet);
            }
        }
        for (Object updatedObjet : updatedObjets) {
            if (updatedObjet instanceof Producto) {
                productoService.edit((Producto) updatedObjet);
            }
            if (updatedObjet instanceof AlmacenUsuarioProducto) {
                almacenUsuarioProductoService.edit((AlmacenUsuarioProducto) updatedObjet);
            }
        }

    }

    @Transactional
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("producto").equals((String) bbb[1])) {
                    productoService.create((Producto) bbb[2]);
                }
                if (("productoLang").equals((String) bbb[1])) {
                    productoLangService.create((ProductoLang) bbb[2]);
                }
                if (("productoEtiqueta").equals((String) bbb[1])) {
                    productoEtiquetaService.create((ProductoEtiqueta) bbb[2]);
                }
            }

            if (("PUT").equals((String) bbb[0])) {
                if (("producto").equals((String) bbb[1])) {
                    productoService.edit((Producto) bbb[2]);
                }
                if (("productoLang").equals((String) bbb[1])) {
                    productoLangService.edit((ProductoLang) bbb[2]);
                }
                if (("productoEtiqueta").equals((String) bbb[1])) {
                    productoEtiquetaService.edit((ProductoEtiqueta) bbb[2]);
                }
            }
        }
        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("producto").equals((String) bbb[1])) {
                    productoService.destroy((String) bbb[3]);
                }
                if (("productoLang").equals((String) bbb[1])) {
                    productoLangService.deleteProductoLangBy_IdP_Lang((String) bbb[3], (String) bbb[4]);
                }
                if (("productoEtiqueta").equals((String) bbb[1])) {
                    productoEtiquetaService.deleteProductoEtiquetaBy_idP_idE((String) bbb[3], (String) bbb[4]);
                }
            }
        }
    }

    public Object[] montaProducto(ProductoArbolJson productoArbolJson, SectorRpl sector, String idUser, Producto parent) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");

        Producto producto = null;

        NivelProducto nivelProducto = null;
        // Producto parent = null;
        UnidadMedida unidadMedida = null;
        String metodo = productoArbolJson.getMethod();

        int nivel = 0;
        try {
            nivel = productoArbolJson.getNivel();
        } catch (Exception ex) {
            throw new Exception("CentralCompras. CRUD Producto-Arbol. Nivel: [" + productoArbolJson.getNivel() + "] contiene un valor no valido.");
        }

        nivelProducto = nivelProductoService.findNivelProducto(nivel);
        if (nivelProducto == null) {
            Object[] valores = {nivel};
            String mensaje = MessageFormat.format("ERROR: CentralCompras. CRUD Producto-Arbol. Id nivel Producto:  no existe. {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        unidadMedida = unidadMedidaService.findUnidadMedida(productoArbolJson.getUnidadMedida());
        if (unidadMedida == null) {
            Object[] valores = {productoArbolJson.getUnidadMedida()};
            String mensaje = MessageFormat.format("ERROR: CentralCompras. CRUD Producto-Arbol. Id unidadMedida:  no existe. {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        if (("POST").equals(productoArbolJson.getMethod())) {
            producto = new Producto();
            producto.setIdProducto(UUID.randomUUID().toString().replaceAll("-", ""));
            producto.setAtributosAuditoria(new DatosAuditoria());
            producto.getAtributosAuditoria().setCreated(new Date());
        } else {  // Modificacion o Delete
            producto = productoService.findProducto(productoArbolJson.getId());
            producto.getAtributosAuditoria().setUpdated(new Date());
        }

        if (producto != null) {

            producto.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            producto.getAtributosAuditoria().setFechaPrevistaActivacion(productoArbolJson.getFechaPrevistaActivacion());
            producto.getAtributosAuditoria().setFechaPrevistaDesactivacion(productoArbolJson.getFechaPrevistaDesactivacion());
            producto.getAtributosAuditoria().setEstadoRegistro(productoArbolJson.getEstado());
            producto.getAtributosAuditoria().setUltimaAccion(productoArbolJson.getMethod());

            producto.setSector(sector);
            producto.setPadreJerarquia(parent);
            producto.setNivelProducto(nivelProducto);
            producto.setNivelInt(nivelProducto.getIdNivelProducto());
            producto.setUnidadMedida(unidadMedida);
            producto.setIdPadreJerarquia(parent.getIdProducto());
            producto.setIdNivelProducto(productoArbolJson.getNivel());

            producto = informaRoute(producto);

        }

        Object[] aaa = new Object[4];
        if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
            aaa[2] = producto;
            aaa[3] = null;
        } else if (ElementEnum.KApp.DELETE.getKApp().equals(metodo)) {
            aaa[2] = null;
            aaa[3] = productoArbolJson.getId();
        }
        aaa[0] = metodo;
        aaa[1] = "producto";

        return aaa;
    }

    public Object[] montaProductoLang(Producto producto, TraduccionDescripcion traduccionDescripcion, String idUser, String metodo) {
        log.info("....................................................................mbm............CONTROL");

        ProductoLang productoLang = new ProductoLang();

        productoLang = productoLangService.findProductoLangBy_IdP_Lang(producto.getIdProducto(), traduccionDescripcion.getIdioma());

        if (productoLang == null) {
            productoLang = new ProductoLang();
            productoLang.setProductoId(producto.getIdProducto());
            productoLang.setProductoEntity(producto);
            productoLang.setIdioma(traduccionDescripcion.getIdioma());
            productoLang.setAtributosAuditoria(new DatosAuditoria());
            productoLang.getAtributosAuditoria().setCreated(new Date());
        } else {
            productoLang.getAtributosAuditoria().setUpdated(new Date());
        }

        if (producto != null) {
            productoLang.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            productoLang.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            productoLang.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
            productoLang.getAtributosAuditoria().setEstadoRegistro(producto.getAtributosAuditoria().getEstadoRegistro());
            productoLang.getAtributosAuditoria().setUltimaAccion(metodo);
            productoLang.setTraduccionDesc(traduccionDescripcion.getTraduccion());
        }

        Object[] aaa = new Object[5];
        aaa[0] = metodo;
        aaa[1] = "productoLang";
        aaa[2] = productoLang;
        aaa[3] = producto.getIdProducto();
        aaa[4] = productoLang.getIdioma();

        traduccionDescripcion.setIdioma(productoLang.getIdioma());  // Para busqueda al final del método y presentación de la persistencia
        return aaa;
    }

    public Object[] montaProductoEtiqueta(Producto producto, List<String> etiquetaObj, String idUser, String metodoProducto) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");

        ProductoEtiqueta productoEtiqueta = null;

        String metodo = etiquetaObj.get(0);
        Object[] aaa = new Object[5];

        Etiqueta etiqueta = etiquetaService.findEtiqueta(etiquetaObj.get(1));
        if (etiqueta == null) {
            Object[] valores = {etiquetaObj.get(1)};
            String mensaje = MessageFormat.format("ERROR: Etiqueta Id {0} no existe. ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        if (KApp.POST.getKApp().equals(metodo)) {
            ProductoEtiqueta productoEtiquetaVerificacion = productoEtiquetaService.findProductoEtiquetaBy_idP_idE(producto.getIdProducto(), etiquetaObj.get(1));
            if (productoEtiquetaVerificacion != null) {
                metodo = KApp.DUMMY_METHOD.getKApp();
            }
        }

        if (KApp.POST.getKApp().equals(metodo)) {
            productoEtiqueta = new ProductoEtiqueta();
            productoEtiqueta.setProductoIdk(producto.getIdProducto());
            productoEtiqueta.setEtiquetaIdk(etiquetaObj.get(1));
            productoEtiqueta.setProducto(producto);
            productoEtiqueta.setEtiqueta(etiqueta);
            productoEtiqueta.setAtributosAuditoria(new DatosAuditoria());
            productoEtiqueta.getAtributosAuditoria().setCreated(new Date());
            productoEtiqueta.getAtributosAuditoria().setUpdated(null);
            productoEtiqueta.getAtributosAuditoria().setDeleted(null);

            productoEtiqueta.getAtributosAuditoria().setEstadoRegistro(KApp.ACTIVO.getKApp());
            productoEtiqueta.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            productoEtiqueta.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
            productoEtiqueta.getAtributosAuditoria().setUltimaAccion(metodo);
            productoEtiqueta.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

            aaa[2] = productoEtiqueta;
            aaa[3] = null;
        } else if (KApp.DELETE.getKApp().equals(metodo)) {
            aaa[2] = null;
            aaa[3] = producto.getIdProducto();
            aaa[4] = etiquetaObj.get(1);
        }

        if (KApp.POST.getKApp().equals(metodo) || KApp.DELETE.getKApp().equals(metodo)) {
            aaa[0] = metodo;
            aaa[1] = "productoEtiqueta";
        }
        return aaa;
    }

    public int[] recurrenteCabecerasProducto(int nivel, String idioma, HashMap filtro, int ultimoNivelSector, int[] ramaCabec) {
        log.info("....................................................................mbm............CONTROL");

        nivel++;

        NivelProductoLang nivelProductoLang = nivelProductoLangService.findNivelProductoLangBy_IdNP_Lang(nivel, idioma);
        ramaCabec[nivel - 1] = nivelProductoLang.getNivelProductoId();
        if (nivel < ultimoNivelSector) {
            recurrenteCabecerasProducto(nivel, idioma, filtro, ultimoNivelSector, ramaCabec);
        }

        return ramaCabec;
    }

    public List<String[]> recurrenteProductosHermanos(int nivel, String idioma, HashMap filtro, String padreJerarquia, int ultimoNivelSector, String[] ramaArbol, List<String[]> ramasArbol, String estado) {
        log.info("....................................................................mbm............CONTROL");

        nivel++;

        // Recupera los hermanos desde el id del padre de la jerarquia
        List<String[]> productosN = productoService.findProductosBy_n_i_d_(nivel, idioma, padreJerarquia, estado);

        for (Object[] productoN : productosN) {
            int nivelNodo = ((Integer) productoN[1]);
            ramaArbol[nivelNodo - 1] = (String) productoN[0];
            for (int ic1 = nivelNodo; ic1 < ultimoNivelSector; ic1++) {
                ramaArbol[ic1] = null;
            }

            recurrenteProductosHermanos(nivel, idioma, filtro, (String) productoN[0], ultimoNivelSector, ramaArbol, ramasArbol, estado);
            ramaArbol = ramaArbol.clone();
        }

        if (productosN.size() == 0) {
            ramasArbol.add(ramaArbol);
        }

        return ramasArbol;
    }

    public String[] recurrenteProductosPadre(int nivel, String idioma, HashMap filtro, String productoId, int ultimoNivelSector, String[] ramaArbol, String estado) {
        log.info("....................................................................mbm............CONTROL");

        // Recupera el padres de forma recurrrrente desde un id padre de la jerarquia
        ProductoLang productoLang = productoLangService.findProductoLangBy_IdP_Lang(productoId, idioma);
        ramaArbol[nivel - 1] = productoLang.getTraduccionDesc();

        nivel--;

        if (nivel > 0) {
            Producto producto = productoService.findProducto(productoId);
            recurrenteProductosPadre(nivel, idioma, filtro, producto.getIdPadreJerarquia(), ultimoNivelSector, ramaArbol, estado);

        }

        return ramaArbol;
    }

    public List<ProductoArbolJson> seleccionProductosCargaArray(List<ProductoArbolJson> reply, List<String[]> productos) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (int ic1 = 0; ic1 < productos.size(); ic1++) {
            //for (String[] datosStr : productos) {
            ProductoArbolJson productoArbolJson = new ProductoArbolJson();
            Object datosStr[] = (Object[]) productos.get(ic1);

            productoArbolJson.setId(String.valueOf(datosStr[0]));
            productoArbolJson.setNivel(Integer.valueOf(String.valueOf(datosStr[1])));
            productoArbolJson.setParent(String.valueOf(datosStr[2]) == null ? KApp.PRODUCTO_UNIVERSAL.getKApp() : String.valueOf(datosStr[2]));
            productoArbolJson.setEstado(String.valueOf(datosStr[3]));
            productoArbolJson.setMethod(String.valueOf(datosStr[4]));
            productoArbolJson.setFechaPrevistaActivacion(Integer.valueOf(String.valueOf(datosStr[5])));
            productoArbolJson.setFechaPrevistaDesactivacion(Integer.valueOf(String.valueOf(datosStr[6])));

            reply.add(productoArbolJson);
        }
        return reply;
    }

    public void routeInformaInicio(Producto producto) throws Exception {
        List<Object> updatedObjets = new ArrayList();

        updatedObjets.add(informaRoute(producto));
        persistencia(new ArrayList(), updatedObjets);

    }

    public Producto informaRoute(Producto producto) {
        StringBuilder route = new StringBuilder();
        
        List<String> routElementes = new ArrayList();
        routElementes.add(producto.getIdProducto());
        routeInformaUno(producto, routElementes);

        Collections.reverse(routElementes);

        route.append(".");
        for (String routElemente : routElementes) {
            route.append(routElemente);
            route.append(".");
        }

        producto.setRoute(route.toString());
        producto.getAtributosAuditoria().setUpdated(new Date());
        if (producto.getDescripcion() == null) {
            producto.setDescripcion("");
        }

        if (producto.getSector().getUltimoNivel() == producto.getNivelInt()) {
            producto.setRouteType("Z");
        } else {
            producto.setRouteType("A");
        }

        return producto;
    }

    private void routeInformaUno(Producto prductoEntity, List<String> reply) {
        if (prductoEntity.getNivelInt() > 0) {
            reply.add(prductoEntity.getIdPadreJerarquia());
        }
        if (prductoEntity.getNivelInt() > 1) {
            Producto productoPadre = productoService.findProducto(prductoEntity.getIdPadreJerarquia());
            routeInformaUno(productoPadre, reply);
        }
    }
}
