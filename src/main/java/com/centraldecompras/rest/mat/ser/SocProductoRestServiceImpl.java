package com.centraldecompras.rest.mat.ser;

import com.centraldecompras.modelo.AlmacenUsuProdExcl;
import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SocProductoAvatar;
import com.centraldecompras.modelo.SocProductoExcl;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoAvatarService;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoExcludesService;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoService;
import com.centraldecompras.rest.lgt.json.ZonaProductoAsigJson;
import com.centraldecompras.rest.mat.ser.interfaces.SocProductoRestService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SocProductoRestServiceImpl implements SocProductoRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocProductoRestServiceImpl.class.getName());

    @Autowired
    private SocProductoService socProductoService;

    @Autowired
    private SocProductoAvatarService socProductoAvatarService;

    @Autowired
    private ProductoService productoService;
    
    @Autowired
    private SocProductoExcludesService socProductoExcludesService;

    public List<Object[]> montaSociedadProducto(List<String> socDateIn, SociedadRpl sociedadRpl, String idUser, String idSoc, String idPrf, String idSec) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Object[]> reply = new ArrayList();
        String metodo = null;

        List<SocProducto> socProductos = new ArrayList<>();

        if (socDateIn.get(0) != null && !"".equals(socDateIn.get(0))) {
            int fechaActivacion = 0;
            int fechaDesactivacion = 0;

            try {
                fechaActivacion = Integer.parseInt(socDateIn.get(1));
                fechaDesactivacion = Integer.parseInt(socDateIn.get(2));
            } catch (Exception e) {
                throw new Exception("Error en las fechas de activacion y desactivacion");
            }

            try {
                socProductos = socProductoService.findSocProductoBy_idS(socDateIn.get(0));
            } catch (Exception e) {
                throw new Exception("Error al leer la sociedad destino " + socDateIn.get(0));
            }

            SocProducto socProducto = null;
            for (SocProducto socProductoOrigen : socProductos) {
                Object[] socProducto_Array = new Object[4]; 

                socProducto = socProductoService.findSocProductoBy_idPidS(socProductoOrigen.getProductoIdk(), sociedadRpl.getIdSociedad());
                if (socProducto == null) {
                    socProducto = new SocProducto();
                    socProducto.setIdSocProducto(UUID.randomUUID().toString().replaceAll("-", ""));
                    socProducto.setSociedad(sociedadRpl);
                    socProducto.setSociedadIdk(sociedadRpl.getIdSociedad());
                    socProducto.setProducto(socProductoOrigen.getProducto());
                    socProducto.setProductoIdk(socProductoOrigen.getProductoIdk());
                    socProducto.setAtributosAuditoria(new DatosAuditoria());
                    metodo = ElementEnum.KApp.POST.getKApp();
                } else {
                    metodo = ElementEnum.KApp.PUT.getKApp();
                }

                socProducto.setAsignada(socProductoOrigen.getAsignada());
                socProducto.getAtributosAuditoria().setCreated(new Date());
                socProducto.getAtributosAuditoria().setUpdated(null);
                socProducto.getAtributosAuditoria().setDeleted(null);
                socProducto.getAtributosAuditoria().setEstadoRegistro("AC");
                socProducto.getAtributosAuditoria().setFechaPrevistaActivacion(fechaActivacion);
                socProducto.getAtributosAuditoria().setFechaPrevistaDesactivacion(fechaDesactivacion);
                socProducto.getAtributosAuditoria().setUltimaAccion(ElementEnum.KApp.POST.getKApp());
                socProducto.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

                socProducto.setCargaDirectaArticulos(socProductoOrigen.getCargaDirectaArticulos());
                socProducto.setModPreciosAlta(socProductoOrigen.getModPreciosAlta());
                socProducto.setModPreciosBaja(socProductoOrigen.getModPreciosBaja());
                socProducto.setPorcentajeMaxAlta(socProductoOrigen.getPorcentajeMaxAlta());
                socProducto.setPorcentajeMaxBaja(socProductoOrigen.getPorcentajeMaxBaja());
                socProducto.setRappel(socProductoOrigen.getRappel());

                socProducto_Array[0] = metodo;
                socProducto_Array[1] = "socProducto";
                socProducto_Array[2] = socProducto;
                socProducto_Array[3] = null;

                reply.add(socProducto_Array);
            }

        }

        return reply;
    }

    public List<ZonaProductoAsigJson> recuperaSociedadProducto(String sociedadId) {
        log.info("....................................................................mbm............CONTROL");

        List<ZonaProductoAsigJson> reply = new ArrayList();
        List<SocProducto> socProductos = socProductoService.findSocProductoBy_idS(sociedadId);
        for (SocProducto socProducto : socProductos) {
            reply.add(datosToJson(socProducto));
        }
        return reply;
    }

    public ZonaProductoAsigJson datosToJson(SocProducto socProducto) {
        log.info("....................................................................mbm............CONTROL");

        ZonaProductoAsigJson reply = new ZonaProductoAsigJson();
        reply = new ZonaProductoAsigJson();
        reply.setId(socProducto.getProductoIdk());
        reply.setSociedadId(socProducto.getSociedadIdk());
        reply.setAsignada(socProducto.getAsignada());
        reply.setFechaPrevistaActivacion(socProducto.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(socProducto.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setMethod(socProducto.getAtributosAuditoria().getUltimaAccion());
        return reply;
    }

    @Transactional
    public void persistencia(List<Object> createdObjets, List<Object> updatedObjets, List<Object> deletdObjets, List<Object[]> listaObjetos) throws Exception {
        
        for (Object createdObjet : createdObjets) {
            if (createdObjet instanceof SocProducto) {
                socProductoService.create((SocProducto) createdObjet);
            }
            if (createdObjet instanceof SocProductoExcl) {
                socProductoExcludesService.create((SocProductoExcl) createdObjet);
            }
        }
        
        for (Object updatedObjet : updatedObjets) { 
            if (updatedObjet instanceof SocProducto) {
                socProductoService.edit((SocProducto) updatedObjet);
            }
            if (updatedObjet instanceof SocProductoExcl) {
                socProductoExcludesService.edit((SocProductoExcl) updatedObjet);
            }
        }
        
        Collections.reverse(deletdObjets);
        for (Object deletdObjet : deletdObjets) { 
            if (deletdObjet instanceof SocProducto) {
                socProductoService.destroy(((SocProducto) deletdObjet).getIdSocProducto());
            } 
            if (deletdObjet instanceof SocProductoExcl) {
                socProductoExcludesService.destroySocProductoExcludesBy_idA_idU_r(((SocProductoExcl) deletdObjet).getSociedadId(), ((SocProductoExcl) deletdObjet).getRoute());
            }
        }  
        
        log.info("....................................................................mbm............CONTROL");

        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("socProducto").equals((String) bbb[1])) {
                    log.info(".....///////.........." + ((SocProducto) bbb[2]).toString());
                    socProductoService.create((SocProducto) bbb[2]);
                }
                if (("socProductoAvatar").equals((String) bbb[1])) {
                    log.info(".....///////.........." + ((SocProductoAvatar) bbb[2]).toString());
                    socProductoAvatarService.create((SocProductoAvatar) bbb[2]);
                }
            }
            if (("PUT").equals((String) bbb[0])) {
                if (("socProducto").equals((String) bbb[1])) {
                    log.info(".....///////.........." + ((SocProducto) bbb[2]).toString());
                    socProductoService.edit((SocProducto) bbb[2]);
                }
                if (("socProductoAvatar").equals((String) bbb[1])) {
                    log.info(".....///////.........." + ((SocProductoAvatar) bbb[2]).toString());
                    socProductoAvatarService.edit((SocProductoAvatar) bbb[2]);
                }
            }
        }

        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("socProducto").equals((String) bbb[1])) {
                    log.info(".....///////.........." + (bbb[1]).toString());
                    socProductoService.destroy((String) bbb[3]);
                }
                if (("socProductoAvatar").equals((String) bbb[1])) {
                    log.info(".....///////.........." + (bbb[1]).toString());
                    socProductoAvatarService.deleteSocProductoAvatarBy_idS_idP((String) bbb[3], (String) bbb[4]);
                }
            }
        }        
        
        
    }

    
    public Map<String, Object> crdExcludes(SocProducto socProducto, String accion) throws Exception {

        Map<String, Object> reply = new HashMap<>();

        List<SocProductoExcl> createExcludes = new ArrayList<>();
        List<SocProductoExcl> updateExcludes = new ArrayList<>();
        List<SocProductoExcl> deleteExcludes = new ArrayList<>();

        // La tabla SocProductoExcl contendrÃ¡ productos excluidos para una sociedad con la path completa hasta el Ãºltimo nivel.
        // PK para verificar existencia: sociedadId, y route: 
        //    CASOS:
        //    A) EXCLUSION: S-P. ExtracciÃ³n de todos los productos de Ãºltimo nivel que sean hijos de S-P con el atributo route (r) informado.
        //       1.- Hacer lo siguiente por cada hijo de Ãºlitmo nivel encontrado (p1, p2, p... pn):
        //          1a.- Verificar que la PK, (S-r) ya existe en la tabla.
        //          1b.- Cuando la PK NO existe, INSERTAR, e informar ademas productoId, nivInt del producto excluido
        //          1c.- Cuando la PK SI existe, informar en BBDD el productoId y nivInt del hijo (UPDATE cuando sea necesario)
        // 
        //    B) ASIGNACION: S-P. ExtracciÃ³n de todos los productos de Ãºltimo nivel que sean hijos de S-P con el atributo route (r) informado.
        //       1.- Hacer lo siguiente por cada hijo de Ãºlitmo nivel encontrado (p1, p2, p... pn):
        //          1a.- Verificar que la PK, (A-r) ya existe en la tabla.
        //          1b.- Cuando la PK NO existe, la asignaciÃ³n no tiene efectos sobre la tabla
        //          1c.- Cuando la PK SI existe, puede ocuarrir: 
        //               * Que productoId e intNiv asignado sea PADRE del Pk de la tabla --> No se toca porque estÃ¡ excluido con un hijo del asignado. 
        //               * Que productoId e intNiv asignado sea HIJO  del Pk de la tabla --> No se toca porque estÃ¡ excluido por estructura de arbol         
        //               * Que productoId e intNiv de tabla SI sea igual que productoId e intNiv asignado. Explorar arbol de padres.
        //                 - ExploraciÃ³n del arbol de padres. Cuando SI encuentra el primer padre excluido, UPDATE, informar en productoId e nivInt los valores del padre excluido
        //                 -                                   Cuando NO encuentra padre excluido, DELETE del registro.        
        // OK
        //    B) HERENCIA: S-P. Puedes aplicar la herencia a una EXCLUSION o a una ASIGNACION
        //       1.- Aplicar la herencia a una exclusion
        //          1a. Buscar exclusiones en el arbol de padres        
        //              **- Cuando SI encuentra exclusiÃ³n en el arbol de padres, actuar como una exclusion.
        //              **- Cuando NO encuentra exclusiÃ³n en el arbol de padres, revisa los hijos
        //                  
        //       2.- Aplicar la herencia a una asignaciÃ³n        
        //          1a. Buscar exclusiones en el arbol de padres        
        //              **- Cuando SI encuentra exclusiÃ³n en el arbol de padres, actuar como una asignaciÃ³n a productoId que tiene un padre con exclusiÃ³n.
        //              **- Cuando NO encuentra exclusiÃ³n en el arbol de padres, revis los hijoa
        //
        //
        //
        String sociedadId = socProducto.getSociedad().getIdSociedad();
        List<Producto> productos = productoService.findProductosBy_idP(socProducto.getProducto().getIdProducto());
        for (Producto producto : productos) {
            SocProductoExcl socProductoExcludes = socProductoExcludesService.findSocProductoExcludesBy_idS_r(sociedadId, producto.getRoute());
            Map<String, Object> aupeUD = new HashMap();

            if ("E".equals(accion)) {   // Exclusion
                if (socProductoExcludes != null) {
                    // Inoformar productoId y nivInt del hijo en el registro: UPDATE del regsitro si es necesario
                    if (socProducto.getProducto().getNivelInt() > socProductoExcludes.getNivelInt()) {
                        socProductoExcludes.setNivelInt(socProducto.getProducto().getNivelInt());
                        socProductoExcludes.setProductoId(socProducto.getProducto().getIdProducto());
                        updateExcludes.add(socProductoExcludes);
                    }
                } else {
                    // INSERT Registro de AlmacenUsuProdExcl
                    socProductoExcludes = new SocProductoExcl();

                    socProductoExcludes.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                    socProductoExcludes.setSociedadId(socProducto.getSociedad().getIdSociedad());
                    socProductoExcludes.setRoute(producto.getRoute());
                    socProductoExcludes.setProductoId(producto.getIdProducto());
                    socProductoExcludes.setNivelInt(socProducto.getProducto().getNivelInt());

                    createExcludes.add(socProductoExcludes);
                }

            } else if ("A".equals(accion)) {    // Asignacion
                if (socProductoExcludes != null) {
                    if (socProducto.getProducto().getNivelInt() > socProductoExcludes.getNivelInt()) {
                        buscaPadreExcluido(socProducto, socProductoExcludes, aupeUD);

                        if (aupeUD.containsKey("aupeU")) {
                            updateExcludes.add((SocProductoExcl) aupeUD.get("aupeU"));
                        }
                        if (aupeUD.containsKey("aupeD")) {
                            deleteExcludes.add((SocProductoExcl) aupeUD.get("aupeD"));
                        }
                    }
                }

            } else if ("H".equals(accion)) {                // Herencia
                if (socProductoExcludes != null) {
                    buscaPadreExcluido(socProducto, socProductoExcludes, aupeUD);

                    if (aupeUD.containsKey("aupeU")) {
                        updateExcludes.add((SocProductoExcl) aupeUD.get("aupeU"));
                    }

                    if (aupeUD.containsKey("aupeD")) {
                        deleteExcludes.add((SocProductoExcl) aupeUD.get("aupeD"));
                    }

                }
            };
        }
        reply.put("createExcludes", createExcludes);
        reply.put("updateExcludes", updateExcludes);
        reply.put("deleteExcludes", deleteExcludes);
        return reply;
    }

    private void buscaPadreExcluido(SocProducto _socProducto, SocProductoExcl socProductoExcludes, Map<String, Object> aupeUD) throws Exception {

        List<Boolean> padreExcluidoBoolean = new ArrayList(); //true-- Tiene padre excluido, false==No tiene padre excluido 
        List<Producto> padreExcluidoObject = new ArrayList();

        // ExploraciÃ³n de los padres
        // Si encuentra padre excluido --> informa almacenUsuarioProductoExcludes y reply = true
        // Si no encuentra padre excluido --> No informa almacenUsuarioProductoExcludes y reply = false 
        if (_socProducto.getProducto().getNivelInt() > 1) {
            recurrenteProductosPadre_Excluido(_socProducto, _socProducto.getProducto(), padreExcluidoBoolean, padreExcluidoObject);
        }
        if (padreExcluidoBoolean.size() > 0) {
            // Inoformar productoId y nivInt del padte en el registro: UPDATE del regsitro si es necesario
            if (padreExcluidoObject.size() > 0) {
                socProductoExcludes.setNivelInt((padreExcluidoObject.get(0)).getNivelInt());
                socProductoExcludes.setProductoId((padreExcluidoObject.get(0)).getIdProducto());
                aupeUD.put("aupeU", socProductoExcludes);

            } else {
                throw new Exception("Hay padre excluido pero no se envÃ­a objeto");
            }

        } else {
            // DELETE registro de AlmacenUsuProdExcl
            if(_socProducto.getProducto().getIdNivelProducto()>=socProductoExcludes.getNivelInt()){ 
                aupeUD.put("aupeD", socProductoExcludes);
            }
        }
    }

    private void recurrenteProductosPadre_Excluido(SocProducto _socProducto, Producto _productoBuscado, List<Boolean> _padreExcluido, List<Producto> _padreExcluidoObject) {
        log.info("Buscamos.." + _productoBuscado.getIdPadreJerarquia());

        Producto productoPadreBuscado = productoService.findProducto(_productoBuscado.getIdPadreJerarquia());

        if (productoPadreBuscado != null && productoPadreBuscado.getNivelInt() > 0) {
            SocProducto socProducto = socProductoService.findSocProductoBy_idS_idP_Estado(
                    _socProducto.getSociedad().getIdSociedad(), 
                    productoPadreBuscado.getIdProducto(), 
                    ElementEnum.KApp.ACTIVO.getKApp());
            if (socProducto != null && false == socProducto.getAsignada()) {
                _padreExcluido.add(Boolean.TRUE);       /* ESTO ES LO QUE BUSCO Y ENCUENTRO: asignacion para almacen/ usuario / producto, que esta excluida (AC no asignado (false)) */

                _padreExcluidoObject.add(productoPadreBuscado);
            } else {
                recurrenteProductosPadre_Excluido(_socProducto, productoPadreBuscado, _padreExcluido, _padreExcluidoObject);
            }
        }
    }

}
