package com.centraldecompras.rest.mat.ser;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.rest.mat.ser.interfaces.ArticuloRestService;
import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.ArticuloDestacado;
import com.centraldecompras.modelo.ArticuloFavorito;
import com.centraldecompras.modelo.ArticuloLangDesc;
import com.centraldecompras.modelo.ArticuloLangNomb;
import com.centraldecompras.modelo.ArticuloProducto;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.ImgDoc;
import com.centraldecompras.modelo.ImgFoto;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgDocService;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgFotoService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloDestacadoService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloFavoritoService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloLangDescService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloLangNombService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloService;
import com.centraldecompras.rest.mat.ArticuloController;
import com.centraldecompras.rest.mat.json.ArticuloJson;
import com.centraldecompras.rest.mat.json.ProductoArbolJson;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ArticuloRestServiceImpl implements ArticuloRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloRestServiceImpl.class.getName()); 


    @Autowired
    private ArticuloService articuloService;

    @Autowired
    private ArticuloLangDescService articuloLangDescService;

    @Autowired
    private ArticuloLangNombService articuloLangNombService;

    @Autowired
    private ArticuloProductoService articuloProductoService;

    @Autowired
    private ArticuloDestacadoService articuloDestacadoService;

    @Autowired
    private ArticuloFavoritoService articuloFavoritoService;

    @Autowired
    private ImgDocService imgDocService;

    @Autowired
    private ImgFotoService imgFotoService;

    @Transactional
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");
                

        try {

            for (Object[] bbb : listaObjetos) {
                if (("POST").equals((String) bbb[0])) {
                    if (("articulo").equals((String) bbb[1])) {
                        articuloService.create((Articulo) bbb[2]);
                    }
                    if (("articuloLangDesc").equals((String) bbb[1])) {
                        articuloLangDescService.create((ArticuloLangDesc) bbb[2]);
                    }
                    if (("articuloLangNomb").equals((String) bbb[1])) {
                        articuloLangNombService.create((ArticuloLangNomb) bbb[2]);
                    }
                    if (("articuloProducto").equals((String) bbb[1])) {
                        articuloProductoService.create((ArticuloProducto) bbb[2]);
                    }
                    if (("articuloDestacado").equals((String) bbb[1])) {
                        articuloDestacadoService.create((ArticuloDestacado) bbb[2]);
                    }
                    if (("articuloFavorito").equals((String) bbb[1])) {
                        articuloFavoritoService.create((ArticuloFavorito) bbb[2]);
                    }
                    if (("imgDoc").equals((String) bbb[1])) {
                        if (("F").equals((String) bbb[4])) {
                            ImgFoto imgFoto = new ImgFoto((ImgDoc) bbb[2]);
                            imgFotoService.create(imgFoto);
                        } else {
                            imgDocService.create((ImgDoc) bbb[2]);
                        }
                    }
                }

                if (("PUT").equals((String) bbb[0])) {
                    if (("articulo").equals((String) bbb[1])) {
                        articuloService.edit((Articulo) bbb[2]);
                    }
                    if (("articuloLangDesc").equals((String) bbb[1])) {
                        articuloLangDescService.edit((ArticuloLangDesc) bbb[2]);
                    }
                    if (("articuloLangNomb").equals((String) bbb[1])) {
                        articuloLangNombService.edit((ArticuloLangNomb) bbb[2]);
                    }
                    if (("articuloProducto").equals((String) bbb[1])) {
                        articuloProductoService.edit((ArticuloProducto) bbb[2]);
                    }
                    if (("articuloDestacado").equals((String) bbb[1])) {
                        articuloDestacadoService.edit((ArticuloDestacado) bbb[2]);
                    }
                    if (("articuloFavorito").equals((String) bbb[1])) {
                        articuloFavoritoService.edit((ArticuloFavorito) bbb[2]);
                    }
                    if (("imgDoc").equals((String) bbb[1])) {
                        if (("F").equals((String) bbb[4])) {
                            ImgFoto imgFoto = new ImgFoto((ImgDoc) bbb[2]);
                            imgFotoService.edit(imgFoto);
                        } else {
                            imgDocService.edit((ImgDoc) bbb[2]);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Object[] valores = {e.getMessage()};
            String mensaje = MessageFormat.format("ERROR: CRUD Articulo. {0} ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("articulo").equals((String) bbb[1])) {
                    articuloService.destroy((String) bbb[3]);
                }
                if (("articuloLangDesc").equals((String) bbb[1])) {
                    articuloLangDescService.deleteArticuloLangDescBy_IdA_Lang((String) bbb[3], (String) bbb[4]);
                }
                if (("articuloLangNomb").equals((String) bbb[1])) {
                    articuloLangNombService.deleteArticuloLangNombBy_IdA_Lang((String) bbb[3], (String) bbb[4]);
                }
                if (("articuloProducto").equals((String) bbb[1])) {
                    articuloProductoService.deleteArticuloProductoBy_A_P((String) bbb[3], (String) bbb[4]);
                }

                if (("articuloDestacado").equals((String) bbb[1])) {
                    articuloDestacadoService.deleteArticuloDestacadoBy_idS_idA((String) bbb[3], (String) bbb[4]);
                }
                if (("articuloFavorito").equals((String) bbb[1])) {
                    articuloFavoritoService.deleteArticuloFavoritoBy_idS_idA_idU((String) bbb[3], (String) bbb[4], (String) bbb[5]);
                }
                if (("imgDoc").equals((String) bbb[1])) {
                    if (("F").equals((String) bbb[4])) {
                        imgFotoService.destroy((String) bbb[3]);
                    } else {
                        imgDocService.destroy((String) bbb[3]);
                    }
                }
            }
        }

    }

    public Articulo montaArticulo(ArticuloJson articuloJson, String idUser, Sociedad sociedad, FormatoVta formatoVta, SocProducto socProducto) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");
                
        Articulo reply = null;

        if (("POST").equals(articuloJson.getMethod())) { 
            reply = new Articulo();
            reply.setIdArticulo(UUID.randomUUID().toString().replaceAll("-", ""));
            reply.setPrecioUniMedNuevo(new BigDecimal(0));
            reply.setAtributosAuditoria(new DatosAuditoria());
            reply.getAtributosAuditoria().setCreated(new Date());
            reply.setPrecioUniMed((new BigDecimal(articuloJson.getPrecioUniMed())).setScale(2, RoundingMode.HALF_EVEN));
            // CargaDirectaArticulos: 0=No puede cargar articulos, 1=Carga con aprobacion, 2=Carga sin aprobacion.
            if (socProducto.getCargaDirectaArticulos() == 2) {
                reply.setEstadoAprobacion(ElementEnum.KApp.APROBADO.getKApp());
            } else {
                reply.setEstadoAprobacion(ElementEnum.KApp.PEND_ALTA_ART.getKApp());
            }
        } else {
            reply = articuloService.findArticulo(articuloJson.getId());
            reply.getAtributosAuditoria().setUpdated(new Date());
        }

        if (("PUT").equals(articuloJson.getMethod())) {
            BigDecimal precioUniMed = reply.getPrecioUniMed();
            BigDecimal precioUniMedNuevo = new BigDecimal(articuloJson.getPrecioUniMedNuevo()).setScale(2, RoundingMode.HALF_EVEN);

            // compareTo: 0=Iguales , 1=Primero mayor, -1=Primero Menor
            if (precioUniMedNuevo.compareTo(new BigDecimal(0)) != 0 && precioUniMedNuevo.compareTo(precioUniMed) != 0) {
                BigDecimal limiteInferior = new BigDecimal(0);
                BigDecimal limiteSuperior = new BigDecimal(0);

                // compareTo: 0=Iguales , 1=Primero mayor, -1=Primero Menor
                if (precioUniMed.compareTo(precioUniMedNuevo) == -1) {
                    limiteInferior = precioUniMed;
                    limiteSuperior = precioUniMed.add(
                            precioUniMed.multiply(socProducto.getPorcentajeMaxAlta()).divide(new BigDecimal(100))
                    );
                }

                // compareTo: 0=Iguales , 1=Primero mayor, -1=Primero Menor
                if (precioUniMed.compareTo(precioUniMedNuevo) == 1) {
                    limiteInferior = precioUniMed.add(
                            precioUniMed.multiply(socProducto.getPorcentajeMaxBaja().multiply(new BigDecimal(-1))).divide(new BigDecimal(100))
                    );
                    limiteSuperior = precioUniMed;
                }

                // compareTo: 0=Iguales , 1=Primero mayor, -1=Segundo Mayor
                if (precioUniMedNuevo.compareTo(limiteInferior) == 1 && precioUniMedNuevo.compareTo(limiteSuperior) == -1) {
                    reply.setPrecioUniMed((new BigDecimal(articuloJson.getPrecioUniMedNuevo())).setScale(2, RoundingMode.HALF_EVEN));
                    reply.setPrecioUniMedNuevo(new BigDecimal(0));
                    reply.setEstadoAprobacion(KApp.APROBADO.getKApp());
                } else {
                    reply.setPrecioUniMedNuevo((new BigDecimal(articuloJson.getPrecioUniMedNuevo())).setScale(2, RoundingMode.HALF_EVEN));
                    reply.setEstadoAprobacion(KApp.PEND_MOD_PRECIO.getKApp());
                }
            }
        }

        if (reply != null) {
            reply.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            reply.getAtributosAuditoria().setFechaPrevistaActivacion(articuloJson.getFechaPrevistaActivacion());
            reply.getAtributosAuditoria().setFechaPrevistaDesactivacion(articuloJson.getFechaPrevistaDesactivacion());
            reply.getAtributosAuditoria().setEstadoRegistro(articuloJson.getEstado());
            reply.getAtributosAuditoria().setUltimaAccion(articuloJson.getMethod());

            reply.setUniMinVta((new BigDecimal(articuloJson.getUniMinVta())).setScale(2, RoundingMode.HALF_EVEN));
            reply.setMoneda(articuloJson.getMoneda());
            reply.setUnidadFormatoVta((new BigDecimal(articuloJson.getUnidadFormatoVta())).setScale(2, RoundingMode.HALF_EVEN));
            reply.setDescrArticulo(articuloJson.getDescrArticulo());
            reply.setNombreArticulo(articuloJson.getNombreArticulo());
            reply.setCodiParaProveedor(articuloJson.getCodiParaProveedor());
            reply.setCodigoEAN(articuloJson.getCodigoEAN());
            reply.setFormatoVta(formatoVta);
            reply.setSociedad(sociedad);
        }

        return reply;
    }

    public Object[] montaArticuloProducto(ArticuloJson articuloJson, Articulo articulo, Producto producto, String idUser) {
        log.info("....................................................................mbm............CONTROL");
                
        ArticuloProducto articuloProducto = null;
        String metodo = articuloJson.getMethod();
        Object[] aaa = null;

        if (ElementEnum.KApp.POST.getKApp().equals(metodo)) {
            articuloProducto = new ArticuloProducto();

            articuloProducto.setArticulo(articulo);
            articuloProducto.setProducto(producto);
            articuloProducto.setAtributosAuditoria(new DatosAuditoria());
            articuloProducto.getAtributosAuditoria().setCreated(new Date());

        } else {
            articuloProducto = articuloProductoService.findArticuloProductoBy_A_P(articulo.getIdArticulo(), producto.getIdProducto());
            articuloProducto.getAtributosAuditoria().setUpdated(new Date());
        }

        if (articuloProducto != null) {
            articuloProducto.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            articuloProducto.getAtributosAuditoria().setFechaPrevistaActivacion(articuloJson.getFechaPrevistaActivacion());
            articuloProducto.getAtributosAuditoria().setFechaPrevistaDesactivacion(articuloJson.getFechaPrevistaDesactivacion());
            articuloProducto.getAtributosAuditoria().setEstadoRegistro(articuloJson.getEstado());
            articuloProducto.getAtributosAuditoria().setUltimaAccion(articuloJson.getMethod());
            articuloProducto.setProductoIdk(producto.getIdProducto());
            articuloProducto.setArticuloIdk(articulo.getIdArticulo());

            aaa = new Object[5];
            aaa[0] = metodo;
            aaa[1] = "articuloProducto";
            aaa[2] = articuloProducto;
            aaa[3] = articuloJson.getId();          // Id del articulo
            aaa[4] = producto.getIdProducto();      // Id del producto
        }

        return aaa;

    }

    public Object[] montaArticuloLangDesc(Articulo articulo, TraduccionDescripcion traduccionDescripcion, String idUser, String metodo) {
        log.info("....................................................................mbm............CONTROL");
        
        ArticuloLangDesc articuloLang = null;
        articuloLang = articuloLangDescService.findArticuloLangDescBy_IdA_Lang(articulo.getIdArticulo(), traduccionDescripcion.getIdioma());

        if (articuloLang == null) {
            articuloLang = new ArticuloLangDesc();
            articuloLang.setArticuloIdk(articulo.getIdArticulo());
            articuloLang.setArticuloEntity(articulo);
            articuloLang.setIdioma(traduccionDescripcion.getIdioma());
            articuloLang.setAtributosAuditoria(new DatosAuditoria());
            articuloLang.getAtributosAuditoria().setCreated(new Date());
            metodo = KApp.POST.getKApp();
        } else {
            articuloLang.getAtributosAuditoria().setUpdated(new Date());
        }

        articuloLang.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
        articuloLang.getAtributosAuditoria().setFechaPrevistaActivacion(0);
        articuloLang.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
        articuloLang.getAtributosAuditoria().setEstadoRegistro(articulo.getAtributosAuditoria().getEstadoRegistro());
        articuloLang.getAtributosAuditoria().setUltimaAccion(metodo);
        articuloLang.setTraduccionDesc(traduccionDescripcion.getTraduccion());

        Object[] aaa = new Object[5];
        aaa[0] = metodo;
        aaa[1] = "articuloLangDesc";
        aaa[2] = articuloLang;
        aaa[3] = articulo.getIdArticulo();
        aaa[4] = articuloLang.getIdioma();

        traduccionDescripcion.setIdioma(articuloLang.getIdioma());  // Para busqueda al final del método y presentación de la persistencia

        return aaa;
    }

    public Object[] montaArticuloLangNomb(Articulo articulo, TraduccionDescripcion traduccionDescripcion, String idUser, String metodo) {
        log.info("....................................................................mbm............CONTROL");
                

        ArticuloLangNomb articuloLangNomb = null;
        articuloLangNomb = articuloLangNombService.findArticuloLangNombBy_IdA_Lang(articulo.getIdArticulo(), traduccionDescripcion.getIdioma());

        if (articuloLangNomb == null) {
            articuloLangNomb = new ArticuloLangNomb();
            articuloLangNomb.setArticuloIdk(articulo.getIdArticulo());
            articuloLangNomb.setArticuloEntity(articulo);
            articuloLangNomb.setIdioma(traduccionDescripcion.getIdioma());
            articuloLangNomb.setAtributosAuditoria(new DatosAuditoria());
            articuloLangNomb.getAtributosAuditoria().setCreated(new Date());
            metodo = KApp.POST.getKApp();
        } else {
            articuloLangNomb.getAtributosAuditoria().setUpdated(new Date());
        }

        articuloLangNomb.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
        articuloLangNomb.getAtributosAuditoria().setFechaPrevistaActivacion(0);
        articuloLangNomb.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
        articuloLangNomb.getAtributosAuditoria().setEstadoRegistro(articulo.getAtributosAuditoria().getEstadoRegistro());
        articuloLangNomb.getAtributosAuditoria().setUltimaAccion(metodo);
        articuloLangNomb.setTraduccionNomb(traduccionDescripcion.getTraduccion());

        Object[] aaa = new Object[5];
        aaa[0] = metodo;
        aaa[1] = "articuloLangNomb";
        aaa[2] = articuloLangNomb;
        aaa[3] = articulo.getIdArticulo();
        aaa[4] = articuloLangNomb.getIdioma();

        traduccionDescripcion.setIdioma(articuloLangNomb.getIdioma());  // Para busqueda al final del método y presentación de la persistencia

        return aaa;
    }

    public Object[] montaArticuloDestacado(String articuloId, boolean destacado, String estado, String method, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion, String idSoc, String idUser) {
        log.info("....................................................................mbm............CONTROL");
        
        ArticuloDestacado articuloDestacado = articuloDestacadoService.findArticuloDestacadoBy_idS_idA(idSoc, articuloId);

        if (articuloDestacado == null) {
            articuloDestacado = new ArticuloDestacado();
            articuloDestacado.setSociedadIdk(idSoc);
            articuloDestacado.setArticuloIdk(articuloId);
            articuloDestacado.setAtributosAuditoria(new DatosAuditoria());
            articuloDestacado.getAtributosAuditoria().setCreated(new Date());
            articuloDestacado.getAtributosAuditoria().setUpdated(null);
            articuloDestacado.getAtributosAuditoria().setDeleted(null);

        } else if (ElementEnum.KApp.PUT.getKApp().equals(method)) {
            articuloDestacado.getAtributosAuditoria().setUpdated(new Date());
        }

        articuloDestacado.setDestacado(destacado);
        articuloDestacado.getAtributosAuditoria().setEstadoRegistro(estado);
        articuloDestacado.getAtributosAuditoria().setUltimaAccion(method);
        articuloDestacado.getAtributosAuditoria().setFechaPrevistaActivacion(fechaPrevistaActivacion);
        articuloDestacado.getAtributosAuditoria().setFechaPrevistaDesactivacion(fechaPrevistaDesactivacion);
        articuloDestacado.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

        Object[] aaa = new Object[6];

        aaa[0] = method;
        aaa[1] = "articuloDestacado";
        aaa[2] = articuloDestacado;
        aaa[3] = articuloDestacado.getSociedadIdk();
        aaa[4] = articuloDestacado.getArticuloIdk();
        aaa[5] = null;

        return aaa;
    }

    public Object[] montaArticuloFavorito(String articuloId, boolean favorito, String estado, String method, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion, String idSoc, String idUser) {
        log.info("....................................................................mbm............CONTROL");
                
        ArticuloFavorito articuloFavorito = articuloFavoritoService.findArticuloFavoritoBy_idS_idA_idU(idSoc, articuloId, idUser);

        if (articuloFavorito == null) {
            articuloFavorito = new ArticuloFavorito();
            articuloFavorito.setSociedadIdk(idSoc);
            articuloFavorito.setArticuloIdk(articuloId);
            articuloFavorito.setUsuarioIdk(idUser);
            articuloFavorito.setAtributosAuditoria(new DatosAuditoria());
            articuloFavorito.getAtributosAuditoria().setCreated(new Date());
            articuloFavorito.getAtributosAuditoria().setUpdated(null);
            articuloFavorito.getAtributosAuditoria().setDeleted(null);
        } else {
            articuloFavorito.getAtributosAuditoria().setUpdated(new Date());
        }

        articuloFavorito.setFavorito(favorito);
        articuloFavorito.getAtributosAuditoria().setEstadoRegistro(estado);
        articuloFavorito.getAtributosAuditoria().setUltimaAccion(method);
        articuloFavorito.getAtributosAuditoria().setFechaPrevistaActivacion(fechaPrevistaActivacion);
        articuloFavorito.getAtributosAuditoria().setFechaPrevistaDesactivacion(fechaPrevistaDesactivacion);
        articuloFavorito.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

        Object[] aaa = new Object[6];
        aaa[0] = method;
        aaa[1] = "articuloFavorito";
        aaa[2] = articuloFavorito;
        aaa[3] = articuloFavorito.getSociedadIdk();
        aaa[4] = articuloFavorito.getArticuloIdk();
        aaa[5] = articuloFavorito.getUsuarioIdk();

        return aaa;
    }

    public Object[] montaArticuloAprobacion(Articulo articulo, String estadoAprobacion, String idUser, String method) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");
                

        Object[] reply = new Object[4];

        Boolean actualizar = false;
        if (articulo.getAtributosAuditoria().getEstadoRegistro().equals(KApp.ACTIVO.getKApp())) {
            if (articulo.getEstadoAprobacion().equals(KApp.PEND_ALTA_ART.getKApp())) {
                articulo.setEstadoAprobacion(estadoAprobacion);
                actualizar = true;
            } else if (articulo.getEstadoAprobacion().equals(KApp.PEND_MOD_PRECIO.getKApp())) {
                articulo.setEstadoAprobacion(estadoAprobacion);
                articulo.setPrecioUniMed(articulo.getPrecioUniMedNuevo());
                articulo.setPrecioUniMedNuevo(new BigDecimal(0));
                actualizar = true;
            }

            if (actualizar) {
                articulo.getAtributosAuditoria().setUltimaAccion(method);
                articulo.getAtributosAuditoria().setUpdated(new Date());
                articulo.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

                reply[0] = method;
                reply[1] = "articulo";
                reply[2] = articulo;
                reply[3] = articulo.getIdArticulo();
            }
        }

        return reply;
    }

    private String evaluaModificacionPrecio(ArticuloJson articuloJson, SocProducto socProducto) {
        log.info("....................................................................mbm............CONTROL");
                
        String reply = KApp.PEND_MOD_PRECIO.getKApp();

        BigDecimal precioUniMed = new BigDecimal(articuloJson.getPrecioUniMed()).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal precioUniMedNuevo = new BigDecimal(articuloJson.getPrecioUniMedNuevo()).setScale(2, RoundingMode.HALF_EVEN);

        if (precioUniMed.doubleValue() < precioUniMedNuevo.doubleValue()) {
            if (precioUniMed.doubleValue() + (precioUniMed.doubleValue() * socProducto.getPorcentajeMaxAlta().doubleValue() / 100) <= precioUniMedNuevo.doubleValue()) {
                reply = KApp.APROBADO.getKApp();
            }
        } else if (precioUniMed.doubleValue() > precioUniMedNuevo.doubleValue()) {
            if (precioUniMed.doubleValue() + (precioUniMed.doubleValue() * socProducto.getPorcentajeMaxBaja().doubleValue() / 100) >= precioUniMedNuevo.doubleValue()) {
                reply = KApp.APROBADO.getKApp();
            }
        }

        return reply;
    }

    public List<ArticuloJson> seleccionArticuloProductoCargaArray(List<ArticuloJson> reply, List<String[]> productos) throws Exception {
        log.info("....................................................................mbm............CONTROL");
                

        for (int ic1 = 0; ic1 < productos.size(); ic1++) {
            //for (String[] datosStr : productos) {
            ArticuloJson articuloJson = new ArticuloJson();
            Object datosStr[] = (Object[]) productos.get(ic1);

            articuloJson.setId(String.valueOf(datosStr[0]));
            articuloJson.setEstado(String.valueOf(datosStr[1]));
            articuloJson.setMethod(String.valueOf(datosStr[2]));
            articuloJson.setFechaPrevistaActivacion(Integer.valueOf(String.valueOf(datosStr[3])));
            articuloJson.setFechaPrevistaDesactivacion(Integer.valueOf(String.valueOf(datosStr[4])));
            articuloJson.setProductoId(String.valueOf(datosStr[5]));

            reply.add(articuloJson);
        }
        return reply;
    }
}
