package com.centraldecompras.rest.mat.ser.interfaces;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.rest.mat.json.ArticuloJson;
import com.centraldecompras.rest.mat.json.ProductoArbolJson;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

public interface ArticuloRestService {

    void persistencia(List<Object[]> listaObjetos) throws Exception;

    Articulo montaArticulo(ArticuloJson articuloJson, String idUser, Sociedad sociedad, FormatoVta formatoVta, SocProducto socProducto) throws NonexistentEntityException, Exception;

    Object[] montaArticuloProducto(ArticuloJson articuloJson, Articulo articulo, Producto producto, String idUser);

    Object[] montaArticuloLangDesc(Articulo articulo, TraduccionDescripcion traduccionDescripcion, String idUser, String metodo);

    Object[] montaArticuloLangNomb(Articulo articulo, TraduccionDescripcion traduccionDescripcion, String idUser, String metodo);

    Object[] montaArticuloDestacado(String articuloId, boolean destacado, String estadot, String method, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion, String idSoc, String idUser);

    Object[] montaArticuloFavorito(String articuloId, boolean favorito, String estadot, String method, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion, String idSoc, String idUser);

    Object[] montaArticuloAprobacion(Articulo articulo, String estadoAprobacion, String idUser, String method) throws NonexistentEntityException, Exception;
    
    List<ArticuloJson> seleccionArticuloProductoCargaArray(List<ArticuloJson> reply, List<String[]> productos) throws Exception;
    
}
