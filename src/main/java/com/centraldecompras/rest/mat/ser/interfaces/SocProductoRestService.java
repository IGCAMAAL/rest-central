package com.centraldecompras.rest.mat.ser.interfaces;

import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.rest.lgt.json.ZonaProductoAsigJson;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.Map;

public interface SocProductoRestService {

    List<Object[]> montaSociedadProducto(List<String> socDateIn, SociedadRpl sociedadRpl, String idUser, String idSoc, String idPrf, String idSec) throws NonexistentEntityException, Exception;

    List<ZonaProductoAsigJson> recuperaSociedadProducto(String sociedadId);

    //void persistencia(List<Object[]> listaObjetos) throws Exception;

    void persistencia(List<Object> createdObjets, List<Object> updatedObjets, List<Object> deletdObjets, List<Object[]> listaObjetos) throws Exception ;
            
    Map<String, Object> crdExcludes(SocProducto socProducto, String accion) throws Exception;

}
