package com.centraldecompras.rest.mat;

import com.centraldecompras.acceso.Sector;
import com.centraldecompras.acceso.service.interfaces.SectorService;
import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.modelo.EtiquetaLang;
import com.centraldecompras.rest.mat.json.ProductoArbolJson;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.ProductoEtiqueta;
import com.centraldecompras.modelo.ProductoLang;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoLangService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.SectorRpl;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenUsuarioProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoEtiquetaService;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoLangService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaService;
import com.centraldecompras.modelo.rpl.service.interfaces.SectorRplService;
import com.centraldecompras.rest.excel.interfaces.ProductoArbolExcelService;
import com.centraldecompras.rest.mat.ser.interfaces.ProductoRestService;
import com.centraldecompras.rest.prm.json.EtiquetaJson;
import com.centraldecompras.util.Converter;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.commons.codec.binary.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ciberado
 */
@RestController
@RequestMapping("/mat")
@Api(value = "Recursos públicos", description = "Demo de recursos accesibles públicamente sin autentificación.")
public class ProductoArbolController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductoArbolController.class.getName());

    @Autowired
    private ProductoService productoService;

    @Autowired
    private ProductoLangService productoLangService;

    @Autowired
    private SectorRplService sectorRplService;

    @Autowired
    private ProductoRestService productoRestService;

    @Autowired
    private ProductoEtiquetaService productoEtiquetaService;

    @Autowired
    ProductoArbolExcelService productoArbolExcelService;

    @Autowired
    private EtiquetaLangService etiquetaLangService;

    @Autowired
    private SectorService sectorService;

    @Autowired
    private NivelProductoLangService nivelProductoLangService;

    @Autowired
    private UnidadMedidaLangService unidadMedidaLangService;

    @Autowired
    private UnidadMedidaService unidadMedidaService;

    @Autowired
    private AlmacenUsuarioProductoService almacenUsuarioProductoService;

    @RequestMapping(value = "/productoarbol", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar el Arbol de productos", notes = "ArbolProductoJson, formato de comunicación con cliente")
    List<ProductoArbolJson> postProductos(
            @RequestBody List<ProductoArbolJson> productosArbolJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");
        SectorRpl sectorRpl = null;

        sectorRpl = sectorRplService.findSectorRpl(idSec);
        if (sectorRpl == null) {
            throw new Exception("CentralCompras. CRUD Producto-Arbol. Id Sector: [" + idSec + "] no existe.");
        }

        List<ProductoArbolJson> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();

        for (ProductoArbolJson productoArbolJson : productosArbolJson) {
            String id = productoArbolJson.getId();
            String metodo = productoArbolJson.getMethod();
            Object[] aaa = new Object[4];

            if (id.equals("0") && !metodo.equals("POST") || !id.equals("0") && metodo.equals("POST")) {
                throw new Exception("CentralCompras. CRUD Producto-Arbol. Id NivelProducto: [" + id + "] y Metodo: [" + metodo + "] no están sincronizados.");
            } else {

                Producto parent = productoService.findProducto(productoArbolJson.getParent());
                if (parent == null) {
                    Object[] valores = {productoArbolJson.getParent()};
                    String mensaje = MessageFormat.format("ERROR: CentralCompras. CRUD Producto-Arbol. Id parent:  no existe. {0}", valores);
                    throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                }

                aaa = productoRestService.montaProducto(productoArbolJson, sectorRpl, idUser, parent);
                Producto producto = (Producto) aaa[2];
                productoArbolJson.setId(producto.getIdProducto());  // Para busqueda al final del método y presentación de la persistencia
                listaObjetos.add(aaa);

                List<TraduccionDescripcion> traduccionesDescripcion = (List<TraduccionDescripcion>) productoArbolJson.getTraducciones();
                for (TraduccionDescripcion traduccionDescripcion : traduccionesDescripcion) {
                    Object[] bbb = new Object[5];

                    bbb = productoRestService.montaProductoLang(producto, traduccionDescripcion, idUser, productoArbolJson.getMethod());
                    ProductoLang productoLang = (ProductoLang) bbb[2];
                    traduccionDescripcion.setIdioma(productoLang.getIdioma());  // Para busqueda al final del método y presentación de la persistencia
                    listaObjetos.add(bbb);
                }

                List<EtiquetaJson> etiquetas = (List<EtiquetaJson>) productoArbolJson.getEtiquetas();
                for (EtiquetaJson etiquetaJson : etiquetas) {
                    List<String> etiquetaObj = new ArrayList();
                    etiquetaObj.add(etiquetaJson.getMethod());
                    etiquetaObj.add(etiquetaJson.getId());
                    Object[] ccc = new Object[4];

                    ccc = productoRestService.montaProductoEtiqueta(producto, etiquetaObj, idUser, productoArbolJson.getMethod());
                    listaObjetos.add(ccc);
                }
            }
        }

        productoRestService.persistencia(listaObjetos);

        for (ProductoArbolJson productoArbolJson : productosArbolJson) {
            Producto producto = productoService.findProducto(productoArbolJson.getId());

            if (producto != null) {
                ProductoArbolJson productoArbolJsonOut = null;
                productoArbolJsonOut = toJson(producto);

                ProductoArbolJson subProductoArbolJson = null;
                List<ProductoArbolJson> subProductosJson = new ArrayList<ProductoArbolJson>();
                List<Producto> subProductos = productoService.findProductosBy_ParentId(producto);
                for (Producto subProducto : subProductos) {
                    subProductoArbolJson = new ProductoArbolJson();
                    subProductoArbolJson = toJson(subProducto);
                    subProductosJson.add(subProductoArbolJson);
                }
                productoArbolJsonOut.setSubproductos(subProductosJson);
                reply.add(productoArbolJsonOut);
            }
        }

        return reply;
    }

    @RequestMapping(value = "/productoarbol/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera arbol de productos", notes = "ArbolProductoJson, formato de comunicación con cliente")

    ProductoArbolJson getProductos(
            @PathVariable String id,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        ProductoArbolJson reply = new ProductoArbolJson();
        reply.setSubproductos(new ArrayList<ProductoArbolJson>());

        if (id.equals("0")) {
            id = KApp.PRODUCTO_UNIVERSAL.getKApp();
        }
        Producto producto = productoService.findProducto(id);

        if (producto != null) {
            reply = toJson(producto);

            ProductoArbolJson subProductoArbolJson = null;
            List<ProductoArbolJson> subProductosJson = new ArrayList<ProductoArbolJson>();
            List<Producto> subProductos = productoService.findProductosBy_ParentId(producto);
            for (Producto subProducto : subProductos) {
                subProductoArbolJson = new ProductoArbolJson();
                subProductoArbolJson = toJson(subProducto);
                subProductosJson.add(subProductoArbolJson);
            }
            reply.setSubproductos(subProductosJson);
        }
        return reply;
    }

    @RequestMapping(value = "/productoarbol/seleccion/{cadena}/{estado}/{cantidad}/{niveles}/{idioma}", method = RequestMethod.GET)
    List<ProductoArbolJson> seleccionProducto(
            @PathVariable String cadena,
            @PathVariable String estado,
            @PathVariable String cantidad,
            @PathVariable String niveles,
            @PathVariable String idioma,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<ProductoArbolJson> reply = new ArrayList<ProductoArbolJson>();
        List<String[]> prodS = new ArrayList<String[]>();

        int cantidadInt = 0;
        try {
            cantidadInt = Integer.parseInt(cantidad);
        } catch (NumberFormatException ex) {
            throw new Exception("CentralCompras. Combo ProductoArbolJson. Parametro 'cantidad' de resultados no es valido : " + ex.getMessage());
        }

        if (cadena == null || cadena.equals("")) {
            throw new Exception("CentralCompras. Combo ProductoArbolJson. Parametro 'cadena' no es valido");
        }

        StringTokenizer st2 = new StringTokenizer(niveles, "-");
        List<Integer> nivelesList = new ArrayList<Integer>();

        try {
            while (st2.hasMoreElements()) {
                String niv = (String) st2.nextElement();
                nivelesList.add(Integer.valueOf(niv));
            }
        } catch (NumberFormatException ex) {
            throw new Exception("CentralCompras. Combo ProductoArbolJson. Parametro 'niveles' contiene datos no validos");
        } catch (Exception ex) {
            throw new Exception("CentralCompras. Combo ProductoArbolJson. Parametro 'niveles' contiene datos no validos");
        }

        /*Simulacion****************** Igual que zonas geograficas */
        prodS = productoService.findProductoTypeAhead(1, cadena, estado.toUpperCase(), cantidadInt, nivelesList, idioma);
        reply = productoRestService.seleccionProductosCargaArray(reply, prodS);

        if (reply.size() < cantidadInt) {
            prodS = productoService.findProductoTypeAhead(2, cadena, estado.toUpperCase(), cantidadInt - reply.size(), nivelesList, idioma);
            reply = productoRestService.seleccionProductosCargaArray(reply, prodS);
        }

        if (!reply.isEmpty()) {
            for (ProductoArbolJson reply1 : reply) {
                ProductoArbolJson productoArbolJson = (ProductoArbolJson) reply1;
                reply1.setTraducciones(new ArrayList<TraduccionDescripcion>());
                List<ProductoLang> productosLang = productoLangService.findProductoLangBy_IdP(productoArbolJson.getId());
                for (ProductoLang productoLang : productosLang) {
                    TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
                    traduccionDescripcion.setIdioma(productoLang.getIdioma());
                    traduccionDescripcion.setTraduccion(productoLang.getTraduccionDesc());
                    reply1.getTraducciones().add(traduccionDescripcion);
                }
            }
        }
        return reply;

    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/excelExportProductos", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera todos los Articulos y los muestra en excel ", notes = "SuppListJson, formato de comunicacion con cliente")
    Map<String, String> getExcelExportProductos(
            @RequestBody Map<String, String> paramInput,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec,
            HttpServletResponse response
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        XSSFWorkbook workbook = null;

        Sector sector = sectorService.findSector(idSec);
        if (sector == null) {
            Object[] valores = {sector};
            String mensaje = MessageFormat.format("ERROR: Productos.xlsx  sector no encontrado. {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }
        int ultimoNivelSector = sector.getUltimoNivel();

        /*
         for (Map<String, String> filtro : filtros) {
         filtrosHash.put(filtro.get("nivel"), filtro.get("descripcionProd"));
         }
         */
        HashMap filtrosHash = new HashMap();
        for (int ic1 = 0; ic1 < ultimoNivelSector; ic1++) {
            filtrosHash.put(ic1, "");
        }
        workbook = productoArbolExcelService.montaExcel(filtrosHash, ultimoNivelSector, paramInput);
        // XSSFWorkbook workbookTest = productoArbolExcelService.montaExcel(filtrosHash, ultimoNivelSector, paramInput);

        String nameFile = KApp.PRODUCTOS.getKApp();
        String extensionFile = KApp.XLSX.getKApp();

        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        workbook.write(outByteStream);

        byte[] outArray = outByteStream.toByteArray();
        byte[] encoded = Base64.encodeBase64(outArray);
        String picture = new String(encoded);

        Map<String, String> reply = new HashMap<>();
        reply.put(KApp.NAME_FILE.getKApp(), nameFile);
        reply.put(KApp.EXTENSION_FILE.getKApp(), extensionFile);
        reply.put(KApp.PICTURE.getKApp(), picture);

        //Write the workbook in file system
  /*      
         try {
         if (workbookTest != null) {
         FileOutputStream out = new FileOutputStream(new File(nameFile.concat(".").concat(extensionFile)));
         workbookTest.write(out);
         out.close();
         log.info(nameFile + "." + extensionFile + " written successfully on disk.");
         }
         } catch (Exception e) {
         Object[] valores = {"Generacion excel de Productos"};
         String mensaje = MessageFormat.format("ERROR: Productos.xlsx no generado. {0}", valores);
         throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
         }
         */
        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/excelImportProductos", method = RequestMethod.POST)
    @ApiOperation(value = "carga masiva de arbol de productos desde Excel ", notes = "Multipart, formato de comunicacion con cliente")
    @Consumes("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    String postExcelImportProductos(
            @RequestBody Map<String, String> excelMapIn,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        Sector sector = sectorService.findSector(idSec);
        if (sector == null) {
            Object[] valores = {idSec};
            String mensaje = MessageFormat.format("ERROR: Productos.xlsx  sector no encontrado. {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }
        SectorRpl sectorRpl = sectorRplService.findSectorRpl(idSec);
        if (sectorRpl == null) {
            Object[] valores = {idSec};
            String mensaje = MessageFormat.format("ERROR: Productos.xlsx  sector no encontrado. {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        List<String[]> unidadesMedidaLang = unidadMedidaLangService.findUnidadMedidaLangBy_Lang(KApp.CATALAN.getKApp());
        List<Object> outConverted = Converter.listArrayStringToHashAndArrayString(unidadesMedidaLang);
        //String[] unidadesMedidaDesc = (String[]) outConverted.get(0);
        HashMap unidadesMedidaHash = (HashMap) outConverted.get(1);

        // Preparación del loop
        // Lectura de los idiomas utilizados. NIVEL_PRODUCTO_LANG
        List<String> idiomasNivelProducto = nivelProductoLangService.findProductoLangKey();
        int numNivelesSector = sector.getUltimoNivel();

        XSSFWorkbook workbook = null;
        List<Object[]> listaObjetos = new ArrayList();

        try {
            String nameFile = (String) excelMapIn.get("excelNomOriB64");
            String excelFileBase64 = (String) excelMapIn.get("excelFilesB64");

            byte[] excelFileByte64 = excelFileBase64.getBytes();
            byte[] excelFileBytes = Base64.decodeBase64(excelFileByte64);
            InputStream excelFileIs = (InputStream) new ByteArrayInputStream(excelFileBytes);

            /*
             File excelFileIs = new File("ProductosIn.xlsx");
             String nameFile = excelFileIs.getName();
             workbook = (XSSFWorkbook) WorkbookFactory.create(excelFile);
            
             //-- encode a Base64
             byte[] bytes = loadFile(excelFileIs);
             byte[] encoded = Base64.encodeBase64(bytes);
             String encodedString = new String(encoded);
            
             //-- decode de Base64
             byte[] decodedString1 = encodedString.getBytes();
             byte[] decodedString2 = Base64.decodeBase64(decodedString1);
             InputStream excelFileIs2 = (InputStream) new ByteArrayInputStream(decodedString2);
             */
            workbook = (XSSFWorkbook) WorkbookFactory.create(excelFileIs);

            XSSFSheet sheet = workbook.getSheet("Productos");
            Iterator<Row> rowIterator = sheet.iterator();
            String method = null;

            List<Object[]> aaaList = new ArrayList();
            Object[] bbb = null;
            List<Object[]> cccList = new ArrayList();

            boolean inicio = true;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if (inicio == true) {
                    row = rowIterator.next();
                    inicio = false;
                }

                Producto parent = null;
                for (int idxNivel = 1; idxNivel <= numNivelesSector; idxNivel++) {

                    // Verificaa si la linea excel es correcta. ----------------
                    boolean productosLangNuevosOK = true;
                    int idxLoopsfalse = 0;
// Investiga estan informados todos los idiomas del nodo del producto
                    for (int idiomaIc = 0; idiomaIc < idiomasNivelProducto.size(); idiomaIc++) {
                        Cell cellDesc = row.getCell(1 + idiomaIc * (numNivelesSector + 1) + (idxNivel));
                        if (cellDesc == null || cellDesc.getStringCellValue().trim().equals("")) {
                            productosLangNuevosOK = false;
                            idxLoopsfalse++;
                        }
                    }
                    if (!productosLangNuevosOK) {   // MBM TODO 
                        if (idxLoopsfalse < idiomasNivelProducto.size()) {
                            Object[] valores = {"-"};
                            String mensaje = MessageFormat.format("ERROR: Productos.xlsx  Descripcion nula o vacia. {0}", valores);
                            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                        }
                    } // Encuentra celda sin descripción, por lo que salta a siguiente linea
                    if (!productosLangNuevosOK) {
                        if (idxLoopsfalse == idiomasNivelProducto.size()) {
                            idxNivel = 4;
                            break;
                        }
                    }

                    // Identificacion de la Unidad de Medida
                    String unidadMedidaId = (String) unidadesMedidaHash.get(row.getCell(0).getStringCellValue());
                    if (unidadMedidaId == null) {
                        Object[] valores = {unidadMedidaId};
                        String mensaje = MessageFormat.format("ERROR: Productos.xlsx  Unidad de medida erronea. {0}", valores);
                        throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                    }
                    UnidadMedida unidadMedida = unidadMedidaService.findUnidadMedida(unidadMedidaId);
                    if (unidadMedida == null) {
                        Object[] valores = {unidadMedidaId};
                        String mensaje = MessageFormat.format("ERROR: Productos.xlsx  Unidad de medida erronea. {0}", valores);
                        throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                    }

                    // La celda excel es correcta y la procesamos. -------------
                    int celdaInt_ProductoId = 1 + idiomasNivelProducto.size() * (numNivelesSector + 1) + (idxNivel - 1);  // id de producto
                    Cell cell_ProductoId = row.getCell(celdaInt_ProductoId);

                    // BIEN, Si id no existe lo toma como alta
                    if (cell_ProductoId == null || cell_ProductoId.getStringCellValue().equals("")) {
                        method = KApp.POST.getKApp();
                    } else {
                        method = KApp.PUT.getKApp();
                    }

                    if (KApp.PUT.getKApp().equals(method)) {
                        Producto producto = productoService.findProducto(cell_ProductoId.getStringCellValue());
                        List<ProductoLang> productosLang = productoLangService.findProductoLangBy_IdP(cell_ProductoId.getStringCellValue());
                        producto.setUnidadMedida(unidadMedida);
                        parent = (Producto) BeanUtils.cloneBean(producto);

                        aaaList = actualizaIdiomasNuevosProductos(idiomasNivelProducto.size(), row, numNivelesSector, idxNivel, producto, idUser, method, productosLang);
                        listaObjetos.addAll(aaaList);
                    }

                    // Post hace tres cosas: a-Crea Productos, b-Crea Idiomas de productos,  c-Forma Arbol de productos.
                    if (KApp.POST.getKApp().equals(method)) {
                        if (idxNivel == 1) {
                            parent = productoService.findProducto(KApp.PRODUCTO_UNIVERSAL.getKApp());
                        }
                        if (parent == null) {
                            Object[] valores = {KApp.PRODUCTO_UNIVERSAL.getKApp()};
                            String mensaje = MessageFormat.format("ERROR: CentralCompras. CRUD Producto-Arbol. Id parent:  no existe. {0}", valores);
                            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
                        }

                        bbb = altaNuevoProducto(sectorRpl, idUser, parent, idxNivel, unidadMedidaId, method);
                        Producto producto = (Producto) bbb[2];
                        parent = (Producto) bbb[2];
                        listaObjetos.add(bbb);

                        cccList = altaIdiomasNuevosProductos(idiomasNivelProducto.size(), row, numNivelesSector, idxNivel, producto, idUser, method);
                        listaObjetos.addAll(cccList);

                        // En principio, el alta masiva de prodcutos con excel, se realiza sin etiuquetas.
                        // List<String> etiquetaObj = new ArrayList();
                        // productoRestService.montaProductoEtiqueta(producto, etiquetaObj, idUser, method);
                    }
                    log.info("..................... 1 Fin Idiomas                   ");
                }
                log.info("..................... 2 Fin niveles                  ");
            }
            log.info("..................... 3 Fin Rows                 ");

        } catch (Exception e) {
            log.info("..................... 4  Exception                ");
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        log.info("..................... 5 Fin del try                 ");

        productoRestService.persistencia(listaObjetos);

        return "OK";
    }

    private Object[] altaNuevoProducto(SectorRpl sectorRpl, String idUser, Producto parent, int idxNivel, String unidadMedidaId, String method) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        Object[] reply = new Object[5];
        ProductoArbolJson productoArbolJson = new ProductoArbolJson();
        productoArbolJson.setId("0");
        productoArbolJson.setNivel(idxNivel);
        productoArbolJson.setUnidadMedida(unidadMedidaId);
        productoArbolJson.setEstado(KApp.ACTIVO.getKApp());
        productoArbolJson.setMethod(method);
        productoArbolJson.setFechaPrevistaActivacion(0);
        productoArbolJson.setFechaPrevistaDesactivacion(0);
        productoArbolJson.setTraducciones(null);
        productoArbolJson.setSubproductos(null);
        productoArbolJson.setEtiquetas(null);
        productoArbolJson.setParent(parent.getIdProducto());

        reply = productoRestService.montaProducto(productoArbolJson, sectorRpl, idUser, parent);
        return reply;
    }

    private List<Object[]> altaIdiomasNuevosProductos(int idiomasNivelProducto, Row row, int numNivelesSector, int idxNivel, Producto producto, String idUser, String method) {
        log.info("....................................................................mbm............CONTROL");

        List<Object[]> reply = new ArrayList<Object[]>();

        TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
        for (int idiomaIc = 0; idiomaIc < idiomasNivelProducto; idiomaIc++) {
            Cell cellIdioma = row.getCell(1 + idiomaIc * (numNivelesSector + 1));
            Cell cellDesc = row.getCell(1 + idiomaIc * (numNivelesSector + 1) + (idxNivel));

            traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(cellIdioma.getStringCellValue());
            traduccionDescripcion.setTraduccion(cellDesc.getStringCellValue());

            Object[] aaa = new Object[5];
            aaa = productoRestService.montaProductoLang(producto, traduccionDescripcion, idUser, method);
            reply.add(aaa);
        }

        return reply;

    }

    private List<Object[]> actualizaIdiomasNuevosProductos(int idiomasNivelProducto, Row row, int numNivelesSector, int idxNivel, Producto producto, String idUser, String method, List<ProductoLang> productosLang) {
        log.info("....................................................................mbm............CONTROL");

        List<Object[]> reply = new ArrayList();

        for (int idiomaIc = 0; idiomaIc < idiomasNivelProducto; idiomaIc++) {
            Cell cellIdioma = row.getCell(1 + idiomaIc * (numNivelesSector + 1));
            Cell cellDesc = row.getCell(1 + idiomaIc * (numNivelesSector + 1) + (idxNivel));

            String idiomaSel = cellIdioma.getStringCellValue();
            String descSel = cellDesc.getStringCellValue();

            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();

            traduccionDescripcion.setIdioma(cellIdioma.getStringCellValue());
            traduccionDescripcion.setTraduccion(descSel);

            String methodProductoLang = KApp.POST.getKApp();
            for (ProductoLang productoLang : productosLang) {
                if (productoLang.getIdioma().equals(idiomaSel)) {
                    methodProductoLang = KApp.PUT.getKApp();
                    break;
                }
            }

            Object[] aaa = new Object[5];
            aaa = productoRestService.montaProductoLang(producto, traduccionDescripcion, idUser, methodProductoLang);
            reply.add(aaa);
        }

        return reply;
    }

    private static byte[] loadFile(File file) throws IOException {
        log.info("....................................................................mbm............CONTROL");

        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }
    /*
     private List<ProductoArbolJson> seleccionProductosCargaArray(List<ProductoArbolJson> reply, List<String[]> productos) throws Exception {

     for (int ic1 = 0; ic1 < productos.size(); ic1++) {
     //for (String[] datosStr : productos) {
     ProductoArbolJson productoArbolJson = new ProductoArbolJson();
     Object datosStr[] = (Object[]) productos.get(ic1);

     productoArbolJson.setId(String.valueOf(datosStr[0]));
     productoArbolJson.setNivel(Integer.valueOf(String.valueOf(datosStr[1])));
     productoArbolJson.setParent(String.valueOf(datosStr[2]) == null ? KApp.PRODUCTO_UNIVERSAL.getKApp() : String.valueOf(datosStr[2]));
     productoArbolJson.setEstado(String.valueOf(datosStr[3]));
     productoArbolJson.setMethod(String.valueOf(datosStr[4]));
     productoArbolJson.setFechaPrevistaActivacion(Integer.valueOf(String.valueOf(datosStr[5])));
     productoArbolJson.setFechaPrevistaDesactivacion(Integer.valueOf(String.valueOf(datosStr[6])));

     reply.add(productoArbolJson);
     }
     return reply;
     }
     */

    private ProductoArbolJson toJson(Producto producto) {
        log.info("....................................................................mbm............CONTROL");

        ProductoArbolJson reply = new ProductoArbolJson();
        reply.setSubproductos(new ArrayList<>());

        reply.setId(producto.getIdProducto());
        if (producto.getIdProducto().equals(KApp.PRODUCTO_UNIVERSAL.getKApp())) {
            reply.setParent(null);
        } else {
            reply.setParent(producto.getPadreJerarquia().getIdProducto());
        }
        reply.setEstado(producto.getAtributosAuditoria().getEstadoRegistro());
        reply.setMethod(producto.getAtributosAuditoria().getUltimaAccion());
        reply.setFechaPrevistaActivacion(producto.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(producto.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setNivel(producto.getNivelInt());
        if (producto.getUnidadMedida() != null) {
            reply.setUnidadMedida(producto.getUnidadMedida().getIdUnidadMedida());
        } else {
            reply.setUnidadMedida(null);
        }

        List<ProductoLang> productosLang = productoLangService.findProductoLangBy_P(producto);
        List<TraduccionDescripcion> traducciones = new ArrayList<TraduccionDescripcion>();
        for (ProductoLang productoLang : productosLang) {
            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(productoLang.getIdioma());
            traduccionDescripcion.setTraduccion(productoLang.getTraduccionDesc());

            traducciones.add(traduccionDescripcion);
        }
        reply.setTraducciones(traducciones);

        // Datos de las etiquetas
        reply.setSubproductos(new ArrayList<>());
        List<EtiquetaJson> etiquetasJson = new ArrayList();
        List<ProductoEtiqueta> productoEtiquetas = productoEtiquetaService.findEtiquetasBy_idP(producto.getIdProducto());
        for (ProductoEtiqueta productoEtiqueta : productoEtiquetas) {
            EtiquetaJson etiquetaJson = new EtiquetaJson();
            etiquetaJson.setMethod(productoEtiqueta.getAtributosAuditoria().getUltimaAccion());
            etiquetaJson.setId(productoEtiqueta.getEtiquetaIdk());

            List<EtiquetaLang> etiquetasLang = etiquetaLangService.findEtiquetaLangBy_E(productoEtiqueta.getEtiqueta());
            List<TraduccionDescripcion> traduccionesEti = new ArrayList();
            for (EtiquetaLang etiquetaLang : etiquetasLang) {
                TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
                traduccionDescripcion.setIdioma(etiquetaLang.getIdioma());
                traduccionDescripcion.setTraduccion(etiquetaLang.getTraduccionDesc());
                traduccionesEti.add(traduccionDescripcion);
            }
            etiquetaJson.setTraducciones(traduccionesEti);
            etiquetasJson.add(etiquetaJson);
        }
        reply.setEtiquetas(etiquetasJson);

        return reply;
    }

    @PostConstruct
    public void init() throws Exception {
        try {
            List<Object> updatedObjets = new ArrayList();
            List<Producto> productos = productoService.findProductoEntities();
            
 
            for (Producto producto : productos) {

                producto = productoRestService.informaRoute(producto);
                updatedObjets.clear();
                updatedObjets.add(producto);
                productoRestService.persistencia(new ArrayList(), updatedObjets);

            }
        } catch (Exception ex) {
            log.warn(ex.getMessage());
            throw ex;
        }
    }

}
