package com.centraldecompras.rest.mat.json;

import com.centraldecompras.rest.fin.json.auxi.AuditImgJson;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import java.util.List;

public class ArticuloJson {

    private String id;
    private String productoId;
    private Double uniMinVta;
    private String sociedadId;
    private String sociedadNombre;
    private Double precioUniMed;
    private Double precioUniMedNuevo;
    private Double unidadFormatoVta;
    private String formatoVentaId;
    private String moneda;
    private String descrArticulo;
    private String nombreArticulo;
    private String codiParaProveedor;
    private String codigoEAN;
    private String estadoAprobacion;
    private List<TraduccionDescripcion> traduccionesDesc;
    private List<TraduccionDescripcion> traduccionesNombre;
    private String estado;
    private String method;
    private int fechaPrevistaActivacion;
    private int fechaPrevistaDesactivacion;
    private List<AuditImgJson> dataImgFoto;
    private List<AuditImgJson> dataImgCatalogo;
    private String ModoCompra;
    private Boolean verPrecio;

    public String getModoCompra() {
        return ModoCompra;
    }

    public void setModoCompra(String ModoCompra) {
        this.ModoCompra = ModoCompra;
    }

    public Boolean getVerPrecio() {
        return verPrecio;
    }

    public void setVerPrecio(Boolean verPrecio) {
        this.verPrecio = verPrecio;
    }
    
    
    
    private List<String[]> imagenes;   

    public String getSociedadId() {
        return sociedadId;
    }

    public void setSociedadId(String sociedadId) {
        this.sociedadId = sociedadId;
    }

    public String getSociedadNombre() {
        return sociedadNombre;
    }

    public void setSociedadNombre(String sociedadNombre) {
        this.sociedadNombre = sociedadNombre;
    }
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductoId() {
        return productoId;
    }

    public void setProductoId(String productoId) {
        this.productoId = productoId;
    }

    public Double getUniMinVta() {
        return uniMinVta;
    }

    public void setUniMinVta(Double uniMinVta) {
        this.uniMinVta = uniMinVta;
    }

    public Double getPrecioUniMed() {
        return precioUniMed;
    }

    public void setPrecioUniMed(Double precioUniMed) {
        this.precioUniMed = precioUniMed;
    }

    public Double getPrecioUniMedNuevo() {
        return precioUniMedNuevo;
    }

    public void setPrecioUniMedNuevo(Double precioUniMedNuevo) {
        this.precioUniMedNuevo = precioUniMedNuevo;
    }

    public Double getUnidadFormatoVta() {
        return unidadFormatoVta;
    }

    public void setUnidadFormatoVta(Double unidadFormatoVta) {
        this.unidadFormatoVta = unidadFormatoVta;
    }

    public String getFormatoVentaId() {
        return formatoVentaId;
    }

    public void setFormatoVentaId(String formatoVentaId) {
        this.formatoVentaId = formatoVentaId;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getDescrArticulo() {
        return descrArticulo;
    }
 
    public void setDescrArticulo(String descrArticulo) {
        this.descrArticulo = descrArticulo;
    }

    public String getNombreArticulo() {
        return nombreArticulo;
    }

    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public String getCodiParaProveedor() {
        return codiParaProveedor;
    }

    public void setCodiParaProveedor(String codiParaProveedor) {
        this.codiParaProveedor = codiParaProveedor;
    }

    public String getCodigoEAN() {
        return codigoEAN;
    }

    public void setCodigoEAN(String codigoEAN) {
        this.codigoEAN = codigoEAN;
    }

    public String getEstadoAprobacion() {
        return estadoAprobacion;
    }

    public void setEstadoAprobacion(String estadoAprobacion) {
        this.estadoAprobacion = estadoAprobacion;
    }

    public List<TraduccionDescripcion> getTraduccionesDesc() {
        return traduccionesDesc;
    }

    public void setTraduccionesDesc(List<TraduccionDescripcion> traduccionesDesc) {
        this.traduccionesDesc = traduccionesDesc;
    }

    public List<TraduccionDescripcion> getTraduccionesNombre() {
        return traduccionesNombre;
    }

    public void setTraduccionesNombre(List<TraduccionDescripcion> traduccionesNombre) {
        this.traduccionesNombre = traduccionesNombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public List<AuditImgJson> getDataImgFoto() {
        return dataImgFoto;
    }

    public void setDataImgFoto(List<AuditImgJson> dataImgFoto) {
        this.dataImgFoto = dataImgFoto;
    }

    public List<AuditImgJson> getDataImgCatalogo() {
        return dataImgCatalogo;
    }

    public void setDataImgCatalogo(List<AuditImgJson> dataImgCatalogo) {
        this.dataImgCatalogo = dataImgCatalogo;
    }

    public List<String[]> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<String[]> imagenes) {
        this.imagenes = imagenes;
    }



}
