/* Copyright 2014, Javier Moreno.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 * And have fun ;-)
 */
package com.centraldecompras.rest.mat.json;


import com.centraldecompras.rest.prm.json.EtiquetaJson;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import java.util.List;

/**
 *
 * @author Miguel
 */


public class ProductoArbolJson {
    
 	private String id;                                          
        private String parent;                                      
        private int     nivel;
        private String unidadMedida;
 	private String estado;                                      
        private String method; 
 	private int fechaPrevistaActivacion;                        
 	private int fechaPrevistaDesactivacion;  
        private List<TraduccionDescripcion> traducciones;
        private List<ProductoArbolJson> subproductos;
        private List<EtiquetaJson> etiquetas;
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }


    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public List<TraduccionDescripcion> getTraducciones() {
        return traducciones;
    }

    public void setTraducciones(List<TraduccionDescripcion> traducciones) {
        this.traducciones = traducciones;
    }

    public List<ProductoArbolJson> getSubproductos() {
        return subproductos;
    }

    public void setSubproductos(List<ProductoArbolJson> subproductos) {
        this.subproductos = subproductos;
    }

    public List<EtiquetaJson> getEtiquetas() {
        return etiquetas;
    }

    public void setEtiquetas(List<EtiquetaJson> etiquetas) {
        this.etiquetas = etiquetas;
    }


}
