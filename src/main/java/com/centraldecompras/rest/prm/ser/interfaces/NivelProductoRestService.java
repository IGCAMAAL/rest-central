package com.centraldecompras.rest.prm.ser.interfaces;

import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.rest.prm.json.ParametroUMedFVta;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

public interface NivelProductoRestService {

    void persistencia(List<Object[]> listaObjetos) throws Exception;

    Object[] montaNivelProductoLang(ParametroUMedFVta parametroUMedFVta, TraduccionDescripcion traduccionDescripcion, NivelProducto nivelProducto, String idUser);

}
