package com.centraldecompras.rest.prm.ser;

import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.UnidadMedidaLang;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaService;
import com.centraldecompras.rest.prm.json.ParametroUMedFVta;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.rest.prm.ser.interfaces.UnidadMedidaRestService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UnidadMedidaRestServiceImpl implements UnidadMedidaRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UnidadMedidaRestServiceImpl.class.getName());

    @Autowired
    private UnidadMedidaService unidadMedidaService;

    @Autowired
    private UnidadMedidaLangService unidadMedidaLangService;

    public Object[] montaUnidadMedida(ParametroUMedFVta parametroUMedFVta, String estadoFVta, String idUser) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");

        UnidadMedida unidadMedida = null;

        String metodo = parametroUMedFVta.getMethod();

        if (("POST").equals(metodo)) {
            unidadMedida = new UnidadMedida();
            unidadMedida.setIdUnidadMedida(UUID.randomUUID().toString().replaceAll("-", ""));
            unidadMedida.setUnidadMedidaCod(" ");
            unidadMedida.setUnidadMedidaDesc(" ");
            unidadMedida.setAtributosAuditoria(new DatosAuditoria());
            unidadMedida.getAtributosAuditoria().setCreated(new Date());
            if (ElementEnum.KApp.PUT.getKApp().equals(metodo)) {
                metodo = ElementEnum.KApp.POST.getKApp();
            }
        } else {  // Modificacion o Delete
            unidadMedida = unidadMedidaService.findUnidadMedida(parametroUMedFVta.getId());
            unidadMedida.getAtributosAuditoria().setUpdated(new Date());
        }

        if (unidadMedida != null) {
            unidadMedida.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            unidadMedida.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            unidadMedida.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
            unidadMedida.getAtributosAuditoria().setEstadoRegistro(estadoFVta);
            unidadMedida.getAtributosAuditoria().setUltimaAccion(metodo);

        }

        Object[] aaa = new Object[4];
        if (ElementEnum.KApp.POST.getKApp().equals(metodo) || ElementEnum.KApp.PUT.getKApp().equals(metodo)) {
            aaa[2] = unidadMedida;
            aaa[3] = null;
        } else if (ElementEnum.KApp.DELETE.getKApp().equals(metodo)) {
            aaa[2] = null;
            aaa[3] = parametroUMedFVta.getId();
        }
        aaa[0] = metodo;
        aaa[1] = "unidadMedida";
        return aaa;
    }

    public Object[] montaUnidadMedidaLang(ParametroUMedFVta parametroUMedFVta, TraduccionDescripcion traduccionDescripcion, UnidadMedida unidadMedida, String idUser) {
        log.info("....................................................................mbm............CONTROL");

        UnidadMedidaLang unidadMedidaLang = new UnidadMedidaLang();

        unidadMedidaLang = unidadMedidaLangService.findUnidadMedidaLangBy_IdFV_Lang(unidadMedida.getIdUnidadMedida(), traduccionDescripcion.getIdioma());
        if (unidadMedidaLang == null) {
            unidadMedidaLang = new UnidadMedidaLang();
            unidadMedidaLang.setUnidadMedidaId(unidadMedida.getIdUnidadMedida());
            unidadMedidaLang.setUnidadMedidaEntity(unidadMedida);
            unidadMedidaLang.setIdioma(traduccionDescripcion.getIdioma());
            unidadMedidaLang.setAtributosAuditoria(new DatosAuditoria());
            unidadMedidaLang.getAtributosAuditoria().setCreated(new Date());
        } else {
            unidadMedidaLang.getAtributosAuditoria().setUpdated(new Date());
        }

        if (unidadMedidaLang != null) {
            unidadMedidaLang.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            unidadMedidaLang.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            unidadMedidaLang.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
            unidadMedidaLang.getAtributosAuditoria().setEstadoRegistro(parametroUMedFVta.getEstado());
            unidadMedidaLang.getAtributosAuditoria().setUltimaAccion(parametroUMedFVta.getMethod());
            unidadMedidaLang.setTraduccionDesc(traduccionDescripcion.getTraduccion());
        }

        Object[] aaa = new Object[5];
        aaa[0] = parametroUMedFVta.getMethod();
        aaa[1] = "unidadMedidaLang";
        aaa[2] = unidadMedidaLang;
        aaa[3] = parametroUMedFVta.getId();
        aaa[4] = traduccionDescripcion.getIdioma();

        return aaa;
    }

    @Transactional
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("unidadMedida").equals((String) bbb[1])) {
                    unidadMedidaService.create((UnidadMedida) bbb[2]);
                }
                if (("unidadMedidaLang").equals((String) bbb[1])) {
                    unidadMedidaLangService.create((UnidadMedidaLang) bbb[2]);
                }
            }

            if (("PUT").equals((String) bbb[0])) {
                if (("unidadMedida").equals((String) bbb[1])) {
                    unidadMedidaService.edit((UnidadMedida) bbb[2]);
                }
                if (("unidadMedidaLang").equals((String) bbb[1])) {
                    unidadMedidaLangService.edit((UnidadMedidaLang) bbb[2]);
                }
            }
        }

        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("unidadMedida").equals((String) bbb[1])) {
                    unidadMedidaService.destroy((String) bbb[3]);
                }
                if (("unidadMedidaLang").equals((String) bbb[1])) {
                    unidadMedidaLangService.deleteUnidadMedidaLangBy_idUM_Lang((String) bbb[3], (String) bbb[4]);
                }
            }
        }
    }

}
