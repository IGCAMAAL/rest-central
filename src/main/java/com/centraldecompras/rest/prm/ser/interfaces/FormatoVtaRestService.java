package com.centraldecompras.rest.prm.ser.interfaces;

import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.rest.prm.json.ParametroUMedFVta;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

public interface FormatoVtaRestService {

    void persistencia(List<Object[]> listaObjetos) throws Exception;

    Object[] montaFormatoVta(ParametroUMedFVta parametroUMedFVta, String estado, String idUser) throws NonexistentEntityException, Exception;

    Object[] montaFormatoVtaLang(ParametroUMedFVta parametroUMedFVta, TraduccionDescripcion traduccionDescripcion, FormatoVta formatoVta, String idUser);

}
