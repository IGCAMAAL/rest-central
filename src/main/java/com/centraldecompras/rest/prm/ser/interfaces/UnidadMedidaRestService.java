package com.centraldecompras.rest.prm.ser.interfaces;

import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.rest.prm.json.ParametroUMedFVta;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

public interface UnidadMedidaRestService {

    void persistencia(List<Object[]> listaObjetos) throws Exception;

    Object[] montaUnidadMedida(ParametroUMedFVta parametroUMedFVta, String estado, String idUser) throws NonexistentEntityException, Exception;

    Object[] montaUnidadMedidaLang(ParametroUMedFVta parametroUMedFVta, TraduccionDescripcion traduccionDescripcion, UnidadMedida unidadMedida, String idUser);

}
