package com.centraldecompras.rest.prm.ser;

import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.NivelProductoLang;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoLangService;
import com.centraldecompras.rest.prm.json.ParametroUMedFVta;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.rest.prm.ser.interfaces.NivelProductoRestService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NivelProductoRestServiceImpl implements NivelProductoRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NivelProductoRestServiceImpl.class.getName());

    @Autowired
    private NivelProductoLangService nivelProductoLangService;

    public Object[] montaNivelProductoLang(ParametroUMedFVta parametroUMedFVta, TraduccionDescripcion traduccionDescripcion, NivelProducto nivelProducto, String idUser) {
        log.info("....................................................................mbm............CONTROL");

        NivelProductoLang nivelProductolang = new NivelProductoLang();
        String metodoUM = parametroUMedFVta.getMethod();

        nivelProductolang = nivelProductoLangService.findNivelProductoLangBy_IdNP_Lang(nivelProducto.getIdNivelProducto(), traduccionDescripcion.getIdioma());

        if (nivelProductolang == null) {
            nivelProductolang = new NivelProductoLang();
            nivelProductolang.setNivelProductoId(nivelProducto.getIdNivelProducto());
            nivelProductolang.setNivelProductoEntity(nivelProducto);
            nivelProductolang.setIdioma(traduccionDescripcion.getIdioma());
            nivelProductolang.setAtributosAuditoria(new DatosAuditoria());
            nivelProductolang.getAtributosAuditoria().setCreated(new Date());
            if (KApp.PUT.getKApp().equals(metodoUM)) {
                metodoUM = KApp.POST.getKApp();
            }

        } else {
            nivelProductolang.getAtributosAuditoria().setUpdated(new Date());
        }

        nivelProductolang.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
        nivelProductolang.getAtributosAuditoria().setFechaPrevistaActivacion(0);
        nivelProductolang.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
        nivelProductolang.getAtributosAuditoria().setEstadoRegistro(parametroUMedFVta.getEstado());
        nivelProductolang.getAtributosAuditoria().setUltimaAccion(metodoUM);

        nivelProductolang.setTraduccionDesc(traduccionDescripcion.getTraduccion());

        Object[] aaa = new Object[5];

        aaa[0] = metodoUM;
        aaa[1] = "nivelProductoLang";
        aaa[2] = nivelProductolang;
        aaa[3] = parametroUMedFVta.getId();
        aaa[4] = traduccionDescripcion.getIdioma();

        return aaa;
    }

    @Transactional
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("nivelProductoLang").equals((String) bbb[1])) {
                    nivelProductoLangService.create((NivelProductoLang) bbb[2]);
                }
            }

            if (("PUT").equals((String) bbb[0])) {
                if (("nivelProductoLang").equals((String) bbb[1])) {
                    nivelProductoLangService.edit((NivelProductoLang) bbb[2]);
                }
            }
        }

        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("nivelProductoLang").equals((String) bbb[1])) {
                    nivelProductoLangService.deleteNivelProductoLangBy_IdNP_Lang((int) bbb[3], (String) bbb[4]);
                }
            }
        }
    }

}
