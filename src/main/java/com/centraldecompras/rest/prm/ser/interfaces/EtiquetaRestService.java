package com.centraldecompras.rest.prm.ser.interfaces;

import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.EtiquetaLang;
import com.centraldecompras.modelo.ProductoLang;
import com.centraldecompras.rest.prm.json.ParametroUMedFVta;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

public interface EtiquetaRestService {

    void persistencia(List<Object[]> listaObjetos) throws Exception;

    Object[] montaEtiqueta(ParametroUMedFVta parametroUMedFVta, String estado, String idUser) throws NonexistentEntityException, Exception;

    Object[] montaEtiquetaLang(ParametroUMedFVta parametroUMedFVta, TraduccionDescripcion traduccionDescripcion, Etiqueta etiqueta, String idUser);

}
