package com.centraldecompras.rest.prm.ser;

import com.centraldecompras.modelo.ArticuloEtiqueta;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.EtiquetaLang;
import com.centraldecompras.modelo.ProductoEtiqueta;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloEtiquetaService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoEtiquetaService;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaService;
import com.centraldecompras.rest.mat.ser.ArticuloRestServiceImpl;
import com.centraldecompras.rest.prm.json.ParametroUMedFVta;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.rest.prm.ser.interfaces.EtiquetaRestService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EtiquetaRestServiceImpl implements EtiquetaRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EtiquetaRestServiceImpl.class.getName()); 


    @Autowired
    private ProductoEtiquetaService productoEtiquetaService;

    @Autowired
    private ArticuloEtiquetaService articuloEtiquetaService;

    @Autowired
    private EtiquetaService etiquetaService;

    @Autowired
    private EtiquetaLangService etiquetaLangService;

    public Object[] montaEtiqueta(ParametroUMedFVta parametroUMedFVta, String estadoFVta, String idUser) throws NonexistentEntityException, Exception {
        Etiqueta etiqueta = null;

        String metodo = parametroUMedFVta.getMethod();

        if (("POST").equals(metodo)) {
            etiqueta = new Etiqueta();
            etiqueta.setIdEtiqueta(UUID.randomUUID().toString().replaceAll("-", ""));
            etiqueta.setEtiquetaCod(" ");
            etiqueta.setEtiquetaDesc(" ");
            etiqueta.setAtributosAuditoria(new DatosAuditoria());
            etiqueta.getAtributosAuditoria().setCreated(new Date());
        } else {  // Modificacion o Delete
            etiqueta = etiquetaService.findEtiqueta(parametroUMedFVta.getId());
            etiqueta.getAtributosAuditoria().setUpdated(new Date());
        }

        if (etiqueta != null) {

            etiqueta.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            etiqueta.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            etiqueta.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
            etiqueta.getAtributosAuditoria().setEstadoRegistro(estadoFVta);
            etiqueta.getAtributosAuditoria().setUltimaAccion(metodo);

        }

        Object[] aaa = new Object[4];
        if (ElementEnum.KApp.POST.getKApp().equals(metodo) || ElementEnum.KApp.PUT.getKApp().equals(metodo)) {
            aaa[2] = etiqueta;
            aaa[3] = null;
        } else if (ElementEnum.KApp.DELETE.getKApp().equals(metodo)) {
            aaa[2] = null;
            aaa[3] = parametroUMedFVta.getId();
        }
        aaa[0] = metodo;
        aaa[1] = "etiqueta";
        return aaa;
    }

    public Object[] montaEtiquetaLang(ParametroUMedFVta parametroUMedFVta, TraduccionDescripcion traduccionDescripcion, Etiqueta etiqueta, String idUser) {

        EtiquetaLang etiquetaLang = new EtiquetaLang();
        
        etiquetaLang = etiquetaLangService.findEtiquetaLangBy_IdE_Lang(etiqueta.getIdEtiqueta(), traduccionDescripcion.getIdioma());
        if (etiquetaLang == null) {
            etiquetaLang = new EtiquetaLang();
            etiquetaLang.setEtiquetaId(etiqueta.getIdEtiqueta());
            etiquetaLang.setEtiquetaEntity(etiqueta);
            etiquetaLang.setIdioma(traduccionDescripcion.getIdioma());
            etiquetaLang.setAtributosAuditoria(new DatosAuditoria());
            etiquetaLang.getAtributosAuditoria().setCreated(new Date());
            if(KApp.PUT.getKApp().equals(parametroUMedFVta.getMethod())){
                parametroUMedFVta.setMethod(KApp.POST.getKApp()); 
            }
        } else {
            etiquetaLang.getAtributosAuditoria().setUpdated(new Date());
        }

        if (etiquetaLang != null) {
            etiquetaLang.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            etiquetaLang.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            etiquetaLang.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
            etiquetaLang.getAtributosAuditoria().setEstadoRegistro(parametroUMedFVta.getEstado());
            etiquetaLang.getAtributosAuditoria().setUltimaAccion(parametroUMedFVta.getMethod());
            etiquetaLang.setTraduccionDesc(traduccionDescripcion.getTraduccion());
        }

        Object[] aaa = new Object[5];
        aaa[0] = parametroUMedFVta.getMethod();
        aaa[1] = "etiquetaLang";
        aaa[2] = etiquetaLang;
        aaa[3] = parametroUMedFVta.getId();
        aaa[4] = traduccionDescripcion.getIdioma();

        return aaa;
    }

    public Etiqueta montaEtiquetaAsig(List<String> etiquetaObj, String estado, String idUser) throws NonexistentEntityException, Exception {
        Etiqueta etiqueta = null;

        // Elemento 0: Method (POST, PUT, DELETE)
        // eLEMENTO 1: id de Etiqueta
        String metodo = (String) etiquetaObj.get(0);
        if (("POST").equals(metodo)) {
            etiqueta = new Etiqueta();
            etiqueta.setIdEtiqueta(UUID.randomUUID().toString().replaceAll("-", ""));
            etiqueta.setEtiquetaCod(" ");
            etiqueta.setEtiquetaDesc(" ");
            etiqueta.setAtributosAuditoria(new DatosAuditoria());
            etiqueta.getAtributosAuditoria().setCreated(new Date());
        } else {  // Modificacion o Delete
            etiqueta = etiquetaService.findEtiqueta((String) etiquetaObj.get(1));
            etiqueta.getAtributosAuditoria().setUpdated(new Date());
        }

        if (etiqueta != null) {

            etiqueta.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            etiqueta.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            etiqueta.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
            etiqueta.getAtributosAuditoria().setEstadoRegistro(estado);
            etiqueta.getAtributosAuditoria().setUltimaAccion(metodo);

        }
        return etiqueta;

    }

    @Transactional
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("etiqueta").equals((String) bbb[1])) {
                    etiquetaService.create((Etiqueta) bbb[2]);
                }
                if (("etiquetaLang").equals((String) bbb[1])) {
                    etiquetaLangService.create((EtiquetaLang) bbb[2]);
                }

                if (("productoEtiqueta").equals((String) bbb[1])) {
                    productoEtiquetaService.create((ProductoEtiqueta) bbb[2]);
                }
                if (("articuloEtiqueta").equals((String) bbb[1])) {
                    articuloEtiquetaService.create((ArticuloEtiqueta) bbb[2]);
                }
            }

            if (("PUT").equals((String) bbb[0])) {
                if (("etiqueta").equals((String) bbb[1])) {
                    etiquetaService.edit((Etiqueta) bbb[2]);
                }
                if (("etiquetaLang").equals((String) bbb[1])) {
                    etiquetaLangService.edit((EtiquetaLang) bbb[2]);
                }
                if (("productoEtiqueta").equals((String) bbb[1])) {
                    productoEtiquetaService.edit((ProductoEtiqueta) bbb[2]);
                }
                if (("articuloEtiqueta").equals((String) bbb[1])) {
                    articuloEtiquetaService.edit((ArticuloEtiqueta) bbb[2]);
                }
            }
        }

        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("etiqueta").equals((String) bbb[1])) {
                    etiquetaService.destroy((String) bbb[3]);
                }
                if (("etiquetaLang").equals((String) bbb[1])) {
                    etiquetaLangService.deleteEtiquetaLangBy_idE_Lang((String) bbb[3], (String) bbb[4]);
                }

                if (("productoEtiqueta").equals((String) bbb[1])) {
                    productoEtiquetaService.deleteProductoEtiquetaBy_idP_idE((String) bbb[3], (String) bbb[4]);
                }
                if (("articuloEtiqueta").equals((String) bbb[1])) {
                    articuloEtiquetaService.deleteArticuloEtiquetaBy_idA_idE((String) bbb[3], (String) bbb[4]);
                }
            }
        }
    }

}
