package com.centraldecompras.rest.prm.ser;

import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.FormatoVtaLang;
import com.centraldecompras.modelo.prm.service.interfaces.FormatoVtaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.FormatoVtaService;
import com.centraldecompras.rest.prm.json.ParametroUMedFVta;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.rest.prm.ser.interfaces.FormatoVtaRestService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FormatoVtaRestServiceImpl implements FormatoVtaRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FormatoVtaRestServiceImpl.class.getName()); 


    @Autowired
    private FormatoVtaService formatoVtaService;

    @Autowired
    private FormatoVtaLangService formatoVtaLangService;

    public Object[] montaFormatoVta(ParametroUMedFVta parametroUMedFVta, String estadoFVta, String idUser) throws NonexistentEntityException, Exception {
        log.info("....................................................................mbm............CONTROL");
                
        FormatoVta formatoVta = null;

        String metodo = parametroUMedFVta.getMethod();

        if (("POST").equals(metodo)) {
            formatoVta = new FormatoVta();
            formatoVta.setIdFormatoVta(UUID.randomUUID().toString().replaceAll("-", ""));
            formatoVta.setTipoEnvase(" ");
            formatoVta.setDescEnvase(" ");
            formatoVta.setAtributosAuditoria(new DatosAuditoria());
            formatoVta.getAtributosAuditoria().setCreated(new Date());
        } else {  // Modificacion o Delete
            formatoVta = formatoVtaService.findFormatoVta(parametroUMedFVta.getId());
            formatoVta.getAtributosAuditoria().setUpdated(new Date());
        }

        if (formatoVta != null) {

            formatoVta.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            formatoVta.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            formatoVta.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
            formatoVta.getAtributosAuditoria().setEstadoRegistro(estadoFVta);
            formatoVta.getAtributosAuditoria().setUltimaAccion(metodo);

        }

        Object[] aaa = new Object[4];
        if (ElementEnum.KApp.POST.getKApp().equals(metodo) || ElementEnum.KApp.PUT.getKApp().equals(metodo)) {
            aaa[2] = formatoVta;
            aaa[3] = null;
        } else if (ElementEnum.KApp.DELETE.getKApp().equals(metodo)) {
            aaa[2] = null;
            aaa[3] = parametroUMedFVta.getId();
        }
        aaa[0] = metodo;
        aaa[1] = "formatoVta";
        return aaa;
    }

    public Object[] montaFormatoVtaLang(ParametroUMedFVta parametroUMedFVta, TraduccionDescripcion traduccionDescripcion, FormatoVta formatoVta, String idUser) {
        log.info("....................................................................mbm............CONTROL");
        
        FormatoVtaLang formatoVtaLang = new FormatoVtaLang();

        formatoVtaLang = formatoVtaLangService.findFormatoVtaLangBy_IdFV_Lang(formatoVta.getIdFormatoVta(), traduccionDescripcion.getIdioma());
        if (formatoVtaLang == null) {
            formatoVtaLang = new FormatoVtaLang();
            formatoVtaLang.setFormatoVtaId(formatoVta.getIdFormatoVta());
            formatoVtaLang.setFormatoVtaEntity(formatoVta);
            formatoVtaLang.setIdioma(traduccionDescripcion.getIdioma());
            formatoVtaLang.setAtributosAuditoria(new DatosAuditoria());
            formatoVtaLang.getAtributosAuditoria().setCreated(new Date());
            if(KApp.PUT.getKApp().equals(parametroUMedFVta.getMethod())){
                parametroUMedFVta.setMethod(KApp.POST.getKApp());         
            }
        } else {
            formatoVtaLang.getAtributosAuditoria().setUpdated(new Date());
            if(KApp.POST.getKApp().equals(parametroUMedFVta.getMethod())){
                parametroUMedFVta.setMethod(KApp.PUT.getKApp());         
            }
        }

        if (formatoVtaLang != null) {
            formatoVtaLang.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            formatoVtaLang.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            formatoVtaLang.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
            formatoVtaLang.getAtributosAuditoria().setEstadoRegistro(parametroUMedFVta.getEstado());
            formatoVtaLang.getAtributosAuditoria().setUltimaAccion(parametroUMedFVta.getMethod());
            formatoVtaLang.setTraduccionDesc(traduccionDescripcion.getTraduccion());
        }

        Object[] aaa = new Object[5];
        aaa[0] = parametroUMedFVta.getMethod();
        aaa[1] = "formatoVtaLangService";
        aaa[2] = formatoVtaLang;
        aaa[3] = parametroUMedFVta.getId();
        aaa[4] = traduccionDescripcion.getIdioma();

        return aaa;
    }

    @Transactional
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");
                
        for (Object[] bbb : listaObjetos) {
            if (("POST").equals((String) bbb[0])) {
                if (("formatoVta").equals((String) bbb[1])) {
                    formatoVtaService.create((FormatoVta) bbb[2]);
                }
                if (("formatoVtaLangService").equals((String) bbb[1])) {
                    formatoVtaLangService.create((FormatoVtaLang) bbb[2]);
                }
            }

            if (("PUT").equals((String) bbb[0])) {
                if (("formatoVta").equals((String) bbb[1])) {
                    formatoVtaService.edit((FormatoVta) bbb[2]);
                }
                if (("formatoVtaLangService").equals((String) bbb[1])) {
                    formatoVtaLangService.edit((FormatoVtaLang) bbb[2]);
                }
            }
        }

        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("formatoVta").equals((String) bbb[1])) {
                    formatoVtaService.destroy((String) bbb[3]);
                }
                if (("formatoVtaLangService").equals((String) bbb[1])) {
                    formatoVtaLangService.deleteFormatoVtaLangBy_idFV_Lang((String) bbb[3], (String) bbb[4]);
                }
            }
        }
    }

}
