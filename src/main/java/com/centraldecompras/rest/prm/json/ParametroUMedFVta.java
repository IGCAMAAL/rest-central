/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.prm.json;

import java.math.BigDecimal;
import java.util.List;

public class ParametroUMedFVta {

    String id;
    String method;
    String estado;
    BigDecimal capacidad;
    
    List<TraduccionDescripcion> traducciones;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<TraduccionDescripcion> getTraducciones() {
        return traducciones;
    }

    public void setTraducciones(List<TraduccionDescripcion> traducciones) {
        this.traducciones = traducciones;
    }

    public BigDecimal getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(BigDecimal capacidad) {
        this.capacidad = capacidad;
    }
    

}
