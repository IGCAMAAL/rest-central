package com.centraldecompras.rest.prm.json;

import com.centraldecompras.modelo.EtiquetaLang;
import java.util.List;

public class EtiquetaAsigJson {
    
 	String id;                                          // :id de producto o articulo 
        String method;                                      //:'POST/PUT/DELETE',
 	int fechaPrevistaActivacion;                        //:20141201, 
 	int fechaPrevistaDesactivacion;                     //:0, 
        String etiquetaId;
        List<EtiquetaLang> etiquetaLang;
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public String getEtiquetaId() {
        return etiquetaId;
    }

    public void setEtiquetaId(String etiquetaId) {
        this.etiquetaId = etiquetaId;
    }

    public List<EtiquetaLang> getEtiquetaLang() {
        return etiquetaLang;
    }

    public void setEtiquetaLang(List<EtiquetaLang> etiquetaLang) {
        this.etiquetaLang = etiquetaLang;
    }

}
