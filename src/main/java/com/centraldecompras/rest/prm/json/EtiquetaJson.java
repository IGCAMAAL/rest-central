package com.centraldecompras.rest.prm.json;


import java.util.List;

public class EtiquetaJson {
    
 	String id;                                          
        String method;                                      
        List<TraduccionDescripcion> traducciones;
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<TraduccionDescripcion> getTraducciones() {
        return traducciones;
    }

    public void setTraducciones(List<TraduccionDescripcion> traducciones) {
        this.traducciones = traducciones;
    }


}
