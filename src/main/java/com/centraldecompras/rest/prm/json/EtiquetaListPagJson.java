package com.centraldecompras.rest.prm.json;

import java.util.List;

public class EtiquetaListPagJson {
    
    int totalPaginas;
    int totalRegistros;   
    int paginaActual; 
    List<ParametroUMedFVta> parametroUMedFVta;

    public EtiquetaListPagJson() {
    }

    public EtiquetaListPagJson(int totalPaginas, int totalRegistros, int paginaActual, List<ParametroUMedFVta> parametroUMedFVta) {
        this.totalPaginas = totalPaginas;
        this.totalRegistros = totalRegistros;
        this.paginaActual = paginaActual;
        this.parametroUMedFVta = parametroUMedFVta;
    }

    public int getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(int totalPaginas) {
        this.totalPaginas = totalPaginas;
    }

    public int getTotalRegistros() {
        return totalRegistros;
    }

    public void setTotalRegistros(int totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    public int getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(int paginaActual) {
        this.paginaActual = paginaActual;
    }

    public List<ParametroUMedFVta> getParametroUMedFVta() {
        return parametroUMedFVta;
    }

    public void setParametroUMedFVta(List<ParametroUMedFVta> parametroUMedFVta) {
        this.parametroUMedFVta = parametroUMedFVta;
    }

}
