/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.centraldecompras.rest.prm.json;

    public class TraduccionDescripcion {

        String idioma; 
        String traduccion;

        public String getIdioma() {
            return idioma;
        }

        public void setIdioma(String idioma) {
            this.idioma = idioma;
        }

        public String getTraduccion() {
            return traduccion;
        }

        public void setTraduccion(String traduccion) {
            this.traduccion = traduccion;
        }

    }