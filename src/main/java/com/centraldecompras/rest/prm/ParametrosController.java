/*
 * Copyright 2014, Javier Moreno.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 * And have fun ;-)
 */
package com.centraldecompras.rest.prm;

import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.rest.prm.json.ParametroUMedFVta;
import com.centraldecompras.rest.prm.json.EtiquetaListPagJson;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.EtiquetaLang;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.FormatoVtaLang;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.NivelProductoLang;
import com.centraldecompras.modelo.UnidadFormatoVta;
import com.centraldecompras.modelo.UnidadFormatoVtaLang;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.UnidadMedidaLang;
import com.centraldecompras.modelo.prm.service.ParametroFiltro;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaService;
import com.centraldecompras.modelo.prm.service.interfaces.FormatoVtaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.FormatoVtaService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoLangService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadFormatoVtaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadFormatoVtaService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaService;
import com.centraldecompras.rest.prm.ser.interfaces.EtiquetaRestService;
import com.centraldecompras.rest.prm.ser.interfaces.FormatoVtaRestService;
import com.centraldecompras.rest.prm.ser.interfaces.NivelProductoRestService;
import com.centraldecompras.rest.prm.ser.interfaces.UnidadMedidaRestService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.apache.commons.beanutils.DynaBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ciberado
 */
@RestController
@RequestMapping("/prm")
@Api(value = "Recursos públicos", description = "Demo de recursos accesibles públicamente sin autentificación.")
public class ParametrosController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ParametrosController.class.getName());

    @Autowired
    private UnidadMedidaService unidadMedidaService;

    @Autowired
    private UnidadMedidaLangService unidadMedidaLangService;

    @Autowired
    private UnidadMedidaRestService unidadMedidaRestService;

    @Autowired
    private FormatoVtaService formatoVtaService;

    @Autowired
    private FormatoVtaLangService formatoVtaLangService;

    @Autowired
    private FormatoVtaRestService formatoVtaRestService;

    @Autowired
    private UnidadFormatoVtaService unidadFormatoVtaService;

    @Autowired
    private UnidadFormatoVtaLangService unidadFormatoVtaLangService;

    @Autowired
    private NivelProductoService nivelProductoService;

    @Autowired
    private NivelProductoLangService nivelProductoLangService;

    @Autowired
    private NivelProductoRestService nivelProductoRestService;

    @Autowired
    private EtiquetaService etiquetaService;

    @Autowired
    private EtiquetaLangService etiquetaLangService;

    @Autowired
    private EtiquetaRestService etiquetaRestService;

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/formatovtapag", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera una coleccion de n Reg de Formatos de venta a partir de una dada con tecnica paginada ", notes = "ParametroUMedFVta, formato de comunicacion con cliente")
    EtiquetaListPagJson getFormatoVtaPag(
            @RequestBody ParametroFiltro parametroFiltro,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<ParametroUMedFVta> parametrosUMedFVta = new ArrayList<ParametroUMedFVta>();

        EtiquetaListPagJson reply = new EtiquetaListPagJson();

        DynaBean datosFormatoVta = (DynaBean) formatoVtaLangService.findFormatoVta_Pag_WithFilter(parametroFiltro);

        int totalPaginas = (int) datosFormatoVta.get("totalPaginas");
        int totalRegistros = (int) datosFormatoVta.get("totalRegistros");
        int paginaActual = (int) datosFormatoVta.get("paginaActual");
        List<String> idFormatosVta = (List<String>) datosFormatoVta.get("idFormatoVta");

        log.info("----<" + idFormatosVta);
        if (idFormatosVta == null) {
            log.info("----<Creamos uno vacio");
            idFormatosVta = new ArrayList<>();
        }

        for (String idFormatoVta : idFormatosVta) {
            ParametroUMedFVta parametroUMedFVta = null;
            FormatoVta formatoVta = formatoVtaService.findFormatoVta(idFormatoVta);
            List<FormatoVtaLang> formatosVtaLang = formatoVtaLangService.findFormatoVtaLangBy_FV(formatoVta);
            parametroUMedFVta = montaFormatoVta(formatosVtaLang);
            parametrosUMedFVta.add(parametroUMedFVta);
        }

        reply.setTotalPaginas(totalPaginas);
        reply.setTotalRegistros(totalRegistros);
        reply.setPaginaActual(paginaActual);
        reply.setParametroUMedFVta(parametrosUMedFVta);

        return reply;

    }

    // --------------------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/etiqueta", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Etiqueta", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> postEtiqueta(
            @RequestBody List<ParametroUMedFVta> parametrosUMedFVta,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws Exception {
        log.info("................................................................................CONTROL");

        List<ParametroUMedFVta> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();

        for (ParametroUMedFVta parametroUMedFVta : parametrosUMedFVta) {
            String id = parametroUMedFVta.getId();
            String metodo = parametroUMedFVta.getMethod();
            Etiqueta etiqueta = null;

            if (id.equals("0") && !metodo.equals("POST") || !id.equals("0") && metodo.equals("POST")) {
                throw new Exception("CentralCompras. CRUD etiquetas. id etiquetaa  y Metodo ne están sincronizados");

            } else {

                Object[] aaa = new Object[4];

                String metodoEti = parametroUMedFVta.getMethod();
                String estadoEti = parametroUMedFVta.getEstado();

                if (ElementEnum.KApp.POST.getKApp().equals(metodoEti) || ElementEnum.KApp.PUT.getKApp().equals(metodoEti)) {

                    aaa = etiquetaRestService.montaEtiqueta(parametroUMedFVta, estadoEti, idUser);
                    etiqueta = (Etiqueta) aaa[2];
                    parametroUMedFVta.setId(etiqueta.getIdEtiqueta());  // para recuperad de BBDD
                    listaObjetos.add(aaa);

                    List<TraduccionDescripcion> traduccionesDescripcion = (List<TraduccionDescripcion>) parametroUMedFVta.getTraducciones();
                    for (TraduccionDescripcion traduccionDescripcion : traduccionesDescripcion) {
                        Object[] bbb = new Object[5];

                        bbb = etiquetaRestService.montaEtiquetaLang(parametroUMedFVta, traduccionDescripcion, etiqueta, idUser);
                        listaObjetos.add(bbb);

                    }
                }
            }
        }

        etiquetaRestService.persistencia(listaObjetos);

        for (ParametroUMedFVta parametroUMedFVta : parametrosUMedFVta) {
            Etiqueta etiqueta = etiquetaService.findEtiqueta(parametroUMedFVta.getId());
            if (etiqueta != null) {
                reply.add(toJson(etiqueta));
            }
        }

        return reply;
    }

    @RequestMapping(value = "/etiqueta", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera la lista entera de Etiqueta", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> getEtiqueta() throws Exception {
        log.info("................................................................................CONTROL");

        List<Etiqueta> etiquetas = etiquetaService.findEtiquetaEntities();
        List<ParametroUMedFVta> parametrosUMedFVta = new ArrayList<ParametroUMedFVta>();

        for (Etiqueta etiqueta : etiquetas) {
            parametrosUMedFVta.add(toJson(etiqueta));
        }

        return parametrosUMedFVta;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/etiquetapag", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera una coleccion de n Reg de Etiqueras a partir de una dada con tecnica paginada ", notes = "ParametroUMedFVta, formato de comunicacion con cliente")
    EtiquetaListPagJson getEtiquetaPag(
            @RequestBody ParametroFiltro parametroFiltro,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<ParametroUMedFVta> parametrosUMedFVta = new ArrayList<ParametroUMedFVta>();

        EtiquetaListPagJson reply = new EtiquetaListPagJson();

        DynaBean datosEtiqueta = (DynaBean) etiquetaLangService.findEtiqueta_Pag_WithFilter(parametroFiltro);

        int totalPaginas = (int) datosEtiqueta.get("totalPaginas");
        int totalRegistros = (int) datosEtiqueta.get("totalRegistros");
        int paginaActual = (int) datosEtiqueta.get("paginaActual");
        List<String> idEtiquetas = (List<String>) datosEtiqueta.get("idEtiquetas");

        log.info("----<" + idEtiquetas);
        if (idEtiquetas == null) {
            log.info("----<Creamos uno vacio");
            idEtiquetas = new ArrayList<>();
        }

        for (String idEtiqueta : idEtiquetas) {
            ParametroUMedFVta parametroUMedFVta = null;
            Etiqueta etiqueta = etiquetaService.findEtiqueta(idEtiqueta);
            List<EtiquetaLang> etiquetasLang = etiquetaLangService.findEtiquetaLangBy_E(etiqueta);
            parametroUMedFVta = montaEtiqueta(etiquetasLang);
            parametrosUMedFVta.add(parametroUMedFVta);
        }

        reply.setTotalPaginas(totalPaginas);
        reply.setTotalRegistros(totalRegistros);
        reply.setPaginaActual(paginaActual);
        reply.setParametroUMedFVta(parametrosUMedFVta);

        return reply;

    }

    @RequestMapping(value = "/etiqueta/seleccion/{cadena}/{idioma}/{estado}/{cantidad}", method = RequestMethod.GET)
    List<ParametroUMedFVta> seleccionEtquetas(
            @PathVariable String cadena,
            @PathVariable String idioma,
            @PathVariable String estado,
            @PathVariable String cantidad,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        log.info("cadena: " + cadena);
        log.info("idioma: " + idioma);
        log.info("estado: " + estado);
        log.info("cantidad: " + cantidad);

        //List<ZonaGeograficaJson> reply = new ArrayList<ZonaGeograficaJson>();
        //List<ZonaGeografica> zonasGeograficas = new ArrayList<ZonaGeografica>();
        List<ParametroUMedFVta> reply = new ArrayList<ParametroUMedFVta>();
        List<EtiquetaLang> etiquetasLang = new ArrayList<EtiquetaLang>();

        int cantidadInt = 0;
        try {
            cantidadInt = Integer.parseInt(cantidad);
        } catch (NumberFormatException ex) {
            throw new Exception("CentralCompras. Combo Etiquetas. Parametro 'cantidad' de resultados no es valido" + ex.getMessage());
        }

        if (cadena == null || cadena.equals("")) {
            throw new Exception("CentralCompras. Combo Etiquetas. Parametro 'cadena' no es valido");
        }

        /*Simulacion******************
         Aqui se deben buscar una cantidad definidad de etiquetas cuyo nombre contiene 'cadena' y cuyo estado es 'estado'
         Estados posibles: 'AC', 'DA', 'TODOS'

         Se ahcen dos busqueda, 
         Primero: Buscar las etiquetas que EMPIEZAN con 'cadena'
         Segundo: Si con la busqueda anterior no llego a la 'cantidad' de etiquetas desead de resultados, busco los que CONTIENEN 'cadena' para complementar hasta los 'cantidad'.
         */
        //zonasGeograficas = zonaGeograficaService.findZonaGeograficaTypeAhead(1,cadena, estado.toUpperCase(), cantidadInt);
        //reply = seleccionZonasCargaArray(reply, zonasGeograficas);
        etiquetasLang = etiquetaLangService.findEtiquetaLangTypeAhead(1, cadena, estado.toUpperCase(), cantidadInt, idioma);
        reply = seleccionEtiquetaCargaArray(reply, etiquetasLang);

        if (reply.size() < cantidadInt) {
            etiquetasLang = etiquetaLangService.findEtiquetaLangTypeAhead(2, cadena, estado.toUpperCase(), cantidadInt - reply.size(), idioma);
            reply = seleccionEtiquetaCargaArray(reply, etiquetasLang);
        }

        return reply;

    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    

    @RequestMapping(value = "/unidadmedida", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Unidades Medida", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> postUnidadMedida(
            @RequestBody List<ParametroUMedFVta> parametrosUMedFVta,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws Exception {
        log.info("................................................................................CONTROL");

        List<ParametroUMedFVta> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();

        for (ParametroUMedFVta parametroUMedFVta : parametrosUMedFVta) {
            String id = parametroUMedFVta.getId();
            String metodo = parametroUMedFVta.getMethod();
            UnidadMedida unidadMedida = null;

            if (id.equals("0") && !metodo.equals("POST") || !id.equals("0") && metodo.equals("POST")) {
                throw new Exception("CentralCompras. CRUD Unidad medida. id unidad medida  y Metodo ne están sincronizados");

            } else {
                String metodoUM = parametroUMedFVta.getMethod();
                String estadoUM = parametroUMedFVta.getEstado();

                if (ElementEnum.KApp.POST.getKApp().equals(metodoUM) || ElementEnum.KApp.PUT.getKApp().equals(metodoUM)) {
                    Object[] aaa = new Object[4];
                    aaa = unidadMedidaRestService.montaUnidadMedida(parametroUMedFVta, estadoUM, idUser);
                    unidadMedida = (UnidadMedida) aaa[2];
                    parametroUMedFVta.setId(unidadMedida.getIdUnidadMedida());  // para recuperad de BBDD
                    listaObjetos.add(aaa);

                    List<TraduccionDescripcion> traduccionesDescripcion = (List<TraduccionDescripcion>) parametroUMedFVta.getTraducciones();
                    for (TraduccionDescripcion traduccionDescripcion : traduccionesDescripcion) {
                        Object[] bbb = new Object[5];
                        bbb = unidadMedidaRestService.montaUnidadMedidaLang(parametroUMedFVta, traduccionDescripcion, unidadMedida, idUser);
                        listaObjetos.add(bbb);
                    }
                }
            }
        }

        unidadMedidaRestService.persistencia(listaObjetos);

        for (ParametroUMedFVta parametroUMedFVta : parametrosUMedFVta) {
            UnidadMedida unidadMedida = unidadMedidaService.findUnidadMedida(parametroUMedFVta.getId());
            if (unidadMedida != null) {
                reply.add(toJson(unidadMedida));
            }
        }

        return reply;
    }

    @RequestMapping(value = "/unidadmedida", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera la lista entera de Unidades Medida", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> getUnidadMedida() throws Exception {
        log.info("................................................................................CONTROL");

        List<UnidadMedida> unidadesMedida = unidadMedidaService.findUnidadMedidaEntities();

        List<ParametroUMedFVta> reply = new ArrayList<>();
        for (UnidadMedida unidadMedida : unidadesMedida) {
            if (unidadMedida != null) {
                reply.add(toJson(unidadMedida));
            }
        }

        return reply;
    }

    @RequestMapping(value = "/formatovta", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Formatos Venta", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> postFormatoVta(
            @RequestBody List<ParametroUMedFVta> parametrosUMedFVta,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec) throws Exception {
        log.info("................................................................................CONTROL");

        List<ParametroUMedFVta> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();

        for (ParametroUMedFVta parametroUMedFVta : parametrosUMedFVta) {
            String id = parametroUMedFVta.getId();
            String metodo = parametroUMedFVta.getMethod();
            FormatoVta formatoVta = null;

            if (id.equals("0") && !metodo.equals("POST") || !id.equals("0") && metodo.equals("POST")) {
                throw new Exception("CentralCompras. CRUD Formato venta. id Formato venta  y Metodo ne están sincronizados");

            } else {
                String metodoFV = parametroUMedFVta.getMethod();
                String estadoFV = parametroUMedFVta.getEstado();

                if (ElementEnum.KApp.POST.getKApp().equals(metodoFV) || ElementEnum.KApp.PUT.getKApp().equals(metodoFV)) {
                    Object[] aaa = new Object[4];
                    aaa = formatoVtaRestService.montaFormatoVta(parametroUMedFVta, estadoFV, idUser);
                    formatoVta = (FormatoVta) aaa[2];
                    parametroUMedFVta.setId(formatoVta.getIdFormatoVta());  // para recuperad de BBDD
                    listaObjetos.add(aaa);

                    List<TraduccionDescripcion> traduccionesDescripcion = (List<TraduccionDescripcion>) parametroUMedFVta.getTraducciones();
                    for (TraduccionDescripcion traduccionDescripcion : traduccionesDescripcion) {
                        Object[] bbb = new Object[5];
                        bbb = formatoVtaRestService.montaFormatoVtaLang(parametroUMedFVta, traduccionDescripcion, formatoVta, idUser);
                        listaObjetos.add(bbb);
                    }
                }
            }
        }

        try {
            formatoVtaRestService.persistencia(listaObjetos);
        } catch (Exception ex) {
            log.warn(ex); 
            ex.printStackTrace();
        }
        for (ParametroUMedFVta parametroUMedFVta : parametrosUMedFVta) {
            FormatoVta formatoVta = formatoVtaService.findFormatoVta(parametroUMedFVta.getId());
            if (formatoVta != null) {
                reply.add(toJson(formatoVta));
            }
        }

        return reply;
    }

    @RequestMapping(value = "/formatovta", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera la lista entera de Unidades Medida", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> geFormatoVta() throws Exception {
        log.info("................................................................................CONTROL");

        List<FormatoVta> formatosVta = formatoVtaService.findFormatoVtaEntities();

        List<ParametroUMedFVta> reply = new ArrayList<>();
        for (FormatoVta formatoVta : formatosVta) {
            if (formatoVta != null) {
                reply.add(toJson(formatoVta));
            }
        }

        return reply;
    }

    @RequestMapping(value = "/unidadformatovta", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Formatos Venta", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> postUnidadFormatoVta(@RequestBody List<ParametroUMedFVta> parametrosUMedFVta,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec) throws Exception {
        log.info("................................................................................CONTROL");

        for (ParametroUMedFVta parametroUMedFVta : parametrosUMedFVta) {
            String id = parametroUMedFVta.getId();
            String metodo = parametroUMedFVta.getMethod();
            String estadoFVta = parametroUMedFVta.getEstado();
            BigDecimal capacidad = parametroUMedFVta.getCapacidad();

            if (id.equals("0") && metodo.equals("POST") || !id.equals("0") && metodo.equals("PUT")) {

                List<TraduccionDescripcion> traduccionesDescripcion = (List<TraduccionDescripcion>) parametroUMedFVta.getTraducciones();
                UnidadFormatoVta unidadFormatoVta = null;

                if (id.equals("0")) {  // Alta
                    unidadFormatoVta = new UnidadFormatoVta();
                    unidadFormatoVta.setIdUnidadFormatoVta(UUID.randomUUID().toString().replaceAll("-", ""));
                    unidadFormatoVta.setTipoCapacEnvase(new BigDecimal(0));
                    unidadFormatoVta.setDescCapacEnvase(id);
                    unidadFormatoVta.setAtributosAuditoria(new DatosAuditoria());
                    unidadFormatoVta.getAtributosAuditoria().setCreated(new Date());
                } else {  // Modificacion
                    unidadFormatoVta = unidadFormatoVtaService.findUnidadFormatoVta(id);
                    unidadFormatoVta.getAtributosAuditoria().setUpdated(new Date());
                }

                if (unidadFormatoVta != null) {
                    unidadFormatoVta.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
                    unidadFormatoVta.getAtributosAuditoria().setFechaPrevistaActivacion(0);
                    unidadFormatoVta.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);

                    unidadFormatoVta.getAtributosAuditoria().setEstadoRegistro(estadoFVta);
                    unidadFormatoVta.getAtributosAuditoria().setUltimaAccion(metodo);
                    unidadFormatoVta.setTipoCapacEnvase(parametroUMedFVta.getCapacidad());

                    if (id.equals("0")) {// Alta
                        unidadFormatoVtaService.create(unidadFormatoVta);
                    } else {  // Modificacion
                        unidadFormatoVtaService.edit(unidadFormatoVta);
                    }

                    for (TraduccionDescripcion traduccionDescripcion : traduccionesDescripcion) {
                        UnidadFormatoVtaLang unidadformatoVtaLang = null;

                        if (id.equals("0")) {  // Alta
                            unidadformatoVtaLang = new UnidadFormatoVtaLang();
                            unidadformatoVtaLang.setUnidadFormatoVtaId(unidadFormatoVta.getIdUnidadFormatoVta());
                            unidadformatoVtaLang.setUnidadFormatoVtaEntity(unidadFormatoVta);
                            unidadformatoVtaLang.setIdioma(traduccionDescripcion.getIdioma());
                            unidadformatoVtaLang.setAtributosAuditoria(new DatosAuditoria());
                            unidadformatoVtaLang.getAtributosAuditoria().setCreated(new Date());
                        } else {  // Modificacion
                            unidadformatoVtaLang = unidadFormatoVtaLangService.findUnidadFormatoVtaLangBy_IdUFV_Lang(unidadFormatoVta.getIdUnidadFormatoVta(), traduccionDescripcion.getIdioma());
                            unidadformatoVtaLang.getAtributosAuditoria().setUpdated(new Date());
                        }

                        if (unidadformatoVtaLang != null) {
                            unidadformatoVtaLang.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
                            unidadformatoVtaLang.getAtributosAuditoria().setFechaPrevistaActivacion(0);
                            unidadformatoVtaLang.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);

                            unidadformatoVtaLang.getAtributosAuditoria().setEstadoRegistro(estadoFVta);
                            unidadformatoVtaLang.getAtributosAuditoria().setUltimaAccion(metodo);

                            unidadformatoVtaLang.setTraduccionDesc(traduccionDescripcion.getTraduccion());

                            if (id.equals("0")) {  // Alta
                                unidadFormatoVtaLangService.create(unidadformatoVtaLang);
                            } else {    // Modificacion
                                unidadFormatoVtaLangService.edit(unidadformatoVtaLang);
                            }
                        }
                    }
                }
            } else {

                throw new Exception("CentralCompras. CRUD unidadFormatoVta. id unidadFormatoVta  y Metodo ne están sincronizados");
            }
        }

        return null;
    }

    @RequestMapping(value = "/unidadformatovta", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera la lista entera de Unidades Medida", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> geUnidadFormatoVta() throws Exception {
        log.info("................................................................................CONTROL");

        List<UnidadFormatoVta> unidadFormatosVta = unidadFormatoVtaService.findUnidadFormatoVtaEntities();
        List<ParametroUMedFVta> parametrosUMedFVta = new ArrayList<ParametroUMedFVta>();

        for (UnidadFormatoVta unidadFormatoVta : unidadFormatosVta) {
            ParametroUMedFVta parametroUMedFVta = new ParametroUMedFVta();
            parametroUMedFVta.setId(unidadFormatoVta.getIdUnidadFormatoVta());
            parametroUMedFVta.setCapacidad(unidadFormatoVta.getTipoCapacEnvase());
            parametroUMedFVta.setEstado(unidadFormatoVta.getAtributosAuditoria().getEstadoRegistro());
            parametroUMedFVta.setMethod(unidadFormatoVta.getAtributosAuditoria().getUltimaAccion());

            List<UnidadFormatoVtaLang> unidadFormatosVtaLang
                    = unidadFormatoVtaLangService.findUnidadFormatoVtaLangById_UFV(unidadFormatoVta);
            List<TraduccionDescripcion> traducciones = new ArrayList<TraduccionDescripcion>();
            for (UnidadFormatoVtaLang unidadFormatoVtaLang : unidadFormatosVtaLang) {
                TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
                traduccionDescripcion.setIdioma(unidadFormatoVtaLang.getIdioma());
                traduccionDescripcion.setTraduccion(unidadFormatoVtaLang.getTraduccionDesc());

                traducciones.add(traduccionDescripcion);
            }
            parametroUMedFVta.setTraducciones(traducciones);
            parametrosUMedFVta.add(parametroUMedFVta);

        }

        return parametrosUMedFVta;
    }

    @RequestMapping(value = "/nivelproducto", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar traducción de niveles de productos", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> postNivelProducto(
            @RequestBody List<ParametroUMedFVta> parametrosUMedFVta,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec // Sector
    ) throws Exception {
        log.info("................................................................................CONTROL");

        List<ParametroUMedFVta> reply = new ArrayList<>();
        List<Object[]> listaObjetos = new ArrayList();

        for (ParametroUMedFVta parametroUMedFVta : parametrosUMedFVta) {

            int id = Integer.parseInt(parametroUMedFVta.getId());
            String metodo = parametroUMedFVta.getMethod();
            String estado = parametroUMedFVta.getEstado();

            if (id == 0) {
                throw new Exception("CentralCompras. CRUD NivelProducto. Id NivelProducto: [" + id + "] y Metodo: [" + metodo + "] no están sincronizados.");

            } else {
                List<TraduccionDescripcion> traduccionesDescripcion = (List<TraduccionDescripcion>) parametroUMedFVta.getTraducciones();
                NivelProducto nivelProducto = null;

                nivelProducto = nivelProductoService.findNivelProducto(id);
                if (nivelProducto == null) {
                    throw new Exception("CentralCompras. CRUD NivelProductoLang. Id NivelProducto: [" + id + "] no existe");
                } else {
                    for (TraduccionDescripcion traduccionDescripcion : traduccionesDescripcion) {
                        Object[] bbb = new Object[5];
                        bbb = nivelProductoRestService.montaNivelProductoLang(parametroUMedFVta, traduccionDescripcion, nivelProducto, idUser);
                        listaObjetos.add(bbb);
                    }
                }
            }
        }

        nivelProductoRestService.persistencia(listaObjetos);

        for (ParametroUMedFVta parametroUMedFVta : parametrosUMedFVta) {
            NivelProducto nivelProducto = nivelProductoService.findNivelProducto(Integer.parseInt(parametroUMedFVta.getId()));
            if (nivelProducto != null) {
                reply.add(toJson(nivelProducto));
            }
        }

        return reply;
    }

    @RequestMapping(value = "/nivelproducto", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera la lista entera de Niveles de Producto", notes = "ParametroUMedFVta es un formato generico para unidades medida, Formato venta, uni Formato venta (embases)")
    List<ParametroUMedFVta> getNivelProducto() throws Exception {
        log.info("................................................................................CONTROL");

        List<ParametroUMedFVta> reply = new ArrayList<ParametroUMedFVta>();
        List<NivelProducto> nivelesProducto = nivelProductoService.findNivelProductoEntities();

        for (NivelProducto nivelProducto : nivelesProducto) {
            if (nivelProducto != null) {
                reply.add(toJson(nivelProducto));
            }
        }

        return reply;
    }

    private ParametroUMedFVta montaFormatoVta(List<FormatoVtaLang> formatosVtaLang) {
        log.info("....................................................................mbm............CONTROL");

        ParametroUMedFVta parametroUMedFVta = new ParametroUMedFVta();
        List<TraduccionDescripcion> traduccionesDescripcion = new ArrayList();
        String estado = null;
        String metodo = null;
        for (FormatoVtaLang formatoVtaLang : formatosVtaLang) {
            parametroUMedFVta.setId(formatoVtaLang.getFormatoVtaId());
            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(formatoVtaLang.getIdioma());
            traduccionDescripcion.setTraduccion(formatoVtaLang.getTraduccionDesc());
            traduccionesDescripcion.add(traduccionDescripcion);
            estado = formatoVtaLang.getAtributosAuditoria().getEstadoRegistro();
            metodo = formatoVtaLang.getAtributosAuditoria().getUltimaAccion();
        }

        parametroUMedFVta.setMethod(metodo);
        parametroUMedFVta.setEstado(estado);
        parametroUMedFVta.setTraducciones(traduccionesDescripcion);
        return parametroUMedFVta;
    }

    private ParametroUMedFVta montaEtiqueta(List<EtiquetaLang> etiquetasLang) {
        log.info("....................................................................mbm............CONTROL");

        ParametroUMedFVta parametroUMedFVta = new ParametroUMedFVta();
        List<TraduccionDescripcion> traduccionesDescripcion = new ArrayList();
        String estado = null;
        String metodo = null;
        for (EtiquetaLang etiquetaLang : etiquetasLang) {
            parametroUMedFVta.setId(etiquetaLang.getEtiquetaId());
            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(etiquetaLang.getIdioma());
            traduccionDescripcion.setTraduccion(etiquetaLang.getTraduccionDesc());
            traduccionesDescripcion.add(traduccionDescripcion);
            estado = etiquetaLang.getAtributosAuditoria().getEstadoRegistro();
            metodo = etiquetaLang.getAtributosAuditoria().getUltimaAccion();
        }

        parametroUMedFVta.setMethod(metodo);
        parametroUMedFVta.setEstado(estado);
        parametroUMedFVta.setTraducciones(traduccionesDescripcion);
        return parametroUMedFVta;
    }

    private List<ParametroUMedFVta> seleccionEtiquetaCargaArray(List<ParametroUMedFVta> reply, List<EtiquetaLang> etiquetasLang) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (EtiquetaLang etiquetaLang : etiquetasLang) {
            ParametroUMedFVta parametroUMedFVta = new ParametroUMedFVta();
            parametroUMedFVta.setId(etiquetaLang.getEtiquetaId());
            parametroUMedFVta.setEstado(etiquetaLang.getAtributosAuditoria().getEstadoRegistro());
            parametroUMedFVta.setMethod(etiquetaLang.getAtributosAuditoria().getUltimaAccion());

            List<TraduccionDescripcion> traduccionDescripcion = new ArrayList();
            traduccionDescripcion.add(new TraduccionDescripcion());
            parametroUMedFVta.setTraducciones(traduccionDescripcion);
            parametroUMedFVta.getTraducciones().get(0).setIdioma(etiquetaLang.getIdioma());
            parametroUMedFVta.getTraducciones().get(0).setTraduccion(etiquetaLang.getTraduccionDesc());

            reply.add(parametroUMedFVta);
        }
        return reply;
    }

    private ParametroUMedFVta toJson(Etiqueta etiqueta) {
        log.info("....................................................................mbm............CONTROL");

        ParametroUMedFVta parametroUMedFVta = new ParametroUMedFVta();
        parametroUMedFVta.setId(etiqueta.getIdEtiqueta());
        parametroUMedFVta.setEstado(etiqueta.getAtributosAuditoria().getEstadoRegistro());
        parametroUMedFVta.setMethod(etiqueta.getAtributosAuditoria().getUltimaAccion());

        List<EtiquetaLang> etiquetasLang = etiquetaLangService.findEtiquetaLangBy_E(etiqueta);
        List<TraduccionDescripcion> traducciones = new ArrayList<TraduccionDescripcion>();
        for (EtiquetaLang etiquetaLang : etiquetasLang) {
            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(etiquetaLang.getIdioma());
            traduccionDescripcion.setTraduccion(etiquetaLang.getTraduccionDesc());
            traducciones.add(traduccionDescripcion);
        }
        parametroUMedFVta.setTraducciones(traducciones);

        return parametroUMedFVta;
    }

    private ParametroUMedFVta toJson(UnidadMedida unidadMedida) {
        log.info("....................................................................mbm............CONTROL");

        ParametroUMedFVta parametroUMedFVta = new ParametroUMedFVta();
        parametroUMedFVta.setId(unidadMedida.getIdUnidadMedida());
        parametroUMedFVta.setEstado(unidadMedida.getAtributosAuditoria().getEstadoRegistro());
        parametroUMedFVta.setMethod(unidadMedida.getAtributosAuditoria().getUltimaAccion());

        List<UnidadMedidaLang> unidadesMedidaLang = unidadMedidaLangService.findUnidadMedidaLangBy_UM(unidadMedida);
        List<TraduccionDescripcion> traducciones = new ArrayList<TraduccionDescripcion>();
        for (UnidadMedidaLang unidadMedidaLang : unidadesMedidaLang) {
            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(unidadMedidaLang.getIdioma());
            traduccionDescripcion.setTraduccion(unidadMedidaLang.getTraduccionDesc());
            traducciones.add(traduccionDescripcion);
        }
        parametroUMedFVta.setTraducciones(traducciones);

        return parametroUMedFVta;
    }

    private ParametroUMedFVta toJson(FormatoVta formatoVta) {
        log.info("....................................................................mbm............CONTROL");

        ParametroUMedFVta parametroUMedFVta = new ParametroUMedFVta();
        parametroUMedFVta.setId(formatoVta.getIdFormatoVta());
        parametroUMedFVta.setEstado(formatoVta.getAtributosAuditoria().getEstadoRegistro());
        parametroUMedFVta.setMethod(formatoVta.getAtributosAuditoria().getUltimaAccion());

        List<FormatoVtaLang> formatosVtaLang = formatoVtaLangService.findFormatoVtaLangBy_FV(formatoVta);
        List<TraduccionDescripcion> traducciones = new ArrayList<TraduccionDescripcion>();
        for (FormatoVtaLang formatoVtaLang : formatosVtaLang) {
            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(formatoVtaLang.getIdioma());
            traduccionDescripcion.setTraduccion(formatoVtaLang.getTraduccionDesc());
            traducciones.add(traduccionDescripcion);
        }
        parametroUMedFVta.setTraducciones(traducciones);

        return parametroUMedFVta;
    }

    private ParametroUMedFVta toJson(NivelProducto nivelProducto) {
        log.info("....................................................................mbm............CONTROL");

        ParametroUMedFVta parametroUMedFVta = new ParametroUMedFVta();
        parametroUMedFVta.setId(String.valueOf(nivelProducto.getIdNivelProducto()));
        parametroUMedFVta.setEstado(nivelProducto.getAtributosAuditoria().getEstadoRegistro());
        parametroUMedFVta.setMethod(nivelProducto.getAtributosAuditoria().getUltimaAccion());

        List<NivelProductoLang> nivelesProductoLang = nivelProductoLangService.findNivelProductoLangBy_NP(nivelProducto);
        List<TraduccionDescripcion> traducciones = new ArrayList<TraduccionDescripcion>();
        for (NivelProductoLang nivelProductoLang : nivelesProductoLang) {
            TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
            traduccionDescripcion.setIdioma(nivelProductoLang.getIdioma());
            traduccionDescripcion.setTraduccion(nivelProductoLang.getTraduccionDesc());
            traducciones.add(traduccionDescripcion);
        }
        parametroUMedFVta.setTraducciones(traducciones);

        return parametroUMedFVta;
    }
}
