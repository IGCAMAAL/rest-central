package com.centraldecompras.rest.excel;

import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.rest.excel.interfaces.SuppExcelService;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PatternFormatting;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

@Service
public class SuppExcelServiceImpl implements SuppExcelService {
    
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SuppExcelServiceImpl.class.getName());
    
    @Autowired
    private SociedadService sociedadService;

    public XSSFWorkbook montaExcelSupp(List<Sociedad> sociedades, List<UsrSocPrf> usrSocPrfs, List<Contacto> contactos, List<Direccion> direcciones) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        Cell cell;
        XSSFSheet sheet;

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        String fecha = "";

        sheet = workbook.createSheet("Sociedades");

        // Fondo para las cabeceras
        CellStyle backg_verde = workbook.createCellStyle();
        backg_verde.setFillForegroundColor(IndexedColors.LIGHT_GREEN.index);
        backg_verde.setFillPattern(PatternFormatting.SOLID_FOREGROUND);
        backg_verde.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        backg_verde.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        backg_verde.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        backg_verde.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);

        // Fondo amarillo para celdas protegidas
        CellStyle backg_amarillo = workbook.createCellStyle();
        backg_amarillo.setFillForegroundColor(IndexedColors.YELLOW.index);
        backg_amarillo.setFillPattern(PatternFormatting.SOLID_FOREGROUND);
        backg_amarillo.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        backg_amarillo.setBorderTop(HSSFCellStyle.BORDER_THIN);
        backg_amarillo.setBorderRight(HSSFCellStyle.BORDER_THIN);
        backg_amarillo.setBorderLeft(HSSFCellStyle.BORDER_THIN);

        // Celda normal
        CellStyle comun_cell = workbook.createCellStyle();
        comun_cell.setBorderRight(HSSFCellStyle.BORDER_THIN);
        comun_cell.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        comun_cell.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        
        // Linea final
        CellStyle linea_final = workbook.createCellStyle();
        linea_final.setBorderRight(HSSFCellStyle.BORDER_THIN);
        linea_final.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        linea_final.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        
        int rownum = cabeceraSociedades(sheet, backg_verde);

        int numR = 0;
        for (Sociedad sociedad : sociedades) {
            numR++;
            Row row = sheet.createRow(rownum++);
            int cellnum = 0;

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) sociedad.getTipoidentificador());
            sheet.autoSizeColumn(cellnum);
            if(numR < sociedades.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) sociedad.getNifnrf());
            sheet.autoSizeColumn(cellnum);
            if(numR < sociedades.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) sociedad.getNombre());
            sheet.autoSizeColumn(cellnum);
            if(numR < sociedades.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) sociedad.getAtributosAuditoria().getEstadoRegistro());
            sheet.autoSizeColumn(cellnum);
            if(numR < sociedades.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((int) sociedad.getAtributosAuditoria().getFechaPrevistaActivacion());
            sheet.autoSizeColumn(cellnum);
            if(numR < sociedades.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((int) sociedad.getAtributosAuditoria().getFechaPrevistaDesactivacion());
            sheet.autoSizeColumn(cellnum);
            if(numR < sociedades.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) sociedad.getIdSociedad());
            sheet.autoSizeColumn(cellnum);
            if(numR < sociedades.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

        }

        sheet = workbook.createSheet("Usuarios_Perfiles");
        rownum = cabeceraUsuarios(sheet, backg_verde);
        numR = 0;
        for (UsrSocPrf usrSocPrf : usrSocPrfs) {
            numR++;
            int cellnum = 0;
            UsrSoc usrSoc = usrSocPrf.getUsrSoc();
            Perfil perfil = usrSocPrf.getPerfil();

            Row row = sheet.createRow(rownum++);
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) usrSoc.getSociedad().getTipoidentificador());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_amarillo);

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) usrSoc.getSociedad().getNifnrf());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_amarillo);

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) usrSoc.getSociedad().getNombre());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_amarillo);

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) usrSoc.getUserCentral().getUsername());
            sheet.autoSizeColumn(cellnum);
              if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) usrSoc.getUserCentral().getEmail());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) usrSoc.getUserCentral().getAtributosAuditoria().getEstadoRegistro());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((int) usrSoc.getUserCentral().getAtributosAuditoria().getFechaPrevistaActivacion());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((int) usrSoc.getUserCentral().getAtributosAuditoria().getFechaPrevistaDesactivacion());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) usrSoc.getUserCentral().getId());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) perfil.getCodPerfil());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) perfil.getDescPerfil());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) perfil.getAtributosAuditoria().getEstadoRegistro());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((int) perfil.getAtributosAuditoria().getFechaPrevistaActivacion());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((int) perfil.getAtributosAuditoria().getFechaPrevistaDesactivacion());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) perfil.getIdPerfil());
            sheet.autoSizeColumn(cellnum);
            if(numR < usrSocPrfs.size()){
                cell.setCellStyle(comun_cell); 
            } else {
                cell.setCellStyle(linea_final); 
            }

        }

        sheet = workbook.createSheet("Contactos");
        rownum = cabeceraContactos(sheet, backg_verde);
        numR = 0;
        for (Contacto contacto : contactos) {
            numR++;
            
            if (contacto.getSociedad() != null) {

                String idSociedad = contacto.getSociedad().getIdSociedad();
                Sociedad sociedad = sociedadService.findSociedad(idSociedad);

                Row row = sheet.createRow(rownum++);
                int cellnum = 0;

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) sociedad.getTipoidentificador());
                sheet.autoSizeColumn(cellnum);
                cell.setCellStyle(backg_amarillo);

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) sociedad.getNifnrf());
                sheet.autoSizeColumn(cellnum);
                cell.setCellStyle(backg_amarillo);

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) sociedad.getNombre());
                sheet.autoSizeColumn(cellnum);
                cell.setCellStyle(backg_amarillo);

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) contacto.getDescripcion());
                sheet.autoSizeColumn(cellnum);
                if(numR < contactos.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) contacto.getTipo());
                sheet.autoSizeColumn(cellnum);
                if(numR < contactos.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) contacto.getValor());
                sheet.autoSizeColumn(cellnum);
                if(numR < contactos.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) contacto.getAtributosAuditoria().getEstadoRegistro());
                sheet.autoSizeColumn(cellnum);
                if(numR < contactos.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((int) contacto.getAtributosAuditoria().getFechaPrevistaActivacion());
                sheet.autoSizeColumn(cellnum);
                if(numR < contactos.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((int) contacto.getAtributosAuditoria().getFechaPrevistaDesactivacion());
                sheet.autoSizeColumn(cellnum);
                if(numR < contactos.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) contacto.getIdContacto());
                sheet.autoSizeColumn(cellnum);
                if(numR < contactos.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

            }
        }

        sheet = workbook.createSheet("Direcciones");
        rownum = cabeceraDirecciones(sheet, backg_verde);
        numR = 0;
        for (Direccion direccion : direcciones) {
            if (direccion.getSociedad() != null) {
                numR++;

                String idSociedad = direccion.getSociedad().getIdSociedad();
                Sociedad sociedad = sociedadService.findSociedad(idSociedad);

                Row row = sheet.createRow(rownum++);
                int cellnum = 0;

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) sociedad.getTipoidentificador());
                sheet.autoSizeColumn(cellnum);
                cell.setCellStyle(backg_amarillo);

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) sociedad.getNifnrf());
                sheet.autoSizeColumn(cellnum);
                cell.setCellStyle(backg_amarillo);

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) sociedad.getNombre());
                sheet.autoSizeColumn(cellnum);
                cell.setCellStyle(backg_amarillo);

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) direccion.getDireccionGoogleFormateada());
                sheet.autoSizeColumn(cellnum);
                if(numR < direcciones.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((double) direccion.getLatitud());
                sheet.autoSizeColumn(cellnum);
                if(numR < direcciones.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((double) direccion.getLongitud());
                sheet.autoSizeColumn(cellnum);
                if(numR < direcciones.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) direccion.getTipoDireccion());
                sheet.autoSizeColumn(cellnum);
                if(numR < direcciones.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) direccion.getAtributosAuditoria().getEstadoRegistro());
                sheet.autoSizeColumn(cellnum);
                if(numR < direcciones.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((int) direccion.getAtributosAuditoria().getFechaPrevistaActivacion());
                sheet.autoSizeColumn(cellnum);
                if(numR < direcciones.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((int) direccion.getAtributosAuditoria().getFechaPrevistaDesactivacion());
                sheet.autoSizeColumn(cellnum);
                if(numR < direcciones.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

                cell = row.createCell(cellnum++);
                cell.setCellValue((String) direccion.getIdDireccion());
                sheet.autoSizeColumn(cellnum);
                if(numR < direcciones.size()){
                    cell.setCellStyle(comun_cell); 
                } else {
                    cell.setCellStyle(linea_final); 
                }

            }
        }

        return workbook;
    }

    private int cabeceraSociedades(XSSFSheet sheet, CellStyle backg_verde) {

        /*
         tipoidentificador;            
         Nifnrf;
         Nombre;
         EstadoRegistro;
         FechaPrevistaActivacion;
         FechaPrevistaDesactivacion;
         IdSociedad;  
         */
        int rownum = 0;

        Row row = sheet.createRow(rownum++);
        int cellnum = 0;
        Cell cell;

        cell = row.createCell(cellnum++);
        cell.setCellValue("T.iden");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nif-Nrf");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nombre");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Estado");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Act");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Desact");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Id Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        return rownum;
    }

    private int cabeceraUsuarios(XSSFSheet sheet, CellStyle backg_verde) {

        /*
         Tipoidentificador_Soc
         Nifnrf_Soc;
         Nombre_Soc;
         Username;  
         Email;
         EstadoRegistro;
         FechaPrevistaActivacion;
         FechaPrevistaDesactivacion;
         UserCentralId; 

         CodPerfil;            
         DescPerfil;
         EstadoRegistro;
         FechaPrevistaActivacion;
         FechaPrevistaDesactivacion;
         IdPerfil; 
         */
        int rownum = 0;

        Row row = sheet.createRow(rownum++);
        int cellnum = 0;
        Cell cell;

        cell = row.createCell(cellnum++);
        cell.setCellValue("T.I. Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nif-Nrf Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nombre Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("User Name");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Email");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Estado Usu");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Act");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Desact");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("User Id");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Cod Perfil");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Desc Perfil");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Estado Perfil");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Act");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Desact");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Id Perfil");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        return rownum;
    }

    private int cabeceraContactos(XSSFSheet sheet, CellStyle backg_verde) {

        /*
         Tipoidentificador
         tNifnrf;
         Nombre
         Descripcion
         Tipo
         Valor

         EstadoRegistro
         FechaPrevistaActivacion
         FechaPrevistaDesactivacion
         IdContacto  
         */
        int rownum = 0;

        Row row = sheet.createRow(rownum++);
        int cellnum = 0;
        Cell cell;

        cell = row.createCell(cellnum++);
        cell.setCellValue("T.I.Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nif-Nrf Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nombre Contacto");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Descripcion Contacto");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Tipo Contacto");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Valor Contacto");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Estado Cont");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Act");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Desact");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Id Contacto");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        return rownum;
    }

    private int cabeceraDirecciones(XSSFSheet sheet, CellStyle backg_verde) {

        /*
         Tipoidentificador  
         Nifnrf
         Nombre               
         DireccionGoogleFormateada
         Latitud
         Longitud                
         TipoDireccion

         EstadoRegistro
         FechaPrevistaActivacion
         FechaPrevistaDesactivacion
         IdDireccion 
         */
        int rownum = 0;

        Row row = sheet.createRow(rownum++);
        int cellnum = 0;
        Cell cell;

        cell = row.createCell(cellnum++);
        cell.setCellValue("T.I.Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nif-Nrf Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nombre Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Direccion");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Latitud");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Longitud");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Tipo Direccion");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Estado");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Act");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Desact");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Id Direccion");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        return rownum;
    }

}
