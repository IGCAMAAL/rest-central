package com.centraldecompras.rest.excel;

import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.NivelProductoLang;
import com.centraldecompras.modelo.ProductoLang;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoLangService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoLangService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaLangService;
import com.centraldecompras.rest.excel.interfaces.ProductoArbolExcelService;
import com.centraldecompras.rest.mat.ser.interfaces.ProductoRestService;
import com.centraldecompras.util.Converter;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ComparisonOperator;
import org.apache.poi.ss.usermodel.ConditionalFormattingRule;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PatternFormatting;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductoArbolExcelServiceImpl implements ProductoArbolExcelService {
    
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductoArbolExcelServiceImpl.class.getName());
    
    @Autowired
    private ProductoService productoService;

    @Autowired
    private ProductoLangService productoLangService;

    @Autowired
    private NivelProductoLangService nivelProductoLangService;

    @Autowired
    private NivelProductoService nivelProductoService;

    @Autowired
    private UnidadMedidaLangService unidadMedidaLangService;

    @Autowired
    private ProductoRestService productoRestService;

    public XSSFWorkbook montaExcel(HashMap filtro, int ultimoNivelSector, Map<String, String> paramInput) {

        int[] ramaCabecB = new int[ultimoNivelSector];

        List<String[]> ramasArbol = new ArrayList();
        String[] ramaArbol = new String[ultimoNivelSector];

        //String idProducto = paramInput.get("idProducto");
        String estado = paramInput.get("estado");
        String idioma = paramInput.get("idioma");

        String padreJerarquia = KApp.PRODUCTO_UNIVERSAL.getKApp();
        int nivel = 0;

        /*
         if (idProducto != null && !idProducto.equals("")) {
         Producto producto = productoService.findProducto(idProducto);
         if (producto != null) {
         padreJerarquia = producto.getIdPadreJerarquia();
         nivel = producto.getNivelInt() - 1;
         }
         } else {
         padreJerarquia = KApp.PRODUCTO_UNIVERSAL.getKApp();
         }
         */
        productoRestService.recurrenteCabecerasProducto(0, idioma, filtro, ultimoNivelSector, ramaCabecB);
        productoRestService.recurrenteProductosHermanos(nivel, idioma, filtro, padreJerarquia, ultimoNivelSector, ramaArbol, ramasArbol, estado);

        XSSFWorkbook workbook = new XSSFWorkbook();

        int rownum = 0;
        Cell cell;
        XSSFSheet sheet;
        sheet = workbook.createSheet("Productos");

        // Bloqueo de hoja y Desbloqueo de celda
        sheet.protectSheet("password");
        CellStyle unlockedCellStyle = workbook.createCellStyle();
        unlockedCellStyle.setLocked(false);

        int lineasNuevas = 5;
        int colDesdeUniMedida = 0;
        int colHastaUniMedida = 0;
        int rowDesdeUniMedida = 1;
        int rowHastaUniMedida = ramasArbol.size() + lineasNuevas;

        DataValidation dataValidation = null;
        DataValidationConstraint constraint = null;
        DataValidationHelper validationHelper = null;

        List<String[]> unidadesMedidaLang = unidadMedidaLangService.findUnidadMedidaLangBy_Lang(KApp.CATALAN.getKApp());
        List<Object> outConverted = Converter.listArrayStringToHashAndArrayString(unidadesMedidaLang);
        String[] descList = (String[]) outConverted.get(0);
        //HashMap descKeyAndId = (HashMap) outConverted.get(1);
        HashMap idKeyAndDesc = (HashMap) outConverted.get(2);

        validationHelper = new XSSFDataValidationHelper(sheet);
        CellRangeAddressList addressList = new CellRangeAddressList(rowDesdeUniMedida, rowHastaUniMedida, colDesdeUniMedida, colHastaUniMedida);
        constraint = validationHelper.createExplicitListConstraint(descList);
        dataValidation = validationHelper.createValidation(constraint, addressList);
        dataValidation.setSuppressDropDownArrow(true);
        sheet.addValidationData(dataValidation);

        // Fondo amarillo para el idioma
        CellStyle backg_amarillo = workbook.createCellStyle();
        backg_amarillo.setFillForegroundColor(IndexedColors.YELLOW.index);
        backg_amarillo.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

        // Fondo para las cabeceras
        CellStyle backg_verde = workbook.createCellStyle();
        backg_verde.setFillForegroundColor(IndexedColors.LIGHT_GREEN.index);
        backg_verde.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

        // Fondo para el cuerpo del excel
        CellStyle backg_grey = workbook.createCellStyle();
        backg_grey.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        backg_grey.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

        List<String> productoLangsKey = nivelProductoLangService.findProductoLangKey();
        HashMap<String, Integer> languageKey = new HashMap<String, Integer>();
        for (int ic1 = 0; ic1 < productoLangsKey.size(); ic1++) {
            String productoLangKey = productoLangsKey.get(ic1);
            languageKey.put(productoLangKey, ic1);
        }

        Row row = sheet.createRow(rownum++);

        // Cabecera ------------------------------------------------------------
        NivelProducto nivelProducto = null;
        List<NivelProductoLang> desNivLang = null;

        for (int ic1 = 0; ic1 < ramaCabecB.length; ic1++) {
            cell = row.createCell(0);
            cell.setCellValue("U. M. Prdt.");
            cell.setCellStyle(backg_verde);
            sheet.autoSizeColumn(0);

            nivelProducto = nivelProductoService.findNivelProducto(ramaCabecB[ic1]);
            desNivLang = nivelProductoLangService.findNivelProductoLangBy_NP(nivelProducto);
            for (int ic2 = 0; ic2 < desNivLang.size(); ic2++) {
                NivelProductoLang nivelProductoLang = desNivLang.get(ic2);

                int idxLang = languageKey.get(nivelProductoLang.getIdioma());
                int idxDesc = ramaCabecB.length + 1;

                cell = row.createCell(1 + idxLang * idxDesc);
                cell.setCellValue((String) nivelProductoLang.getIdioma());
                cell.setCellStyle(backg_verde);
                sheet.autoSizeColumn(1 + idxLang * idxDesc);

                cell = row.createCell((1 + idxLang * idxDesc) + ic1 + 1);
                cell.setCellValue((String) nivelProductoLang.getTraduccionDesc());
                cell.setCellStyle(backg_verde);
                sheet.autoSizeColumn((1 + idxLang * idxDesc) + ic1 + 1);
            }
        }

        // Productos existentes-------------------------------------------------
        for (String[] ramaArbolB : ramasArbol) {
            List<String> idiomasExistentes = new ArrayList();
            List<ProductoLang> productosLang = null;

            List<String> idiomasExistentesWork = null; 
            List<ProductoLang> productosLangWork = null; 

            try {

                for (int ic1 = 0; ic1 < ramaArbolB.length; ic1++) {

                    productosLang = productoLangService.findProductoLangBy_IdP(ramaArbolB[ic1]);

                    // Corrige la descripcion en caso de encontrarse con nodos vacios.
                    if (productosLang.size() != 0) {
                        for (ProductoLang productoLang : productosLang) {
                            idiomasExistentes.add(productoLang.getIdioma());
                        }

                        productosLangWork = new ArrayList();
                        idiomasExistentesWork = new ArrayList();

                        productosLangWork.addAll(productosLang);
                        idiomasExistentesWork.addAll(idiomasExistentes);

                    } else {
                       
                        for(ProductoLang productoLangWork : productosLangWork){
                            productoLangWork.setTraduccionDesc(" ");
                            productoLangWork.setProductoId(" "); 
                            productoLangWork.setProductoEntity(null);
                            productoLangWork.setAtributosAuditoria(null);
                        }
                    }

                    if (ic1 == 0) {
                        String unidadMedidaId = productoService.findProducto(ramaArbolB[ic1]).getUnidadMedida().getIdUnidadMedida();
                        row = sheet.createRow(rownum++);
                        cell = row.createCell(0);
                        cell.setCellValue((String) idKeyAndDesc.get(unidadMedidaId));
                        cell.setCellStyle(unlockedCellStyle);
                    }

                    int idxNumNiv = ramaArbolB.length + 1;
                    for (int ic2 = 0; ic2 < productosLangWork.size(); ic2++) {

                        ProductoLang productoLangEle = productosLangWork.get(ic2); 
                        int idxLang = languageKey.get(productoLangEle.getIdioma());

                        // Celda del idioma
                        cell = row.createCell(1 + idxLang * idxNumNiv);
                        cell.setCellValue((String) productoLangEle.getIdioma());
                        cell.setCellStyle(backg_amarillo);
                        sheet.autoSizeColumn(1 + idxLang * idxNumNiv);

                        // Celda de la descripcion del producto
                        cell = row.createCell(1 + (idxLang * idxNumNiv) + 1 + ic1);
                        cell.setCellValue((String) productoLangEle.getTraduccionDesc());
                        cell.setCellStyle(unlockedCellStyle);
                        sheet.autoSizeColumn(1 + (idxLang * idxNumNiv) + 1 + ic1);

                        // Idioma nuevo en Productos existentes---------------------
                        for (Map.Entry<String, Integer> entry : languageKey.entrySet()) {
                            String idiomaNivelProducto = entry.getKey();
                            boolean contiene = idiomasExistentesWork.contains(idiomaNivelProducto);
                            if (!contiene) {
                                idxLang = languageKey.get(idiomaNivelProducto);

                                // Celda del nuevo idioma
                                cell = row.createCell(1 + idxLang * idxNumNiv);
                                cell.setCellValue(idiomaNivelProducto);
                                cell.setCellStyle(backg_amarillo);
                                sheet.autoSizeColumn(1 + idxLang * idxNumNiv);

                                // Celda con descripcion vacia. El usuario la cumplimentará manualmente en el excel
                                cell = row.createCell(1 + (idxLang * idxNumNiv) + 1 + ic1);
                                cell.setCellValue(" ");
                                cell.setCellStyle(unlockedCellStyle);
                                sheet.autoSizeColumn(1 + (idxLang * idxNumNiv) + 1 + ic1);
                            }
                        }
                    }

                    // Celda con el id del producto
                    int idxIdPr = (idxNumNiv * languageKey.size());
                    cell = row.createCell(1 + (idxIdPr) + ic1);
                    cell.setCellValue((String) ramaArbolB[ic1]);
                    sheet.autoSizeColumn(1 + (idxIdPr) + ic1);
                    cell.setCellStyle(backg_grey);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Productos nuevos ----------------------------------------------------
        for (int ib1 = 0; ib1 < lineasNuevas; ib1++) {
            row = sheet.createRow(rownum++);
            cell = row.createCell(0);
            cell.setCellValue(KApp.SELECCIONA.getKApp());
            cell.setCellStyle(unlockedCellStyle);

            String[] ramaArbolB = ramasArbol.get(0);
            for (int ic1 = 0; ic1 < ramaArbolB.length; ic1++) {

                for (Map.Entry<String, Integer> languageEntry : languageKey.entrySet()) {
                    String idiomaNivelProducto = languageEntry.getKey();

                    int idxLang = languageKey.get(idiomaNivelProducto);
                    int idxdesc = ramaArbolB.length + 1;

                    cell = row.createCell(1 + idxLang * idxdesc);
                    cell.setCellValue(idiomaNivelProducto);
                    cell.setCellStyle(backg_amarillo);
                    sheet.autoSizeColumn(1 + idxLang * idxdesc);

                    cell = row.createCell(1 + (idxLang * idxdesc) + ic1 + 1);
                    cell.setCellValue(" ");
                    cell.setCellStyle(unlockedCellStyle);
                    sheet.autoSizeColumn(1 + (idxLang * idxdesc) + ic1 + 1);
                }
            }
        }

        SheetConditionalFormatting sheetCF = sheet.getSheetConditionalFormatting();
        ConditionalFormattingRule rule1 = sheetCF.createConditionalFormattingRule(ComparisonOperator.EQUAL, KApp.SELECCIONA.getKApp());
        PatternFormatting fill1 = rule1.createPatternFormatting();
        fill1.setFillForegroundColor(IndexedColors.CORAL.index);
        fill1.setFillPattern(PatternFormatting.SOLID_FOREGROUND);

        sheet.lockDeleteColumns();
        sheet.lockDeleteRows();

        return workbook;
    }
}
