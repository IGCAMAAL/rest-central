package com.centraldecompras.rest.excel;

import com.centraldecompras.acceso.Sector;
import com.centraldecompras.rest.excel.interfaces.ArticuloExcelService;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.service.PerfilServiceImpl;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.NivelProductoLang;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoLangService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoService;
import com.centraldecompras.rest.mat.ser.interfaces.ProductoRestService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PatternFormatting;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArticuloExcelServiceImpl implements ArticuloExcelService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloExcelServiceImpl.class.getName());
    
    @Autowired
    private SociedadService sociedadService;

    @Autowired
    private ProductoRestService productoRestService;

    @Autowired
    private NivelProductoService nivelProductoService;

    @Autowired
    private NivelProductoLangService nivelProductoLangService;

    @Override
    public XSSFWorkbook montaExcel(List<Object[]> articulosProductos, Sector sector, String idioma, String estado) throws NonexistentEntityException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        Cell cell;
        XSSFSheet sheet;

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        String fecha = "";

        
        sheet = workbook.createSheet("Articulos");

        // Bloqueo de hoja y Desbloqueo de celda
        sheet.protectSheet("password");
        CellStyle unlockedCellStyle = workbook.createCellStyle();
        unlockedCellStyle.setLocked(false);
        
        // Fondo para las cabeceras
        CellStyle backg_verde = workbook.createCellStyle();
        backg_verde.setFillForegroundColor(IndexedColors.LIGHT_GREEN.index);
        backg_verde.setFillPattern(PatternFormatting.SOLID_FOREGROUND);
        
         // Fondo para el cuerpo del excel
        CellStyle backg_grey = workbook.createCellStyle();
        backg_grey.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        backg_grey.setFillPattern(PatternFormatting.SOLID_FOREGROUND);
        
        int[] ramaCabec = new int[sector.getUltimoNivel()];
        productoRestService.recurrenteCabecerasProducto(0, idioma, null, sector.getUltimoNivel(), ramaCabec);

        int rownum = cabeceraArticulos_Productos(sheet, ramaCabec, idioma, backg_verde);

        for (Object[] articuloProducto : articulosProductos) {

            /*
             00 l.articuloEntity.idArticulo
             01 l.idioma 
             02 l.traduccionDesc
             03 l.articuloEntity.uniMinVta
             04 l.articuloEntity.precioUniMed
             05 l.articuloEntity.unidadFormatoVta
             06 l.articuloEntity.codiParaProveedor
             07 l.articuloEntity.codigoEAN
             08 l.articuloEntity.formatoVta.tipoEnvase
             09 l.articuloEntity.sociedad.id
             10 p.productoIdk
             11 l.articuloEntity.atributosAuditoria.estadoRegistro
             12 l.articuloEntity.atributosAuditoria.fechaPrevistaActivacion, 
             13 l.articuloEntity.atributosAuditoria.fechaPrevistaDesactivacion
             14 l.articuloEntity.
             */
            String sociedadId = (String) articuloProducto[9];
            Sociedad sociedad = sociedadService.findSociedad(sociedadId);
            if (sociedad == null) {
                Object[] valores = {sociedadId};
                String mensaje = MessageFormat.format("ERROR: Sociedad {0}, no encontrada. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }

            DecimalFormat df = new DecimalFormat("0.00");
            Double umv = ((BigDecimal) articuloProducto[3]).doubleValue();
            Double pum = ((BigDecimal) articuloProducto[4]).doubleValue();
            Double ufv = ((BigDecimal) articuloProducto[5]).doubleValue();
            double pf = (int) Math.round(pum * ufv * 100) / (double) 100;

            String[] ramaArbol = new String[sector.getUltimoNivel()];
            productoRestService.recurrenteProductosPadre(sector.getUltimoNivel(), idioma, null, (String) articuloProducto[10], sector.getUltimoNivel(), ramaArbol, estado);

            Row row = sheet.createRow(rownum++);
            int cellnum = 0;

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) sociedad.getNombre());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) articuloProducto[0]);        //  00 a.idArticulo   
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) articuloProducto[1]);        //  01 al.idioma,
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) articuloProducto[2]);        //  02 al.traduccionDesc
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue(umv);                                 //  03 a.uniMinVta
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue(pum);                                 //  04 a.precioUniMed
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(unlockedCellStyle);
            
            
            cell = row.createCell(cellnum++);
            cell.setCellValue(ufv);                                 //  05 a.unidadFormatoVta
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(unlockedCellStyle);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue(pf);                                  //  Precio formato
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) articuloProducto[6]);        //  06 a.codiParaProveedor
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(unlockedCellStyle);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) articuloProducto[7]);        //  07 a.codigoEAN
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) articuloProducto[8]);        //  08 a.formatoVta.tipoEnvase
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) articuloProducto[10]);       //  10 ap.productoIdk
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) articuloProducto[11]);       //  11 al.articuloEntity.atributosAuditoria.estadoRegistro
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) articuloProducto[12]);          //  12 al.articuloEntity.atributosAuditoria.fechaPrevistaActivacion
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) articuloProducto[13]);          //  13 al.articuloEntity.atributosAuditoria.fechaPrevistaDesactivacion
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_grey);
            
            for (String ramaEle : ramaArbol) {
                cell = row.createCell(cellnum++);
                cell.setCellValue((String) ramaEle);                //  nn Elementos del arbol
                sheet.autoSizeColumn(cellnum);
                cell.setCellStyle(backg_grey);
            }

        }

        return workbook;
    }

    private int cabeceraArticulos_Productos(XSSFSheet sheet, int[] ramaCabec, String idioma, CellStyle backg_verde) {

        /*
         09 l.articuloEntity.sociedad.id
        
         00 l.articuloEntity.idArticulo
         01 l.idioma 
         02 l.traduccionDesc
         03 l.articuloEntity.uniMinVta
         04 l.articuloEntity.precioUniMed
        
         05 l.articuloEntity.unidadFormatoVta
         06 l.articuloEntity.codiParaProveedor
         07 l.articuloEntity.codigoEAN
         08 l.articuloEntity.formatoVta.tipoEnvase
         10 p.productoIdk
        
         11 l.articuloEntity.atributosAuditoria.estadoRegistro
         12 l.articuloEntity.atributosAuditoria.fechaPrevistaActivacion, 
         13 l.articuloEntity.atributosAuditoria.fechaPrevistaDesactivacion
         14 l.articuloEntity.
         */
        int rownum = 0;

        Row row = sheet.createRow(rownum++);
        int cellnum = 0;
        Cell cell;

        cell = row.createCell(cellnum++);
        cell.setCellValue("Sociedad");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("Id Articulo");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("Idioma");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("Nombr articulo");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("uniMinVta");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("precioUniMed");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("unidadFormatoVta");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("Precio Formato");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("codiParaProveedor");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("codigoEAN");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("tipoEnvase");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("productoIdk");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("estadoRegistro");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("fechaPrevistaActivacion");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("fechaPrevistaDesactivacion");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        // Cabecera ------------------------------------------------------------
        for (int ic1 = 0; ic1 < ramaCabec.length; ic1++) {
            cell = row.createCell(cellnum++);

            NivelProductoLang nivelProductoLang = nivelProductoLangService.findNivelProductoLangBy_IdNP_Lang(ramaCabec[ic1], idioma);
            cell.setCellValue((String) nivelProductoLang.getTraduccionDesc());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(backg_verde);
        }

        return rownum;
    }

}
