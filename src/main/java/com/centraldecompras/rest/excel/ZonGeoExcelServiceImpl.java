package com.centraldecompras.rest.excel;

import com.centraldecompras.rest.excel.interfaces.ZonGeoExcelService;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.ZonasGeograficasAutorizadas;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PatternFormatting;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ZonGeoExcelServiceImpl implements ZonGeoExcelService {
    
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ZonGeoExcelServiceImpl.class.getName());
    
    @Autowired
    private SociedadService sociedadService;

    public XSSFWorkbook montaExcel(List<ZonaGeografica> zonasGeografica, List<SocZonGeo> socZonsGeo, List<ZonasGeograficasAutorizadas> zonasGeograficasAutorizadas) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        Cell cell;
        XSSFSheet sheet;
        String padre = null;

        // Fondo para las cabeceras
        CellStyle backg_verde = workbook.createCellStyle();
        backg_verde.setFillForegroundColor(IndexedColors.LIGHT_GREEN.index);
        backg_verde.setFillPattern(PatternFormatting.SOLID_FOREGROUND);
        backg_verde.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        backg_verde.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
        backg_verde.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
        backg_verde.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);

        // Celda normal
        CellStyle comun_cell = workbook.createCellStyle();
        comun_cell.setBorderRight(HSSFCellStyle.BORDER_THIN);
        comun_cell.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        comun_cell.setBorderBottom(HSSFCellStyle.BORDER_THIN);

        sheet = workbook.createSheet("zonasGeografica");

        int rownum = cabeceraZonasGeograficas(sheet, backg_verde);
        for (ZonaGeografica zonaGeografica : zonasGeografica) {
            Row row = sheet.createRow(rownum++);
            int cellnum = 0;

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getDescripcion());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) zonaGeografica.getNivelInt());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            padre = (zonaGeografica.getPadreJerarquia() == null) ? "" : zonaGeografica.getPadreJerarquia().getDescripcion();
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) padre);
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getAtributosAuditoria().getEstadoRegistro());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) zonaGeografica.getAtributosAuditoria().getFechaPrevistaActivacion());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) zonaGeografica.getAtributosAuditoria().getFechaPrevistaDesactivacion());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getIdZonaGeografica());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getIdPadreJerarquia());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getEtiqueta());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
        }

        sheet = workbook.createSheet("Sociedades_ZonasGeo");
        rownum = cabeceraSocZonasGeograficas(sheet, backg_verde);
        for (SocZonGeo socZonGeo : socZonsGeo) {

            Sociedad sociedad = sociedadService.findSociedad(socZonGeo.getSociedad().getIdSociedad());
            ZonaGeografica zonaGeografica = socZonGeo.getZonaGeografica();

            Row row = sheet.createRow(rownum++);
            int cellnum = 0;
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) sociedad.getTipoidentificador());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) sociedad.getNifnrf());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) sociedad.getNombre());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getDescripcion());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) zonaGeografica.getNivelInt());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            padre = (zonaGeografica.getPadreJerarquia() == null) ? "" : zonaGeografica.getPadreJerarquia().getDescripcion();
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) padre);
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getAtributosAuditoria().getEstadoRegistro());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) zonaGeografica.getAtributosAuditoria().getFechaPrevistaActivacion());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) zonaGeografica.getAtributosAuditoria().getFechaPrevistaDesactivacion());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getIdZonaGeografica());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getIdPadreJerarquia());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeografica.getEtiqueta());
            sheet.autoSizeColumn(cellnum);
            cell.setCellStyle(comun_cell);
        }

        sheet = workbook.createSheet("zonasGeograficasAutorizadas");
        rownum = 0;
        for (ZonasGeograficasAutorizadas zonaGeograficasAutorizada : zonasGeograficasAutorizadas) {
            Row row = sheet.createRow(rownum++);
            int cellnum = 0;
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getZonaGeografica().getDescripcion());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) zonaGeograficasAutorizada.getZonaGeografica().getNivelInt());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getZonaGeografica().getPadreJerarquia().getDescripcion());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getZonaGeografica().getIdPadreJerarquia());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getZonaGeografica().getIdZonaGeografica());


            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getZonaGeografica().getEtiqueta());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getPersona().getUserName());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getPersona().getApellido1());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getPersona().getApellido2());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getAtributosAuditoria().getEstadoRegistro());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) zonaGeograficasAutorizada.getAtributosAuditoria().getFechaPrevistaActivacion());
            
            cell = row.createCell(cellnum++);
            cell.setCellValue((int) zonaGeograficasAutorizada.getAtributosAuditoria().getFechaPrevistaDesactivacion());

            cell = row.createCell(cellnum++);
            cell.setCellValue((String) zonaGeograficasAutorizada.getIdZonasGeograficasDeAccion());

        }

        return workbook;
    }

    private int cabeceraZonasGeograficas(XSSFSheet sheet, CellStyle backg_verde) {

        /*
         Descripcion            
         NivelInt
         Descripcion padre
            
         EstadoRegistro(
         FechaPrevistaActivacion
         FechaPrevistaDesactivacion
        
         IdZonaGeografica
         IdPadreJerarquia
         Etiqueta

         */
        int rownum = 0;

        Row row = sheet.createRow(rownum++);
        int cellnum = 0;
        Cell cell;

        cell = row.createCell(cellnum++);
        cell.setCellValue("Desc Zona");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("NivelInt");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Desc Zona Sup");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Estado");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Act");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Desact");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Id Zona");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Id Zona Sup");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue(" ");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        return rownum;
    }


    private int cabeceraSocZonasGeograficas(XSSFSheet sheet, CellStyle backg_verde) {

        /*

            Tipoidentificador
            Nifnrf
            Nombre
        
            Descripcion
            NivelInt
            padre
        
            EstadoRegistro
            FechaPrevistaActivacion
            FechaPrevistaDesactivacion
            IdZonaGeografica
            dPadreJerarquia
            Etiqueta


         */
        int rownum = 0;

        Row row = sheet.createRow(rownum++);
        int cellnum = 0;
        Cell cell;

        cell = row.createCell(cellnum++);
        cell.setCellValue("T.I. Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nif-Nrf Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Nombre Soc");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("Desc Zona");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("NivelInt");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Desc Zona Sup");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Estado");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Act");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Fecha Desact");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Id Zona");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue("Id Zona Sup");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        cell = row.createCell(cellnum++);
        cell.setCellValue(" ");
        sheet.autoSizeColumn(cellnum);
        cell.setCellStyle(backg_verde);

        return rownum;
    }

}
