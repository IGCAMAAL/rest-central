package com.centraldecompras.rest.excel.interfaces;


import java.util.HashMap;
import java.util.Map;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public interface ProductoArbolExcelService {

    XSSFWorkbook montaExcel(HashMap filtro, int ultimoNivelSector, Map<String, String> paramInput);
}
