/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.excel.interfaces;


import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.ZonasGeograficasAutorizadas;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public interface ZonGeoExcelService {

    XSSFWorkbook montaExcel(List<ZonaGeografica> zonasGeografica, List<SocZonGeo> socZonsGeo, List<ZonasGeograficasAutorizadas>  zonasGeograficasAutorizadas );
}
