/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.excel.interfaces;


import com.centraldecompras.acceso.Sector;
import com.centraldecompras.modelo.ArticuloProducto;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public interface ArticuloExcelService {

    XSSFWorkbook montaExcel(List<Object[]> articulosProductos, Sector sector, String idioma, String estado)  throws NonexistentEntityException;
}
