/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.rest.excel.interfaces;


import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.Direccion;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public interface SuppExcelService {

    XSSFWorkbook montaExcelSupp(List<Sociedad> sociedades, List<UsrSocPrf> usrSocPrfs, List<Contacto> contactos, List<Direccion> direcciones);
}
