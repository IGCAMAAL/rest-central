package com.centraldecompras.rest.BORRAR;

import com.centraldecompras.rest.ven.json.auxi.ConDatFraJson;
import java.util.List;

public class DiasEntregaJson {

    private String sociedadId;

    private List<ConDatFraJson> condiciones;              // SociedadId que emite la opinion. No necesario por recoger del header
    private List<ConDatFraJson> datosFacturacion;    // UsuarioId que emite la opinion.No necesario por recoger del haeder

    public DiasEntregaJson() {
    }

    public DiasEntregaJson(String sociedadId, List<ConDatFraJson> condiciones, List<ConDatFraJson> datosFacturacion) {
        this.sociedadId = sociedadId;
        this.condiciones = condiciones;
        this.datosFacturacion = datosFacturacion;
    }

    public String getSociedadId() {
        return sociedadId;
    }

    public void setSociedadId(String sociedadId) {
        this.sociedadId = sociedadId;
    }

    public List<ConDatFraJson> getCondiciones() {
        return condiciones;
    }

    public void setCondiciones(List<ConDatFraJson> condiciones) {
        this.condiciones = condiciones;
    }

    public List<ConDatFraJson> getDatosFacturacion() {
        return datosFacturacion;
    }

    public void setDatosFacturacion(List<ConDatFraJson> datosFacturacion) {
        this.datosFacturacion = datosFacturacion;
    }

}
