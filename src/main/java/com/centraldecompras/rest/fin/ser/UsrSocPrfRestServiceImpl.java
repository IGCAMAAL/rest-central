package com.centraldecompras.rest.fin.ser;

import com.centraldecompras.acceso.DatosAuditoriaAcceso;
import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.acceso.service.interfaces.UsrSocPrfService;
import com.centraldecompras.rest.fin.json.PerfilJson;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import com.centraldecompras.rest.fin.ser.interfaces.UsrSocPrfRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsrSocPrfRestServiceImpl implements UsrSocPrfRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UsrSocPrfRestServiceImpl.class.getName()); 

    @Autowired
    private UsrSocPrfService usrSocPrfService;

    public List<Object[]> montaUsrSocPrf(UsrSoc prsSoc, Perfil perfil, PerfilJson perfilJson, String idUser, String metodoPadre, UsuarioJson usuarioJson) throws Exception {
        log.info("....................................................................mbm............CONTROL");
                
        List<Object[]> reply = new ArrayList();
        Object[] usrSocPrf_Array = new Object[4];
        UsrSocPrf usrSocPrf = null;

        if ((KApp.DELETE.getKApp().equals(metodoPadre))) {
            perfilJson.setMethod(KApp.DELETE.getKApp());
        }
        String metodo = perfilJson.getMethod();

        if (KApp.POST.getKApp().equals(metodo)) {
            usrSocPrf = new UsrSocPrf();
            usrSocPrf.setIdUsrSocPrf(UUID.randomUUID().toString().replaceAll("-", ""));
            usrSocPrf.setAtributosAuditoria(new DatosAuditoriaAcceso());
            usrSocPrf.setUsrSoc(prsSoc);
            usrSocPrf.setPerfil(perfil);
            usrSocPrf.getAtributosAuditoria().setCreated(new Date());
            usrSocPrf.getAtributosAuditoria().setDeleted(null);
            usrSocPrf.setAsociadoAdministrador(null);
            usrSocPrf.setEstadoSesion(Boolean.FALSE);
        } else {
            usrSocPrf = usrSocPrfService.findUsrSocByUP(prsSoc, perfil);
            if (usrSocPrf == null) {
                Object[] valores = {prsSoc.toString(), perfil.toString()};
                String mensaje = MessageFormat.format("ERROR: usrSocPrfId  Id {0} no relacionado con Perfil {1}. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            } else{
                usrSocPrf.getAtributosAuditoria().setUpdated(new Date());
            }
        }

        if (usrSocPrf != null) {
            usrSocPrf.setMail(perfilJson.getMailPerfil());
            usrSocPrf.getAtributosAuditoria().setEstadoRegistro(usuarioJson.getEstado());
            usrSocPrf.getAtributosAuditoria().setFechaPrevistaActivacion(usuarioJson.getFechaPrevistaActivacion());
            usrSocPrf.getAtributosAuditoria().setFechaPrevistaDesactivacion(usuarioJson.getFechaPrevistaDesactivacion());
            usrSocPrf.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            usrSocPrf.getAtributosAuditoria().setUltimaAccion(metodo);
        }
/*
        if (!KApp.DELETE.getKApp().equals(metodo)) {
            metodo = KApp.DUMMY_METHOD.getKApp();
        }
*/
        usrSocPrf_Array[0] = metodo; 
        usrSocPrf_Array[1] = "usrSocPrf";
        usrSocPrf_Array[2] = usrSocPrf;
        usrSocPrf_Array[3] = usrSocPrf.getIdUsrSocPrf();

        reply.add(usrSocPrf_Array);

        return reply;
    }

}
