package com.centraldecompras.rest.fin.ser;

import com.centraldecompras.acceso.DatosAuditoriaAcceso;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.rest.fin.json.SuppListJson;
import com.centraldecompras.rest.fin.ser.interfaces.SociedadRestService;
import com.centraldecompras.rest.fin.ser.interfaces.SociedadRplRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SociedadRestServiceImpl implements SociedadRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SociedadRestServiceImpl.class.getName()); 

    @Autowired
    private SociedadService sociedadService;

    public Sociedad montaSociedad(SuppListJson suppListJson, HashMap<String, Object> parYaud) throws Exception {
        log.info("....................................................................(............CONTROL");
                
        Sociedad reply = null;

        if (suppListJson.getMethod().equals(KApp.POST.getKApp())) {
            reply = new Sociedad();
            reply.setIdSociedad(UUID.randomUUID().toString().replaceAll("-", ""));
        } else {
            reply = sociedadService.findSociedad(suppListJson.getId());
        }
        
        if (suppListJson.getMethod().equals(KApp.PUT.getKApp()) || suppListJson.getMethod().equals(KApp.POST.getKApp())) {
            reply.setTipoidentificador(suppListJson.getTipoIdentificador());
            reply.setNifnrf(suppListJson.getIdentificador());
            reply.setNombre(suppListJson.getNombre());
            
            reply.setAtributosAuditoria(new DatosAuditoriaAcceso());
            reply.getAtributosAuditoria().setCreated(new Date());
            reply.getAtributosAuditoria().setUpdated(new Date());
            reply.getAtributosAuditoria().setDeleted(null);
            reply.getAtributosAuditoria().setEstadoRegistro((String) parYaud.get("estado"));
            reply.getAtributosAuditoria().setFechaPrevistaActivacion((Integer) parYaud.get("fpAct"));
            reply.getAtributosAuditoria().setFechaPrevistaDesactivacion((Integer) parYaud.get("fpDes"));
            reply.getAtributosAuditoria().setUltimaAccion((String) parYaud.get("method"));
            reply.getAtributosAuditoria().setUsuarioUltimaAccion((String) parYaud.get("idUser"));
        }

        return reply;
    }

    public SuppListJson recuperaSociedad(Sociedad sociedad){
        log.info("................................................................................CONTROL");
                
        SuppListJson reply =  new SuppListJson();
        
        reply.setId(sociedad.getIdSociedad());
        reply.setEstado(sociedad.getAtributosAuditoria().getEstadoRegistro());
        reply.setFechaPrevistaActivacion(sociedad.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(sociedad.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setMethod(sociedad.getAtributosAuditoria().getUltimaAccion());
        reply.setNombre(sociedad.getNombre());
        reply.setIdentificador(sociedad.getNifnrf());
        reply.setTipoIdentificador(sociedad.getTipoidentificador());
        
        reply.setUsuarios(new ArrayList<>());
        reply.setContactos(new ArrayList<>());
        reply.setDirecciones(new ArrayList<>());
        
        return reply;
    }

    public SuppListJson recuperaSociedadDatosBasicos(Sociedad sociedad){
        log.info("....................................................................mbm............CONTROL");
                
         SuppListJson reply = new SuppListJson();

        reply.setId(sociedad.getIdSociedad());
        reply.setEstado(sociedad.getAtributosAuditoria().getEstadoRegistro());
        reply.setFechaPrevistaActivacion(sociedad.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(sociedad.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.setMethod(sociedad.getAtributosAuditoria().getUltimaAccion());
        reply.setNombre(sociedad.getNombre());
        reply.setIdentificador(sociedad.getNifnrf());
        reply.setTipoIdentificador(sociedad.getTipoidentificador());

        reply.setContactos(new ArrayList());
        reply.setCpyPrdArb(new ArrayList());
        reply.setCpyZonGeo(new ArrayList());
        reply.setDataImgCatalogo(new ArrayList());
        reply.setDataImgFotoUsu(new ArrayList());
        reply.setDataImgLogo(new ArrayList());
        reply.setDirecciones(new ArrayList());
        reply.setImagenes(new ArrayList());
        reply.setProductosAsignados(new ArrayList());
        reply.setUsuarios(new ArrayList());
        reply.setZonasAsignadas(new ArrayList());
        return reply;
    }
}
