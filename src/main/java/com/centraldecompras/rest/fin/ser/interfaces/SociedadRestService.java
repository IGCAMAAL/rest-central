package com.centraldecompras.rest.fin.ser.interfaces;


import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.rest.fin.json.SuppListJson;
import java.util.HashMap;

public interface SociedadRestService {

    Sociedad montaSociedad(SuppListJson suppListJson, HashMap<String, Object> parYaud) throws Exception;
    
    SuppListJson recuperaSociedad(Sociedad sociedad);
    
    SuppListJson recuperaSociedadDatosBasicos(Sociedad sociedad);
}
