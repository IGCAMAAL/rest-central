package com.centraldecompras.rest.fin.ser;

import com.centraldecompras.acceso.DatosAuditoriaAcceso;
import com.centraldecompras.acceso.Sector;
import com.centraldecompras.acceso.SectoresAutorizados;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.acceso.service.interfaces.SectorService;
import com.centraldecompras.acceso.service.interfaces.SectoresAutorizadosService;
import com.centraldecompras.rest.fin.json.PerfilJson;
import com.centraldecompras.rest.fin.json.PermisoJson;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import com.centraldecompras.rest.fin.ser.interfaces.SectoresAutorizadosRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SectoresAutorizadosRestServiceImpl implements SectoresAutorizadosRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SectoresAutorizadosRestServiceImpl.class.getName());

    @Autowired
    private SectorService sectorService;

    @Autowired
    private SectoresAutorizadosService sectoresAutorizadosService;

    public List<Object[]> montaSectoresAutorizados(UsrSocPrf usrSocPrf, PerfilJson perfilJson, String idSec, String idUser, String metodoPadre, UsuarioJson usuarioJson) throws Exception {
        log.info("................................................................................CONTROL");
                
        List<Object[]> reply = new ArrayList();
        Object[] sectorAutorizado_Array = new Object[4]; 
        SectoresAutorizados sectorAutorizado = null;

        if ((KApp.DELETE.getKApp().equals(metodoPadre))) {
            perfilJson.setMethod(KApp.DELETE.getKApp());
        }
        String metodo = perfilJson.getMethod();

        PermisoJson permisoJson = perfilJson.getPermiso();
        if (permisoJson == null) {
            Object[] valores = {"Permisos no informados"};
            String mensaje = MessageFormat.format("ERROR: Sociedad,usuario, perfil y sector:  {0}", valores);
            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
        }

        Sector sector = sectorService.findSector(idSec);
        if (sector == null) {
            Object[] valores = {idSec};
            String mensaje = MessageFormat.format("ERROR: Sector Id {0}, no econtrado. ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        if (KApp.POST.getKApp().equals(metodo)) {
            sectorAutorizado = new SectoresAutorizados();
            sectorAutorizado.setIdSectoresAutorizados(UUID.randomUUID().toString().replaceAll("-", ""));
            sectorAutorizado.setAtributosAuditoria(new DatosAuditoriaAcceso());
            sectorAutorizado.setPrsSocPrf(usrSocPrf);
            sectorAutorizado.setSector(sector);
            sectorAutorizado.getAtributosAuditoria().setCreated(new Date());
            sectorAutorizado.getAtributosAuditoria().setDeleted(null);
        } else {
            sectorAutorizado = sectoresAutorizadosService.findUsrSocBy_USP_Sid(usrSocPrf, sector);
            if (sectorAutorizado == null) {
                Object[] valores = {idSec, usrSocPrf.getIdUsrSocPrf()};
                String mensaje = MessageFormat.format("ERROR: Sector Id {0} y IdUsrSocPrf {1}, no estan relacionados. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            } else {
                sectorAutorizado.getAtributosAuditoria().setUpdated(new Date());
            }
        }

        if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
            sectorAutorizado.setCargaDirectaArticulos(permisoJson.getCargaDirectaArticulos());
            sectorAutorizado.setModPreciosBaja(permisoJson.getModPreciosBaja());
            sectorAutorizado.setModPreciosAlta(permisoJson.getModPreciosAlta());
            sectorAutorizado.setPorcentajeMaxBaja((new BigDecimal(permisoJson.getPorcentajeMaxBaja())).setScale(2, RoundingMode.HALF_EVEN));
            sectorAutorizado.setPorcentajeMaxAlta((new BigDecimal(permisoJson.getPorcentajeMaxAlta())).setScale(2, RoundingMode.HALF_EVEN));

            sectorAutorizado.getAtributosAuditoria().setEstadoRegistro(usuarioJson.getEstado());
            sectorAutorizado.getAtributosAuditoria().setFechaPrevistaActivacion(usuarioJson.getFechaPrevistaActivacion());
            sectorAutorizado.getAtributosAuditoria().setFechaPrevistaDesactivacion(usuarioJson.getFechaPrevistaDesactivacion());
            sectorAutorizado.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            sectorAutorizado.getAtributosAuditoria().setUltimaAccion(metodo);
        }

        sectorAutorizado_Array[0] = metodo;
        sectorAutorizado_Array[1] = "sectorAutorizado";
        sectorAutorizado_Array[2] = sectorAutorizado;
        sectorAutorizado_Array[3] = sectorAutorizado.getIdSectoresAutorizados();

        reply.add(sectorAutorizado_Array);

        return reply;

    }

}
