package com.centraldecompras.rest.fin.ser;

import com.centraldecompras.acceso.DatosAuditoriaAcceso;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.service.interfaces.UsrSocService;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import com.centraldecompras.rest.fin.ser.interfaces.UsrSocRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsrSocRestServiceImpl implements UsrSocRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UsrSocRestServiceImpl.class.getName());
 
    @Autowired
    private UsrSocService usrSocService;

    public List<Object[]> montaUsrSoc(UserCentral persona, Sociedad sociedad, String idUser, String metodoPadre, UsuarioJson usuarioJson) throws Exception {
        log.info("....................................................................mbm............CONTROL");
                
        List<Object[]> reply = new ArrayList();
        Object[] usrSoc_Array = new Object[4]; 
        UsrSoc usrSoc = null;

        if ((KApp.DELETE.getKApp().equals(metodoPadre))) {
            usuarioJson.setMethod(KApp.DELETE.getKApp());
        }
        String metodo = usuarioJson.getMethod();

        if (KApp.POST.getKApp().equals(metodo)) {
            usrSoc = new UsrSoc();
            usrSoc.setIdUsrSoc(UUID.randomUUID().toString().replaceAll("-", ""));
            usrSoc.setUserCentral(persona);
            usrSoc.setSociedad(sociedad);
            usrSoc.setAtributosAuditoria(new DatosAuditoriaAcceso());
            usrSoc.getAtributosAuditoria().setCreated(new Date());
            usrSoc.getAtributosAuditoria().setDeleted(null);
        } else {
            usrSoc = usrSocService.findPrsSocByUS(persona, sociedad);
            if (usrSoc == null) {
                Object[] valores = {persona.getId(), sociedad.getIdSociedad()};
                String mensaje = MessageFormat.format("ERROR: PersonaId {0} no relacionado con Sociedad {1}. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            } else {
                usrSoc.getAtributosAuditoria().setUpdated(new Date());
            }
        }

        if (usrSoc != null) {
            usrSoc.getAtributosAuditoria().setEstadoRegistro(usuarioJson.getEstado());
            usrSoc.getAtributosAuditoria().setFechaPrevistaActivacion(usuarioJson.getFechaPrevistaActivacion());
            usrSoc.getAtributosAuditoria().setFechaPrevistaDesactivacion(usuarioJson.getFechaPrevistaDesactivacion());
            usrSoc.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            usrSoc.getAtributosAuditoria().setUltimaAccion(metodo);
        }
        /*
         if (!KApp.DELETE.getKApp().equals(metodo)) {
         metodo = KApp.PUT.getKApp();
         }
         */
        usrSoc_Array[0] = metodo;
        usrSoc_Array[1] = "usrSoc";
        usrSoc_Array[2] = usrSoc;
        usrSoc_Array[3] = usrSoc.getIdUsrSoc();

        reply.add(usrSoc_Array);

        return reply;
    }

}
