package com.centraldecompras.rest.fin.ser.interfaces;


import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.rest.fin.json.PerfilJson;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import java.util.List;

public interface UsrSocPrfRestService {

    List<Object[]> montaUsrSocPrf(UsrSoc prsSoc, Perfil perfil, PerfilJson perfilJson, String idUser, String metodoPadre, UsuarioJson usuarioJson) throws Exception;
}
