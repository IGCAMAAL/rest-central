package com.centraldecompras.rest.fin.ser.interfaces;


import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import java.util.HashMap;
import java.util.List;

public interface UsrSocRestService {

    List<Object[]> montaUsrSoc(UserCentral persona, Sociedad sociedad, String idUser, String metodoPadre, UsuarioJson usuarioJson) throws Exception;
}
