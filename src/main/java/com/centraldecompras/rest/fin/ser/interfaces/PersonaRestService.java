package com.centraldecompras.rest.fin.ser.interfaces;


import com.centraldecompras.rest.fin.json.UsuarioJson;
import java.util.HashMap;
import java.util.List;

public interface PersonaRestService {

    List<Object[]> montaPersona(UsuarioJson usuarioJson, String IdNewUsr, String idUser, String metodoPadre) throws Exception;
}
