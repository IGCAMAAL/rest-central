package com.centraldecompras.rest.fin.ser.interfaces;


import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import java.util.List;

public interface UserCentralRestService {

    List<Object[]> montaUserCentral(UsuarioJson usuarioJson, String metodoPadre) throws Exception;
    
    List<UsuarioJson> recuperaUsersCentral(Sociedad sociedad, String usuarioId, String idSec);
    
    void persistencia(List<Object> createObjects, List<Object> updateObjects ) throws Exception ;
}
