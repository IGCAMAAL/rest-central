package com.centraldecompras.rest.fin.ser;

import com.centraldecompras.acceso.SectoresAutorizados;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.acceso.service.interfaces.SectoresAutorizadosService;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import com.centraldecompras.acceso.service.interfaces.UsrSocPrfService;
import com.centraldecompras.acceso.service.interfaces.UsrSocService;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.DireccionGoogleComponente;
import com.centraldecompras.modelo.DireccionGoogleType;
import com.centraldecompras.modelo.ImgDoc;
import com.centraldecompras.modelo.ImgFoto;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.cmo.service.interfaces.ContactoService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleComponenteService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleTypeService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionService;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgDocService;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgFotoService;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoService;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoService;
import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.rest.fin.SuppController;
import com.centraldecompras.rest.fin.ser.interfaces.SuppRestService;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SuppRestServiceImpl implements SuppRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SuppRestServiceImpl.class.getName());


    @Autowired
    private SociedadService sociedadService;
    @Autowired
    private SociedadRplService sociedadRplService;

    @Autowired
    private UserCentralService userCentralService;
    @Autowired
    private PersonaService personaService;

    @Autowired
    private UsrSocService usrSocService;
    @Autowired
    private UsrSocPrfService usrSocPrfService;
    @Autowired
    private SectoresAutorizadosService sectoresAutorizadosService;

    @Autowired
    private ContactoService contactoService;

    @Autowired
    private DireccionService direccionService;
    @Autowired
    private DireccionGoogleComponenteService direccionGoogleComponenteService;
    @Autowired
    private DireccionGoogleTypeService direccionGoogleTypeService;

    @Autowired
    private SocZonGeoService socZonGeoService;
    @Autowired
    private SocProductoService socProductoService;
    @Autowired
    private ImgDocService imgDocService;

    @Autowired
    private ImgFotoService imgFotoService;

    @Transactional
    @Override
    public void persistencia(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");
                
        for (Object[] bbb : listaObjetos) {

            if (("POST").equals((String) bbb[0])) {
                if (("sociedad").equals((String) bbb[1])) {
                    sociedadService.create((Sociedad) bbb[2]);
                }
                if (("sociedadRpl").equals((String) bbb[1])) {
                    sociedadRplService.create((SociedadRpl) bbb[2]);
                }
                if (("userCentral").equals((String) bbb[1])) {
                    userCentralService.create((UserCentral) bbb[2]);
                }
                if (("persona").equals((String) bbb[1])) {
                    personaService.create((Persona) bbb[2]);
                }
                if (("usrSoc").equals((String) bbb[1])) {
                    usrSocService.create((UsrSoc) bbb[2]);
                }
                if (("usrSocPrf").equals((String) bbb[1])) {
                    usrSocPrfService.create((UsrSocPrf) bbb[2]);
                }
                if (("sectorAutorizado").equals((String) bbb[1])) {
                    sectoresAutorizadosService.create((SectoresAutorizados) bbb[2]);
                }
                if (("direccion").equals((String) bbb[1])) {
                    direccionService.create((Direccion) bbb[2]);
                }
                if (("direccionGoogleComponente").equals((String) bbb[1])) {
                    direccionGoogleComponenteService.create((DireccionGoogleComponente) bbb[2]);
                }

                if (("direccionGoogleType").equals((String) bbb[1])) {
                    direccionGoogleTypeService.create((DireccionGoogleType) bbb[2]);
                }

                if (("contacto").equals((String) bbb[1])) {
                    contactoService.create((Contacto) bbb[2]);
                }

                if (("socZonGeo").equals((String) bbb[1])) {
                    socZonGeoService.create((SocZonGeo) bbb[2]);
                }
                if (("socProducto").equals((String) bbb[1])) {
                    socProductoService.create((SocProducto) bbb[2]);
                }
                if (("imgDoc").equals((String) bbb[1])) {
                    if (("F").equals((String) bbb[4])) {
                        ImgFoto imgFoto = new ImgFoto((ImgDoc) bbb[2]);
                        imgFotoService.create(imgFoto);
                    } else {
                        imgDocService.create((ImgDoc) bbb[2]);
                    }
                }
            }

            if (("PUT").equals((String) bbb[0])) {
                if (("sociedad").equals((String) bbb[1])) {
                    sociedadService.edit((Sociedad) bbb[2]);
                }
                if (("sociedadRpl").equals((String) bbb[1])) {
                    sociedadRplService.edit((SociedadRpl) bbb[2]);
                }
                if (("userCentral").equals((String) bbb[1])) {
                    userCentralService.edit((UserCentral) bbb[2]);
                }
                if (("persona").equals((String) bbb[1])) {
                    personaService.edit((Persona) bbb[2]);
                }
                if (("usrSoc").equals((String) bbb[1])) {
                    usrSocService.edit((UsrSoc) bbb[2]);
                }
                if (("usrSocPrf").equals((String) bbb[1])) {
                    usrSocPrfService.edit((UsrSocPrf) bbb[2]);
                }
                if (("sectorAutorizado").equals((String) bbb[1])) {
                    sectoresAutorizadosService.edit((SectoresAutorizados) bbb[2]);
                }

                if (("direccion").equals((String) bbb[1])) {
                    Direccion dir = (Direccion) bbb[2];
                    direccionGoogleTypeService.destroyDireccionesGoogleTypeBy_DDC_B(dir.getIdDireccion(), null);
                    direccionService.edit((Direccion) bbb[2]);
                }
                if (("direccionGoogleComponente").equals((String) bbb[1])) {
                    DireccionGoogleComponente comp = (DireccionGoogleComponente) bbb[2];
                    direccionGoogleTypeService.destroyDireccionesGoogleTypeBy_DDC_B(null, comp.getIdDireccionGoogleComponente());
                    direccionGoogleComponenteService.edit((DireccionGoogleComponente) bbb[2]);
                }

                if (("contacto").equals((String) bbb[1])) {
                    contactoService.edit((Contacto) bbb[2]);
                }

                if (("socZonGeo").equals((String) bbb[1])) {
                    socZonGeoService.edit((SocZonGeo) bbb[2]);
                }
                if (("socProducto").equals((String) bbb[1])) {
                    socProductoService.edit((SocProducto) bbb[2]);
                }
                if (("imgDoc").equals((String) bbb[1])) {
                    if (("F").equals((String) bbb[4])) {
                        ImgFoto imgFoto = new ImgFoto((ImgDoc) bbb[2]);
                        imgFotoService.edit(imgFoto);
                    } else {
                        imgDocService.edit((ImgDoc) bbb[2]);
                    }
                }
            }
        }

        Collections.reverse(listaObjetos);
        for (Object[] bbb : listaObjetos) {
            if (("DELETE").equals((String) bbb[0])) {
                if (("sociedad").equals((String) bbb[1])) {
                    log.info("..........................................sociedad-" + (String) bbb[3]);
                    sociedadService.destroy((String) bbb[3]);
                }
                if (("sociedadRpl").equals((String) bbb[1])) {
                    log.info("..........................................sociedadRpl-" + (String) bbb[3]);
                    sociedadRplService.destroy((String) bbb[3]);
                }
                if (("userCentral").equals((String) bbb[1])) {
                    log.info("..........................................userCentral-" + (String) bbb[3]);
                    userCentralService.destroy((String) bbb[3]);
                }
                if (("persona").equals((String) bbb[1])) {
                    log.info("..........................................persona-" + (String) bbb[3]);
                    personaService.destroy((String) bbb[3]);
                }
                if (("usrSoc").equals((String) bbb[1])) {
                    log.info("..........................................usrSoc-" + (String) bbb[3]);
                    usrSocService.destroy((String) bbb[3]);
                }
                if (("usrSocPrf").equals((String) bbb[1])) {
                    log.info("..........................................usrSocPrf-" + (String) bbb[3]);
                    usrSocPrfService.destroy((String) bbb[3]);
                }
                if (("sectorAutorizado").equals((String) bbb[1])) {
                    log.info("..........................................sectorAutorizado-" + (String) bbb[3]);
                    sectoresAutorizadosService.destroy((String) bbb[3]);
                }

                if (("direccion").equals((String) bbb[1])) {
                    log.info("..........................................direccion-" + (String) bbb[3]);
                    direccionGoogleTypeService.destroyDireccionesGoogleTypeBy_DDC_B((String) bbb[3], null);
                    direccionService.destroy((String) bbb[3]);
                }
                if (("direccionGoogleComponente").equals((String) bbb[1])) {
                    log.info("..........................................direccionGoogleComponente-" + (String) bbb[3]);
                    direccionGoogleTypeService.destroyDireccionesGoogleTypeBy_DDC_B(null, (String) bbb[3]);
                    direccionGoogleComponenteService.destroy((String) bbb[3]);
                }

                if (("contacto").equals((String) bbb[1])) {
                    log.info("..........................................contacto-" + (String) bbb[3]);
                    contactoService.destroy((String) bbb[3]);
                }

                if (("socZonGeo").equals((String) bbb[1])) {
                    log.info("..........................................socZonGeo-" + (String) bbb[3]);
                    socZonGeoService.destroy((String) bbb[3]);
                }

                if (("socProducto").equals((String) bbb[1])) {
                    log.info("..........................................socProducto-" + (String) bbb[3]);
                    socProductoService.destroy((String) bbb[3]);
                }

                if (("imgDoc").equals((String) bbb[1])) {
                    log.info("..........................................imgDoc-" + (String) bbb[3]);
                    if (("F").equals((String) bbb[4])) {
                        imgFotoService.destroy((String) bbb[3]);
                    } else {
                        imgDocService.destroy((String) bbb[3]);
                    }
                }
            }
        }
    }

}
