package com.centraldecompras.rest.fin.ser;

import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import com.centraldecompras.rest.fin.ser.interfaces.PersonaRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaRestServiceImpl implements PersonaRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PersonaRestServiceImpl.class.getName());
 
    @Autowired
    private PersonaService personaService;

    public List<Object[]> montaPersona(UsuarioJson usuarioJson, String IdNewUsr, String idUser, String metodoPadre) throws Exception {
        log.info("....................................................................(............CONTROL");
        
        List<Object[]> reply = new ArrayList();
        Object[] persona_Array = new Object[4]; 
        Persona persona = null;

        if ((KApp.DELETE.getKApp().equals(metodoPadre))) {
            usuarioJson.setMethod(KApp.DELETE.getKApp());
        }
        String metodo = usuarioJson.getMethod();
        
        if (KApp.POST.getKApp().equals(metodo)) {
            persona = new Persona();
            persona.setIdPersona(IdNewUsr);
            persona.setAtributosAuditoria(new DatosAuditoria());   
            persona.getAtributosAuditoria().setCreated(new Date());
            persona.getAtributosAuditoria().setDeleted(null);
        } else {
            persona = personaService.findPersona(usuarioJson.getId());
            if (persona == null) {
                Object[] valores = {usuarioJson.getUsuario()};
                String mensaje = MessageFormat.format("ERROR: usuario {0} no encontrado. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            }
            persona.getAtributosAuditoria().setUpdated(new Date());
        }

        if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
            persona.setUserName(usuarioJson.getUsuario());
            persona.setNombre("");
            persona.setApellido1("");
            persona.setApellido2("");
            persona.setCifpasport("");
            persona.setFechaNacimiento(0);
            persona.setEmail(usuarioJson.getMail());

            persona.getAtributosAuditoria().setEstadoRegistro(usuarioJson.getEstado());
            persona.getAtributosAuditoria().setFechaPrevistaActivacion(usuarioJson.getFechaPrevistaActivacion());
            persona.getAtributosAuditoria().setFechaPrevistaDesactivacion(usuarioJson.getFechaPrevistaDesactivacion());
            persona.getAtributosAuditoria().setUltimaAccion(usuarioJson.getMethod());
            persona.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
        }

        persona_Array[0] = metodo;
        persona_Array[1] = "persona";
        persona_Array[2] = persona;
        persona_Array[3] = persona.getIdPersona();
        reply.add(persona_Array);

        return reply;

    }
}
