package com.centraldecompras.rest.fin.ser.interfaces;


import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.rest.fin.json.PerfilJson;
import com.centraldecompras.rest.fin.json.PermisoJson;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import java.util.HashMap;
import java.util.List;

public interface SectoresAutorizadosRestService {

    List<Object[]> montaSectoresAutorizados(UsrSocPrf usrSocPrf, PerfilJson perfilJson, String idSec, String idUser, String metodoPadre, UsuarioJson usuarioJson) throws Exception ;
}
