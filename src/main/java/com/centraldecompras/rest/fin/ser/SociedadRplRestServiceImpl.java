package com.centraldecompras.rest.fin.ser;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.rest.fin.ser.interfaces.SociedadRplRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SociedadRplRestServiceImpl implements SociedadRplRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SociedadRplRestServiceImpl.class.getName()); 

    @Autowired
    private SociedadRplService sociedadRplService;

    public List<Object[]> montaSociedadRpl(Sociedad sociedad, String metodo) throws Exception {
        log.info("....................................................................mbm............CONTROL");
                
        List<Object[]> reply = new ArrayList();
        Object[] sociedadRpl_Array = new Object[4];
        SociedadRpl sociedadRpl = null;

        if (KApp.POST.getKApp().equals(metodo)) {
            sociedadRpl = new SociedadRpl();
            sociedadRpl.setIdSociedad(sociedad.getIdSociedad());
        } else {
            sociedadRpl = sociedadRplService.findSociedadRpl(sociedad.getIdSociedad());
            if (sociedadRpl == null) {
                Object[] valores = {sociedad.getNombre()};
                String mensaje = MessageFormat.format("ERROR: Sociedad {0} no encontada en sociedadRpl. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            } 
        }

        sociedadRpl_Array[0] = metodo;
        sociedadRpl_Array[1] = "sociedadRpl";
        sociedadRpl_Array[2] = sociedadRpl;
        sociedadRpl_Array[3] = sociedadRpl.getIdSociedad();
        reply.add(sociedadRpl_Array);

        return reply; 

    }
}
