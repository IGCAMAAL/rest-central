package com.centraldecompras.rest.fin.ser;

import com.centraldecompras.acceso.DatosAuditoriaAcceso;
import com.centraldecompras.acceso.Sector;
import com.centraldecompras.acceso.SectoresAutorizados;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.acceso.service.interfaces.SectorService;
import com.centraldecompras.acceso.service.interfaces.SectoresAutorizadosService;
import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import com.centraldecompras.acceso.service.interfaces.UsrSocPrfService;
import com.centraldecompras.acceso.service.interfaces.UsrSocService;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.UnidadMedidaLang;
import com.centraldecompras.rest.fin.json.PerfilJson;
import com.centraldecompras.rest.fin.json.PermisoJson;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import com.centraldecompras.rest.fin.ser.interfaces.UserCentralRestService;
import com.centraldecompras.zglobal.PasswordGenerator;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserCentralRestServiceImpl implements UserCentralRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UserCentralRestServiceImpl.class.getName()); 

    @Autowired
    private UserCentralService userCentralService;

    @Autowired
    private UsrSocService usrSocService;

    @Autowired
    private UsrSocPrfService usrSocPrfService;

    @Autowired
    SectoresAutorizadosService sectoresAutorizadosService;

    @Autowired
    private SectorService sectorService;

    public List<Object[]> montaUserCentral(UsuarioJson usuarioJson, String metodoPadre) throws Exception {
        log.info("....................................................................mbm............CONTROL");
                
        List<Object[]> reply = new ArrayList();
        Object[] userCentral_Array = new Object[4];
        UserCentral userCentral = null;
        String password = null;

        if ((KApp.DELETE.getKApp().equals(metodoPadre))) {
            usuarioJson.setMethod(KApp.DELETE.getKApp());
        }
        String metodo = usuarioJson.getMethod();

        if (KApp.POST.getKApp().equals(metodo)) {
            userCentral = new UserCentral();
            userCentral.setId(UUID.randomUUID().toString().replaceAll("-", ""));
            password = PasswordGenerator.getPassword(PasswordGenerator.NUMEROS + PasswordGenerator.MAYUSCULAS + PasswordGenerator.MINUSCULAS, 10);
            userCentral.setMd5password(password);
            userCentral.setAtributosAuditoria(new DatosAuditoriaAcceso());
            userCentral.getAtributosAuditoria().setCreated(new Date());
            userCentral.getAtributosAuditoria().setDeleted(null);
        } else {
            userCentral = userCentralService.findUserCentral(usuarioJson.getId());
            if (userCentral == null) {
                Object[] valores = {usuarioJson.getUsuario()};
                String mensaje = MessageFormat.format("ERROR: usuario {0} no encontrado. ", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            } else {
                userCentral.getAtributosAuditoria().setUpdated(new Date());
            }

        }

        if (KApp.POST.getKApp().equals(metodo) || KApp.PUT.getKApp().equals(metodo)) {
            userCentral.setUsername(usuarioJson.getUsuario());
            userCentral.setEmail(usuarioJson.getMail());
            userCentral.setPerfilPublico(usuarioJson.getPerfilPublico());
            userCentral.getAtributosAuditoria().setEstadoRegistro(usuarioJson.getEstado());
            userCentral.getAtributosAuditoria().setFechaPrevistaActivacion(usuarioJson.getFechaPrevistaActivacion());
            userCentral.getAtributosAuditoria().setFechaPrevistaDesactivacion(usuarioJson.getFechaPrevistaDesactivacion());
            userCentral.getAtributosAuditoria().setUltimaAccion(usuarioJson.getMethod());
            userCentral.getAtributosAuditoria().setUsuarioUltimaAccion(usuarioJson.getUsuario());
        }

        userCentral_Array[0] = metodo;
        userCentral_Array[1] = "userCentral";
        userCentral_Array[2] = userCentral; 
        userCentral_Array[3] = userCentral.getId();
        reply.add(userCentral_Array);

        return reply;

    }

    public List<UsuarioJson> recuperaUsersCentral(Sociedad sociedad, String usuarioId, String idSec) {
        log.info("....................................................................mbm............CONTROL");
                
        List<UsuarioJson> usuariosJson = new ArrayList<UsuarioJson>();

        if (usuarioId != null) {
            UserCentral userCentral = userCentralService.findUserCentral(usuarioId);
            UsrSoc usrSoc = usrSocService.findPrsSocByUS(userCentral, sociedad);
            UsuarioJson usuarioJson = toJson(userCentral, usrSoc, idSec);
            usuariosJson.add(usuarioJson);
        } else {
            List<UsrSoc> usrSocs = usrSocService.findPrsSocByS(sociedad);
            for (UsrSoc usrSoc : usrSocs) {
                UserCentral userCentral = userCentralService.findUserCentral(usrSoc.getUserCentral().getId());
                UsuarioJson usuarioJson = toJson(userCentral, usrSoc, idSec);
                usuariosJson.add(usuarioJson);
            }
        }

        return usuariosJson;
    }

    private UsuarioJson toJson(UserCentral userCentral, UsrSoc usrSoc, String idSec) {
        log.info("....................................................................mbm............CONTROL");
                
        UsuarioJson reply = new UsuarioJson();
        Sector sector = sectorService.findSector(idSec);

        reply.setUsuario(userCentral.getUsername());
        reply.setMail(userCentral.getEmail());
        reply.setPerfilPublico(userCentral.getPerfilPublico());
        reply.setId(userCentral.getId());
        reply.setMethod(userCentral.getAtributosAuditoria().getUltimaAccion());
        reply.setEstado(userCentral.getAtributosAuditoria().getEstadoRegistro());
        reply.setFechaPrevistaActivacion(userCentral.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.setFechaPrevistaDesactivacion(userCentral.getAtributosAuditoria().getFechaPrevistaDesactivacion());

        if (usrSoc != null) {
            List<PerfilJson> perfiles = new ArrayList();

            List<UsrSocPrf> usrSocPrfs = usrSocPrfService.findByUsrSoc(usrSoc);
            for (UsrSocPrf usrSocPrf : usrSocPrfs) {
                PerfilJson perfilJson = new PerfilJson();
                SectoresAutorizados sectorAutorizado = sectoresAutorizadosService.findUsrSocBy_USP_Sid(usrSocPrf, sector);
                perfilJson.setId(usrSocPrf.getIdUsrSocPrf());
                perfilJson.setMailPerfil(usrSocPrf.getMail());
                perfilJson.setMethod(usrSocPrf.getAtributosAuditoria().getUltimaAccion());
                perfilJson.setPerfilCod(usrSocPrf.getPerfil().getCodPerfil());

                perfilJson.setPermiso(new PermisoJson());
                perfilJson.getPermiso().setId(sectorAutorizado.getIdSectoresAutorizados());
                perfilJson.getPermiso().setCargaDirectaArticulos(sectorAutorizado.getCargaDirectaArticulos());
                perfilJson.getPermiso().setModPreciosAlta(sectorAutorizado.getModPreciosAlta());
                perfilJson.getPermiso().setModPreciosBaja(sectorAutorizado.getModPreciosBaja());
                perfilJson.getPermiso().setPorcentajeMaxAlta(sectorAutorizado.getPorcentajeMaxAlta().doubleValue());
                perfilJson.getPermiso().setPorcentajeMaxBaja(sectorAutorizado.getPorcentajeMaxBaja().doubleValue());

                perfiles.add(perfilJson);
            }
            reply.setPerfiles(perfiles);
        } else {
            reply.setPerfiles(new ArrayList<>());
        }
        return reply;
    }

    @Transactional
    public void persistencia(List<Object> createObjects, List<Object> updateObjects ) throws Exception {
        log.info("....................................................................mbm............CONTROL");
                
        for(Object createObject: createObjects){
            if(createObject instanceof UserCentral){
                userCentralService.create((UserCentral)createObject);
            }
        }
        
        for(Object updateObject: updateObjects){
            if(updateObject instanceof UserCentral){
                userCentralService.edit((UserCentral)updateObject);
            }
        }

    }

}
