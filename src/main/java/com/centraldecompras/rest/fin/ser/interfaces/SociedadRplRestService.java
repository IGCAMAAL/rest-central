package com.centraldecompras.rest.fin.ser.interfaces;


import com.centraldecompras.acceso.Sociedad;
import java.util.List;

public interface SociedadRplRestService {

    List<Object[]> montaSociedadRpl(Sociedad sociedad, String metodo) throws Exception;
}
