package com.centraldecompras.rest.fin.json;

import java.math.BigDecimal;

public class PerfilJson {

    private String id;
    private String perfilCod;
    private String method;
    private String mailPerfil;
    private PermisoJson permiso;

    public PerfilJson() {
    }

    public PerfilJson(String id, String perfilCod, String method, String mailPerfil, PermisoJson permiso) {
        this.id = id;
        this.perfilCod = perfilCod; 
        this.method = method;
        this.mailPerfil = mailPerfil;
        this.permiso = permiso;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPerfilCod() {
        return perfilCod;
    }

    public void setPerfilCod(String perfilCod) {
        this.perfilCod = perfilCod;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMailPerfil() {
        return mailPerfil;
    }

    public void setMailPerfil(String mailPerfil) {
        this.mailPerfil = mailPerfil;
    }

    public PermisoJson getPermiso() {
        return permiso;
    }

    public void setPermiso(PermisoJson permiso) {
        this.permiso = permiso;
    }


}
