package com.centraldecompras.rest.fin.json;

import com.centraldecompras.rest.fin.json.PermisoJson;
import java.util.List;

public class UsuarioJson {

    private String id;
    private String method;
    private String estado;
    private int fechaPrevistaActivacion;
    private int fechaPrevistaDesactivacion; 
    
    private String mail;
    private String usuario;
    private Boolean perfilPublico;
    private List<PerfilJson> perfiles;    
    
    public UsuarioJson() {
    }

    public UsuarioJson(String id, String method, String estado, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion, String mail, String usuario, Boolean perfilPublico, List<PerfilJson> perfiles) {
        this.id = id;
        this.method = method;
        this.estado = estado;
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
        this.mail = mail;
        this.usuario = usuario;
        this.perfilPublico = perfilPublico;
        this.perfiles = perfiles;
    }
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }
    
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

 
    public Boolean getPerfilPublico() {
        return perfilPublico;
    }

    public void setPerfilPublico(Boolean perfilPublico) {
        this.perfilPublico = perfilPublico;
    }

    public List<PerfilJson> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<PerfilJson> perfiles) {
        this.perfiles = perfiles;
    }

  
}
