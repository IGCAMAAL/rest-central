package com.centraldecompras.rest.fin.json.auxi;

import com.centraldecompras.modelo.SociedadRpl;
import java.util.Collection;
import java.util.Objects;
import java.util.logging.Logger;

public class SociedadRplJsonOut {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SociedadRplJsonOut.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    private SociedadRpl sociedadRpl;
    private Collection<DireccionJsonOut> direccionJsonOut;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public SociedadRplJsonOut() {

    }

    public SociedadRplJsonOut(SociedadRpl sociedadRpl, Collection<DireccionJsonOut> direccionJsonOut) {
        this.sociedadRpl = sociedadRpl;
        this.direccionJsonOut = direccionJsonOut;
    }



    public SociedadRpl getSociedadRpl() {
        return sociedadRpl;
    }

    public void setSociedadRpl(SociedadRpl sociedadRpl) {
        this.sociedadRpl = sociedadRpl;
    }

    public Collection<DireccionJsonOut> getDireccionJsonOut() {
        return direccionJsonOut;
    }

    public void setDireccionJsonOut(Collection<DireccionJsonOut> direccionJsonOut) {
        this.direccionJsonOut = direccionJsonOut;
    }



    // Métodos Basicos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.sociedadRpl);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SociedadRplJsonOut other = (SociedadRplJsonOut) obj;
        if (!Objects.equals(this.sociedadRpl, other.sociedadRpl)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SociedadRplJsonOut{" + "sociedadRpl=" + sociedadRpl + ", direccionJsonOut=" + direccionJsonOut + '}';
    }



}
