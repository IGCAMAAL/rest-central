package com.centraldecompras.rest.fin.json;


public class ContactoJson {

    String id;
    String method;
    String estado;
   
    String tipo;
    String valor;
    String descripcion;

    String nombre;
    String cargo;
    Boolean visibleProve;
    Boolean visibleAsoc;
                            
    public ContactoJson() {
    }

    public ContactoJson(String id, String method, String estado, String tipo, String valor, String descripcion, String nombre, String cargo, Boolean visibleProve, Boolean visibleAsoc) {
        this.id = id;
        this.method = method;
        this.estado = estado;
        this.tipo = tipo;
        this.valor = valor;
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.cargo = cargo;
        this.visibleProve = visibleProve;
        this.visibleAsoc = visibleAsoc;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getVisibleProve() {
        return visibleProve;
    }

    public void setVisibleProve(Boolean visibleProve) {
        this.visibleProve = visibleProve;
    }

    public Boolean getVisibleAsoc() {
        return visibleAsoc;
    }

    public void setVisibleAsoc(Boolean visibleAsoc) {
        this.visibleAsoc = visibleAsoc;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

}
