package com.centraldecompras.rest.fin.json;

import com.centraldecompras.rest.fin.json.auxi.AuditImgJson;
import com.centraldecompras.rest.lgt.json.ZonaProductoAsigJson;
import java.util.List;

public class SuppListJson {
    
    String id;
    String method;
    String estado;
    int fechaPrevistaActivacion;
    int fechaPrevistaDesactivacion;   
    
    String tipoIdentificador;
    String identificador;
    String nombre;
    
    List<UsuarioJson> usuarios;
    List<ContactoJson> contactos;
    List<DireccionJson> direcciones;
    
    // ['Soc origen', 'FAct','FDesc'],
    List<String> cpyPrdArb;
    List<String> cpyZonGeo; 
    
    // Datos de las imagenes enviadas
    List<AuditImgJson> dataImgLogo;
    List<AuditImgJson> dataImgFotoUsu;
    List<AuditImgJson> dataImgCatalogo;
    
    // String [] contiene 5 eleentos: 
    // - 1er: Tipo imagen (LG(Logo), FU(Fotografia usuario), CT(Catalogo))
    // - 2do: Id identificativo del registro de la imagen en la tabla. Es el que se usarà para cargar posteriormente la imagen
    // - 3ro: Nombre original del fichero
    // - 4to: Descripcion infomrada por el usuario
    // - 5to: Id de la entidad propietaria. (Para LG, el id es el de sociedad, para FU el id es el de usuario, para CG es el de sociedad)
    List<String[]> imagenes;                    
    
    List<ZonaProductoAsigJson> zonasAsignadas;
    List<ZonaProductoAsigJson> productosAsignados;
    
  
    
    public SuppListJson() {
    }

    public SuppListJson(List<UsuarioJson> usuarios, List<ContactoJson> contactos, List<DireccionJson> direcciones) {
        this.usuarios = usuarios;
        this.contactos = contactos;
        this.direcciones = direcciones;
    }

    public SuppListJson(String id, String method, String estado, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion, String tipoIdentificador, String identificador, String nombre, List<String> cpyPrdArb, List<String> cpyZonGeo, List<UsuarioJson> usuarios, List<ContactoJson> contactos, List<DireccionJson> direcciones) {
        this.id = id;
        this.method = method;
        this.estado = estado;
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
        this.tipoIdentificador = tipoIdentificador;
        this.identificador = identificador;
        this.nombre = nombre;
        this.cpyPrdArb = cpyPrdArb;
        this.cpyZonGeo = cpyZonGeo;
        this.usuarios = usuarios;
        this.contactos = contactos;
        this.direcciones = direcciones;
    }
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public String getTipoIdentificador() {
        return tipoIdentificador;
    }

    public void setTipoIdentificador(String tipoIdentificador) {
        this.tipoIdentificador = tipoIdentificador;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<UsuarioJson> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuarioJson> usuarios) {
        this.usuarios = usuarios;
    }

    public List<DireccionJson> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(List<DireccionJson> direcciones) {
        this.direcciones = direcciones;
    }

    public List<ContactoJson> getContactos() {
        return contactos;
    }

    public void setContactos(List<ContactoJson> contactos) {
        this.contactos = contactos;
    }

    public List<String> getCpyPrdArb() {
        return cpyPrdArb;
    }

    public void setCpyPrdArb(List<String> cpyPrdArb) {
        this.cpyPrdArb = cpyPrdArb;
    }

    public List<String> getCpyZonGeo() {
        return cpyZonGeo;
    }

    public void setCpyZonGeo(List<String> cpyZonGeo) {
        this.cpyZonGeo = cpyZonGeo;
    }

    
    public List<String[]> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<String[]> imagenes) {
        this.imagenes = imagenes;
    }

    public List<ZonaProductoAsigJson> getZonasAsignadas() {
        return zonasAsignadas;
    }

    public void setZonasAsignadas(List<ZonaProductoAsigJson> zonasAsignadas) {
        this.zonasAsignadas = zonasAsignadas;
    }

    public List<ZonaProductoAsigJson> getProductosAsignados() {
        return productosAsignados;
    }

    public void setProductosAsignados(List<ZonaProductoAsigJson> productosAsignados) {
        this.productosAsignados = productosAsignados;
    }

    public List<AuditImgJson> getDataImgLogo() {
        return dataImgLogo;
    }

    public void setDataImgLogo(List<AuditImgJson> dataImgLogo) {
        this.dataImgLogo = dataImgLogo;
    }

    public List<AuditImgJson> getDataImgFotoUsu() {
        return dataImgFotoUsu;
    }

    public void setDataImgFotoUsu(List<AuditImgJson> dataImgFotoUsu) {
        this.dataImgFotoUsu = dataImgFotoUsu;
    }

    public List<AuditImgJson> getDataImgCatalogo() {
        return dataImgCatalogo;
    }

    public void setDataImgCatalogo(List<AuditImgJson> dataImgCatalogo) {
        this.dataImgCatalogo = dataImgCatalogo;
    }

    
}
