package com.centraldecompras.rest.fin.json.auxi;

import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.DireccionGoogleComponente;
import com.centraldecompras.modelo.DireccionGoogleType;
import java.util.Collection;
import java.util.Objects;
import java.util.logging.Logger;

public class DireccionJsonOut {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(DireccionJsonOut.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    private Direccion direccion;
    private Collection<DireccionGoogleComponente> direccionGoogleComponente;
    private Collection<DireccionGoogleType> direccionGoogleType;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public DireccionJsonOut() {

    }

    public DireccionJsonOut(Direccion direccion, Collection<DireccionGoogleComponente> direccionGoogleComponente, Collection<DireccionGoogleType> direccionGoogleType) {
        this.direccion = direccion;
        this.direccionGoogleComponente = direccionGoogleComponente;
        this.direccionGoogleType = direccionGoogleType;
    }


    public Direccion getDireccion() {
        return direccion;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Collection<DireccionGoogleComponente> getDireccionGoogleComponente() {
        return direccionGoogleComponente;
    }

    public void setDireccionGoogleComponente(Collection<DireccionGoogleComponente> direccionGoogleComponente) {
        this.direccionGoogleComponente = direccionGoogleComponente;
    }

    public Collection<DireccionGoogleType> getDireccionGoogleType() {
        return direccionGoogleType;
    }

    public void setDireccionGoogleType(Collection<DireccionGoogleType> direccionGoogleType) {
        this.direccionGoogleType = direccionGoogleType;
    }

    // Métodos Basicos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.direccion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DireccionJsonOut other = (DireccionJsonOut) obj;
        if (!Objects.equals(this.direccion, other.direccion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DireccionJsonOut{" + "direccion=" + direccion + ", direccionGoogleComponente=" + direccionGoogleComponente + ", direccionGoogleType=" + direccionGoogleType + '}';
    }


}
