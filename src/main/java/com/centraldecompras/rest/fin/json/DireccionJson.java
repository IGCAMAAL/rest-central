package com.centraldecompras.rest.fin.json;

import java.util.ArrayList;
import java.util.List;

public class DireccionJson {

    private String id;
    private String method;
    private String estado;

    private String formateada;
    private List<String> types;
    private LocationJson location;

    private Boolean visibleProve; // :1 es true
    private Boolean visibleAsoc; //  :0 es false 
    
    private String descripcion;
    private List<ComponenteJson> componentes;

    public DireccionJson() {
        componentes = new ArrayList<ComponenteJson>();
        types = new ArrayList<String>();
        location = new LocationJson();
    }

    public DireccionJson(String id, String method, String estado, String formateada, List<String> types, LocationJson location, Boolean visibleProve, Boolean visibleAsoc, String descripcion, List<ComponenteJson> componentes) {
        this.id = id;
        this.method = method;
        this.estado = estado;
        this.formateada = formateada;
        this.types = types;
        this.location = location;
        this.visibleProve = visibleProve;
        this.visibleAsoc = visibleAsoc;
        this.descripcion = descripcion;
        this.componentes = componentes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<ComponenteJson> getComponentes() {
        return componentes;
    }

    public void setComponentes(List<ComponenteJson> componentes) {
        this.componentes = componentes;
    }

    public String getFormateada() {
        return formateada;
    }

    public void setFormateada(String formateada) {
        this.formateada = formateada;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public LocationJson getLocation() {
        return location;
    }

    public void setLocation(LocationJson location) {
        this.location = location;
    }

    public Boolean getVisibleProve() {
        return visibleProve;
    }

    public void setVisibleProve(Boolean visibleProve) {
        this.visibleProve = visibleProve;
    }

    public Boolean getVisibleAsoc() {
        return visibleAsoc;
    }

    public void setVisibleAsoc(Boolean visibleAsoc) {
        this.visibleAsoc = visibleAsoc;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
