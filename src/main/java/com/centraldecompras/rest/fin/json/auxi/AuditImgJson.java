package com.centraldecompras.rest.fin.json.auxi;

public class AuditImgJson {
    
    String id;
    String method;
    String estado;
    int fechaPrevistaActivacion;
    int fechaPrevistaDesactivacion;   
    
    String idSoc;
    String usuarioWeb;

    String descrpcion;
    String b64Picture;
    String b64NombreOriginal;
            
    public AuditImgJson()  {
    }

    public AuditImgJson(String id, String method, String estado, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion, String idSoc, String usuarioWeb, String descrpcion, String b64Picture, String b64NombreOriginal) {
        this.id = id;
        this.method = method;
        this.estado = estado;
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
        this.idSoc = idSoc;
        this.usuarioWeb = usuarioWeb;
        this.descrpcion = descrpcion;
        this.b64Picture = b64Picture;
        this.b64NombreOriginal = b64NombreOriginal;
    }

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public String getIdSoc() {
        return idSoc;
    }

    public void setIdSoc(String idSoc) {
        this.idSoc = idSoc;
    }

    public String getUsuarioWeb() {
        return usuarioWeb;
    }

    public void setUsuarioWeb(String usuarioWeb) {
        this.usuarioWeb = usuarioWeb;
    }

    public String getDescrpcion() {
        return descrpcion;
    }

    public void setDescrpcion(String descrpcion) {
        this.descrpcion = descrpcion;
    }

    public String getB64NombreOriginal() {
        return b64NombreOriginal;
    }

    public void setB64NombreOriginal(String b64NombreOriginal) {
        this.b64NombreOriginal = b64NombreOriginal;
    }

    
    public String getB64Picture() {
        return b64Picture;
    }

    public void setB64Picture(String b64Picture) {
        this.b64Picture = b64Picture;
    }

    
}
