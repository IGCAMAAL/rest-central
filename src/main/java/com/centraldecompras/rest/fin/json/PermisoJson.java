package com.centraldecompras.rest.fin.json;

import java.math.BigDecimal;

public class PermisoJson {

    private String id;
    private int cargaDirectaArticulos;
    private int modPreciosBaja;
    private double porcentajeMaxBaja;
    private int modPreciosAlta;
    private double porcentajeMaxAlta; 
    private double rappel; 

    public PermisoJson() {
    }

    public PermisoJson(String id, int cargaDirectaArticulos, int modPreciosBaja, double porcentajeMaxBaja, int modPreciosAlta, double porcentajeMaxAlta, double rappel) {
        this.id = id;
        this.cargaDirectaArticulos = cargaDirectaArticulos;
        this.modPreciosBaja = modPreciosBaja;
        this.porcentajeMaxBaja = porcentajeMaxBaja;
        this.modPreciosAlta = modPreciosAlta;
        this.porcentajeMaxAlta = porcentajeMaxAlta;
        this.rappel = rappel;
    }

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCargaDirectaArticulos() {
        return cargaDirectaArticulos;
    }

    public void setCargaDirectaArticulos(int cargaDirectaArticulos) {
        this.cargaDirectaArticulos = cargaDirectaArticulos;
    }

    public int getModPreciosBaja() {
        return modPreciosBaja;
    }

    public void setModPreciosBaja(int modPreciosBaja) {
        this.modPreciosBaja = modPreciosBaja;
    }

    public double getPorcentajeMaxBaja() {
        return porcentajeMaxBaja;
    }

    public void setPorcentajeMaxBaja(double porcentajeMaxBaja) {
        this.porcentajeMaxBaja = porcentajeMaxBaja;
    }

    public int getModPreciosAlta() {
        return modPreciosAlta;
    }

    public void setModPreciosAlta(int modPreciosAlta) {
        this.modPreciosAlta = modPreciosAlta;
    }

    public double getPorcentajeMaxAlta() {
        return porcentajeMaxAlta;
    }

    public void setPorcentajeMaxAlta(double porcentajeMaxAlta) {
        this.porcentajeMaxAlta = porcentajeMaxAlta;
    }

    public double getRappel() {
        return rappel;
    }

    public void setRappel(double rappel) {
        this.rappel = rappel;
    }

}
