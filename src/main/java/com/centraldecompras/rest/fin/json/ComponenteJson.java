package com.centraldecompras.rest.fin.json;

import java.util.ArrayList;
import java.util.List;

public class ComponenteJson {
    String id;
    String method;
    String estado;
    
    String long_name;
    String short_name;
    List<String> types;

    public ComponenteJson() {
        types= new ArrayList<String>();
    }

    public ComponenteJson(String id, String method, String estado, String long_name, String short_name, List<String> types) {
        this.id = id;
        this.method = method;
        this.estado = estado;
        this.long_name = long_name;
        this.short_name = short_name;
        this.types = types;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
    
    
}
