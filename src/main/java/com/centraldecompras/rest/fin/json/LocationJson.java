package com.centraldecompras.rest.fin.json;

public class LocationJson {

    double lat;
    double lng;

    public LocationJson() {
    }

    public LocationJson(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

}
