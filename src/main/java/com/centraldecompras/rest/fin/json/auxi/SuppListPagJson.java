package com.centraldecompras.rest.fin.json.auxi;

import com.centraldecompras.rest.fin.json.SuppListJson;
import java.util.List;

public class SuppListPagJson {
    
    int totalPaginas;
    int totalRegistros;   
    int paginaActual; 
    List<SuppListJson> suppListJson;

    public SuppListPagJson() {
    }

    public SuppListPagJson(int totalPaginas, int totalRegistros, int paginaActual, List<SuppListJson> suppListJson) {
        this.totalPaginas = totalPaginas;
        this.totalRegistros = totalRegistros;
        this.paginaActual = paginaActual;
        this.suppListJson = suppListJson;
    }

    public int getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(int totalPaginas) {
        this.totalPaginas = totalPaginas;
    }

    public int getTotalRegistros() {
        return totalRegistros;
    }

    public void setTotalRegistros(int totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    public int getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(int paginaActual) {
        this.paginaActual = paginaActual;
    }

    public List<SuppListJson> getSuppListJson() {
        return suppListJson;
    }

    public void setSuppListJson(List<SuppListJson> suppListJson) {
        this.suppListJson = suppListJson;
    }



    


}
