package com.centraldecompras.rest.fin;

import com.centraldecompras.rest.fin.json.SuppListJson;
import com.centraldecompras.rest.fin.json.auxi.AuditImgJson;
import com.centraldecompras.rest.fin.json.auxi.SuppListPagJson;
import com.centraldecompras.rest.fin.json.UsuarioJson;
import com.centraldecompras.zglobal.PasswordGenerator;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.cmo.service.interfaces.ContactoService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.acceso.FiltroSoc;
import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.acceso.Sector;
import com.centraldecompras.acceso.SectoresAutorizados;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.acceso.service.interfaces.PerfilService;
import com.centraldecompras.acceso.service.interfaces.SectorService;
import com.centraldecompras.acceso.service.interfaces.SectoresAutorizadosService;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import com.centraldecompras.acceso.service.interfaces.UsrSocPrfService;
import com.centraldecompras.acceso.service.interfaces.UsrSocService;
import com.centraldecompras.modelo.ImgDoc;
import com.centraldecompras.modelo.ImgFoto;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgDocService;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgFotoService;
import com.centraldecompras.rest.cmo.ser.interfaces.ImgDocRestService;
import com.centraldecompras.rest.excel.interfaces.SuppExcelService;
import com.centraldecompras.rest.fin.json.PerfilJson;
import com.centraldecompras.rest.fin.ser.interfaces.PersonaRestService;
import com.centraldecompras.rest.fin.ser.interfaces.SectoresAutorizadosRestService;
import com.centraldecompras.rest.fin.ser.interfaces.SociedadRestService;
import com.centraldecompras.rest.fin.ser.interfaces.SociedadRplRestService;
import com.centraldecompras.rest.fin.ser.interfaces.SuppRestService;
import com.centraldecompras.rest.fin.ser.interfaces.UserCentralRestService;
import com.centraldecompras.rest.fin.ser.interfaces.UsrSocPrfRestService;
import com.centraldecompras.rest.fin.ser.interfaces.UsrSocRestService;
import com.centraldecompras.rest.lgt.ser.interfaces.ContactoRestService;
import com.centraldecompras.rest.lgt.ser.interfaces.DireccionRestService;
import com.centraldecompras.rest.lgt.ser.interfaces.SocZonGeoRestService;
import com.centraldecompras.rest.mat.ser.interfaces.SocProductoRestService;
import com.centraldecompras.util.SendEMailViaGMail;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ciberado
 */
@RestController
@RequestMapping("/fin")
@Api(value = "Recursos privados", description = "Ver 1.0 CRU de Asociados Admin, Asociados, Proveedores y Fabricantes")
public class SuppController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SuppController.class.getName());

    @Autowired
    private ImgDocService imgDocService;

    @Autowired
    private SociedadRplService sociedadRplService;

    @Autowired
    private SociedadService sociedadService;

    @Autowired
    private UserCentralService userCentralService;

    @Autowired
    private UsrSocPrfService usrSocPrfService;

    @Autowired
    private UsrSocService usrSocService;

    @Autowired
    private PerfilService perfilService;

    @Autowired
    private DireccionService direccionService;

    @Autowired
    private ContactoService contactoService;

    @Autowired
    private SectoresAutorizadosService sectoresAutorizadosService;

    @Autowired
    private SectorService sectorService;

    @Autowired
    private DireccionRestService direccionRestService;

    @Autowired
    private ContactoRestService contactoRestService;

    @Autowired
    private SuppExcelService suppExcelService;

    @Autowired
    private SuppRestService suppRestService;

    @Autowired
    private SociedadRplRestService sociedadRplRestService;

    @Autowired
    private UserCentralRestService userCentralRestService;

    @Autowired
    private PersonaRestService personaRestService;

    @Autowired
    private SociedadRestService sociedadRestService;

    @Autowired
    private UsrSocRestService usrSocRestService;

    @Autowired
    private UsrSocPrfRestService usrSocPrfRestService;

    @Autowired
    private SectoresAutorizadosRestService sectoresAutorizadosRestService;

    @Autowired
    private SocZonGeoRestService zonaGeograficaAsigRestService;

    @Autowired
    private SocProductoRestService productoArbolAsigRestService;

    @Autowired
    private ImgDocRestService imgDocRestService;

//    @Autowired
//    private SendEMailViaGMail sendEMailViaGMail;
    @Autowired
    private ImgFotoService imgFotoService;

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/usrWeb", method = RequestMethod.POST)
    @ApiOperation(value = "Valida que el usuario Web existe", notes = "String, Verifica formato de usrWeb y existencia")
    String[] getUserWeb(
            @RequestBody String[] stringJson,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");
        Map<String, String> replies = new HashMap<>();

        replies.put("correcto-existe", "SS");
        replies.put("correcto-inexistente", "SN");
        replies.put("incorrecto", "NN");

        String[] reply = new String[1];
        reply[0] = replies.get("correcto-inexistente");
        boolean boo = stringJson[0].matches("[a-zA-Z0-9_.]{5,}");
        if (boo) {
            UserCentral userCentral = userCentralService.findUserCentralBy_UN(stringJson[0]);
            if (userCentral != null) {
                reply[0] = replies.get("correcto-existe");
            }
        } else {
            reply[0] = replies.get("incorrecto");
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/identificadorEmpresa", method = RequestMethod.POST)
    @ApiOperation(value = "Valida que el identificador empresa existe", notes = "String, Verifica existencia")
    String[] getIdentificadorEmpresa(
            @RequestBody String[] tipoIdentificadorNifnrf,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");
        Map<String, String> replies = new HashMap<>();

        replies.put("correcto-existe", "SS");
        replies.put("correcto-inexistente", "SN");
        replies.put("incorrecto", "NN");

        String[] reply = new String[1];
        reply[0] = replies.get("correcto-inexistente");

        Sociedad sociedad = sociedadService.findSociedadBy_TNR_NR(tipoIdentificadorNifnrf[0], tipoIdentificadorNifnrf[1]);
        if (sociedad != null) {
            reply[0] = replies.get("correcto-existe");
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/supppag", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera una coleccion de n Reg de Sociedad/Usuario/Persona/perfil a partir de uno dado con tecnica paginada ", notes = "SuppListJson, formato de comunicacion con cliente")
    SuppListPagJson getSuppPag(
            @RequestBody FiltroSoc filtroSoc,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        SuppListPagJson reply = new SuppListPagJson();
        List<SuppListJson> suppListJsons = new ArrayList<SuppListJson>();
        DynaBean datosSocUsuPrf = (DynaBean) sociedadService.findUsrSocPrf_Pag_WithFilter(filtroSoc);

        int totalPaginas = (int) datosSocUsuPrf.get("totalPaginas");
        int totalRegistros = (int) datosSocUsuPrf.get("totalRegistros");
        int paginaActual = (int) datosSocUsuPrf.get("paginaActual");
        List<String> idSociedades = (List<String>) datosSocUsuPrf.get("idSociedades");

        log.info("----<" + idSociedades);
        if (idSociedades == null) {
            log.info("----<Creamos uno vacio");
            idSociedades = new ArrayList<>();
        }

        for (String idSociedad : idSociedades) {
            SuppListJson suppListJson = new SuppListJson();
            Sociedad sociedad = sociedadService.findSociedad(idSociedad);
            suppListJson = extraccionDatosCompletosSociedadNEW(sociedad, idSec);
            suppListJsons.add(suppListJson);
        }

        reply.setTotalPaginas(totalPaginas);
        reply.setTotalRegistros(totalRegistros);
        reply.setPaginaActual(paginaActual);
        reply.setSuppListJson(suppListJsons);
        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/supplist/{maxResults}/{firstResult}", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera una coleccion de n Reg de Sociedad/Usuario/Persona/perfil a partir de uno dado ", notes = "SuppListJson, formato de comunicacion con cliete")
    List<SuppListJson> getSuppList(
            @PathVariable int maxResults,
            @PathVariable int firstResult,
            @RequestBody List<String> perfiles,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<SuppListJson> reply = new ArrayList<SuppListJson>();

        Sector sector = sectorService.findSector(idSec); // En este momento, idSec contiene el sector que se utilizará
        List<SectoresAutorizados> sectoresAutorizadosList = sectoresAutorizadosService.findUSP_By_Sid(sector);

        HashMap hashSociedad = new HashMap();
        for (SectoresAutorizados sectorAutorizado : sectoresAutorizadosList) {
            UsrSocPrf usrSocPrf = sectorAutorizado.getPrsSocPrf();

            for (String perfil : perfiles) {
                Perfil perfilObj = perfilService.findPerfilBy_CP(perfil);
                if (usrSocPrf.getPerfil().getIdPerfil().equals(perfilObj.getIdPerfil())) {
                    SuppListJson suppListJson = new SuppListJson();
                    Sociedad sociedad = sociedadService.findSociedad(usrSocPrf.getUsrSoc().getSociedad().getIdSociedad());
                    String value = (String) hashSociedad.get(sociedad.getIdSociedad());
                    if (value != null) {
                        // Nothing todo
                    } else {
                        hashSociedad.put(sociedad.getIdSociedad(), sociedad.getIdSociedad());
                        if (sociedad != null) {
                            suppListJson = sociedadRestService.recuperaSociedadDatosBasicos(sociedad);
                            reply.add(suppListJson);
                        }
                    }
                }
            }

        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/excel/{maxResults}/{firstResult}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera todas las Sociedad/Usuario/perfil y los muestra en excel ", notes = "SuppListJson, formato de comunicacion con cliente")
    Map<String, String> getSuppExcel(
            @PathVariable int maxResults,
            @PathVariable int firstResult,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");
        Map<String, String> reply = new HashMap<>();

        List<Sociedad> sociedades = sociedadService.findSociedadEntities(true, maxResults, firstResult);
        List<UsrSocPrf> usrSocPrfs = usrSocPrfService.findUsrSocPrfEntities(true, maxResults, firstResult);
        List<Contacto> contactos = contactoService.findContactoEntities(true, maxResults, firstResult);
        List<Direccion> direcciones = direccionService.findDireccionEntities(true, maxResults, firstResult);

        XSSFWorkbook workbook = null;
        workbook = suppExcelService.montaExcelSupp(sociedades, usrSocPrfs, contactos, direcciones);
        XSSFWorkbook workbookTest = suppExcelService.montaExcelSupp(sociedades, usrSocPrfs, contactos, direcciones);

        String nameFile = KApp.SOC_USU_PERF.getKApp();
        String extensionFile = KApp.XLSX.getKApp();

        ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
        workbook.write(outByteStream);

        byte[] outArray = outByteStream.toByteArray();
        byte[] encoded = Base64.encodeBase64(outArray);
        String picture = new String(encoded);

        reply.put(KApp.NAME_FILE.getKApp(), nameFile);
        reply.put(KApp.EXTENSION_FILE.getKApp(), extensionFile);
        reply.put(KApp.PICTURE.getKApp(), picture);

        try { //Write the workbookTest in file system
            if (workbookTest != null) {
                FileOutputStream out = new FileOutputStream(new File(nameFile.concat(".").concat(extensionFile)));
                workbookTest.write(out);
                out.close();
                log.info(nameFile + "." + extensionFile + " written successfully on disk.");
            }
        } catch (Exception e) {
            Object[] valores = {"Generacion excel de sociedades"};
            String mensaje = MessageFormat.format("ERROR: SociedadesUsuariosPerfiles.xlsx no generado. {0}", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/supp/{idSociedad}/{idUsuario}/{idPerfil}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera Sociedad/Usuario/Persona/perfil", notes = "proveedorJson, formato de comunicacion con cliete. El parámetro {id} es el id de usuario")
    SuppListJson getSupp(
            @PathVariable String idSociedad,
            @PathVariable String idUsuario,
            @PathVariable String idPerfil,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");
        // Necesito 3 id: Sociedad, UserCentral, Perfil
        SuppListJson reply = new SuppListJson();

        Sociedad sociedad = sociedadService.findSociedad(idSociedad);
        SociedadRpl sociedadRpl = sociedadRplService.findSociedadRpl(idSociedad);

        reply = sociedadRestService.recuperaSociedad(sociedad);
        reply.setUsuarios(userCentralRestService.recuperaUsersCentral(sociedad, idUsuario, idSec));
        reply.setContactos(contactoRestService.recuperaContactos(sociedadRpl, null, null));
        reply.setDirecciones(direccionRestService.recuperaDirecciones(sociedadRpl, null, null));

        reply.setZonasAsignadas(zonaGeograficaAsigRestService.recuperaZonasGeoAsignadasASociedad(sociedad.getIdSociedad()));
        reply.setProductosAsignados(productoArbolAsigRestService.recuperaSociedadProducto(sociedad.getIdSociedad()));

        List<String> entitiesId = new ArrayList();
        entitiesId.add(idSociedad);
        entitiesId.add(idUsuario);

        reply.setImagenes(imgDocService.findImgDocBy_SU(entitiesId));
        reply.getImagenes().addAll(imgFotoService.findImgFotoBy_SU(entitiesId));

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/fileUpload", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ApiOperation(value = "Registrar Asoc / Prov / Fabr , imagenes y copis de ZGeo porduct ", notes = "NO Usa multipart")
    @ResponseBody
    SuppListJson postSupp(
            @RequestBody SuppListJson suppListJsonIN,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws Exception {
        log.info("................................................................................CONTROL");

        SuppListJson reply = new SuppListJson();
        List<String[]> imagenes = new ArrayList();
        SociedadRpl sociedadRpl = null;
        Sociedad sociedadBBDD = null;
        Sector sector = null;

        List<Object[]> datosMontadosSociedad = new ArrayList();
        List<Object[]> datosMontadosZonasGeograficas = new ArrayList();
        List<Object[]> datosMontadosProductos = new ArrayList();
        List<Object[]> datosMontadosImagen = new ArrayList();

        sector = sectorService.findSector(idSec);

        if (sector == null) {
            Object[] valores = {idSec};
            String mensaje = MessageFormat.format("ERROR: Sector Id {0}, no econtrado. ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        datosMontadosSociedad = addSupp(suppListJsonIN, idUser, idSoc, idPrf, idSec);
        sociedadRpl = (SociedadRpl) datosMontadosSociedad.get(1)[2];
        HashMap userWebId = fusionaUserWebId(datosMontadosSociedad);

        datosMontadosZonasGeograficas = zonaGeograficaAsigRestService.montaZonasGeoAsigadasASociedad(suppListJsonIN.getCpyZonGeo(), sociedadRpl, idUser, idSoc, idPrf, idSec);
        datosMontadosSociedad.addAll(datosMontadosZonasGeograficas);

        datosMontadosProductos = productoArbolAsigRestService.montaSociedadProducto(suppListJsonIN.getCpyPrdArb(), sociedadRpl, idUser, idSoc, idPrf, idSec);
        datosMontadosSociedad.addAll(datosMontadosProductos);

        /*  LG=LOGO, FU=FOTO_USUARIO, FP=FOTO_PRODUCTO, CT=CATALOGO */
        for (int ic0 = 0; ic0 < 3; ic0++) {
            List<AuditImgJson> auditImgesJson = new ArrayList();
            // String[] pictures = null;
            String type = null;
            switch (ic0) {
                case 0:
                    auditImgesJson = suppListJsonIN.getDataImgLogo();
                    type = KApp.FOTO_LOGO.getKApp();
                    break;
                case 1:
                    auditImgesJson = suppListJsonIN.getDataImgFotoUsu();
                    type = KApp.FOTO_USUARIO.getKApp();
                    break;
                case 2:
                    auditImgesJson = suppListJsonIN.getDataImgCatalogo();
                    type = KApp.CATALOGO.getKApp();
                    break;
            }

            if (auditImgesJson != null) {
                for (AuditImgJson auditImgJson : auditImgesJson) {
                    boolean imagenPOST_OK = true;
                    if (KApp.POST.getKApp().equals(auditImgJson.getMethod()) && (auditImgJson.getB64Picture() == null || "".equals(auditImgJson.getB64Picture()))) {
                        imagenPOST_OK = false;      // Metodo POST sin imagen
                        imagenes.add(new String[]{auditImgJson.getMethod(), "KO Imagen no creada", " ", auditImgJson.getDescrpcion(), auditImgJson.getB64NombreOriginal(), ""});
                    }

                    if (imagenPOST_OK) {
                        datosMontadosImagen = imgDocRestService.crudDDBB(type, auditImgJson, sociedadRpl.getIdSociedad(), idUser, userWebId);
                        if (datosMontadosImagen.size() > 0) {
                            datosMontadosSociedad.addAll(datosMontadosImagen);
                            String idImagen = ((ImgDoc) datosMontadosImagen.get(0)[2]).getIdImgDoc();
                            auditImgJson.setId(idImagen);
                        }
                    }
                }
            } else {
                auditImgesJson = new ArrayList();
            }

            switch (ic0) {
                case 0:
                    suppListJsonIN.setDataImgLogo(auditImgesJson);
                    break;
                case 1:
                    suppListJsonIN.setDataImgFotoUsu(auditImgesJson);
                    break;
                case 2:
                    suppListJsonIN.setDataImgCatalogo(auditImgesJson);
                    break;
            }
        }

        try {
            suppRestService.persistencia(datosMontadosSociedad);
        } catch (Exception ex) {
            Object[] valores = {ex.getMessage()};
            String mensaje = MessageFormat.format("ERROR: No se realiza alta en BBDD por erores en datos: ", valores);
            ex.printStackTrace();
            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
        }

        List<String> entitiesId = new ArrayList();
        if (!suppListJsonIN.getMethod().equals(KApp.DELETE.getKApp())) {

            sociedadBBDD = sociedadService.findSociedad(sociedadRpl.getIdSociedad());
            reply = extraccionDatosCompletosSociedadNEW(sociedadBBDD, idSec);
            reply.setCpyZonGeo(suppListJsonIN.getCpyZonGeo());
            reply.setCpyPrdArb(suppListJsonIN.getCpyPrdArb());
            reply.setDataImgLogo(suppListJsonIN.getDataImgLogo());
            reply.setDataImgFotoUsu(suppListJsonIN.getDataImgFotoUsu());
            reply.setDataImgCatalogo(suppListJsonIN.getDataImgCatalogo());

            //entitiesId.add(sociedadBBDD.getIdSociedad());
            //reply.setImagenes(imgDocService.findImgDocBy_SU(entitiesId));
            //reply.getImagenes().addAll(imgFotoService.findImgFotoBy_SU(entitiesId));
        }

        for (UsuarioJson usuarioJson : reply.getUsuarios()) {
            UserCentral userCentralEmail = userCentralService.findUserCentral(usuarioJson.getId());



            if (userCentralEmail != null && KApp.POST.getKApp().equals(usuarioJson.getMethod())) {

                List<PerfilJson> perfiles = usuarioJson.getPerfiles();
                String mailPerfil = "";
                for (PerfilJson perfil : perfiles) {
                    if("".equals(mailPerfil)){
                        mailPerfil = perfil.getMailPerfil();
                    }
                }
            
                Thread sendEMailViaGMail = new SendEMailViaGMail(
                        "SendEMailViaGMailThread",
                        userCentralEmail.getNombre(),
                        userCentralEmail.getUsername(),
                        userCentralEmail.getMd5password(),
                        /* userCentralEmail.getEmail() */ mailPerfil,
                        userCentralEmail.getId(),
                        userCentralService);

                sendEMailViaGMail.start();

            }
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/imagen/{idImagen}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera Imagen de Logo o foto de usuario o foto de producto", notes = "El parámetro {idImagen} es el id de la imagen")
    void getImagen(
            @PathVariable String idImagen,
            HttpServletResponse response,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        byte[] picture = null;

        ImgDoc imgDoc = null;
        ImgFoto imgFoto = null;

        String extension = null;
        String nombreOri = null;

        try {
            imgFoto = imgFotoService.findImgFoto(idImagen);

            if (imgFoto == null) {
                imgDoc = imgDocService.findImgDoc(idImagen);
                extension = imgDoc.getExtension();
                nombreOri = imgDoc.getNomOriginal();

                picture = imgDocService.findImgPic_idI(idImagen);

            } else {
                extension = imgFoto.getExtension();
                nombreOri = imgFoto.getNomOriginal();

                picture = imgFotoService.findImgPic_idI(idImagen);
            }

            response.setContentType("image/" + extension);
            response.setHeader("Content-disposition", "attachment; filename=" + nombreOri);

            response.getOutputStream().write(picture);
            response.flushBuffer();

        } catch (IOException ex) {
            log.info("Error writing file to output stream. Filename was " + idImagen + " - " + ex);
            throw new RuntimeException("IOError writing file to output stream");
        }
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/suppdatossociedad/{idSociedad}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera Datos de uan Sociedad", notes = "SuppListJson, formato de comunicacion con cliete. El parámetro {id} es el id de usuario")
    SuppListJson getDatosCompletosSociedad(
            @PathVariable String idSociedad,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");
        // Necesito 3 id: Sociedad, UserCentral, Perfil
        SuppListJson reply = new SuppListJson();

        reply = extraccionDatosCompletosSociedadNEW(sociedadService.findSociedad(idSociedad), idSec);

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/chgPassword", method = RequestMethod.POST)
    @ApiOperation(value = "El usuario Cambia password y envia email", notes = "El parámetro {idusuario} es el id de usuario")
    // {"userWeb":"usuarioWeb","oldPassword":"viejaClave","newPassword":"nuevaClave","email":"nuevoEmail@gmail.com"} 
    Map<String, String> chgPassword(
            @RequestBody Map<String, String> datosCambio,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        String userWeb = datosCambio.get("userWeb");
        String oldPassword = datosCambio.get("oldPassword");
        String newPassword = datosCambio.get("newPassword");
        String email = datosCambio.get("email");

        UserCentral userCentralByUW = userCentralService.findUserCentralBy_UN(userWeb);
        if (userCentralByUW == null) {
            Object[] valores = {userWeb};
            String mensaje = MessageFormat.format("ERROR chgPassword: El usuario {0} no existe", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        Sociedad sociedad = sociedadService.findSociedad(idSoc);
        if (sociedad == null) {
            Object[] valores = {idSoc};
            String mensaje = MessageFormat.format("ERROR chgPassword: La sociedad {0} no existe", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        Perfil perfil = perfilService.findPerfil(idPrf);
        if (perfil == null) {
            Object[] valores = {idPrf};
            String mensaje = MessageFormat.format("ERROR chgPassword: El perfil {0} no existe", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        UsrSoc usrSoc = usrSocService.findPrsSocByUS(userCentralByUW, sociedad);
        if (usrSoc == null) {
            Object[] valores = {userCentralByUW.toString(), sociedad.toString()};
            String mensaje = MessageFormat.format("ERROR chgPassword: No exite combinación para el usuario[ {0} ] con la sociedad [ {1} ] ", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        UsrSocPrf usrSocPrf = usrSocPrfService.findUsrSocByUP(usrSoc, perfil);
        if (usrSocPrf == null) {
            Object[] valores = {usrSoc.getSociedad().toString(), usrSoc.getUserCentral().toString(), perfil.toString()};
            String mensaje = MessageFormat.format("ERROR chgPassword: La sociedad [ {0} ], el usuario [ {1} ] y el perfil [{2}] no estan relacionados", valores);
            throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
        }

        boolean persisData = false;
        if (newPassword != null && !newPassword.equals("")) {
            if (!oldPassword.equals(userCentralByUW.getMd5password())) {
                Object[] valores = {userWeb};
                String mensaje = MessageFormat.format("ERROR chgPassword: La password utilizada no corresponde al usuario {0}", valores);
                throw new NonexistentEntityException(MessageFormat.format(mensaje, new Throwable()));
            } else {
                userCentralByUW.setMd5password(newPassword);
                persisData = true;
            }
        }

        if (email != null && !email.equals("")) {
            usrSocPrf.setMail(email);
            persisData = true;
        }

        datosCambio.replace("oldPassword", "*********");
        datosCambio.replace("newPassword", "*********");

        if (persisData) {
            usrSocPrfService.edit_B(usrSocPrf);
            if (newPassword != null && !newPassword.equals("")) {
                datosCambio.put("Rest Password", "Cambiada con exito");
            }
            if (email != null && !email.equals("")) {
                datosCambio.put("Rest email", "Cambiado con exito");
            }
        }

        return datosCambio;
    }

// ----------------------------------------------------------------------------------------------------------------------------------------------------------    
    @RequestMapping(value = "/newPassword/{idusuario}", method = RequestMethod.GET)
    @ApiOperation(value = "Recupera Genera password y envia email", notes = "El parámetro {idusuario} es el id de usuario")
    void getNewPassword(
            @PathVariable String idusuario,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        List<Object> createObjects = new ArrayList();
        List<Object> updateObjects = new ArrayList();

        String password = PasswordGenerator.getPassword(PasswordGenerator.NUMEROS + PasswordGenerator.MAYUSCULAS + PasswordGenerator.MINUSCULAS, 10);
        UserCentral userCentral = userCentralService.findUserCentral(idusuario);

        if (userCentral != null) {
            userCentral.setMd5password(password);

            updateObjects.add(userCentral);
            userCentralRestService.persistencia(createObjects, updateObjects);

            Thread sendEMailViaGMail = new SendEMailViaGMail(
                    "SendEMailViaGMailThread",
                    userCentral.getNombre(),
                    userCentral.getUsername(),
                    userCentral.getMd5password(),
                    userCentral.getEmail(),
                    userCentral.getId(),
                    userCentralService);
            sendEMailViaGMail.start();
        }
    }

    public SuppListJson extraccionDatosCompletosSociedadNEW(Sociedad sociedad, String idSec) {
        SuppListJson reply = new SuppListJson();
        SociedadRpl sociedadRpl = sociedadRplService.findSociedadRpl(sociedad.getIdSociedad());

        List<String> entitiesId = new ArrayList();
        reply = sociedadRestService.recuperaSociedad(sociedad);
        reply.setUsuarios(userCentralRestService.recuperaUsersCentral(sociedad, null, idSec));
        reply.setContactos(contactoRestService.recuperaContactos(sociedadRpl, null, null));
        reply.setDirecciones(direccionRestService.recuperaDirecciones(sociedadRpl, null, null));

        reply.setZonasAsignadas(zonaGeograficaAsigRestService.recuperaZonasGeoAsignadasASociedad(sociedad.getIdSociedad()));
        reply.setProductosAsignados(productoArbolAsigRestService.recuperaSociedadProducto(sociedad.getIdSociedad()));

        entitiesId.add(sociedad.getIdSociedad());
        for (UsuarioJson usuarioJson : reply.getUsuarios()) {
            entitiesId.add(usuarioJson.getId());
        }

        reply.setImagenes(imgDocService.findImgDocBy_SU(entitiesId));

        return reply;
    }

    private Perfil cargaPerfil(PerfilJson perfilJson) throws Exception {
        Perfil reply = null;

        reply = perfilService.findPerfilBy_CP(perfilJson.getPerfilCod());
        if (reply == null) {
            throw new Exception("CentralCompras.CRUD Usuario. Consulta Perfil. codPerfil " + perfilJson + " no existe.");
        }

        return reply;
    }

    private List<Object[]> addSupp(SuppListJson suppListJson, String idUser, String idSoc, String idPrf, String idSec) throws Exception {

        log.info("................................................................................CONTROL");

        //SuppListJson reply = new SuppListJson();
        List<Object[]> listaObjetos = new ArrayList();

        if (idUser.equals("null")) {
            idUser = null;
        }
        if (idSoc.equals("null")) {
            idSoc = null;
        }
        if (idSec.equals("null")) {
            idSec = null;
        }
        if (idPrf.equals("null")) {
            idPrf = null;
        }

        HashMap<String, Object> parYaud = new HashMap<String, Object>();
        parYaud.put("idUser", idUser);
        parYaud.put("idSoc", idSoc);
        parYaud.put("idPrf", idPrf);
        parYaud.put("idSec", idSec);

        parYaud.put("id", suppListJson.getId());
        parYaud.put("estado", suppListJson.getEstado());
        parYaud.put("method", suppListJson.getMethod());
        parYaud.put("fpAct", suppListJson.getFechaPrevistaActivacion());
        parYaud.put("fpDes", suppListJson.getFechaPrevistaDesactivacion());

        if (suppListJson.getId().equals("0") && !suppListJson.getMethod().equals("POST")
                || !suppListJson.getId().equals("0") && suppListJson.getMethod().equals("POST")) {
            throw new Exception("CentralCompras. CRUD Asoc/Prov/Fabr. id supp  y Metodo ne estan sincronizados");
        }

        Object[] siciedad_Array = new Object[4];

        Sociedad sociedad = null;
        SociedadRpl sociedadRpl = null;
        UserCentral userCentral = null;
        Perfil perfil = null;
        UsrSoc usrSoc = null;
        UsrSocPrf usrSocPrf = null;

        String metodoPadre = suppListJson.getMethod();
        if (KApp.DELETE.getKApp().equals(metodoPadre)) {
            parYaud.replace("method", KApp.DELETE.getKApp());
        }

        sociedad = sociedadRestService.montaSociedad(suppListJson, parYaud);
        suppListJson.setId(sociedad.getIdSociedad());  // Para busqueda al final del método y presentación de la persistencia

        siciedad_Array[0] = metodoPadre; 
        siciedad_Array[1] = "sociedad";
        siciedad_Array[2] = sociedad;
        siciedad_Array[3] = suppListJson.getId();
        listaObjetos.add(siciedad_Array); 

        List<Object[]> sociedadRpl_Array = sociedadRplRestService.montaSociedadRpl(sociedad, metodoPadre);
        listaObjetos.addAll(sociedadRpl_Array); 
        sociedadRpl = (SociedadRpl) sociedadRpl_Array.get(0)[2]; 

        List<UsuarioJson> usuariosJson = suppListJson.getUsuarios();
        //List<UserCentral> usuariosCentral = new ArrayList();
        for (UsuarioJson usuarioJson : usuariosJson) {
            List<Object[]> userCentral_Array = userCentralRestService.montaUserCentral(usuarioJson, metodoPadre);
            listaObjetos.addAll(userCentral_Array);
            userCentral = (UserCentral) userCentral_Array.get(0)[2];  
          //  usuariosCentral.add(userCentral);
            if (KApp.DELETE.getKApp().equals(usuarioJson.getMethod())) {
                metodoPadre = usuarioJson.getMethod();
            }

            List<Object[]> persona_Array = personaRestService.montaPersona(usuarioJson, userCentral.getId(), idUser, metodoPadre);
            listaObjetos.addAll(persona_Array); 

            List<Object[]> usrSoc_Array = usrSocRestService.montaUsrSoc(userCentral, sociedad, idUser, metodoPadre, usuarioJson);
            listaObjetos.addAll(usrSoc_Array);
            usrSoc = (UsrSoc) usrSoc_Array.get(0)[2]; 

            List<PerfilJson> perfiles = usuarioJson.getPerfiles();
            for (PerfilJson perfilJson : perfiles) {
                perfil = cargaPerfil(perfilJson);
                List<Object[]> usrSocPrf_Array = usrSocPrfRestService.montaUsrSocPrf(usrSoc, perfil, perfilJson, idUser, metodoPadre, usuarioJson);
                listaObjetos.addAll(usrSocPrf_Array);
                usrSocPrf = (UsrSocPrf) usrSocPrf_Array.get(0)[2]; 
                if (usrSocPrf != null) {
                    List<Object[]> sectorAutorizado_Array = sectoresAutorizadosRestService.montaSectoresAutorizados(usrSocPrf, perfilJson, idSec, idUser, metodoPadre, usuarioJson);
                    listaObjetos.addAll(sectorAutorizado_Array); 
                }
            }
        }

        if (suppListJson.getDirecciones() != null) {
            List<Object[]> direcciones = direccionRestService.montaDirecciones(sociedadRpl, null, null, suppListJson.getDirecciones(), parYaud); // Sociedad, Persona, Almacen
            listaObjetos.addAll(direcciones);
        }

        if (suppListJson.getContactos() != null) {
            List<Object[]> contactos = contactoRestService.montaContactos(sociedadRpl, null, null, suppListJson.getContactos(), parYaud);    // Sociedad, Persona, Almacen
            listaObjetos.addAll(contactos);
        }

        return listaObjetos;
    }

    private HashMap fusionaUserWebId(List<Object[]> datosMontadosSociedad) {
        HashMap userWebId = new HashMap();
        
        for (Object[] datoMontadoSociedad_Array : datosMontadosSociedad) {  
            if (("userCentral").equals((String) datoMontadoSociedad_Array[1])) {
                UserCentral userCentral = (UserCentral) datoMontadoSociedad_Array[2];
                userWebId.put(userCentral.getUsername(), userCentral.getId());
            }
        }

        return userWebId;
    }

}
