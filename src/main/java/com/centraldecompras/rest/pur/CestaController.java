package com.centraldecompras.rest.pur;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.ArticuloLangNomb;
import com.centraldecompras.modelo.ArticuloProducto;
import com.centraldecompras.modelo.CompraCesta;
import com.centraldecompras.modelo.CompraLinea;
import com.centraldecompras.modelo.CompraPedido;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.ProductoLang;
import com.centraldecompras.modelo.TipoIva;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.cmo.service.interfaces.ContadoresService;
import com.centraldecompras.modelo.cmo.service.interfaces.TipoIvaService;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenService;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenUsuarioProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloLangDescService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloLangNombService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoLangService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.pur.service.interfaces.CompraCestaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraLineaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraPedidoService;
import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.rest.lgt.ser.interfaces.AlmacenRestService;
import com.centraldecompras.rest.mat.json.ArticuloJson;
import com.centraldecompras.rest.prm.json.TraduccionDescripcion;
import com.centraldecompras.rest.pur.ser.interfraces.CompraRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pur")
@Api(value = "Recurso privado", description = "Gestion de almacenes")
public class CestaController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CestaController.class.getName());

    @Autowired
    private CompraCestaService compraCestaService;

    @Autowired
    private CompraLineaService compraLineaService;

    @Autowired
    private CompraRestService compraRestService;

    @Autowired
    private SociedadService sociedadService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private ArticuloService articuloService;

    @Autowired
    private CompraPedidoService compraPedidoService;

    @Autowired
    private TipoIvaService tipoIvaService;

    @Autowired
    private AlmacenService almacenService;

    @Autowired
    private ProductoService productoService;

    @Autowired
    private UserCentralService userCentralService;

    @Autowired
    private ArticuloLangNombService articuloLangNombService;

    @Autowired
    private ArticuloLangDescService articuloLangDescService;

    @Autowired
    private AlmacenUsuarioProductoService almacenUsuarioProductoService;

    @Autowired
    private AlmacenRestService almacenRestService;

    @Autowired
    private ProductoLangService productoLangService;

    @Autowired
    private SocProductoService socProductoService;

    @Autowired
    private ArticuloProductoService articuloProductoService;

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/almacenusuarioproducto/typeahead", method = RequestMethod.POST)
    List<Map<String, Object>> seleccionProductoTypeAhead(
            @RequestBody Map<String, Object> parametrosFiltro,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList();

        try {
            parametrosFiltro.put("solicitud", "typeAheadProducto");
            validacion_AlmacenUsuarioArticuloPag(parametrosFiltro);

            List<String> productosEtiquetas = compraCestaService.findProductWithLabels(parametrosFiltro);
            if (productosEtiquetas != null && productosEtiquetas.size() == 0) {
                parametrosFiltro.put("productosConEtiqueta", compraCestaService.findProductWithLabels(parametrosFiltro));
            }

            if (reply.size() < (Integer) parametrosFiltro.get("cantidad")) {
                List<Object[]> prods = compraCestaService.findProductoTypeAhead(new Integer(1), parametrosFiltro);
                productosTypeAheadToJson(reply, prods);
            }

            if (reply.size() < (Integer) parametrosFiltro.get("cantidad")) {
                List<Object[]> prods = compraCestaService.findProductoTypeAhead(new Integer(2), parametrosFiltro);
                productosTypeAheadToJson(reply, prods);
            }

        } catch (Exception ex) {
            throw (ex);
        }

        return reply;

    }

    // Esta entrada ya no debe funcionar para el typeAhead de productos.
    // Se va a transformar para un filtro de articulos para la carga de la cesta
    // No hacer nada hasta que esté decidico
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/almacenusuarioarticulo/typeahead", method = RequestMethod.POST)
    List<Map<String, Object>> seleccionArticuloTypeAhead(
            @RequestBody Map<String, Object> parametrosFiltro,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList();

        try {
            parametrosFiltro.put("solicitud", "typeAheadArticulo");
            validacion_AlmacenUsuarioArticuloPag(parametrosFiltro);

            List<String> productosEtiquetas = compraCestaService.findProductWithLabels(parametrosFiltro);
            if (productosEtiquetas != null && productosEtiquetas.size() == 0) {
                parametrosFiltro.put("productosConEtiqueta", compraCestaService.findProductWithLabels(parametrosFiltro));
            }
            if (reply.size() < (Integer) parametrosFiltro.get("cantidad")) {
                List<Object[]> prods = compraCestaService.findArticuloTypeAhead(new Integer(1), parametrosFiltro);
                articulosTypeAheadToJson(reply, prods);
            }

            if (reply.size() < (Integer) parametrosFiltro.get("cantidad")) {
                List<Object[]> prods = compraCestaService.findArticuloTypeAhead(new Integer(2), parametrosFiltro);
                articulosTypeAheadToJson(reply, prods);
            }

        } catch (Exception ex) {
            throw (ex);
        }

        return reply;

    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/listalmacenusuarioarticulopag", method = RequestMethod.POST)
    @ApiOperation(value = "Devuelve una relación de Articulos para seleccionar lo que comprar, según condiciones de compra del usuario en el almacen.", notes = "nota")
    Map<String, Object> listAlmacenUsuarioArticuloPag(
            @RequestBody Map<String, Object> parametrosFiltro,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        Map<String, Object> reply = new HashMap<>();

        try {

            parametrosFiltro.put("solicitud", "paginacion");

            validacion_AlmacenUsuarioArticuloPag(parametrosFiltro);

            List<String> productosEtiquetas = compraCestaService.findProductWithLabels(parametrosFiltro);
            if (productosEtiquetas != null && productosEtiquetas.size() == 0) {
                parametrosFiltro.put("productosConEtiqueta", compraCestaService.findProductWithLabels(parametrosFiltro));
            }
            Map<String, Object> almacenUsuarioProductoMap = (Map<String, Object>) compraCestaService.findArticulosForSelect_Pag_WithFilter(parametrosFiltro);

            List<Object[]> articulos_aup = (List<Object[]>) almacenUsuarioProductoMap.get("articulos_aup");

            reply.put("articuloJsonList", articuloList_Json(articulos_aup));
            reply.put("totalPaginas", (Integer) almacenUsuarioProductoMap.get("totalPaginas"));
            reply.put("totalRegistros", (Integer) almacenUsuarioProductoMap.get("totalRegistros"));
            reply.put("paginaActual", (Integer) almacenUsuarioProductoMap.get("paginaActual"));

        } catch (Exception ex) {
            throw (ex);
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/articuloscestapag", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera una coleccion de n lieas de pedido de una cesta determinada ", notes = "notes")
    Map<String, Object> articulosCestaPag(
            @RequestBody Map<String, Object> parametrosFiltro,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        Map<String, Object> reply = new HashMap();

        try {

            List<Map<String, Object>> lineaJsonList = new ArrayList();

            validacion_CestaArticuloPag(parametrosFiltro);

            Map<String, Object> compraLineaMap = (Map<String, Object>) compraLineaService.findCompraLinea_PagWithFilter_C(parametrosFiltro);

            List<CompraLinea> compraLineas = (List<CompraLinea>) compraLineaMap.get("compraLineas");

            for (CompraLinea compraLinea : compraLineas) {
                Map<String, Object> compraLineaJson = compraCestaLinea_Json(compraLinea);

                compraLineaJson.put("articulo", articulo_Json(compraLinea.getArticulo()));
                lineaJsonList.add(compraLineaJson);
            }

            reply.put("lineaJsonList", lineaJsonList);
            reply.put("totalPaginas", (Integer) compraLineaMap.get("totalPaginas"));
            reply.put("totalRegistros", (Integer) compraLineaMap.get("totalRegistros"));
            reply.put("paginaActual", (Integer) compraLineaMap.get("paginaActual"));

        } catch (Exception ex) {
            throw (ex);
        }

        return reply;

    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/cestacomprapag", method = RequestMethod.POST)
    @ApiOperation(value = "Recupera una coleccion de n Reg de Cestas a a partir de un filtro de seleccion ", notes = "notes")
    Map<String, Object> cestaCompraPag(
            @RequestBody Map<String, Object> parametrosFiltro,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        Map<String, Object> compraCestaMap = null;
        Map<String, Object> reply = new HashMap();
        List<Map<String, Object>> cestaJsonList = new ArrayList();

        Integer totalPaginas = 0;
        Integer totalRegistros = 0;
        Integer paginaActual = 0;

        try {
            validacion_CestaPag(parametrosFiltro);
            compraCestaMap = (Map<String, Object>) compraCestaService.findCompraCesta_PagWithFilter_dSUE(parametrosFiltro);

            totalPaginas = (Integer) compraCestaMap.get("totalPaginas");
            totalRegistros = (Integer) compraCestaMap.get("totalRegistros");
            paginaActual = (Integer) compraCestaMap.get("paginaActual");
            List<CompraCesta> cestas = (List<CompraCesta>) compraCestaMap.get("cestas");

            List<String> estadosLinea = new ArrayList();
            estadosLinea.add("AC");
            SimpleDateFormat fechaFormat = new SimpleDateFormat("yyyyMMdd");
            for (CompraCesta compraCesta : cestas) {
                Map<String, Object> cestaJson = new HashMap<String, Object>();

                if (compraCesta.getAtributosAuditoria().getCreated() != null) {
                    cestaJson.put("create", Integer.parseInt(fechaFormat.format(compraCesta.getAtributosAuditoria().getCreated().getTime())));
                } else {
                    cestaJson.put("create", null);
                }
                if (compraCesta.getAtributosAuditoria().getUpdated() != null) {
                    cestaJson.put("update", Integer.parseInt(fechaFormat.format(compraCesta.getAtributosAuditoria().getUpdated().getTime())));
                } else {
                    cestaJson.put("update", null);
                }
                if (compraCesta.getAtributosAuditoria().getDeleted() != null) {
                    cestaJson.put("delete", Integer.parseInt(fechaFormat.format(compraCesta.getAtributosAuditoria().getDeleted().getTime())));
                } else {
                    cestaJson.put("delete", null);
                }

                cestaJson.put("estadoReg", compraCesta.getAtributosAuditoria().getEstadoRegistro());
                cestaJson.put("fechaPrevistaActivacion", compraCesta.getAtributosAuditoria().getFechaPrevistaActivacion());
                cestaJson.put("fechaPrevistaDesactivacion", compraCesta.getAtributosAuditoria().getFechaPrevistaDesactivacion());
                cestaJson.put("ultimaAccion", compraCesta.getAtributosAuditoria().getUltimaAccion());
                cestaJson.put("usuarioUltimaAccion", compraCesta.getAtributosAuditoria().getUsuarioUltimaAccion());

                cestaJson.put("cestaDescripcion", compraCesta.getCestaDescripcion());
                cestaJson.put("cestaEstado", compraCesta.getCestaEstado());
                cestaJson.put("fechaEntregaCesta", compraCesta.getFechaEntregaCesta());
                cestaJson.put("cestaId", compraCesta.getIdCesta());
                cestaJson.put("moneda", compraCesta.getMoneda());
                cestaJson.put("personaId", compraCesta.getPersona().getIdPersona());   // TODO
                cestaJson.put("sociedadId", compraCesta.getSociedad().getIdSociedad());

                List<String[]> proveedorList = compraLineaService.findCompraLinea_ProvList(compraCesta, estadosLinea);
                cestaJson.put("proveedorList", proveedorList);

                cestaJsonList.add(cestaJson);
            }
        } catch (Exception ex) {
            throw (ex);
        }

        reply.put("totalPaginas", totalPaginas);
        reply.put("totalRegistros", totalRegistros);
        reply.put("paginaActual", paginaActual);
        reply.put("cestaJsonList", cestaJsonList);

        return reply;

    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/cestacabcrud", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Cesta de la compra", notes = "cestaCompraMap, formato de comunicacion con cliente")
    Map<String, Object> cestaCabCrud(
            @RequestBody Map<String, Object> cestaCabMap,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        Map<String, Object> reply = new HashMap();
        CompraCesta compraCesta = null;
        List<Object> createdObjects = new ArrayList();
        List<Object> updatedbjects = new ArrayList();

        try {
            cestaCabMap.put("idUser", idUser);
            validacion_CestaCabCrud(cestaCabMap);
            compraCesta = compraRestService.montaCompraCesta(cestaCabMap);
            if (KApp.POST.getKApp().equals(compraCesta.getAtributosAuditoria().getUltimaAccion())) {
                createdObjects.add(compraCesta);
            } else {
                updatedbjects.add(compraCesta);
            }
            compraRestService.persistenciaCompra(createdObjects, updatedbjects);

            reply = compraCestaCab_Json(compraCestaService.findCompraCesta(compraCesta.getIdCesta()));
        } catch (Exception ex) {
            ex.printStackTrace();
             StackTraceElement[] stackTraceElements = ex.getStackTrace();
             log.warn(stackTraceElements[0] + " - - " +stackTraceElements[stackTraceElements.length - 1]);
            throw ex;
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/lineacrud", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Cesta de la compra", notes = "cestaCompraMap, formato de comunicacion con cliente")
    Map<String, Object> lineaCrud(
            @RequestBody Map<String, Object> lineaMap,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        Map<String, Object> reply = new HashMap();
        CompraLinea compraLinea = null;
        List<Object> createdObjects = new ArrayList();
        List<Object> updatedbjects = new ArrayList();

        try {
            lineaMap.put("idUser", idUser);
            validacion_LineaCrud(lineaMap);
            compraLinea = compraRestService.montaCompraLinea(lineaMap);
            if (KApp.POST.getKApp().equals(compraLinea.getAtributosAuditoria().getUltimaAccion())) {
                createdObjects.add(compraLinea);
            } else {
                updatedbjects.add(compraLinea);
            }
            compraRestService.persistenciaCompra(createdObjects, updatedbjects);

            reply = compraCestaLinea_Json(compraLineaService.findCompraLinea(compraLinea.getCompraLineaId()));

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/cestauno/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "REcupera los datos de una cesta", notes = " formato de comunicacion con cliete")
    Map<String, Object> getCesta(
            @PathVariable String id,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        Map<String, Object> reply = new HashMap();

        reply = (compraCesta_Json(compraCestaService.findCompraCesta(id), compraLineaService.findCompraLineasCesta(id)));

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/cestadistribucion", method = RequestMethod.POST)
    @ApiOperation(value = "Distribuye la cesta en pedidos", notes = "cestaCompraMap, formato de comunicacion con cliente")
    List<Map<String, Object>> distribucionCesta(
            @RequestBody List<Map<String, Object>> cestaCompraMap,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        List<Map<String, Object>> reply = new ArrayList<>();

        /*
         [{cestaId: 'd838583f42ac4ad4b666458537b38e10', 
         pedidoCabecera:[{proveedorId:"nnnnnnn",
         fechaEntrega:yyyyMMdd,
         comentario:"nnnnnnnnnn"},
         {   proveedorId:"nnnnnnn",
         fechaEntrega:yyyyMMdd,
         comentario:"nnnnnnnnnn"}
         ]}];
         */
        String idCesta = null;

        if (cestaCompraMap == null || cestaCompraMap.isEmpty() || cestaCompraMap.size() == 0) {
            Object[] valores = {idCesta};
            String mensaje = MessageFormat.format("ERROR: No hay cestas para tratar. ", valores);
            log.error(mensaje);
            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
        }

        for (Map<String, Object> cestaIdMap : cestaCompraMap) {
            idCesta = (String) cestaIdMap.get("cestaId");
            CompraCesta compraCesta = null;
            try {
                compraCesta = compraCestaService.findCompraCesta(idCesta, KApp.CESTA_ABIERTA.getKApp());
            } catch (Exception ex) {
                Object[] valores = {idCesta};
                String mensaje = MessageFormat.format("ERROR: Distribucion Cesta: Cesta {0}, no encontrada o bloqueada por usuario. " + ex.getMessage(), valores);
                log.error(mensaje + ex.getMessage());
                throw new Exception(MessageFormat.format(mensaje, new Throwable()));
            }

            // Bloquea la cesta
            compraRestService.modificaCompraCestaEstado(compraCesta, KApp.CESTA_BLOQUEADA.getKApp(), idUser);

            try {
                List<HashMap> pedidosCabeceras = (List<HashMap>) cestaIdMap.get("pedidoCabecera");
                if (pedidosCabeceras.size() == 0) {
                    Object[] valores = {idCesta};
                    String mensaje = MessageFormat.format("ERROR: La cesta {0}, no tiene cabeceras de pedido. ", valores);
                    log.error(mensaje);
                    throw new Exception(MessageFormat.format(mensaje, new Throwable()));
                } else {
                    for (HashMap pedidoCabecera : pedidosCabeceras) {

                        String proveedorId = (String) pedidoCabecera.get("proveedorId");
                        Integer fechaEntrega = (Integer) pedidoCabecera.get("fechaEntrega");
                        String comentario = (String) pedidoCabecera.get("comentario");

                        Sociedad proveedor = sociedadService.findSociedad(proveedorId);
                        if (proveedor == null) {
                            Object[] valores = {proveedorId};
                            String mensaje = MessageFormat.format("ERROR: Crea Pedido: Entidad Sociedad Proveedora {0}, no se ha encontrad. ", valores);
                            log.error(mensaje);
                            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
                        }

                        CompraPedido compraPedido = compraRestService.montaPedidoDistribucion(compraCesta, proveedor, KApp.POST.getKApp(), idUser, KApp.ACTIVO.getKApp(), fechaEntrega, comentario);
                        try {
                            compraRestService.persistenceGrabaPedido(compraPedido);

                        } catch (Exception ex) {
                            Object[] valores = {proveedorId};
                            String mensaje = MessageFormat.format("ERROR: Pedido no creado par el Proveedor {0}. " + "-" + ex.getMessage(), valores);
                            log.error(mensaje);
                            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
                        }
                    }
                }

            } catch (Exception ex) {
                // Abre la cesta
                compraRestService.modificaCompraCestaEstado(compraCesta, KApp.CESTA_ABIERTA.getKApp(), idUser);
                log.error(ex.getMessage());
                throw ex;
            }

            // Cierra la cesta
            compraRestService.modificaCompraCestaEstado(compraCesta, KApp.CESTA_CERRADA.getKApp(), idUser);

        }

        for (Map<String, Object> cestaIdMap : cestaCompraMap) {
            idCesta = (String) cestaIdMap.get("cestaId");
            Map<String, Object> cesta = compraCesta_Json(compraCestaService.findCompraCesta(idCesta), compraLineaService.findCompraLineasCesta(idCesta));
            reply.add(cesta);
        }

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    /* Este método es viejo y desaparecerá */
    @RequestMapping(value = "/cestacrud", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Cesta de la compra", notes = "cestaCompraMap, formato de comunicacion con cliente")
    Map<String, Object> postCesta(
            @RequestBody Map<String, Object> compraCestaMap,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        Map<String, Object> reply = new HashMap();
        List<Object[]> listaObjetos = new ArrayList();

        String cestaId = null;
        String cestaEstado = null;
        boolean cestaFavorita = false;

        String cestaDescripcion = null;
        String sociedadId = null;
        String personaId = null;
        int fechaEntrega = 0;

        String moneda = null;
        String methodRegistro = null;
        String estadoRegistro = null;

        try {
            cestaId = (String) compraCestaMap.get("cestaId");
            cestaEstado = (String) compraCestaMap.get("cestaEstado");
            cestaFavorita = (boolean) compraCestaMap.get("cestaFavorita");

            sociedadId = (String) compraCestaMap.get("sociedadId");
            cestaDescripcion = (String) compraCestaMap.get("cestaDescripcion");
            personaId = (String) compraCestaMap.get("personaId");
            fechaEntrega = (int) compraCestaMap.get("fechaEntrega");

            moneda = (String) compraCestaMap.get("moneda");
            methodRegistro = (String) compraCestaMap.get("method");
            estadoRegistro = (String) compraCestaMap.get("estado");

        } catch (Exception e) {
            throw new Exception("CentralCompras. CRUD Cesta." + e.getMessage());
        }

        List<Map<String, Object>> lineasCompra = new ArrayList<>();
        try {
            lineasCompra = (List<Map<String, Object>>) compraCestaMap.get("lineasCompra");
        } catch (Exception e) {
            throw new Exception("CentralCompras. CRUD Cesta." + e.getMessage());
        }

        if (("0").equals(cestaId) && !(KApp.POST.getKApp()).equals(methodRegistro) || !("0").equals(cestaId) && (KApp.POST.getKApp()).equals(methodRegistro)) {
            throw new Exception("CentralCompras. CRUD Cesta. Id-cesta  y Metodo no estan sincronizados");
        }

        Object[] aaa = new Object[4];
        aaa = compraRestService.montaCompraCesta(cestaId, sociedadId, personaId, cestaEstado, moneda, methodRegistro, estadoRegistro, fechaEntrega, cestaFavorita, idUser, cestaDescripcion);
        CompraCesta compraCesta = (CompraCesta) aaa[2];     // Para busqueda al final del método y presentación de la persistencia
        listaObjetos.add(aaa);

        for (Map<String, Object> lineaCompra : lineasCompra) {
            Object[] bbb = new Object[4];
            bbb = compraRestService.montaCompraLinea(compraCesta, null, lineaCompra, idUser, cestaEstado, null);
            listaObjetos.add(bbb);
        }

        try {
            compraRestService.persistenciaCompra(listaObjetos);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("CentralCompras. Persistence Cesta." + ex.getMessage());
        }

        reply = compraCesta_Json(compraCestaService.findCompraCesta(compraCesta.getIdCesta()), compraLineaService.findCompraLineasCesta(compraCesta.getIdCesta()));

        return reply;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------------------------------------------------------------
    private Map<String, Object> compraCesta_Json(CompraCesta compraCesta, List<CompraLinea> compraLineas) {
        Map<String, Object> reply = compraCestaCab_Json(compraCesta);

        List<Map<String, Object>> lineasMap = new ArrayList();
        for (CompraLinea compraLinea : compraLineas) {
            Map<String, Object> lineaMap = new HashMap<>();
            lineasMap.add(compraCestaLinea_Json(compraLinea));
        }
        reply.put("lineasCompra", lineasMap);
        return reply;
    }

    private Map<String, Object> compraCestaCab_Json(CompraCesta compraCesta) {

        Map<String, Object> reply = new HashMap<>();

        reply.put("cestaId", compraCesta.getIdCesta());
        reply.put("sociedadId", compraCesta.getSociedad() != null ? compraCesta.getSociedad().getIdSociedad() : "");
        reply.put("personaId", compraCesta.getPersona().getIdPersona());
        reply.put("fechaEntregaCesta", compraCesta.getFechaEntregaCesta());
        reply.put("cestaEstado", compraCesta.getCestaEstado());
        reply.put("cestaFavorita", compraCesta.isCestaFavorita());
        reply.put("moneda", compraCesta.getMoneda());
        reply.put("cestaDescripcion", compraCesta.getCestaDescripcion());
        reply.put("method", compraCesta.getAtributosAuditoria().getUltimaAccion());
        reply.put("estado", compraCesta.getAtributosAuditoria().getEstadoRegistro());
        reply.put("fechaPrevistaActivacion", compraCesta.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.put("fechaPrevistaDesactivacion", compraCesta.getAtributosAuditoria().getFechaPrevistaDesactivacion());

        return reply;
    }

    private Map<String, Object> compraCestaLinea_Json(CompraLinea compraLinea) {

        Map<String, Object> reply = new HashMap<>();

        reply.put("importeIva", (BigDecimal) compraLinea.getImporteIva());
        reply.put("precioAplicado", (BigDecimal) compraLinea.getPrecioAplicado());
        reply.put("articuloId", (String) compraLinea.getArticulo().getIdArticulo());
        reply.put("estado", (String) compraLinea.getAtributosAuditoria().getEstadoRegistro());
        reply.put("ivaDesc", compraLinea.getTipoIva().getPorcentajeIva());
        reply.put("cestaLin", (int) compraLinea.getCestaLin());
        reply.put("importeLineaConIva", (BigDecimal) compraLinea.getImporteLineaConIva());
        reply.put("cantidadPedida", (BigDecimal) compraLinea.getCantidadPedida());
        reply.put("descuentoAplicado", (BigDecimal) compraLinea.getDescuentoAplicado());
        reply.put("compraLineaId", (String) compraLinea.getCompraLineaId());
        reply.put("cestaDesc", compraLinea.getCesta().getCestaDescripcion());
        reply.put("lineaEstado", (String) compraLinea.getLineaEstado());
        reply.put("pedidoDesc", compraLinea.getPedido() != null ? (String) (compraLinea.getPedido().getPedidoNumero() + " - " + compraLinea.getPedido().getCompradorSociedad().getNombre()) : null);
        reply.put("idUser", compraLinea.getAtributosAuditoria().getUsuarioUltimaAccion());
        reply.put("ivaId", compraLinea.getTipoIva().getTipoIvaId());
        reply.put("cestaId", (String) compraLinea.getCesta().getIdCesta());
        reply.put("pedidoId", compraLinea.getPedido() != null ? (String) compraLinea.getPedido().getCompraPedidoId() : null);
        reply.put("pedidoLin", (int) compraLinea.getPedidoLin());
        reply.put("importeLineaSinIva", (BigDecimal) compraLinea.getImporteLineaSinIva());
        reply.put("descuentoPorcAplicado", (BigDecimal) compraLinea.getDescuentoPorcAplicado());
        reply.put("method", compraLinea.getAtributosAuditoria().getUltimaAccion());
        reply.put("articuloDesc", compraLinea.getArticulo().getDescrArticulo());

        return reply;
    }

    private List<ArticuloJson> articuloList_Json(List<Object[]> articulos_aup) {
        List<ArticuloJson> articuloJsonList = new ArrayList();
        for (Object[] articulo_aup : articulos_aup) {
            ArticuloProducto articuloProducto = (ArticuloProducto) articulo_aup[0];
            //AlmacenUsuarioProducto almacenUsuarioProducto = (AlmacenUsuarioProducto) articulo_aup[2];
            ArticuloJson articuloJson = articulo_Json(articuloProducto.getArticulo());
            articuloJson.setEstado(articuloProducto.getAtributosAuditoria().getEstadoRegistro());
            articuloJson.setProductoId(articuloProducto.getProducto().getIdProducto());
            //articuloJson.setModoCompra(almacenUsuarioProducto.getModoCompra());
            //articuloJson.setVerPrecio(almacenUsuarioProducto.isVerPrecio());
            articuloJson.setModoCompra("1");
            articuloJson.setVerPrecio(true);
            articuloJsonList.add(articuloJson);
        }
        return articuloJsonList;
    }

    private ArticuloJson articulo_Json(Articulo articulo) {

        ArticuloJson articuloJson = new ArticuloJson();
        articuloJson.setCodiParaProveedor(articulo.getCodiParaProveedor());
        articuloJson.setCodigoEAN(articulo.getCodigoEAN());
        articuloJson.setEstado(articulo.getAtributosAuditoria().getEstadoRegistro());
        articuloJson.setFormatoVentaId(articulo.getFormatoVta().getIdFormatoVta());
        articuloJson.setId(articulo.getIdArticulo());
        articuloJson.setMoneda(articulo.getMoneda());
        articuloJson.setPrecioUniMed(articulo.getPrecioUniMed().doubleValue());
        articuloJson.setPrecioUniMedNuevo(articulo.getPrecioUniMedNuevo().doubleValue());

        articuloJson.setSociedadId(articulo.getSociedad().getIdSociedad());
        articuloJson.setSociedadNombre(articulo.getSociedad().getNombre());
        articuloJson.setUniMinVta(articulo.getUniMinVta().doubleValue());
        articuloJson.setUnidadFormatoVta(articulo.getUnidadFormatoVta().doubleValue());

        Producto producto = articuloProductoService.findProductosBy_A(articulo.getIdArticulo());
        articuloJson.setProductoId(producto.getIdProducto());

        List<ArticuloLangNomb> articulosLangNomb = articuloLangNombService.findArticuloLangNombBy_A(articulo);
        List<TraduccionDescripcion> traduccionesNomb = new ArrayList<>();
        for (ArticuloLangNomb articuloLangNombEle : articulosLangNomb) {
            TraduccionDescripcion traduccionNombre = new TraduccionDescripcion();
            traduccionNombre.setIdioma(articuloLangNombEle.getIdioma());
            traduccionNombre.setTraduccion(articuloLangNombEle.getTraduccionNomb());
            traduccionesNomb.add(traduccionNombre);
        }
        articuloJson.setTraduccionesNombre(traduccionesNomb);

        return articuloJson;
    }

    private void productosTypeAheadToJson(List<Map<String, Object>> reply, List<Object[]> prodS) {
        log.info("....................................................................mbm............CONTROL");

        for (Object[] prod : prodS) {
            Map<String, Object> productoMap = new HashMap();
            List<TraduccionDescripcion> traduccionesDescripcionPrd = new ArrayList();
            Producto p = (Producto) prod[0];
            List<ProductoLang> productosLang = productoLangService.findProductoLangBy_IdP((String) p.getIdProducto());
            int nivel = 0;
            for (ProductoLang productoLangEle : productosLang) {
                TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
                traduccionDescripcion.setIdioma(productoLangEle.getIdioma());
                traduccionDescripcion.setTraduccion(productoLangEle.getTraduccionDesc());
                traduccionesDescripcionPrd.add(traduccionDescripcion);
                nivel = productoLangEle.getProductoEntity().getNivelInt();
            }

            productoMap.put("traducciones", traduccionesDescripcionPrd);
            productoMap.put("id", p.getIdProducto());
            productoMap.put("parent", p.getIdPadreJerarquia());

            reply.add(productoMap);
        }

    }

    private void articulosTypeAheadToJson(List<Map<String, Object>> reply, List<Object[]> prodS) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        List<TraduccionDescripcion> traduccionesDescripcionArt = new ArrayList();

        for (Object[] prod : prodS) {
            ArticuloProducto articuloProducto = (ArticuloProducto) prod[0];
            Map<String, Object> articulo = new HashMap();

            articulo.put("id", articuloProducto.getArticuloIdk());

            traduccionesDescripcionArt = new ArrayList();
            List<ArticuloLangNomb> articulosLangNomb = articuloLangNombService.findArticuloLangNombBy_IdA(articuloProducto.getArticuloIdk());
            for (ArticuloLangNomb articuloLangNombEle : articulosLangNomb) {
                TraduccionDescripcion traduccionDescripcion = new TraduccionDescripcion();
                traduccionDescripcion.setIdioma(articuloLangNombEle.getIdioma());
                traduccionDescripcion.setTraduccion(articuloLangNombEle.getTraduccionNomb());
                traduccionesDescripcionArt.add(traduccionDescripcion);
            }
            articulo.put("traduccionesDescripcionArt", traduccionesDescripcionArt);

            reply.add(articulo);
        }
    }

    private void validacion_CestaCabCrud(Map<String, Object> cestaCabMap) throws Exception {
        Sociedad sociedad = null;
        Persona persona = null;

        try {
            String cestaId = (String) cestaCabMap.get("cestaId");
            String cestaDescripcion = (String) cestaCabMap.get("cestaDescripcion");
            String cestaEstado = (String) cestaCabMap.get("cestaEstado");
            boolean cestaFavorita = (boolean) cestaCabMap.get("cestaFavorita");
            String sociedadId = (String) cestaCabMap.get("sociedadId");
            String personaId = (String) cestaCabMap.get("personaId");
            int fechaEntrega = (int) cestaCabMap.get("fechaEntrega");
            String moneda = (String) cestaCabMap.get("moneda");
            String method = (String) cestaCabMap.get("method");

            sociedad = sociedadService.findSociedad(sociedadId);
            if (sociedad == null) {
                Object[] valores = {sociedadId};
                throw new Exception(MessageFormat.format("ERROR: Cesta: Sociedad {0}, no encontrada. ", valores));
            } else {
                cestaCabMap.put("sociedadEntity", sociedad);
            }

            persona = personaService.findPersona(personaId);
            if (persona == null) {
                Object[] valores = {personaId};
                throw new Exception(MessageFormat.format("ERROR: Cesta: Persona {0}, no encontrada. ", valores));
            } else {
                cestaCabMap.put("personaEntity", persona);
            }

            if (("0").equals(cestaId) && !(KApp.POST.getKApp()).equals(method) || !("0").equals(cestaId) && (KApp.POST.getKApp()).equals(method)) {
                throw new Exception("CentralCompras. CRUD Cesta. Id-Linea cesta  y Metodo no estan sincronizados");
            }

        } catch (Exception e) {
            throw new Exception("CentralCompras. CRUD Cesta." + e.getMessage());
        }

    }

    private void validacion_LineaCrud(Map<String, Object> lineaMap) throws Exception {
        Articulo articulo = null;
        CompraCesta compraCesta = null;
        CompraPedido compraPedido = null;

        try {
            String lineaCompraId = (String) lineaMap.get("compraLineaId");
            String cestaId = (String) lineaMap.get("cestaId");
            String pedidoId = (String) lineaMap.get("pedidoId");
            String articuloId = (String) lineaMap.get("articuloId");
            String tipoIvaId = (String) lineaMap.get("tipoIvaId");
            String lineaEstado = (String) lineaMap.get("lineaEstado");
            String method = (String) lineaMap.get("method");

            BigDecimal cantidadPedida = new BigDecimal(lineaMap.get("cantidadPedida").toString()).setScale(4, RoundingMode.HALF_EVEN);
            BigDecimal precioAplicado = new BigDecimal(lineaMap.get("precioAplicado").toString()).setScale(4, RoundingMode.HALF_EVEN);
            BigDecimal descuentoPorcAplicado = new BigDecimal(lineaMap.get("descuentoPorcAplicado").toString()).setScale(4, RoundingMode.HALF_EVEN);
            BigDecimal descuentoAplicado = new BigDecimal(lineaMap.get("descuentoAplicado").toString()).setScale(4, RoundingMode.HALF_EVEN);
            BigDecimal importeLineaSinIva = new BigDecimal(lineaMap.get("importeLineaSinIva").toString()).setScale(4, RoundingMode.HALF_EVEN);
            BigDecimal importeIva = new BigDecimal(lineaMap.get("importeIva").toString()).setScale(4, RoundingMode.HALF_EVEN);
            BigDecimal importeLineaConIva = new BigDecimal(lineaMap.get("importeLineaConIva").toString()).setScale(4, RoundingMode.HALF_EVEN);

            compraCesta = compraCestaService.findCompraCesta(cestaId);
            if (compraCesta == null) {
                Object[] valores = {cestaId};
                throw new Exception(MessageFormat.format("ERROR: LineaPedido: cestaId {0}, no encontrada. ", valores));
            } else {
                lineaMap.put("compraCestaEntity", compraCesta);
            }

            if (pedidoId != null) {
                compraPedido = compraPedidoService.findCompraPedido(pedidoId);
                if (compraPedido == null) {
                    Object[] valores = {pedidoId};
                    throw new Exception(MessageFormat.format("ERROR: LineaPedido: pedidoId {0}, no encontrada. ", valores));
                }
            }
            lineaMap.put("compraPedidoEntity", compraPedido);

            articulo = articuloService.findArticulo(articuloId);
            if (articulo == null) {
                Object[] valores = {articuloId};
                throw new Exception(MessageFormat.format("ERROR: LineaPedido: articuloId {0}, no encontrada. ", valores));
            } else {
                lineaMap.put("articuloEntity", articulo);
            }

            TipoIva tipoIva = tipoIvaService.findTipoIva(tipoIvaId);
            if (tipoIva == null) {
                Object[] valores = {tipoIva};
                throw new NonexistentEntityException(MessageFormat.format("Cesta Crud: Id  de Iva{0}, no encontrad", valores));
            } else {
                lineaMap.put("tipoIvaEntity", tipoIva);
            }

            if (("0").equals(lineaCompraId) && !(KApp.POST.getKApp()).equals(method) || !("0").equals(lineaCompraId) && (KApp.POST.getKApp()).equals(method)) {
                throw new Exception("CentralCompras. CRUD Cesta. Id-Linea cesta  y Metodo no estan sincronizados");
            }

        } catch (Exception e) {
            throw new Exception("CentralCompras. CRUD Cesta." + e.getMessage());
        }

    }

    private void validacion_CestaPag(Map<String, Object> parametrosFiltro) throws Exception {
        Sociedad sociedad = null;
        Persona usuario = null;

        Calendar createdIni = Calendar.getInstance();
        Calendar createdFin = Calendar.getInstance();
        Integer numeroRegistrosPagina = 0;
        Integer paginaActual = 0;

        SimpleDateFormat inFormat = new SimpleDateFormat("yyyyMMdd");

        try {
            if (!parametrosFiltro.containsKey("createdIni") || parametrosFiltro.get("createdIni") == null || (int) parametrosFiltro.get("createdIni") == 0) {
                createdIni.setTime(inFormat.parse(String.valueOf(19000101)));
            } else {
                createdIni.setTime(inFormat.parse(String.valueOf((Integer) parametrosFiltro.get("createdIni"))));
            }
            parametrosFiltro.put("createdIni", createdIni);

            if (!parametrosFiltro.containsKey("createdFin") || parametrosFiltro.get("createdFin") == null || (int) parametrosFiltro.get("createdFin") == 0) {
                createdFin.setTime(inFormat.parse(String.valueOf(29990101)));
            } else {
                createdFin.setTime(inFormat.parse(String.valueOf((Integer) parametrosFiltro.get("createdFin"))));
            }
            parametrosFiltro.put("createdFin", createdFin);

            if (!parametrosFiltro.containsKey("numeroRegistrosPagina") || (Integer) parametrosFiltro.get("numeroRegistrosPagina") == 0) {
                numeroRegistrosPagina = 5;
            } else {
                numeroRegistrosPagina = (Integer) parametrosFiltro.get("numeroRegistrosPagina");
            }
            parametrosFiltro.replace("numeroRegistrosPagina", numeroRegistrosPagina);

            if (!parametrosFiltro.containsKey("paginaActual") || (Integer) parametrosFiltro.get("paginaActual") == 0) {
                paginaActual = 1;
            } else {
                paginaActual = (Integer) parametrosFiltro.get("paginaActual");
            }
            parametrosFiltro.replace("paginaActual", paginaActual);

            if (!parametrosFiltro.containsKey("sociedad") || parametrosFiltro.get("sociedad") == null) {
                throw new Exception("Sociedad id no informada");
            } else {
                sociedad = sociedadService.findSociedad((String) parametrosFiltro.get("sociedad"));
                if (sociedad == null) {
                    throw new Exception("Sociedad id no informada");
                } else {
                    parametrosFiltro.put("sociedadEntity", (Sociedad) sociedad);
                }
            }

            if (!parametrosFiltro.containsKey("persona") || parametrosFiltro.get("persona") == null) {
                throw new Exception("Persona id no informada");
            } else {
                usuario = personaService.findPersona((String) parametrosFiltro.get("persona"));
                if (usuario == null) {
                    throw new Exception("Persona id no informada");
                } else {
                    parametrosFiltro.put("usuarioEntity", usuario);
                }
            }

        } catch (Exception e) {
            throw new Exception("CentralCompras. CRUD Cesta." + e.getMessage());
        }
    }

    private void validacion_CestaArticuloPag(Map<String, Object> parametrosFiltro) throws Exception {
        CompraCesta compraCesta = null;
        Integer numeroRegistrosPagina = 0;
        Integer paginaActual = 0;

        try {
            if (!parametrosFiltro.containsKey("numeroRegistrosPagina") || (Integer) parametrosFiltro.get("numeroRegistrosPagina") == 0) {
                numeroRegistrosPagina = 5;
            } else {
                numeroRegistrosPagina = (Integer) parametrosFiltro.get("numeroRegistrosPagina");
            }
            parametrosFiltro.replace("numeroRegistrosPagina", numeroRegistrosPagina);

            if (!parametrosFiltro.containsKey("paginaActual") || (Integer) parametrosFiltro.get("paginaActual") == 0) {
                paginaActual = 1;
            } else {
                paginaActual = (Integer) parametrosFiltro.get("paginaActual");
            }
            parametrosFiltro.replace("paginaActual", paginaActual);

            if (!parametrosFiltro.containsKey("cestaId") || parametrosFiltro.get("cestaId") == null) {
                throw new Exception("Cesta id no informada");
            } else {
                compraCesta = compraCestaService.findCompraCesta((String) parametrosFiltro.get("cestaId"));
                if (compraCesta == null) {
                    throw new Exception("Cesta id no informada");
                } else {
                    parametrosFiltro.put("compraCestaEntity", compraCesta);
                }
            }

            List<String> estados = (List<String>) parametrosFiltro.get("estados");
            String idioma = (String) parametrosFiltro.get("idioma");

        } catch (Exception e) {
            throw new Exception("CentralCompras. Lista articulos Cesta." + e.getMessage());
        }

    }

    private void validacion_AlmacenUsuarioArticuloPag(Map<String, Object> parametrosFiltro) throws Exception {
        Almacen almacen = null;
        UserCentral userCentral = null;
        Producto producto = null;
        Integer numeroRegistrosPagina = 0;
        Integer paginaActual = 0;
        Integer cantidad = 0;

        try {

            //
            //
            /* ......................................................................................................................................................................................................
             * Parametros necesarios en las tres solicitudes: Paginación,  TypeAhead Articulos y TypeAhead Prodcutos,s
             * ......................................................................................................................................................................................................
             */
            if (!parametrosFiltro.containsKey("almacenId") || parametrosFiltro.get("almacenId") == null) {                                      /* ------------- almacenId ------------- */

                throw new Exception("Almacen id no informado");
            } else {
                almacen = almacenService.findAlmacen((String) parametrosFiltro.get("almacenId"));
                if (almacen == null) {
                    throw new Exception("Almacen id no encontrado");
                } else {
                    parametrosFiltro.put("almacenEntity", almacen);
                }
            }

            if (!parametrosFiltro.containsKey("usuarioId") || parametrosFiltro.get("usuarioId") == null) {                                      /* ------------- usuarioId ------------- */

                throw new Exception("Usuario id no informado");
            } else {
                userCentral = userCentralService.findUserCentral((String) parametrosFiltro.get("usuarioId"));
                if (userCentral == null) {
                    throw new Exception("Persona id no encontrado");
                } else {
                    parametrosFiltro.put("userCentralEntity", userCentral);
                }
            }

            ZonaGeografica zonaGeografica = almacen.getZonaGeografica();                                                                        /* ------------- zonaGeografica y zonasPadreAlmacen ------------- */

            if (zonaGeografica == null) {
                throw new Exception("zonaGeografica no puede ser null");
            } else {
                parametrosFiltro.put("zonaGeograficaEntity", zonaGeografica);
            }
            StringTokenizer st = new StringTokenizer(zonaGeografica.getRoute(), ".");
            List<String> zonasPadreAlmacen = new ArrayList<>();
            while (st.hasMoreTokens()) {
                zonasPadreAlmacen.add(st.nextToken());
            }
            parametrosFiltro.put("zonasPadreAlmacen", zonasPadreAlmacen);

            List<String> estados = new ArrayList();                                                                                          /* ------------- estados ------------- */

            estados.add(KApp.ACTIVO.getKApp());
            parametrosFiltro.put("estados", estados);

            //parametrosFiltro.put("asignada", Boolean.TRUE);                                                                                   /* ------------- asigmada ------------- */
            //
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");                                                                            /* ------------- hoy ------------- */

            Calendar hoyCalendar = Calendar.getInstance();
            Date hoyDate = hoyCalendar.getTime();
            String hoyString = sdf.format(hoyDate);
            Integer hoy = Integer.parseInt(hoyString);
            parametrosFiltro.put("hoy", hoy);

            //
            //
            /* ......................................................................................................................................................................................................
             * Parametros necesarios solo para la paginacion
             * ......................................................................................................................................................................................................
             */
            if ("paginacion".equals(parametrosFiltro.get("solicitud"))) {

                numeroRegistrosPagina = (!parametrosFiltro.containsKey("numeroRegistrosPagina") || (Integer) parametrosFiltro.get("numeroRegistrosPagina") == 0) ? 5 : (Integer) parametrosFiltro.get("numeroRegistrosPagina");
                if (!parametrosFiltro.containsKey("numeroRegistrosPagina")) {
                    parametrosFiltro.put("numeroRegistrosPagina", numeroRegistrosPagina);
                } else {
                    parametrosFiltro.replace("numeroRegistrosPagina", numeroRegistrosPagina);
                }

                paginaActual = (!parametrosFiltro.containsKey("paginaActual") || (Integer) parametrosFiltro.get("paginaActual") == 0) ? 1 : (Integer) parametrosFiltro.get("paginaActual");
                if (!parametrosFiltro.containsKey("paginaActual")) {
                    parametrosFiltro.put("paginaActual", paginaActual);
                } else {
                    parametrosFiltro.replace("paginaActual", paginaActual);
                }

                if (!parametrosFiltro.containsKey("productoId") || parametrosFiltro.get("productoId") == null) {                                /* ------------- productoId --------------- */

                    throw new Exception("Producto id no informado");
                } else {
                    producto = productoService.findProducto((String) parametrosFiltro.get("productoId"));
                    if (producto == null) {
                        throw new Exception("Producto id no encontrado");
                    } else {
                        parametrosFiltro.put("productoEntity", producto);
                    }
                }

            }

            //
            //
            /* ......................................................................................................................................................................................................
             * Parametros necesarios solo para TypeAhead Articulo
             * ......................................................................................................................................................................................................
             */
            if ("paginacion".equals(parametrosFiltro.get("solicitud"))) {

                parametrosFiltro.remove("descripcionProd");
                if (!parametrosFiltro.containsKey("cadenaArt")) {                                                                               /* ------------- cadenaArt / idioma ------------- */

                    throw new Exception("Atriburo cadena no informado");
                } else {
                    if (!parametrosFiltro.containsKey("idioma") || parametrosFiltro.get("idioma") == null) {
                        throw new Exception("Idioma no informado");
                    }
                }

                if (!parametrosFiltro.containsKey("productoId") || parametrosFiltro.get("productoId") == null) {                                /* ------------- productoId --------------- */

                    throw new Exception("Producto id no informado");
                } else {
                    producto = productoService.findProducto((String) parametrosFiltro.get("productoId"));
                    if (producto == null) {
                        throw new Exception("Producto id no encontrado");
                    } else {
                        parametrosFiltro.put("productoEntity", producto);
                    }
                }
            }

            //
            //
            /* ......................................................................................................................................................................................................
             * Parametros necesarios solo para TypeAhead Producto
             * ......................................................................................................................................................................................................
             */
            if ("typeAheadProducto".equals(parametrosFiltro.get("solicitud"))) {

                parametrosFiltro.remove("cadenaArt");
                if (!parametrosFiltro.containsKey("descripcionProd")) {                                                                               /* ------------- descripcionProd / idioma ------------- */

                    throw new Exception("Atriburo descripcionProd no informado");
                } else {
                    if (!parametrosFiltro.containsKey("idioma") || parametrosFiltro.get("idioma") == null) {
                        throw new Exception("Idioma no informado");
                    }
                }

            }

            //
            //
            /* ......................................................................................................................................................................................................
             * Parametros necesarios solo para TypeAhead ( _Articulo y _Producto )
             * ......................................................................................................................................................................................................
             */
            if ("typeAheadArticulo".equals(parametrosFiltro.get("solicitud")) || "typeAheadProducto".equals(parametrosFiltro.get("solicitud"))) {
                cantidad = (!parametrosFiltro.containsKey("cantidad") || (Integer) parametrosFiltro.get("cantidad") == 0) ? 5 : (Integer) parametrosFiltro.get("cantidad");  /* ------------- cantidad ---------------- */

                if (!parametrosFiltro.containsKey("cantidad")) {
                    parametrosFiltro.put("cantidad", cantidad);
                } else {
                    parametrosFiltro.replace("cantidad", cantidad);
                }
            }

        } catch (Exception ex) {
            throw new Exception("CentralCompras. Lista articulos para selección de compra." + ex.getMessage());
        }
    }

    public void recurrenteSocProducto_Asignado(String productoId, List<String> estados, Boolean asignado, List<List<String>> _sociedadesSirvenProdcuto) {
        List<String> sociedadesSirvenProdcuto = socProductoService.findSociedadesSirvenProdcuto(productoId, estados, Boolean.TRUE);
        _sociedadesSirvenProdcuto.add(sociedadesSirvenProdcuto);

        Producto productoPadre = productoService.findProducto(productoService.findProducto(productoId).getIdPadreJerarquia());
        if (productoPadre != null && productoPadre.getNivelInt() > 0) {
            recurrenteSocProducto_Asignado(productoPadre.getIdProducto(), estados, Boolean.TRUE, _sociedadesSirvenProdcuto);
        }

    }

}
