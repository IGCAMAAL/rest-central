package com.centraldecompras.rest.pur;

import com.centraldecompras.modelo.CompraCesta;
import com.centraldecompras.modelo.CompraLinea;
import com.centraldecompras.modelo.CompraPedido;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.pur.service.interfaces.CompraCestaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraLineaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraPedidoService;
import com.centraldecompras.rest.pur.ser.interfraces.CompraRestService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
 
@RestController
@RequestMapping("/pur")
@Api(value = "Recurso privado", description = "Gestion de almacenes")
public class PedidoController {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PedidoController.class.getName());


    @Autowired
    private CompraCestaService compraCestaService;

    @Autowired
    private CompraPedidoService compraPedidoService;

    @Autowired
    private CompraLineaService compraLineaService;

    @Autowired
    private CompraRestService compraRestService;


    @RequestMapping(value = "/pedidocrud", method = RequestMethod.POST)
    @ApiOperation(value = "Registrar Pedido de la compra", notes = "cestaCompraMap, formato de comunicacion con cliente")
    Map<String, Object> postPedido(
            @RequestBody Map<String, Object> compraPedidoMap,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {

        log.info("................................................................................CONTROL");

        Map<String, Object> reply = new HashMap();
        List<Object[]> listaObjetos = new ArrayList();

        String cestaId = null;
        String compraPedidoId = null;
        String pedidoEstado = null;
        String methodReg = null;

        try {
            cestaId = (String) compraPedidoMap.get("cestaId");
            compraPedidoId = (String) compraPedidoMap.get("pedidoId");
            pedidoEstado = (String) compraPedidoMap.get("pedidoEstado");
            methodReg = (String) compraPedidoMap.get("method");

        } catch (Exception e) {
            throw new Exception("CentralCompras. CRUD Pedido." + e.getMessage());
        }

        // para forzar
        CompraCesta compraCesta = null;
        if (!cestaId.equals("0")) {
            compraCesta = compraCestaService.findCompraCesta(cestaId);
            if (compraCesta == null) {
                Object[] valores = {cestaId};
                String mensaje = MessageFormat.format("ERROR: Cesta: {0}, no encontrada. ", valores);
                throw new Exception(MessageFormat.format(mensaje, new Throwable()));
            } else if (!compraCesta.getCestaEstado().equals(KApp.CESTA_ABIERTA.getKApp())) {
                Object[] valores = {compraCesta.getCestaEstado()};
                String mensaje = MessageFormat.format("ERROR: El estado {0} de la Cestano NO permite actualizar pedidos. ", valores);
                throw new Exception(MessageFormat.format(mensaje, new Throwable()));
            }
        }

        CompraPedido compraPedido = null;
        if (!compraPedidoId.equals("0")) {
            compraPedido = compraPedidoService.findCompraPedido(compraPedidoId);
            if (compraPedido != null) {
                if (!compraPedido.getPedidoEstado().equals(KApp.PEDIDO_PTE_ACEP.getKApp())) {
                    Object[] valores = {compraPedido.getPedidoEstado()};
                    String mensaje = MessageFormat.format("ERROR: El estado {0} del Pedido NO permite actualizar pedidos. ", valores);
                    throw new Exception(MessageFormat.format(mensaje, new Throwable()));
                }
            }
        }

        List<Map<String, Object>> lineasCompra = new ArrayList<>();
        try {
            lineasCompra = (List<Map<String, Object>>) compraPedidoMap.get("lineasCompra");
        } catch (Exception e) {
            throw new Exception("CentralCompras. CRUD Pedido." + e.getMessage());
        }

        if (("0").equals(compraPedidoId) && !(KApp.POST.getKApp()).equals(methodReg) || !("0").equals(compraPedidoId) && (KApp.POST.getKApp()).equals(methodReg)) {
            throw new Exception("CentralCompras. CRUD Cesta. Id-cesta  y Metodo no estan sincronizados");
        }

        // Considerar: Modificación Pedido de Cesta. Estado de la cesta y Estado del pedido
        // Considerar: Modificación Pedido SIN cesta. Nuevo pedido, modificación pedido (Estado del pedido)
        Object[] aaa = new Object[4];
        aaa = compraRestService.montaCompraPedido(compraPedidoMap, compraPedido, compraCesta, idUser);
        compraPedido = (CompraPedido) aaa[2];     // Para busqueda al final del método y presentación de la persistencia
        listaObjetos.add(aaa);

        for (Map<String, Object> lineaCompra : lineasCompra) {
            // Si no hay cesta, no considerar el estado de la cesta
            // Considerar el estado del pedido
            // Considerar el estado de la linea
            Object[] bbb = new Object[4];
            bbb = compraRestService.montaCompraLinea(compraCesta, compraPedido, lineaCompra, idUser, null, pedidoEstado);
            listaObjetos.add(bbb);
        }

        try {
            compraRestService.persistenciaCompra(listaObjetos);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("CentralCompras. Persistence Cesta." + ex.getMessage());
        }
        reply = compraPedidoToJson(
                compraPedidoService.findCompraPedido(compraPedido.getCompraPedidoId()),
                compraLineaService.findCompraLineasPedido(compraPedido.getCompraPedidoId())
        );

        return reply;
    }

    @RequestMapping(value = "/pedidouno/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "REcupera los datos de un pedido", notes = " formato de comunicacion con cliete")
    Map<String, Object> getPedido(
            @PathVariable String id,
            @RequestHeader("idUser") String idUser,
            @RequestHeader("idSoc") String idSoc,
            @RequestHeader("idPrf") String idPrf,
            @RequestHeader("idSec") String idSec
    ) throws NonexistentEntityException, Exception {
        log.info("................................................................................CONTROL");

        Map<String, Object> reply = new HashMap();

        reply = (compraPedidoToJson(
                compraPedidoService.findCompraPedido(id),
                compraLineaService.findCompraLineasPedido(id)));

        return reply;
    }

    private Map<String, Object> compraPedidoToJson(CompraPedido compraPedido, List<CompraLinea> compraLineas) {

        Map<String, Object> reply = new HashMap<>();

        List<Map<String, Object>> lineasMap = new ArrayList();
        for (CompraLinea compraLinea : compraLineas) {
            Map<String, Object> lineaMap = new HashMap<>();

            lineaMap.put("id", compraLinea.getCompraLineaId());
            lineaMap.put("articuloId", compraLinea.getArticulo().getIdArticulo());
            lineaMap.put("tipoIvaId", compraLinea.getTipoIva().getClaveIva());
            lineaMap.put("pedidoId", (compraLinea.getPedido() == null) ? null : compraLinea.getPedido().getCompraPedidoId());
            lineaMap.put("cantidadPedida", compraLinea.getCantidadPedida());
            lineaMap.put("precioAplicado", compraLinea.getPrecioAplicado());
            lineaMap.put("descuentoPorcAplicado", compraLinea.getDescuentoPorcAplicado());
            lineaMap.put("descuentoAplicado", compraLinea.getDescuentoAplicado());
            lineaMap.put("importeLineaSinIva", compraLinea.getImporteLineaSinIva());
            lineaMap.put("importeIva", compraLinea.getImporteIva());
            lineaMap.put("importeLineaConIva", compraLinea.getImporteLineaConIva());
            lineaMap.put("lineaEstado", compraLinea.getLineaEstado());
            lineaMap.put("method", compraLinea.getAtributosAuditoria().getUltimaAccion());
            lineaMap.put("estado", compraLinea.getAtributosAuditoria().getEstadoRegistro());
            lineaMap.put("fechaPrevistaActivacion", compraLinea.getAtributosAuditoria().getFechaPrevistaActivacion());
            lineaMap.put("fechaPrevistaDesactivacion", compraLinea.getAtributosAuditoria().getFechaPrevistaDesactivacion());

            lineasMap.add(lineaMap);
        }

        reply.put("pedidoId", compraPedido.getCompraPedidoId());
        reply.put("sociedadCompradorId", compraPedido.getCompradorSociedad().getIdSociedad());
        reply.put("personaCompradorId", compraPedido.getCompradorPersona().getIdPersona());
        reply.put("sociedadProveedorId", compraPedido.getProveedorSociedad().getIdSociedad());
        reply.put("proveedorDireccion", compraPedido.getProveedorDireccion());
        reply.put("proveedorEmail", compraPedido.getProveedorEmail());
        reply.put("proveedorFax", compraPedido.getProveedorFax());
        reply.put("proveedorPersonaContacto", compraPedido.getProveedorPersonaContactoId());
        reply.put("proveedorTelefono", compraPedido.getProveedorTelefono());
        
        reply.put("fechaEntregaCesta", compraPedido.getPedidoFechaEntrega());
        reply.put("pedidoEstado", compraPedido.getPedidoEstado());
        reply.put("pedidoFavorito", compraPedido.isPedidoFavorito());
        reply.put("moneda", compraPedido.getPedidoMoneda());
        reply.put("method", compraPedido.getAtributosAuditoria().getUltimaAccion());
        reply.put("estado", compraPedido.getAtributosAuditoria().getEstadoRegistro());
        reply.put("fechaPrevistaActivacion", compraPedido.getAtributosAuditoria().getFechaPrevistaActivacion());
        reply.put("fechaPrevistaDesactivacion", compraPedido.getAtributosAuditoria().getFechaPrevistaDesactivacion());
        reply.put("lineasCompra", lineasMap);

        return reply;
    }

}
