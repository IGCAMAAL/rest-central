package com.centraldecompras.rest.pur.ser.interfraces;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.modelo.CompraCesta;
import com.centraldecompras.modelo.CompraLinea;
import com.centraldecompras.modelo.CompraPedido;
import java.util.List;
import java.util.Map;

public interface CompraRestService {
    
    void persistenciaCompra(List<Object> createdObjects, List<Object> updatedbjects) throws Exception;
    CompraCesta montaCompraCesta(Map<String, Object> cestaCabMap) throws Exception;    
    CompraLinea montaCompraLinea(Map<String, Object> lineaMap) throws Exception;    

    
    void persistenciaCompra(List<Object[]> listaObjetos) throws Exception;
    Object[] montaCompraCesta(String idCesta, String sociedadId, String personaId, String cestaEstado, String moneda, String methodC, String estadoC, int fechaEntregaCesta, boolean cestaFavorita, String idUser, String cestaDescripcion) throws Exception;
    Object[] montaCompraPedido(Map<String, Object> cestaCompraMap, CompraPedido compraPedido, CompraCesta compraCesta, String idUser) throws Exception;
    Object[] montaCompraLinea(CompraCesta compraCesta, CompraPedido compraPedido, Map<String, Object> lineaCompra, String idUser, String cestaEstado, String pedidoEstado) throws Exception;

    void modificaCompraCestaEstado(CompraCesta compraCesta, String estado, String idUser) throws Exception;
    CompraPedido montaPedidoDistribucion(CompraCesta compraCesta, Sociedad proveedor, String method, String idUser, String estadoRegistro,Integer fechaEntrega,String comentario) throws Exception;
    List<Object[]> montaLineasDistribucion(CompraPedido compraPedido, String method, String idUser, String estadoRegistro) throws Exception;
    void persistenceGrabaPedido(CompraPedido compraPedido) throws Exception ;
}
