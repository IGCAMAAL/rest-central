package com.centraldecompras.rest.pur.ser;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.CompraCesta;
import com.centraldecompras.modelo.CompraLinea;
import com.centraldecompras.modelo.CompraPedido;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.TipoIva;
import com.centraldecompras.modelo.cmo.service.interfaces.ContactoService;
import com.centraldecompras.modelo.cmo.service.interfaces.ContadoresService;
import com.centraldecompras.modelo.cmo.service.interfaces.TipoIvaService;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraCestaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraLineaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraPedidoService;
import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.rest.pur.CestaController;
import com.centraldecompras.rest.pur.ser.interfraces.CompraRestService;
import com.centraldecompras.zglobal.enums.CompraEstadosEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.enums.UtilsMethods;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompraRestServiceImpl implements CompraRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CompraRestServiceImpl.class.getName());

    Map<String, String> transaccionesPermitidas = new HashMap<String, String>();

    @Autowired
    private CompraCestaService compraCestaService;

    @Autowired
    private CompraPedidoService compraPedidoService;

    @Autowired
    private CompraLineaService compraLineaService;

    @Autowired
    private SociedadService sociedadService;

    @Autowired
    private ContactoService contactoService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private ArticuloService articuloService;

    @Autowired
    private TipoIvaService tipoIvaService;

    @Autowired
    private ContadoresService contadoresService;

    private void informaTransaccionesPermitidas() {
        // Cesta abierta puede pasar a 
        transaccionesPermitidas.put("C_OPE_CLO", "Y");   // Cerrada
        transaccionesPermitidas.put("C_OPE_ANU", "Y");   // Anulada
        transaccionesPermitidas.put("C_OPE_BLQ", "Y");   // Bloqueada
        transaccionesPermitidas.put("C_OPE_CRUP", "Y");  // Crud de pedidos
        transaccionesPermitidas.put("C_OPE_CRUL", "Y");  // Crud de lineas

        // Cesta Bloqueada puede pasar a
        transaccionesPermitidas.put("C_BLQ_OPE", "Y");   // Abieta
        transaccionesPermitidas.put("C_BLQ_CLO", "Y");   // Cerrada
        transaccionesPermitidas.put("C_BLQ_ANU", "Y");   // Anulada

        // Pedido abierto puede pasar a
        transaccionesPermitidas.put("P_OPE_ANU", "Y");  // Anulado
        transaccionesPermitidas.put("P_OPE_PTE", "Y");  // Pendiente aprobacion
        transaccionesPermitidas.put("P_OPE_ENV", "Y");  // Enviado
        transaccionesPermitidas.put("P_OPE_CRUP", "Y");  // Crud de pedidos
        transaccionesPermitidas.put("P_OPE_CRUL", "Y");  // Crud de lineas

        // Pedido Pendiente de aprobacion puede pasar a
        transaccionesPermitidas.put("P_PTE_ANU", "Y");  // Anulado
        transaccionesPermitidas.put("P_PTE_APD", "Y");  // Aprobado
        transaccionesPermitidas.put("P_PTE_RCZ", "Y");  // Rechazado

        // Pedido Aprobado puede pasar a
        transaccionesPermitidas.put("P_APD_ANU", "Y");  // Anulado
        transaccionesPermitidas.put("P_APD_ENV", "Y");  // Enviado
        transaccionesPermitidas.put("P_APD_RCZ", "Y");  // Rechazado

        // Pedido Enviado puede pasar a
        transaccionesPermitidas.put("P_ENV_CFP", "Y");  // Confirmacion parcial
        transaccionesPermitidas.put("P_ENV_CFT", "Y");  // Confirmación total

        // Pedido Rechazado puede pasar a
        transaccionesPermitidas.put("P_RCZ_OPE", "Y");  // Abierto
        transaccionesPermitidas.put("P_RCZ_ANU", "Y");  // Anulado

        // Pedido Confirmado parcialmente puede pasar a
        transaccionesPermitidas.put("P_CFP_CFP", "Y");  // Confirmacion parcial
        transaccionesPermitidas.put("P_CFP_CFT", "Y");  // Confirmación total
        transaccionesPermitidas.put("P_CFP_CLO", "Y");  // Cerrado 

        // Pedido Confirmado totalmente puede pasar a
        transaccionesPermitidas.put("P_CFT_CLO", "Y");  // Cerrado 

    }

    @Transactional
    public void persistenciaCompra(List<Object[]> listaObjetos) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Object[] aaa : listaObjetos) {
            if (("POST").equals((String) aaa[0])) {
                if (("compraCesta").equals((String) aaa[1])) {
                    compraCestaService.create((CompraCesta) aaa[2]);
                }
                if (("compraLinea").equals((String) aaa[1])) {
                    compraLineaService.create((CompraLinea) aaa[2]);
                }
                if (("compraPedido").equals((String) aaa[1])) {
                    compraPedidoService.create((CompraPedido) aaa[2]);
                }
            }

            if (("PUT").equals((String) aaa[0])) {
                if (("compraCesta").equals((String) aaa[1])) {
                    compraCestaService.edit((CompraCesta) aaa[2]);
                }
                if (("compraLinea").equals((String) aaa[1])) {
                    compraLineaService.edit((CompraLinea) aaa[2]);
                }
            }

        }

        Collections.reverse(listaObjetos);
        for (Object[] aaa : listaObjetos) {
            if (("DELETE").equals((String) aaa[0])) {
                if (("compraCesta").equals((String) aaa[1])) {
                    compraCestaService.destroy((String) aaa[3]);
                }
                if (("compraLinea").equals((String) aaa[1])) {
                    compraLineaService.destroy((String) aaa[3]);
                }
            }
        }
    }

    @Transactional
    public void persistenciaCompra(List<Object> createdObjects, List<Object> updatedbjects) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for (Object object : createdObjects) {
            if (object instanceof CompraCesta) {
                compraCestaService.create((CompraCesta) object);
            }
            if (object instanceof CompraLinea) {
                compraLineaService.create((CompraLinea) object);
            }
        }

        for (Object object : updatedbjects) {
            if (object instanceof CompraCesta) {
                compraCestaService.edit((CompraCesta) object);
            }
            if (object instanceof CompraLinea) {
                compraLineaService.edit((CompraLinea) object);
            }
        }
    }

    @Transactional
    public void modificaCompraCestaEstado(CompraCesta compraCesta, String estadoCestaFuturo, String idUser) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        String estadoCestaActual = compraCesta.getCestaEstado();
        informaTransaccionesPermitidas();

        // De Abierta a Anulada, Cerrada, Bloqueada
        // De Bloqueada a Abierta, Anulada, Cerrada
        try {
            if (transaccionesPermitidas.get("C_".concat(estadoCestaActual).concat("_").concat(estadoCestaFuturo)).equals("Y")) {
                compraCesta.setCestaEstado(estadoCestaFuturo);
                compraCesta.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
                compraCesta.getAtributosAuditoria().setUpdated(new Date());
                compraCestaService.edit(compraCesta);
            } else {
                Object[] valores = {"EstadoCestaActual: " + estadoCestaActual, "- EstadoCestaFuturo " + estadoCestaFuturo,};
                String mensaje = MessageFormat.format("ERROR: Cesta: Cambio de estado en cesta no permitido: de {0} a {1} ", valores);
                throw new Exception(MessageFormat.format(mensaje, new Throwable()));
            }

        } catch (Exception ex) {
            Logger.getLogger(CompraRestServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Transactional
    public void modificaCompraPedidoEstado(CompraPedido compraPedido, String estadoPedidoFuturo, String idUser) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        String estadoPedidoActual = compraPedido.getPedidoEstado();
        informaTransaccionesPermitidas();
        try {
            if (transaccionesPermitidas.get("C_".concat(estadoPedidoActual).concat("_").concat(estadoPedidoFuturo)).equals("Y")) {
                compraPedido.setPedidoEstado(estadoPedidoFuturo);
                compraPedido.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
                compraPedido.getAtributosAuditoria().setUpdated(new Date());
                compraPedidoService.edit(compraPedido);
            } else {
                Object[] valores = {"EstadoPedidoActual: " + estadoPedidoActual, "- EstadoPedidoFuturo " + estadoPedidoFuturo,};
                String mensaje = MessageFormat.format("ERROR: Cesta: Cambio de estado en pedido no permitido: de {0} a {1} ", valores);
                throw new Exception(MessageFormat.format(mensaje, new Throwable()));
            }

        } catch (Exception ex) {
            Logger.getLogger(CompraRestServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Object[] montaCompraCesta(String idCesta, String sociedadId, String personaId, String cestaEstado, String moneda, String methodResitro, String estadoRegistro, int fechaEntregaCesta, boolean cestaFavorita, String idUser, String cestaDescripcion) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        Object[] reply = new Object[4];
        CompraCesta compraCesta = null;

        Sociedad sociedad = null;
        sociedad = sociedadService.findSociedad(sociedadId);
        if (sociedad == null) {
            Object[] valores = {sociedadId};
            throw new Exception(MessageFormat.format("ERROR: Cesta: Sociedad {0}, no encontrada. ", valores));
        }

        Persona persona = null;
        persona = personaService.findPersona(personaId);
        if (persona == null) {
            Object[] valores = {personaId};
            throw new Exception(MessageFormat.format("ERROR: Cesta: Persona {0}, no encontrada. ", valores));
        }

        compraCesta = compraCestaService.findCompraCesta(idCesta);

        if (compraCesta == null && !KApp.POST.getKApp().equals(methodResitro)) {
            Object[] valores = {idCesta};
            throw new NonexistentEntityException(MessageFormat.format("Cesta Crud: Cesta {0}, no encontrad", valores));
        }

        if (compraCesta == null) {
            compraCesta = new CompraCesta();
            compraCesta.setIdCesta(UUID.randomUUID().toString().replaceAll("-", ""));
            compraCesta.setAtributosAuditoria(new DatosAuditoria());
            compraCesta.getAtributosAuditoria().setCreated(new Date());
        } else {
            compraCesta.getAtributosAuditoria().setUpdated(new Date());
        }

        compraCesta.setCestaEstado(cestaEstado);
        compraCesta.setCestaFavorita(cestaFavorita);
        compraCesta.setFechaEntregaCesta(fechaEntregaCesta);
        compraCesta.setCestaDescripcion(cestaDescripcion);
        compraCesta.setMoneda(moneda);
        compraCesta.setSociedad(sociedad);
        compraCesta.setPersona(persona);

        compraCesta.getAtributosAuditoria().setEstadoRegistro(estadoRegistro);
        compraCesta.getAtributosAuditoria().setFechaPrevistaActivacion(0);
        compraCesta.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
        compraCesta.getAtributosAuditoria().setUltimaAccion(methodResitro);
        compraCesta.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

        reply[0] = methodResitro;
        reply[1] = "compraCesta";
        reply[2] = compraCesta;
        reply[3] = compraCesta.getIdCesta();

        return reply;
    }

    public CompraCesta montaCompraCesta(Map<String, Object> cestaCabMap) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        CompraCesta reply = null;
        String method = null;

        try {

            try {
                reply = compraCestaService.findCompraCesta((String) cestaCabMap.get("cestaId"));
            } catch (Exception ex)  {
                log.info("Cesta no concontrada" + (String) cestaCabMap.get("cestaId")); 
            }
            if (reply == null) {
                reply = new CompraCesta();
                reply.setIdCesta(UUID.randomUUID().toString().replaceAll("-", ""));
                reply.setAtributosAuditoria(new DatosAuditoria());
                reply.setSociedad((Sociedad) cestaCabMap.get("sociedadEntity"));
                reply.setPersona((Persona) cestaCabMap.get("personaEntity"));
                reply.setMoneda((String) cestaCabMap.get("moneda"));
                reply.getAtributosAuditoria().setCreated(new Date());
                method = KApp.POST.getKApp();
            } else {
                reply.getAtributosAuditoria().setUpdated(new Date());
                method = KApp.PUT.getKApp();
            }

            reply.setCestaDescripcion((String) cestaCabMap.get("cestaDescripcion"));
            reply.setCestaEstado((String) cestaCabMap.get("cestaEstado"));
            reply.setCestaFavorita((boolean) cestaCabMap.get("cestaFavorita"));
            reply.setFechaEntregaCesta((int) cestaCabMap.get("fechaEntrega"));

            reply.getAtributosAuditoria().setUltimaAccion(method);
            reply.getAtributosAuditoria().setEstadoRegistro(KApp.ACTIVO.getKApp());
            reply.getAtributosAuditoria().setUsuarioUltimaAccion((String) cestaCabMap.get("idUser"));

            reply.getAtributosAuditoria().setFechaPrevistaActivacion(0);
            reply.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);

        } catch (Exception ex) {
            throw ex;
        }

        return reply;
    }

    public Object[] montaCompraPedido(Map<String, Object> compraPedidoMap, CompraPedido compraPedido, CompraCesta compraCesta, String idUser) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        Object[] reply = new Object[4];

        String compraPedidoId = null;
        String pedidoEstado = null;
        boolean pedidoFavorito = false;

        String sociedadCompradoraId = null;
        String personaCompradoraId = null;
        int fechaEntrega = 0;

        String sociedadProveedoraId = null;

        String moneda = null;
        String methodReg = null;
        String estadoReg = null;

        try {
            compraPedidoId = (String) compraPedidoMap.get("pedidoId");
            pedidoEstado = (String) compraPedidoMap.get("pedidoEstado");
            pedidoFavorito = (boolean) compraPedidoMap.get("pedidoFavorito");

            sociedadCompradoraId = (String) compraPedidoMap.get("sociedadCompradoraId");
            personaCompradoraId = (String) compraPedidoMap.get("personaCompradoraId");
            fechaEntrega = (int) compraPedidoMap.get("fechaEntrega");

            sociedadProveedoraId = (String) compraPedidoMap.get("sociedadProveedoraId");

            moneda = (String) compraPedidoMap.get("moneda");
            methodReg = (String) compraPedidoMap.get("method");
            estadoReg = (String) compraPedidoMap.get("estado");
        } catch (Exception e) {
            throw new Exception("CentralCompras. CRUD Cesta." + e.getMessage());
        }

        Sociedad sociedadCompradora = sociedadService.findSociedad(sociedadCompradoraId);
        if (sociedadCompradora == null) {
            Object[] valores = {sociedadCompradoraId};
            String mensaje = MessageFormat.format("ERROR: Crea Pedido: Sociedad Proveedora {0}, no se ha encontrad0. ", valores);
            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
        }
        Persona personaCompradora = personaService.findPersona(personaCompradoraId);
        if (personaCompradora == null) {
            Object[] valores = {personaCompradoraId};
            String mensaje = MessageFormat.format("ERROR: Crea Pedido: Persona compradora {0}, no se ha encontrad0. ", valores);
            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
        }
        Sociedad sociedadProveedora = sociedadService.findSociedad(sociedadProveedoraId);
        if (sociedadProveedora == null) {
            Object[] valores = {sociedadProveedoraId};
            String mensaje = MessageFormat.format("ERROR: Crea Pedido: Sociedad Proveedora {0}, no se ha encontrad0. ", valores);
            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
        }

        if ((KApp.POST.getKApp().equals(methodReg) && compraPedido != null) || (!KApp.POST.getKApp().equals(methodReg) && compraPedido == null)) {
            Object[] valores = {methodReg, compraPedidoId};
            String mensaje = MessageFormat.format("ERROR: Pedido: Metodo {0} y accion {1}, no estan coordinados. ", valores);
            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
        }

        if (compraPedido == null) {
            compraPedido = new CompraPedido();
            compraPedido.setCompraPedidoId(UUID.randomUUID().toString().replaceAll("-", ""));
            compraPedido.setAtributosAuditoria(new DatosAuditoria());
            compraPedido.getAtributosAuditoria().setCreated(new Date());
        } else {
            compraPedido.getAtributosAuditoria().setUpdated(new Date());
        }

        compraPedido.setCompraCesta(compraCesta);
        compraPedido.setCompradorSociedad(sociedadCompradora);
        compraPedido.setCompradorPersona(personaCompradora);

        compraPedido.setPedidoEstado(pedidoEstado);
        compraPedido.setPedidoFavorito(pedidoFavorito);
        compraPedido.setPedidoFechaEntrega(fechaEntrega);
        compraPedido.setPedidoMoneda(moneda);

        compraPedido.setProveedorSociedad(sociedadProveedora);
        compraPedido.setProveedorPersonaContactoId(null);               // TODO
        compraPedido.setProveedorDireccion(null);                       // TODO
        compraPedido.setProveedorTelefono(null);                        // TODO
        compraPedido.setProveedorFax(null);                             // TODO
        compraPedido.setProveedorEmail(null);                           // TODO

        compraPedido.setCompradorAlmacen(null);                         // TODO
        compraPedido.setPedidoNumero(0);                                // TODO
        compraPedido.setPedidoFechaEnvio(0);                            // TODO

        compraPedido.getAtributosAuditoria().setEstadoRegistro(estadoReg);
        compraPedido.getAtributosAuditoria().setFechaPrevistaActivacion(0);
        compraPedido.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
        compraPedido.getAtributosAuditoria().setUltimaAccion(methodReg);
        compraPedido.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

        reply[0] = methodReg;
        reply[1] = "compraPedido";
        reply[2] = compraPedido;
        reply[3] = compraPedido.getCompraPedidoId();

        return reply;

    }

    public Object[] montaCompraLinea(CompraCesta compraCesta, CompraPedido compraPedido, Map<String, Object> compraLineaMap, String idUser, String cestaEstado, String pedidoEstado) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        Object[] reply = new Object[4];
        CompraLinea compraLinea = null;

        String idLinea = (String) compraLineaMap.get("compraLineaId");
        String articuloId = (String) compraLineaMap.get("articuloId");
        String tipoIvaId = (String) compraLineaMap.get("tipoIvaId");
        String lineaEstado = (String) compraLineaMap.get("lineaEstado");
        String methodRegistro = (String) compraLineaMap.get("method");
        String estadoRegistro = (String) compraLineaMap.get("estado");

        if (("0").equals(idLinea) && !(KApp.POST.getKApp()).equals(methodRegistro) || !("0").equals(idLinea) && (KApp.POST.getKApp()).equals(methodRegistro)) {
            throw new Exception("CentralCompras. CRUD Cesta. Id-Linea cesta  y Metodo no estan sincronizados");
        }

        if (!KApp.POST.getKApp().equals(methodRegistro)) {
            compraLinea = compraLineaService.findCompraLinea(idLinea);
            if (compraLinea != null) {
                compraLinea.getAtributosAuditoria().setUpdated(new Date());
            }
        } else {
            compraLinea = new CompraLinea();
            compraLinea.setCompraLineaId(UUID.randomUUID().toString().replaceAll("-", ""));
            compraLinea.setAtributosAuditoria(new DatosAuditoria());
            compraLinea.getAtributosAuditoria().setCreated(new Date());
        }

        if (compraLinea == null) {
            Object[] valores = {(compraPedido == null) ? null : compraPedido.getCompraPedidoId(), idLinea};
            throw new NonexistentEntityException(MessageFormat.format("Cesta/Pedido/Linea Crud: La linea {1} del pedido {0} no se ha encontrado. Error datos", valores));
        }

        //Double cantidadPedida = lineaCompra.get("");
        Double cantidadPedida = 0.0;
        Object cantidadPedidaObject = compraLineaMap.get("cantidadPedida");
        if (cantidadPedidaObject instanceof Integer) {
            cantidadPedida = Double.parseDouble(String.valueOf(cantidadPedidaObject));
        } else {
            cantidadPedida = (Double) cantidadPedidaObject;
        }

        //Double precioAplicado = lineaCompra.get("");
        Double precioAplicado = 0.0;
        Object precioAplicadoObject = compraLineaMap.get("precioAplicado");
        if (precioAplicadoObject instanceof Integer) {
            precioAplicado = Double.parseDouble(String.valueOf(precioAplicadoObject));
        } else {
            precioAplicado = (Double) precioAplicadoObject;
        }

        //Double descuentoPorcAplicado = lineaCompra.get("");
        Double descuentoPorcAplicado = 0.0;
        Object descuentoPorcAplicadoObject = compraLineaMap.get("descuentoPorcAplicado");
        if (descuentoPorcAplicadoObject instanceof Integer) {
            descuentoPorcAplicado = Double.parseDouble(String.valueOf(descuentoPorcAplicadoObject));
        } else {
            descuentoPorcAplicado = (Double) descuentoPorcAplicadoObject;
        }

        //Double descuentoAplicado = lineaCompra.get("");
        Double descuentoAplicado = 0.0;
        Object descuentoAplicadoObject = compraLineaMap.get("descuentoAplicado");
        if (descuentoAplicadoObject instanceof Integer) {
            descuentoAplicado = Double.parseDouble(String.valueOf(descuentoAplicadoObject));
        } else {
            descuentoAplicado = (Double) descuentoAplicadoObject;
        }

        //Double importeLineaSinIva = lineaCompra.get("");
        Double importeLineaSinIva = 0.0;
        Object importeLineaSinIvaObject = compraLineaMap.get("importeLineaSinIva");
        if (importeLineaSinIvaObject instanceof Integer) {
            importeLineaSinIva = Double.parseDouble(String.valueOf(importeLineaSinIvaObject));
        } else {
            importeLineaSinIva = (Double) importeLineaSinIvaObject;
        }

        //Double importeIva = lineaCompra.get("");
        Double importeIva = 0.0;
        Object importeIvaObject = compraLineaMap.get("importeIva");
        if (importeIvaObject instanceof Integer) {
            importeIva = Double.parseDouble(String.valueOf(importeIvaObject));
        } else {
            importeIva = (Double) importeIvaObject;
        }

        //Double importeLineaConIva = lineaCompra.get("");
        Double importeLineaConIva = 0.0;
        Object importeLineaConIvaObject = compraLineaMap.get("importeLineaConIva");
        if (importeLineaConIvaObject instanceof Integer) {
            importeLineaConIva = Double.parseDouble(String.valueOf(importeLineaConIvaObject));
        } else {
            importeLineaConIva = (Double) importeLineaConIvaObject;
        }

        Articulo articulo = articuloService.findArticulo(articuloId);
        if (articulo == null) {
            Object[] valores = {articuloId};
            throw new NonexistentEntityException(MessageFormat.format("Cesta Crud: Id  de Articulo{0}, no encontrad", valores));
        }

        TipoIva tipoIva = tipoIvaService.findTipoIva(tipoIvaId);
        if (tipoIva == null) {
            Object[] valores = {tipoIva};
            throw new NonexistentEntityException(MessageFormat.format("Cesta Crud: Id  de Iva{0}, no encontrad", valores));
        }

        compraLinea.setCesta(compraCesta);
        compraLinea.setPedido(compraPedido);
        compraLinea.setArticulo(articulo);
        compraLinea.setTipoIva(tipoIva);
        compraLinea.setCestaLin(0);
        compraLinea.setPedidoLin(0);

        compraLinea.setCantidadPedida((new BigDecimal(cantidadPedida)).setScale(4, RoundingMode.HALF_EVEN));
        compraLinea.setPrecioAplicado((new BigDecimal(precioAplicado)).setScale(4, RoundingMode.HALF_EVEN));
        compraLinea.setDescuentoAplicado((new BigDecimal(descuentoAplicado)).setScale(4, RoundingMode.HALF_EVEN));
        compraLinea.setDescuentoPorcAplicado((new BigDecimal(descuentoPorcAplicado)).setScale(2, RoundingMode.HALF_EVEN));
        compraLinea.setImporteLineaSinIva((new BigDecimal(importeLineaSinIva)).setScale(4, RoundingMode.HALF_EVEN));
        compraLinea.setImporteIva((new BigDecimal(importeIva)).setScale(4, RoundingMode.HALF_EVEN));
        compraLinea.setImporteLineaConIva((new BigDecimal(importeLineaConIva)).setScale(4, RoundingMode.HALF_EVEN));
        compraLinea.setLineaEstado(lineaEstado);

        compraLinea.getAtributosAuditoria().setEstadoRegistro(estadoRegistro);
        compraLinea.getAtributosAuditoria().setFechaPrevistaActivacion(0);
        compraLinea.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
        compraLinea.getAtributosAuditoria().setUltimaAccion(methodRegistro);
        compraLinea.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

        compraLinea.setSociedad(compraLinea.getArticulo().getSociedad());

        reply[0] = methodRegistro;
        reply[1] = "compraLinea";
        reply[2] = compraLinea;
        reply[3] = compraLinea.getCompraLineaId();

        return reply;
    }

    public CompraLinea montaCompraLinea(Map<String, Object> lineaMap) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        CompraLinea reply = null;

        reply = compraLineaService.findCompraLinea((String) lineaMap.get("compraLineaId"));
        if (reply == null) {
            reply = new CompraLinea();
            reply.setCompraLineaId(UUID.randomUUID().toString().replaceAll("-", ""));
            reply.setAtributosAuditoria(new DatosAuditoria());
            reply.getAtributosAuditoria().setUltimaAccion(KApp.POST.getKApp());
            reply.getAtributosAuditoria().setCreated(new Date());
            reply.setCestaLin(0);
            reply.setPedidoLin(0);

        } else {
            reply.getAtributosAuditoria().setUpdated(new Date());
            reply.getAtributosAuditoria().setUltimaAccion(KApp.PUT.getKApp());
        }

        reply.setCesta((CompraCesta) lineaMap.get("compraCestaEntity"));
        reply.setPedido((CompraPedido) lineaMap.get("compraPedidoEntity"));
        reply.setArticulo((Articulo) lineaMap.get("articuloEntity"));
        reply.setCantidadPedida(new BigDecimal(lineaMap.get("cantidadPedida").toString()).setScale(4, RoundingMode.HALF_EVEN));
        reply.setLineaEstado((String) lineaMap.get("lineaEstado"));
        reply.getAtributosAuditoria().setUltimaAccion((String) lineaMap.get("method"));

        reply.setTipoIva((TipoIva) lineaMap.get("tipoIvaEntity"));                                                                                  // No lo envía el cliente
        reply.setPrecioAplicado(new BigDecimal(lineaMap.get("precioAplicado").toString()).setScale(4, RoundingMode.HALF_EVEN));                    // No lo envía el cliente
        reply.setDescuentoPorcAplicado(new BigDecimal(lineaMap.get("descuentoPorcAplicado").toString()).setScale(4, RoundingMode.HALF_EVEN));      // No lo envía el cliente
        reply.setDescuentoAplicado(new BigDecimal(lineaMap.get("descuentoAplicado").toString()).setScale(4, RoundingMode.HALF_EVEN));              // No lo envía el cliente
        reply.setImporteLineaSinIva(new BigDecimal(lineaMap.get("importeLineaSinIva").toString()).setScale(4, RoundingMode.HALF_EVEN));            // No lo envía el cliente      
        reply.setImporteIva(new BigDecimal(lineaMap.get("importeIva").toString()).setScale(4, RoundingMode.HALF_EVEN));                            // No lo envía el cliente
        reply.setImporteLineaConIva(new BigDecimal(lineaMap.get("importeLineaConIva").toString()).setScale(4, RoundingMode.HALF_EVEN));            // No lo envía el cliente

        reply.getAtributosAuditoria().setEstadoRegistro(KApp.ACTIVO.getKApp());
        reply.getAtributosAuditoria().setUsuarioUltimaAccion((String) lineaMap.get("idUser"));
        reply.getAtributosAuditoria().setFechaPrevistaActivacion(0);
        reply.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);

        reply.setSociedad(reply.getArticulo().getSociedad());

        return reply;
    }

    public CompraPedido montaPedidoDistribucion(
            CompraCesta compraCesta,
            Sociedad proveedor,
            String method,
            String idUser,
            String estadoRegistro,
            Integer fechaEntrega,
            String comentario
    ) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        CompraPedido reply = null;

        Integer anioDocumento = UtilsMethods.convertDateToInt(new Date(), new SimpleDateFormat("yyyy"));

        reply = new CompraPedido();
        reply.setCompraPedidoId(UUID.randomUUID().toString().replaceAll("-", ""));
        reply.setAtributosAuditoria(new DatosAuditoria());
        reply.getAtributosAuditoria().setCreated(new Date());

        reply.setCompraCesta(compraCesta);
        reply.setCompradorSociedad(compraCesta.getSociedad());
        reply.setCompradorPersona(compraCesta.getPersona());

        reply.setProveedorSociedad(proveedor);
        reply.setProveedorPersonaContactoId("");               // TODO
        reply.setProveedorDireccion("");                       // TODO
        reply.setProveedorTelefono("");                        // TODO
        reply.setProveedorFax("");                             // TODO
        reply.setProveedorEmail("");                           // TODO
        reply.setCompradorAlmacen(null);                       // TODO

        reply.setPedidoNumero(contadoresService.incrementaContador(anioDocumento, KApp.NUM_PDD.getKApp()));
        reply.setPedidoFechaEnvio(fechaEntrega);

        reply.getAtributosAuditoria().setEstadoRegistro(estadoRegistro);
        reply.getAtributosAuditoria().setFechaPrevistaActivacion(0);
        reply.getAtributosAuditoria().setFechaPrevistaDesactivacion(0);
        reply.getAtributosAuditoria().setUltimaAccion(method);
        reply.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);

        reply.setPedidoEstado(KApp.PEDIDO_ACEPTADO.getKApp());
        return reply;

    }

    public List<Object[]> montaLineasDistribucion(CompraPedido compraPedido, String method, String idUser, String estadoRegistro) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        List<Object[]> reply = new ArrayList<>();

        List<String> compraLineaIds = compraLineaService.findCompraLineas_idsDeUnaSociedadList(compraPedido);

        for (String compraLineaId : compraLineaIds) {
            CompraLinea compraLinea = compraLineaService.findCompraLinea(compraLineaId);
            compraLinea.setPedido(compraPedido);
            Object[] aaa = new Object[4];
            aaa[0] = method;
            aaa[1] = "compraLinea";
            aaa[2] = compraLinea;
            aaa[3] = compraLinea.getCompraLineaId();
            reply.add(aaa);
        }

        return reply;

    }

    @Transactional
    @Override
    public void persistenceGrabaPedido(CompraPedido compraPedido) throws Exception {
        try {
            compraPedidoService.create(compraPedido);
            List<String> estados = new ArrayList();
            estados.add(KApp.LINEA_ABIERTA.getKApp());
            List<CompraLinea> comprasLineasPedido = compraLineaService.findCompraLineasPedido(compraPedido, estados);
            for (CompraLinea compraLineasPedido : comprasLineasPedido) {
                compraLineasPedido.setPedido(compraPedido);
                compraLineasPedido.setLineaEstado(KApp.LINEA_ASIGNADA.getKApp());
                compraLineaService.edit(compraLineasPedido);
            }
            compraPedido.setPedidoEstado(KApp.PEDIDO_PTE_ACEP.getKApp());

        } catch (Exception ex) {
            throw ex;
        }
    }

}
