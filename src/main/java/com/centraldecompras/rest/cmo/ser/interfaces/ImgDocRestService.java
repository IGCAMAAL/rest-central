package com.centraldecompras.rest.cmo.ser.interfaces;

import com.centraldecompras.rest.fin.json.auxi.AuditImgJson;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface ImgDocRestService {

    List<Object[]> crudDDBB(String type, AuditImgJson auditImgJson, String idSociedad, String idUser, HashMap userWebId) throws IOException, Exception;
}
