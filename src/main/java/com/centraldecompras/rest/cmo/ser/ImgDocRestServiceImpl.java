package com.centraldecompras.rest.cmo.ser;

import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.ImgDoc;
import com.centraldecompras.modelo.ImgFoto;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgDocService;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgFotoService;
import com.centraldecompras.rest.cmo.ser.interfaces.ImgDocRestService;
import com.centraldecompras.rest.fin.json.auxi.AuditImgJson;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImgDocRestServiceImpl implements ImgDocRestService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImgDocRestServiceImpl.class.getName()); 

    @Autowired
    private ImgDocService imgDocService;

    @Autowired
    private ImgFotoService imgFotoService;

    public List<Object[]> crudDDBB(String type, AuditImgJson auditImgJson, String idReference, String idUser, HashMap userWebId) throws IOException, Exception {
        List<Object[]> reply = new ArrayList();
        String metodo = auditImgJson.getMethod();
        Object[] aaa = new Object[5];

        ImgDoc imgDoc = null;
        ImgFoto imgFoto = null;
        String uuidEntidadRef = null;

        if (type.equals(KApp.FOTO_USUARIO.getKApp())) {
            uuidEntidadRef = (String) userWebId.get(auditImgJson.getUsuarioWeb());
        } else {
            uuidEntidadRef = idReference;
        }

        if (!KApp.POST.getKApp().equals(metodo)) {
            if (type.substring(0, 1).equals("F")) {
                imgFoto = imgFotoService.findImgFoto(auditImgJson.getId());
                if (imgFoto != null) {
                    imgDoc = new ImgDoc(imgFoto);
                }
            } else {
                imgDoc = imgDocService.findImgDoc(auditImgJson.getId());
            }

            if (imgDoc != null) {
                imgDoc.getAtributosAuditoria().setUpdated(new Date());
            }
        } else {
            imgDoc = new ImgDoc();
            imgDoc.setAtributosAuditoria(new DatosAuditoria());
            imgDoc.setIdImgDoc(UUID.randomUUID().toString().replaceAll("-", ""));
            imgDoc.getAtributosAuditoria().setCreated(new Date());
        }

        if (imgDoc != null) {
            imgDoc.getAtributosAuditoria().setEstadoRegistro(auditImgJson.getEstado());
            imgDoc.getAtributosAuditoria().setFechaPrevistaActivacion(auditImgJson.getFechaPrevistaActivacion());
            imgDoc.getAtributosAuditoria().setFechaPrevistaDesactivacion(auditImgJson.getFechaPrevistaDesactivacion());
            imgDoc.getAtributosAuditoria().setUltimaAccion(auditImgJson.getMethod());
            imgDoc.getAtributosAuditoria().setUsuarioUltimaAccion(idUser);
            imgDoc.setDescripcion(auditImgJson.getDescrpcion());
            imgDoc.setTipo(type);
            imgDoc.setUuidReferen(uuidEntidadRef);               // Es la entidad de referencia de referencia: Id de Soc, id de Usu, id de Producto

            if (KApp.POST.getKApp().equals(metodo)) {
                if (auditImgJson.getB64Picture() != null && !("").equals(auditImgJson.getB64Picture())) {
                    imgDoc.setExtension(auditImgJson.getB64NombreOriginal().substring(auditImgJson.getB64NombreOriginal().lastIndexOf(".") + 1));
                    imgDoc.setNomOriginal(auditImgJson.getB64NombreOriginal());
                    imgDoc.setPicture(auditImgJson.getB64Picture().getBytes());
                }
            }

            if (imgDoc.getUuidReferen() != null) {
                aaa[0] = metodo;
                aaa[1] = "imgDoc";
                aaa[2] = imgDoc;
                aaa[3] = imgDoc.getIdImgDoc();
                aaa[4] = imgDoc.getTipo().substring(0, 1);

                reply.add(aaa);
            }
        }

        return reply;
    }

}
