package com.centraldecompras.zapp;

import com.centraldecompras.zglobal.app.AppModelo;
import com.centraldecompras.zglobal.app.AppUUID;
import java.io.File;
import java.util.concurrent.TimeUnit;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.system.ApplicationPidListener;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import com.javiermoreno.springboot.rest.RemoteApplicationProperties;
import com.centraldecompras.util.SimpleThread;
import javax.servlet.MultipartConfigElement;
import org.springframework.boot.context.embedded.MultipartConfigFactory;

import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

//@EnableJpaRepositories(...) (no necesaria por autoconfiguración)
//@EnableTransactionManagement (no necesario por autoconfiguración)
@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties
@EnableWebMvcSecurity
@ComponentScan(value = {
    "com.centraldecompras.acceso",
    "com.centraldecompras.modelo",
    
    "com.centraldecompras.rest.aut",
    "com.centraldecompras.rest.excel",
    "com.centraldecompras.rest.fin",
    "com.centraldecompras.rest.lgt",
    "com.centraldecompras.rest.mat",
    "com.centraldecompras.rest.prm",
    "com.centraldecompras.rest.pur",
    "com.centraldecompras.rest.ven",
    
    "com.centraldecompras.rest.cmo.ser",
    "com.centraldecompras.rest.fin.ser",
    "com.centraldecompras.rest.lgt.ser",
    "com.centraldecompras.rest.mat.ser",
    "com.centraldecompras.rest.prm.ser",
    "com.centraldecompras.rest.pur.ser",
    "com.centraldecompras.rest.ven.ser",
    
    "com.centraldecompras.util",

    "com.javiermoreno.springboot.modelo",
    "com.javiermoreno.springboot.rest"
})
@EntityScan(basePackages = {
    "com.centraldecompras.acceso",
    "com.centraldecompras.modelo"
})

public class App {

    @Autowired
    private RemoteApplicationProperties remoteProps;

    @Value("${server.port}")
    private int serverPort;

    public static void main(String[] args) throws Exception {
                ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .showBanner(true)
                .sources(App.class)
                .run(args);

        context.addApplicationListener(new ApplicationPidListener());
    }
    
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public static ScheduledAnnotationBeanPostProcessor scheduledAnnotationBeanPostProcessor() {
        return new ScheduledAnnotationBeanPostProcessor();
    }

    @Bean
    public static StandardServletMultipartResolver multipartResolver() {
        StandardServletMultipartResolver resolver = new StandardServletMultipartResolver();
        return resolver;
    }
    
    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
        //factory.setPort(7777); (está definido en el application.properties
        factory.setSessionTimeout(10, TimeUnit.MINUTES);
        factory.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/errores/error404.html"),
                new ErrorPage(HttpStatus.UNAUTHORIZED, "/errores/error401.html"),
                new ErrorPage(HttpStatus.FORBIDDEN, "/errores/error403.html"));
        // Activación gzip sobre http (*NO* activar sobre ssl, induce ataques.)
        // http://stackoverflow.com/questions/21410317/using-gzip-compression-with-spring-boot-mvc-javaconfig-with-restful
        factory.addConnectorCustomizers((TomcatConnectorCustomizer) (Connector connector) -> {
            AbstractHttp11Protocol httpProtocol = (AbstractHttp11Protocol) connector.getProtocolHandler();
            httpProtocol.setCompression("on");
            httpProtocol.setCompressionMinSize(256);
            String mimeTypes = httpProtocol.getCompressableMimeTypes();
            String mimeTypesWithJson = mimeTypes + "," + MediaType.APPLICATION_JSON_VALUE;
            httpProtocol.setCompressableMimeTypes(mimeTypesWithJson);

        });

        factory.addAdditionalTomcatConnectors(createSslConnector());
        // En el caso de que se desee sustituír http por https: ************************
        // keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650
        // final String keystoreFilePath = "keystore.p12";
        // final String keystoreType = "PKCS12";
        // final String keystoreProvider = "SunJSSE";
        // final String keystoreAlias = "tomcat"; 
        // factory.addConnectorCustomizers((TomcatConnectorCustomizer) (Connector con) -> {
        // con.setScheme("https");
        // con.setSecure(true);
        // Http11NioProtocol proto = (Http11NioProtocol) con.getProtocolHandler();
        // proto.setSSLEnabled(true);
        // @todo: Descarga el fichero con el certificado actual 
        // File keystoreFile = new File(keystoreFilePath);
        // proto.setKeystoreFile(keystoreFile.getAbsolutePath());
        // proto.setKeystorePass(remoteProps.getKeystorePass());
        // proto.setKeystoreType(keystoreType);
        // proto.setProperty("keystoreProvider", keystoreProvider);
        // proto.setKeyAlias(keystoreAlias);
        //});
        return factory;
    }
/*
    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("128KB");
        factory.setMaxRequestSize("128KB");
        return factory.createMultipartConfig();
    }
    */
    private Connector createSslConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
        final String keystoreFilePath = "keystore.p12";
        final String keystoreType = "PKCS12";
        final String keystoreProvider = "SunJSSE";
        final String keystoreAlias = "tomcat";
        File keystoreFile = new File(keystoreFilePath);
        // File truststore = new ClassPathResource("keystore").getFile();
        connector.setScheme("https");
        connector.setSecure(true);
        if (serverPort != 0) {
            connector.setPort(serverPort + 1);
        }
        protocol.setSSLEnabled(true);
        protocol.setKeystoreFile(keystoreFile.getAbsolutePath());
        protocol.setKeystorePass(remoteProps.getKeystorePass());
        protocol.setKeystoreType(keystoreType);
        protocol.setProperty("keystoreProvider", keystoreProvider);
        protocol.setKeyAlias(keystoreAlias);
        // protocol.setTruststoreFile(truststore.getAbsolutePath());
        // protocol.setTruststorePass("changeit");
        // protocol.setKeyAlias("apitester");
        return connector;
    }

}
