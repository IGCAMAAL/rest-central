package com.centraldecompras.util;

import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
 
@Component
@Service
@Scope("prototype")
public class SendEMailViaGMail extends Thread {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SendEMailViaGMail.class.getName());

    private static final String username = "centralcomprasdemo1";
    private static final String password = "cosmeticos1";

    //private static final String username = "centralcompres@areafdesign.com";
    //private static final String password = "bragasdeesparto";    

    private String nombrePersona;
    private String userWeb;
    private String palabraPaso;
    private String usuarioEmail;
    private String estadoEnvíoCorreo;
    private String descripcionEstadoEnvíoCorreo;
    private String idUser;
    private UserCentralService userCentralService;

    public SendEMailViaGMail() {
    }

    public SendEMailViaGMail(String str, String nombrePersona, String userWeb, String palabraPaso, String usuarioEmail, String idUser, UserCentralService userCentralService) {
        super(str);
        this.nombrePersona = nombrePersona;
        this.userWeb = userWeb;
        this.palabraPaso = palabraPaso;
        this.usuarioEmail = usuarioEmail;
        this.idUser = idUser;
        this.userCentralService = userCentralService;
    }

    @Override
    public void run() {
        log.info("....................................................................mbm............CONTROL");
                
        Boolean verdad = true;
        UserCentral userCentral = null;

        while (verdad) {

            log.info("SendEMailViaGMail..........." + getName() + " - " + new Date() + " - " + "Usuario web" + " - " + userWeb);
            log.info("SendEMailViaGMail..........." + getName() + " - " + new Date() + " - " + "Usuario web" + " - " + userWeb + ".................CONTROL");

            try {
                envioCorreo(nombrePersona, userWeb, palabraPaso, usuarioEmail);
                this.setEstadoEnvíoCorreo(KApp.SUCCESS.getKApp());
                this.setDescripcionEstadoEnvíoCorreo(" ");

            } catch (AddressException ex) {
                Logger.getLogger(SendEMailViaGMail.class.getName()).log(Level.SEVERE, null, ex);
                this.setEstadoEnvíoCorreo(KApp.ERROR.getKApp());
                if (ex.getMessage().length() > 30) {
                    this.setDescripcionEstadoEnvíoCorreo(ex.getMessage().substring(0, 30));
                } else {
                    this.setDescripcionEstadoEnvíoCorreo(ex.getMessage());
                }

            } catch (MessagingException ex) {
                Logger.getLogger(SendEMailViaGMail.class.getName()).log(Level.SEVERE, null, ex);
                this.setEstadoEnvíoCorreo(KApp.ERROR.getKApp());
                if (ex.getMessage().length() > 30) {
                    this.setDescripcionEstadoEnvíoCorreo(ex.getMessage().substring(0, 30));
                } else {
                    this.setDescripcionEstadoEnvíoCorreo(ex.getMessage());
                }

            } finally {
                try {
                    verdad = false;
                    userCentral = userCentralService.findUserCentral(idUser);
                    if (userCentral != null) {
                        if (estadoEnvíoCorreo.length() > 15) {
                            userCentral.setEstadoEnvíoCorreo(estadoEnvíoCorreo.substring(0,15));
                        } else {
                            userCentral.setEstadoEnvíoCorreo(estadoEnvíoCorreo);
                        }

                        if (descripcionEstadoEnvíoCorreo.length() > 128) {
                            userCentral.setDescripcionEstadoEnvíoCorreo(descripcionEstadoEnvíoCorreo.substring(0,128));
                        } else {
                            userCentral.setDescripcionEstadoEnvíoCorreo(descripcionEstadoEnvíoCorreo);
                        }

                        userCentralService.edit(userCentral);
                    }
                } catch (Exception ex) {
                    log.warn("..................................", ex);
                }
            }
        }
    }

    private class GMailAuthenticator extends Authenticator {

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, password);
        }
    }

    public void envioCorreo(String nombrePersona, String userWeb, String palabraPaso, String usuarioEmail) throws AddressException, MessagingException {
        log.info("....................................................................mbm............CONTROL");
        
        //String from = "centralcompres@areafdesign.com";
        String from = "centralcomprasdemo1@gmail.com";
        String to = usuarioEmail;
        String cc = "centralcomprasdemo1@gmail.com";

        // Set properties
        Properties props = new Properties();
        //props.put("mail.smtp.host", "mail.areafdesign.com");
        //props.put("mail.smtp.port", 587);
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", 25);        
        props.put("mail.debug", "true");
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        // Get session with authenticator
        Session session = Session.getInstance(props, new GMailAuthenticator());

        // Instantiate a message
        Message msg = new MimeMessage(session);

        // Set the FROM message
        msg.setFrom(new InternetAddress(from));

        // The recipients can be more than one so we use an array but you can
        // use 'new InternetAddress(to)' for only one address.
        InternetAddress[] addressTo = {new InternetAddress(to)};
        msg.setRecipients(Message.RecipientType.TO, addressTo);
        InternetAddress[] addressCc = {new InternetAddress(cc)};
            //msg.setRecipients(Message.RecipientType.CC, addressCc);

        // Set the message subject and date we sent it.
        msg.setSubject("Alta en Portal Central de Compras");
        msg.setSentDate(new Date());

        // Set message content
        msg.setText(
                "Enhorabuena: " + ((nombrePersona != null) ? nombrePersona + ";" : " ")
                + " \nTu usuario ha sido dado de alta en la Central de Compras con el password que se indica. "
                + " \nUsuario: " + userWeb
                + " \nPassword: " + palabraPaso
                + " \n Entra en la URL http://ec2-54-76-103-159.eu-west-1.compute.amazonaws.com/login.html y cambia la password");

        // Send the message
        Transport.send(msg);

    }

    public String getEstadoEnvíoCorreo() {
        return estadoEnvíoCorreo;
    }

    public void setEstadoEnvíoCorreo(String estadoEnvíoCorreo) {
        this.estadoEnvíoCorreo = estadoEnvíoCorreo;
    }

    public String getDescripcionEstadoEnvíoCorreo() {
        return descripcionEstadoEnvíoCorreo;
    }

    public void setDescripcionEstadoEnvíoCorreo(String descripcionEstadoEnvíoCorreo) {
        this.descripcionEstadoEnvíoCorreo = descripcionEstadoEnvíoCorreo;
    }

}
