package com.centraldecompras.util;


import java.util.Date;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
 
@Controller
public class ExceptionHandlerController {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ExceptionHandlerController.class.getName());
    public static final String DEFAULT_ERROR_VIEW = "error";

    @ExceptionHandler(value = {Exception.class})
    public String defaultErrorHandler(Exception e) {
        log.info("................................................................................CONTROL");        log.info("................................................................................CONTROL");
        
        String mav = "";

        mav = mav.concat("datetime").concat(new Date().toString());
        mav = mav.concat("exception").concat(e.getMessage());
        return mav;
    }

    @ExceptionHandler(value = {RuntimeException.class})
    public String defaultErrorHandler(RuntimeException e) {
        log.info("................................................................................CONTROL");
        String mav = "";

        mav = mav.concat("datetime").concat(new Date().toString());
        mav = mav.concat("exception").concat(e.getMessage());
        return mav;
    }
}
