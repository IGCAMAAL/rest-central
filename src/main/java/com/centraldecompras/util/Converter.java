package com.centraldecompras.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Converter {
    
    public static List<Object> listArrayStringToHashAndArrayString(List<String[]> inputs) {
        List<Object> reply = new ArrayList<>();
        
        String[] descList = new String[inputs.size()];
        HashMap<String, String> descKeyAndId = new HashMap();
        HashMap<String, String> idKeyAndDesc = new HashMap();
        
        for(Object[] input : inputs){
            descList[inputs.indexOf(input)] = (String)input[0];     // descripcion
            descKeyAndId.put((String)input[0], (String)input[1]);   // descripcion , id
            idKeyAndDesc.put((String)input[1], (String)input[0]);   // descripcion , id
        }
        
        reply.add(descList);
        reply.add(descKeyAndId);
        reply.add(idKeyAndDesc);
         
        return reply;

    }
}
