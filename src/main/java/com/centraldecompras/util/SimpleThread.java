package com.centraldecompras.util;

import com.centraldecompras.acceso.service.interfaces.PerfilService;
import com.centraldecompras.modelo.rpl.service.interfaces.PerfilRplService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;

@Component
@Scope("prototype")
public class SimpleThread extends Thread {
    
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SimpleThread.class.getName());
    
    @Autowired
    private PerfilService perfilService;
    @Autowired
    private PerfilRplService perfilRplService;
    

    public SimpleThread() {
    }
    public SimpleThread(String str) {
        super(str);
    }

    @Override
    public void run() {
        log.info("....................................................................mbm............CONTROL");
        
        while (true) {
            
            int cuenta1 = perfilService.getPerfilCount();
            int cuenta2 = perfilRplService.getPerfilRplCount();
            
            log.info("SimpleThread..........." + getName() + " - " + new Date() + " - " + cuenta1 + " - " + cuenta2);
            log.info("SimpleThread..........." + getName() + " - " + new Date() + " - " + cuenta1 + " - " + cuenta2 + ".................CONTROL");

            try {
                //sleep(14400000);      // Descansa durante 4 horas.  Produccion
                   sleep(3600000);      // Descansa durante 1 hora.
                //   sleep(60000);      // Descansa durante 1 minuto. Pruebas y desarrollo
            } catch (InterruptedException e) {
            }
        }
    }

}
